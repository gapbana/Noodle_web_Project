<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
	$('#result').DataTable();
});
</script>

<script language="javascript" type="text/javascript">
  function doback() {
	  location.href='price.htm?mode=priceSearch'; 
  }
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
     		<div class="col-lg-12">
     		 	<h3 class="page-header">����ѵ��Ҥ��Թ��� �ѹ��� ${date} <span class="navigator">[PR03]</span></h3>
          	</div>   
		</div>
		<div class="row">
			<div class="col-lg-6">
				<span class="navigator" >�Ҥ��Թ��� > ����ѵ��Ҥ��Թ���</span>				
			</div>
	     	<div class="col-lg-6 text-right">     	
	     		<button type="button" class="btn btn-primary" onclick="doback();">��͹��Ѻ</button>    	
	        </div>   
		</div>
		<br>
		<div class="row">
     		<div class="col-lg-12">
     		<c:if test="${fn:length(productList)> 0 }">
		 	<div class="panel panel-info">
	      		<div class="panel-heading black-text" style="font-size:18px;"> ������鹡��</div>
	      	<div class="panel-body">
	      		<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="result" >
					<thead>						
						<tr class="grey lighten-4">
							<th width="10%" rowspan="2">����</th>
							<th colspan="2"><div align="center">��./���</div></th>
							<th width="16%" rowspan="2"><div align="right">�Ҥ���ǹ��ҧ (�ҷ/��.)</div></th>
							<th width="16%" colspan="2"><div align="center">>15 �ѹ</div></th>
							<th width="16%" colspan="2"><div align="center"><15 �ѹ</div></th>
						</tr>
						<tr class="grey lighten-4">							
							<th width="10%"><div align="center">10 ����</div></th>
							<th width="10%"><div align="center">12 ����</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productList}" var="item" varStatus="row">
						<c:if test="${item.productType.id eq '1' }">   
						<tr>
							<td>${item.code }</td>
					   		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m10}" /></td>
					 		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m12}" /></td>
					 		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.diff}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceOver}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceLess}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>				 						 		
						</tr>
						</c:if>
						</c:forEach>
					</tbody>
				</table>
	      	</div>
	    </div>
	    <div class="panel panel-info">
     		<div class="panel-heading black-text" style="font-size:18px;">���硢������</div>
     		<div class="panel-body">
				<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" id="result" >
					<thead>
						<tr class="grey lighten-4">
							<th width="10%" rowspan="2">����</th>
							<th colspan="2"><div align="center">��./���</div></th>
							<th width="16%" rowspan="2"><div align="right">�Ҥ���ǹ��ҧ (�ҷ/��.)</div></th>
							<th width="16%" colspan="2"><div align="center">>15 �ѹ</div></th>
							<th width="16%" colspan="2"><div align="center"><15 �ѹ</div></th>
						</tr>
						<tr class="grey lighten-4">
							
							<th width="10%"><div align="center">10 ����</div></th>
							<th width="10%"><div align="center">12 ����</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productList}" var="item" varStatus="row">
						<c:if test="${item.productType.id eq '2' }">   
						<tr>
							<td>${item.code }</td>
					   		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m10}" /></td>
					 		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m12}" /></td>
					 		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.diff}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceOver}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceLess}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>					 						 		
						</tr>
						</c:if>
						</c:forEach>				
					</tbody>
				</table>
			</div>
			</div>  
			</c:if> 
		</div>
    </div>
    <div class="row">
     	<div class="col-lg-12 text-right">
     		<button type="button" class="btn btn-primary" onclick="doback();">��͹��Ѻ</button><br>
        </div>   
	</div>
		<br>
			
	</div>    
</body>
</html>