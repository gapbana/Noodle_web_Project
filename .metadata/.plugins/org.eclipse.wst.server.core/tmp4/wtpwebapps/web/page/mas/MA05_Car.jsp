<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#masterForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},

			fields : {
				licensePlate: {
					validators : {
						notEmpty : {
							message : '��سҡ�͡ : ����¹ö'
						}
					}
				},					
				
			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			$("button[name='btnSave']").attr('disabled', true);

			document.masterForm.mode.value = 'carSave';
			document.masterForm.submit();
		});
	});
	
	
</script>

<script language="javascript" type="text/javascript">
  function submitFormInit(mode) {
  	document.nationForm.mode.value = mode;
  	document.nationForm.edit.value = '';
    document.nationForm.submit();    
  }
  
  function submitFormAdd(mode) {
	  if ($("#accountForm").valid()) {
	  	document.nationForm.mode.value = mode;
	    document.nationForm.submit();
	  }
  }
  
  function doEdit(id) {
    	document.masterForm.mode.value = 'carEdit';
  		document.masterForm.id.value = id;
     	document.masterForm.submit();    
  }
  
  function doDelete(id) {
  		document.masterForm.mode.value = 'carDel';
		document.masterForm.id.value = id;
   		document.masterForm.submit();    
}
  
  
  
  
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�ѹ�֡/���ö��÷ء <span class="navigator">[MA05]</span></h3>
				<span class="navigator" >�����ž�鹰ҹ > �������١��� > �ѹ�֡/��䢢�����ö��÷ء</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      		<html:form action="/master" styleId="masterForm" styleClass="form-horizontal" enctype="multipart/form-data">
				<html:hidden property="mode" />
				<html:hidden property="id" />
				<html:hidden property="custId" />
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="red-text">* </span>����¹ö:</label>
					<div class="col-sm-2">
					<html:text property="licensePlate" styleClass="form-control"></html:text>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">������:</label>
					<div class="col-sm-2">
					<html:text property="brand" styleClass="form-control"></html:text>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">���:</label>
					<div class="col-sm-2">
					<html:text property="models" styleClass="form-control"></html:text>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">����-���ʡ�� ���Ѻ:</label>
					<div class="col-sm-4">
					<html:text property="driverName" styleClass="form-control"></html:text>
						
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">�ٻ����:</label>
					<div class="col-sm-4"><html:file property="uploadFile" styleClass="form-control" accept="image/*"></html:file></div>
				</div>		
				<div class="form-group">
					<div class="col-sm-4 col-sm-offset-2">	
						<button type="submit" class="btn btn-success"  >�ѹ�֡</button>           
	            		<button type="button" class="btn btn-danger" onclick="location.href='master.htm?mode=customer'">¡��ԡ</button> 
	       			</div>
	       		</div>
	       </html:form>
		</div>
	</div>
	<c:if test="${fn:length(carList)> 0 }">
	       	<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="10%"><div align="center">�ӴѺ</div></th>
						<th width="15%">����¹ö</th>
						<th width="15%">������</th>
						<th width="15%">���</th>
						<th width="35%">����-���ʡ�Ť��Ѻ</th>
						<th width="10%"></th>
					</tr>
				</thead>
				
				<tbody>
				 <c:forEach items="${carList}" var="item" varStatus="row">
					<tr >
					<td align="center">${row.index + 1 }</td>  
					    <td>${item.licensePlate}</td>  
					    <td>${item.brand}</td>  
					    <td>${item.model}</td>
					   	<td>${item.driverName}</td>  
					  	<td align="center"> 				  		
							<a class="btn btn-info btn-xs" href="#" onclick="doEdit('${item.id}')"><i class="fa fa-pencil btn-info"></i></a>
							<a class="btn btn-danger btn-xs" href="#" onclick="doDelete('${item.id}')"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>					
				</c:forEach>
				</tbody>
			</table>
	       		</c:if>
</body>
</html>