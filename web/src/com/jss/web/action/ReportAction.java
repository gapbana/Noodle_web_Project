package com.jss.web.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.jss.entity.Customer;
import com.jss.entity.GapOrderItem;
import com.jss.entity.Orders;
import com.jss.entity.Transport;
import com.jss.service.GapCustomerService;
import com.jss.service.GapOrderItemService;
import com.jss.service.OrdersService;
import com.jss.service.UserService;
import com.jss.web.common.OptionDTO;
import com.jss.web.xls.ExportReport1;
import com.jss.web.xls.ExportReport2;


public class ReportAction extends CoreAction{	
	private OrdersService ordersService;
	
	public ActionForward reportMonthlySales(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Date date = new Date();
		SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
		int years = Integer.parseInt(sdfYear.format(date)) + 543;
		
		List<OptionDTO> yearList = new ArrayList<OptionDTO>();
		for (int i = 2560; i < (years+3); i++) {
			OptionDTO year = new OptionDTO();
			year.setValue(String.valueOf(i));
			year.setLabel(String.valueOf(i));
			yearList.add(year);
		}
		
		String monthName = "มกราคม,กุมพาพันธ์,มีนาคม,เมษายน,พฤษภาคม,มิถุนายน,กรกฎาคม,สิงหาคม,กันยายน,ตุลาคม,พฤศจิกายน,ธันวาคม";
		String[] arr1 = monthName.split(",");
		
		List<OptionDTO> monthList = new ArrayList<OptionDTO>();
		int num = 00;
		for (String row : arr1) {
			OptionDTO month = new OptionDTO();
			num += 1;
			month.setValue(String.format("%02d", num));
			month.setLabel(row);		
			monthList.add(month);
			
		}
		List<Orders> ordersList = new ArrayList<>();
		dynaForm.set("year", String.valueOf(years));
		dynaForm.set("month", sdfMonth.format(date));
		dynaForm.set("comboYear", yearList);
		dynaForm.set("comboMonth", monthList);
		dynaForm.set("resultList", ordersList);
		
		return mapping.findForward("RP01");
	}
	
	
public ActionForward reportWeekSales(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Date date = new Date();
		SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
		int years = Integer.parseInt(sdfYear.format(date)) + 543;
		
		List<OptionDTO> yearList = new ArrayList<OptionDTO>();
		for (int i = 2560; i < (years+3); i++) {
			OptionDTO year = new OptionDTO();
			year.setValue(String.valueOf(i));
			year.setLabel(String.valueOf(i));
			yearList.add(year);
		}
		
		String monthName = "มกราคม,กุมพาพันธ์,มีนาคม,เมษายน,พฤษภาคม,มิถุนายน,กรกฎาคม,สิงหาคม,กันยายน,ตุลาคม,พฤศจิกายน,ธันวาคม";
		String[] arr1 = monthName.split(",");
		
		List<OptionDTO> monthList = new ArrayList<OptionDTO>();
		int num = 00;
		for (String row : arr1) {
			OptionDTO month = new OptionDTO();
			num += 1;
			month.setValue(String.format("%02d", num));
			month.setLabel(row);		
			monthList.add(month);
			
		}
		List<Orders> ordersList = new ArrayList<>();
		dynaForm.set("year", String.valueOf(years));
		dynaForm.set("month", sdfMonth.format(date));
		dynaForm.set("comboYear", yearList);
		dynaForm.set("comboMonth", monthList);
		dynaForm.set("resultList", ordersList);
		
		return mapping.findForward("RP04");
	}

/*public ActionForward reportDaySales(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
	
	DynaActionForm dynaForm = (DynaActionForm) form;
	Date date = new Date();
	SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
	int years = Integer.parseInt(sdfYear.format(date)) + 543;
	
	List<OptionDTO> yearList = new ArrayList<OptionDTO>();
	for (int i = 2560; i < (years+3); i++) {
		OptionDTO year = new OptionDTO();
		year.setValue(String.valueOf(i));
		year.setLabel(String.valueOf(i));
		yearList.add(year);
	}
	
	String monthName = "มกราคม,กุมพาพันธ์,มีนาคม,เมษายน,พฤษภาคม,มิถุนายน,กรกฎาคม,สิงหาคม,กันยายน,ตุลาคม,พฤศจิกายน,ธันวาคม";
	String[] arr1 = monthName.split(",");
	
	List<OptionDTO> monthList = new ArrayList<OptionDTO>();
	int num = 00;
	for (String row : arr1) {
		OptionDTO month = new OptionDTO();
		num += 1;
		month.setValue(String.format("%02d", num));
		month.setLabel(row);		
		monthList.add(month);
		
	}
	List<Orders> ordersList = new ArrayList<>();
	dynaForm.set("year", String.valueOf(years));
	dynaForm.set("month", sdfMonth.format(date));
	dynaForm.set("comboYear", yearList);
	dynaForm.set("comboMonth", monthList);
	dynaForm.set("resultList", ordersList);
	
	return mapping.findForward("RP03");
}*/

public ActionForward reportDaySales(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
	
	DynaActionForm dynaForm = (DynaActionForm) form;
	Date date = new Date();
	SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
	int years = Integer.parseInt(sdfYear.format(date)) + 543;
	
	List<OptionDTO> yearList = new ArrayList<OptionDTO>();
	for (int i = 2560; i < (years+3); i++) {
		OptionDTO year = new OptionDTO();
		year.setValue(String.valueOf(i));
		year.setLabel(String.valueOf(i));
		yearList.add(year);
	}
	
	String monthName = "มกราคม,กุมพาพันธ์,มีนาคม,เมษายน,พฤษภาคม,มิถุนายน,กรกฎาคม,สิงหาคม,กันยายน,ตุลาคม,พฤศจิกายน,ธันวาคม";
	String[] arr1 = monthName.split(",");
	
	List<OptionDTO> monthList = new ArrayList<OptionDTO>();
	int num = 00;
	for (String row : arr1) {
		OptionDTO month = new OptionDTO();
		num += 1;
		month.setValue(String.format("%02d", num));
		month.setLabel(row);		
		monthList.add(month);
		
	}
	List<Orders> ordersList = new ArrayList<>();
	dynaForm.set("year", String.valueOf(years));
	dynaForm.set("month", sdfMonth.format(date));
	dynaForm.set("comboYear", yearList);
	dynaForm.set("comboMonth", monthList);
	dynaForm.set("resultList", ordersList);
	
	/*List<GapOrderItem> orderList = GapOrderItemService.getOrderItem();*/
	
		return mapping.findForward("RP03");
}
	
	
	public ActionForward reportMonthlySalesSearch(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Date date = new Date();
		SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
		int years = Integer.parseInt(sdfYear.format(date)) + 543;
		
		List<OptionDTO> yearList = new ArrayList<OptionDTO>();
		for (int i = 2560; i < (years+3); i++) {
			OptionDTO year = new OptionDTO();
			year.setValue(String.valueOf(i));
			year.setLabel(String.valueOf(i));
			yearList.add(year);
		}
		
		String monthName = "มกราคม,กุมพาพันธ์,มีนาคม,เมษายน,พฤษภาคม,มิถุนายน,กรกฎาคม,สิงหาคม,กันยายน,ตุลาคม,พฤศจิกายน,ธันวาคม";
		String[] arr1 = monthName.split(",");
		
		List<OptionDTO> monthList = new ArrayList<OptionDTO>();
		int num = 00;
		for (String row : arr1) {
			OptionDTO month = new OptionDTO();
			num += 1;
			month.setValue(String.format("%02d", num));
			month.setLabel(row);		
			monthList.add(month);
			
		}
		
		List<Orders> ordersList = ordersService.getOrdersReport1List(dynaForm.getString("year"), dynaForm.getString("month"));
		Double totalUnit = 0.0;
		Double totalPrice = 0.0;
		for (Orders row : ordersList) {
			totalUnit += row.getTotalUnit();
			totalPrice += row.getTotalPrice();
		}
		dynaForm.set("resultList", ordersList);
		request.setAttribute("resultSize", ordersList.size());
		request.setAttribute("resultList", ordersList);
		request.setAttribute("totalUnit", totalUnit);
		request.setAttribute("totalPrice", totalPrice);
		dynaForm.set("comboYear", yearList);
		dynaForm.set("comboMonth", monthList);
		
		return mapping.findForward("RP01");
	}

	public ActionForward exportExcelReport1(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
	
		DynaActionForm dynaForm = (DynaActionForm) form;		
		List<Orders> resultList = (List<Orders>) dynaForm.get("resultList");
        CoreExcelAction.executeExcel(response, ExportReport1.exportExcel(request, resultList));        
        return null;
	}
	

	
	
	public ActionForward reportMonthlySalesHome(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Date date = new Date();
		SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
		int years = Integer.parseInt(sdfYear.format(date)) + 543;
		
		dynaForm.set("year", String.valueOf(years));
		dynaForm.set("month", sdfMonth.format(date));
	
		
		return reportMonthlySalesSearch(mapping, dynaForm, request, response);
	}
	
	public ActionForward reportMonthlyTransport (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		List<Orders> ordersList = ordersService.getMonthlyTransport();
		Double totalUnit = 0.0;
		Double totalPrice = 0.0;
		for (Orders row : ordersList) {
			Double unit = row.getTotalUnit()-row.gettUnit();
			Double price = row.getTotalPrice()-row.gettPrice();
			
			totalUnit += unit;
			totalPrice += price;
		}
		
		dynaForm.set("resultList", ordersList);
		request.setAttribute("totalUnit", totalUnit);
		request.setAttribute("totalPrice", totalPrice);
		request.setAttribute("resultList", ordersList);
		
		return mapping.findForward("RP02");
	}
	
	public ActionForward exportExcelReport2(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		DynaActionForm dynaForm = (DynaActionForm) form;		
		List<Orders> resultList = (List<Orders>) dynaForm.get("resultList");
        CoreExcelAction.executeExcel(response, ExportReport2.exportExcel(request, resultList));        
        return null;
	}
	
	
	public OrdersService getOrdersService() {
		return ordersService;
	}
	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}
	
	

}
