package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.UserGroupDao;
import com.jss.entity.UserGroup;

public class UserGroupDaoImpl extends CoreDaoImpl<UserGroup, Serializable> implements UserGroupDao {

	public UserGroupDaoImpl(Class<UserGroup> entityClass) {
		super(entityClass);
	}

	@Override
	public List<UserGroup> findGroupList(String id) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(UserGroup.class);
		criteria.addOrder(Order.asc("name"));	
		
		if(id != null && !id.equals("")){
			criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}


}