package com.jss.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;
import com.edu.util.DateUtil;

@Entity
@Table(name = "stock")
public class Stock extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "stock_id") 
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "stock_date")
	private Date date;
	
	@Column(name = "stock_time")
	private String time;
	
	@Column(name = "stock_line")
	private Integer line;
	
	@Column(name = "stock_unit")
	private Double unit;
	
	@Transient 
	private String productName;
	@Transient 
	private Double weight;
	@Transient
	private String stockDateShow;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public Double getUnit() {
		return unit;
	}

	public void setUnit(Double unit) {
		this.unit = unit;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getStockDateShow() {
		return DateUtil.date2StringTh(getDate(), "dd/MM/yyyy");
	}

	public void setStockDateShow(String stockDateShow) {
		this.stockDateShow = stockDateShow;
	}
	
}
