package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrdersProduct;
import com.jss.entity.ProductType;


public interface OrderIncomeService extends CoreService<OrderIncome, Serializable>{
	
	public List<OrderIncome> getOrder(String ordersId) throws DataAccessException;
	public List<OrderIncome> getAllOrder() throws DataAccessException;
}
