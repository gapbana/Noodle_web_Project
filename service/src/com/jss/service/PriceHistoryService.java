package com.jss.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.PriceHistory;
import com.jss.entity.Product;

public interface PriceHistoryService extends CoreService<PriceHistory, Serializable> {
	
	public List<PriceHistory> getPriceHistoryList(String priceId) throws DataAccessException;
	public boolean savePriceHistory(List<PriceHistory> phList) throws DataAccessException;
	public List<PriceHistory> getPriceHistoryByProduct(String productId, String date, String time) throws DataAccessException;
	public List<PriceHistory> getPriceHistoryByPriceID(String productId,String priceId) throws DataAccessException;
}
