<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
	$("#deposit").hide();
	$("#delivery").hide();
	$("#discountCash").hide();
	$("#discount300Ton").hide();
	$("#discount500Ton").hide();
	$("#discountS").hide();
	
	var totalAmount =  '${totalAmount}';
	var totalUnit = '${totalUnit}';
	
	if(totalUnit >= 300 && totalUnit < 500){
		$("#discount300Ton").show();
		var Ton300Total = (totalUnit*1000)*0.10;
		$("#Ton300Total").html("- "+numeral(Ton300Total).format('0,0.000'));
		totalAmount = totalAmount - Ton300Total;
		
	}else if (totalUnit >= 500) {
		$("#discount500Ton").show();
		
		var Ton500Total = (totalUnit*1000)*0.20;
		$("#Ton500Total").html("- "+numeral(Ton500Total).format('0,0.000'));
		
		totalAmount = totalAmount - Ton500Total;

	}
	

	var transport =  $('input[name="transport"]:checked').val();
	if(transport =='N'){		
		$("#delivery").show();
		var delivery = $("#transportPrice").val();
		$("#deliveryM").html("+ "+numeral(delivery).format('0,0.000'));
	
		totalAmount =parseFloat(totalAmount) + parseFloat(delivery);
		
	}
	
	var pay3 =  $('input[name="pay"]:checked').val();
	if(pay3 =='D'){		
		var payPercent = $("#payPercent").val();
		var money = (totalAmount*payPercent)/100;
		
		$("#deposit").show();
		
		$("#depositM").html(payPercent+"%");
		$("#depositT").html("- "+numeral(money).format('-0,0.000'));
		
		totalAmount = totalAmount - money;
	}else if(pay3 =='C'){

		$("#discountCash").show();		
		var cashTotal = (totalUnit*1000)*0.20;
		$("#cashTotal").html("- "+numeral(cashTotal).format('0,0.000'));
		
		totalAmount = totalAmount - cashTotal;
		
	}
	
	var discount = '${discount}';
	if(discount > 0 && discount <= 100){
		$("#discountS").show();
		$("#discountP").html(discount+"%");
		var totalDiscount = (totalAmount * discount)/100;
		$("#discountM").html("- "+numeral(totalDiscount).format('0,0.000'));
		
		totalAmount = totalAmount - totalDiscount;
		
	}
	
	$("#totalAmount").html(numeral(totalAmount).format('#,##0.000'));
});
</script>

<script language="javascript" type="text/javascript">
  function submitFormInit(mode) {
  	document.nationForm.mode.value = mode;
  	document.nationForm.edit.value = '';
    document.nationForm.submit();    
  }
  
  function submitFormAdd(mode) {
	  if ($("#accountForm").valid()) {
	  	document.nationForm.mode.value = mode;
	    document.nationForm.submit();
	  }
  }
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">��������´����Թ��� <span class="navigator">[TR03]</span></h3>			
			</div>        
		</div>
		<div class="row">
			<div class="col-lg-6">
				<span class="navigator" >�Ѵ���Թ��� > ��������´</span>	
			</div>
	     	<div class="col-lg-6 text-right">     	
	     		<button type="button" class="btn btn-primary btn-sm" onclick="javascript:history.back();">��͹��Ѻ</button>
	        </div>   
		</div>
		<br>
		<div class="row">
	      	<div class="col-lg-12">
				<html:form action="/order" styleId="orderForm" styleClass="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">�Ţ�������Թ���:</label>
					<div class="col-sm-3 control-text">	${transportCode }</div>					
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">�Ţ������觫���:</label>
					<div class="col-sm-3 control-text">	${orderCode }</div>
					<label class="col-sm-2 control-label"> �ѹ���:</label>
					<div class="col-sm-3 control-text" >${date }</div>
				</div>				
    			<div class="form-group">
					<label class="col-sm-2 control-label"> �١���:</label>
					<div class="col-sm-3 control-text">${custName }</div>
					<label class="col-sm-2 control-label"> ��ѡ�ҹ���:</label>
					<div class="col-sm-3 control-text">${saleName }</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-2 control-label">�ôԵ</label>
					<div class="col-sm-2 control-text">${credit } �ѹ</div>
				    <div class="col-sm-1">
				    </div>
				   	<label class="col-sm-2 control-label">�ͧ:</label>
					<div class="col-sm-3">
						<label class="radio-inline">
							<html:radio property="book" value="S" styleId="book1" disabled="true"></html:radio>�ͧ���� &nbsp;							     	
						</label>
						<label class="radio-inline">
							<html:radio property="book" value="P" styleId="book2" disabled="true"></html:radio>�ͧ�Ҥ�		     
						</label>
						<c:if test="${book eq 'P' }">
							<label class="radio-inline">
								<html:text property="bookPrice" styleId="bookPrice" styleClass="form-control" size="5" maxlength="6" value="" disabled="true"></html:text> 		     
							</label>
						</c:if>
				    </div>	
				</div>
				<div class="form-group">	    
				    <label class="col-sm-2 control-label">�͡���:</label>
					<div class="col-sm-3">
						<label class="radio-inline">
							<html:radio property="bill" value="JSW" disabled="true" ></html:radio>JSW &nbsp;
						</label>
						<label class="radio-inline">
							<html:radio property="bill" value="JSB" disabled="true" ></html:radio>JSB		     
						</label>
				    </div>	
					<label class="col-sm-2 control-label">��ê���:</label>
					<div class="col-sm-3">
						<label class="radio-inline">
							<html:radio property="pay" value="C" styleId="pay1" disabled="true"></html:radio>�Թʴ &nbsp;
						</label>
						<label class="radio-inline">
							<html:radio property="pay" value="B" styleId="pay2" disabled="true"></html:radio>����١���&nbsp;    
						</label>
						<label class="radio-inline">
							<html:radio property="pay" value="D" styleId="pay3" disabled="true"></html:radio>�Ѵ��	     
						</label>
						<c:if test="${pay eq 'D' }">
							<label class="radio-inline">
							    <html:text property="payPercent" styleId="payPercent" styleClass="form-control" size="5" disabled="true" maxlength="3"></html:text>
							</label>
						</c:if>
				    </div>				  
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">��ǹŴ:</label>
				    <div class="col-sm-3 control-text">	${discount}</div>
					<label class="col-sm-2 control-label">��â���</label>
					<div class="col-sm-5">
						<label class="radio-inline">
							<html:radio property="transport" value="Y" styleId="transport1" disabled="true"></html:radio>�Ѻ�ͧ &nbsp;
						</label>
						<label class="radio-inline">
							<html:radio property="transport" value="N" styleId="transport2" disabled="true"></html:radio>�����&nbsp;	     
						</label>
						<c:if test="${transport eq 'N' }">
							<label class="radio-inline">
								<html:text property="transportPrice" styleId="transportPrice" disabled="true" styleClass="form-control" size="5" maxlength="7"></html:text>	     
							</label>
						</c:if>				   	 	
				    </div>
	    
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">�������:</label>
				    <div class="col-sm-3">			    	
						<label class="checkbox-inline">
                    		<html:checkbox property="sample" value="Y" disabled="true"></html:checkbox>������  
                      	</label>						    	
						<label class="checkbox-inline">
							<html:checkbox property="brochure" value="Y" disabled="true"></html:checkbox>�ê����   
						</label>
						<label class="checkbox-inline">
							<html:checkbox property="card" value="Y" disabled="true"></html:checkbox>����� 
						</label>
				    </div>
				    <div class="col-sm-3 col-sm-offset-2">			    	
						<label class="checkbox-inline">
                    		<html:checkbox property="vat" value="Y" disabled="true"></html:checkbox>No Vat  
                      	</label>						    						
				    </div>							
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">�����˵�:</label>
				    <div class="col-sm-3 control-text"> ${remark }</div>					
				</div>
				<div class="table-responsive">  
						 <c:if test="${fn:length(productList) > 0}">  
						 <h3>��¡����觫����Թ���</h3>               
					      <table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="result" >
							<thead>						
								<tr class="grey lighten-4">
									<th width="5%"><div align="center">�ӴѺ</div></th>
									<th width="15%">�Թ���</th>
									<th width="14%"><div align="right">�Ҥҵ�Ҵ/��. (�ҷ)</div></th>	
									<th width="12%"><div align="right">�ҤҢ��/��. (�ҷ)</div></th>								
									<th width="10%"><div align="center">�ӹǹ  (���)</div></th>	
									<th width="10%"><div align="center">�ӹǹ  (�ѹ)</div></th>									
									<th width="10%"><div align="right">�Ҥ���� (�ҷ)</div></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${productList}" var="item" varStatus="row">
								<tr>
									<td align="center">${row.index + 1}</td>
							   		<td>${item.product.productFullName }</td>		
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceId.price}" /></td>	
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceUnit}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0" value = "${item.line}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0.000" value = "${item.weight}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${item.price}" /></td>				 		
								</tr>
								</c:forEach>
								<tr>
									<td align="right" colspan="4">���</td>
									<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0" value = "${totalLine}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${totalUnit}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${totalAmount}" /></label></td>						 		
								</tr>								
								<tr id="discountCash">
									<td align="right" colspan="5">��ǹŴ�Թʴ </td>
									<td align="right"><label >20 ʵ.</label> </td>
							 		<td align="right"><label id="cashTotal"></label></td>				 		
								</tr>	
								<tr id="discount300Ton">
									<td align="right" colspan="5">��ǹŴ 300 �ѹ</td>
									<td align="right"><label>10 ʵ.</label> </td>
							 		<td align="right"><label id="Ton300Total"></label></td>						 		
								</tr>	
								<tr id="discount500Ton">
									<td align="right" colspan="5">��ǹŴ 500 �ѹ </td>
									<td align="right"><label>20 ʵ.</label> </td>
							 		<td align="right"><label id="Ton500Total"></label></td>						 		
								</tr>	
								<tr id="discountS">
									<td align="right" colspan="5">��ǹŴ</td>
							 		<td align="right"><label id="discountP"></label> </td>
							 		<td align="right"><label id="discountM"></label></td>						 		
								</tr>
								<tr id="deposit">
									<td align="right" colspan="5">�Ѵ��</td>
							 		<td align="right"><label id="depositM"></label> </td>
							 		<td align="right"><label id="depositT"></label></td>						 		
								</tr>	
								<tr id="delivery">
									<td align="right" colspan="6">��Ң��� </td>
							 		<td align="right"><label id="deliveryM"></label></td>						 		
								</tr>
								<tr>
									<td align="right" colspan="6">�Ҥ����������</td>
							 		<td align="right"><label id="totalAmount"></label></td>						 		
								</tr>							
							</tbody>
						  </table>
						  </c:if>
				      	</div>			
	       </html:form>
	       	
	    </div>
	    <div class="row">
			<div class="col-lg-12 text-right">     	
				<button type="button" class="btn btn-primary btn-sm" onclick="javascript:history.back();">��͹��Ѻ</button>
			</div>   
		</div>
		<br><br><br>
	</div>
</div>
</div>
</body>
</html>