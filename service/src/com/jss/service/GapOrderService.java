package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.GapOrder;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface GapOrderService extends CoreService<GapOrder, Serializable> {
	
	public List<GapOrder> getOrder() throws DataAccessException;
	public List<GapOrder> getOrderToday() throws DataAccessException;
	public List<GapOrder> getOrderSelect(String startDay, String endDay) throws DataAccessException;
	public List<GapOrder> getOrderMonth(String year, String month) throws DataAccessException;
}
