<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<html>
<title>GTT Noodle Home</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Inconsolata">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">

<style>
body,h1,h5 {
	font-family: "Raleway", sans-serif
}

body,html {
	height: 100%
}

.bgimg {
	background-image: url('./33.jpg');
	min-height: 100%;
	background-position: center;
	background-size: cover;
}
</style>


<script language="javascript" type="text/javascript">
	function submitFormInit(mode) {
		document.customerForm.mode.value = mode;
		document.customerForm.edit.value = '';
		document.customerForm.submit();
	}

	function submitFormAdd(mode) {
		if ($("#customerForm").valid()) {
			document.customerForm.mode.value = mode;
			document.customerForm.submit();
		}
	}

	function doSave() {
		document.forms[1].mode.value = 'registerSave';
		document.forms[1].submit();
		alert("บันทึกเข้าสู่ระบบแล้ว");
	}

	function doLogin() {
		document.forms[0].mode.value = 'customerLogin';
		document.forms[0].submit();

	}
	
	function login() {
		$('#myModal').modal('show'); 
	}
	
</script>

<script>
$(document).ready(function(){
    $("#aboutButton").click(function(){
        $("#about").modal({backdrop: false});
    });
});
</script>


<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

<script language="javascript" type="text/javascript">
	
<%-- กำหนดค้าเริ่ม --%>
	/* $(document).ready(function() {
		$("#useto").hide();
		$("#no_login").hide();
		$("#yes_login").hide();
		$("#Q_login").hide();
		$("#Q_use").hide();
		$("#yes_useto").hide();
		$("#no_useto").hide();
		$("#Q_regis").hide();
		$("#no_regis").hide();
		$("#yes_regis").hide();
		$("#Q_table").hide();
		$("#s_table").hide();

	}); */

	$(function() {
		$("#email").attr("placeholder", "กรุณากรอกอีเมลล์");
		$("#mobile").attr("placeholder", "กรุณากรอกเบอร์โทรศัพท์");
		$("#name").attr("placeholder", "กรุณากรอกชื่อ-นามสกุล");
		$("#username").attr("placeholder", "กรุณากรอกชื่อผู้ใช้งาน");
		$("#password").attr("placeholder", "กรุณากรอกรหัสผ่าน");
		$("#addr").attr("placeholder", "กรุณากรอกที่อยู่ในการจัดส่งอาหาร");
		$("#userLogin").attr("placeholder", "กรุณากรอกชื่อผู้ใช้งาน");
		$("#pwdLogin").attr("placeholder", "กรุณากรอกรหัสผ่าน");
		
		
		
	});
</script>


<script language="javascript" type="text/javascript">
	function doWelcome2(id) {
		document.getElementById("welcome").innerHTML = "คลิกเพื่อเริ่มต้นใช้งาน";

	}

	function doWelcome3(id) {
		document.getElementById("welcome").innerHTML = "<strong>GTT Noodle</strong><br>ยินดีต้อนรับ</button>";

	}
<%-- press button submit on contact modal --%>
	/* function doWelcome(id) {
		$("#Q_use").show();
		$("#welcome").hide();
		$("#yes_useto").show();
		$("#no_useto").show();
	}

	function doUse(id) {
		$("#no_login").show();
		$("#yes_login").show();
		$("#Q_login").show();

		$("#yes_useto").hide();
		$("#no_useto").hide();
		$("#Q_use").hide();

	}

	function doNoUse(id) {
		$("#no_regis").show();
		$("#yes_regis").show();
		$("#Q_regis").show();

		$("#yes_useto").hide();
		$("#no_useto").hide();
		$("#Q_use").hide();

	}

	function doOrder(id) { //สั่งอาหาร
		$("#no_login").hide();
		$("#yes_login").hide();
		$("#Q_login").hide();
		$("#no_regis").hide();
		$("#yes_regis").hide();
		$("#Q_regis").hide();

		$("#Q_table").show();
		$("#s_table").show();

	} */
</script>

<script language="javascript" type="text/javascript">
	function doTable(id) {
		$("#Q_table").hide();
		$("#s_table").hide();

	}
</script>


<body>

	<%-- text center --%>
	<div class="bgimg w3-display-container w3-text-white">
		<%-- <div class="w3-display-middle w3-jumbo" align="center">
			<p style="color: black; text-shadow: 4px 4px 5px #fff" Id="welcome"
				onclick="doWelcome(this.value)" onmouseover="doWelcome2(this.value)"
				onmouseout="doWelcome3(this.value)">
				<strong>GTT Noodle</strong><br>ยินดีต้อนรับ
			</p>


			เคยใช้ใหม
			<h1 id="Q_use">
				เคยใช้บริการร้านเราหรือไม่ ?
				</h2>
				<p>
					<button Id="yes_useto" onclick="doUse(this.value);"
						class="w3-button w3-green">เคยใช้แล้ว</button>
				</p>
				<p>
					<button Id="no_useto" onclick="doNoUse(this.value);"
						class="w3-button w3-red">ยังไม่เคย..พึ่งใช้ครั้งแรก</button>
				</p>

				กดเลือก เคยใช้
				<p id="Q_login">ต้องการล็อคอินหรือไม่ ?</p>
				<p>
					<button Id="no_login"
						onclick="location.href='customer.htm?mode=chooseMenu'"
						class="w3-button w3-green">ไม่ต้องการ..สั่งอาหารเลย</button>
				</p>
				<p>
					<button Id="yes_login" class="w3-button w3-red" data-toggle="modal"
						data-target="#myModal">ต้องการล็อคอิน</button>
				<p>

					กดเลือก ไม่เคยใช้
				<p id="Q_regis">ต้องการสมัครสมาชิกใหม ?</p>
				<p>
					<button Id="no_regis"
						onclick="location.href='customer.htm?mode=chooseMenu'"
						class="w3-button w3-green">ไม่ต้องการ..สั่งอาหารเลย</button>
				</p>
				<p>
					<button Id="yes_regis" class="w3-button w3-red" data-toggle="modal"
						data-target="#myModal">ต้องการสมัคร เพื่อใช้งานครั้งต่อไป</button>
				</p>
		</div> --%>

		<%-- menu and contact --%>
		<div class="w3-display-topleft w3-container w3-xlarge" align="left">
			<br>
			<p>
				<button
					onclick="location.href='customer.htm?mode=chooseMenu'"
					class="w3-button w3-black" data-toggle="tooltip"
					data-placement="right" title="เมนู">
					<span class="glyphicon glyphicon-cutlery"></span>
				</button>
			</p>
			<p data-toggle="tooltip" data-placement="right" title="เกี่ยวกับเรา  & ติดต่อเรา">
				
				<button 
					id="aboutButton" type="button"
					class="w3-button w3-black" onclick="location.href='customer.htm?mode=about'">
					<i class="fas fa-envelope"></i>
				</button>
				
			</p>
			
		</div>

		<%-- cart car --%>
		<div class="w3-display-topright w3-container w3-xlarge" align="right">
			<br>
			<p>
				<button onclick="location.href='order.htm?mode=index'"w
					class="w3-button w3-black" data-toggle="tooltip"
					data-placement="left" title="ตะกร้าสินค้า">
					<span class="glyphicon glyphicon-shopping-cart"></span>
				</button>
			</p>
	
			<p data-toggle="tooltip" data-placement="left" title="เข้าสู่ระบบ หรือ สมัครใช้งาน">
				<button onclick="login()"
					class="w3-button w3-black"  >
					<span class="fas fa-sign-in-alt"></span>
				</button>
			</p>
		</div>

		<div class="w3-display-bottomleft w3-container">
			<span class="w3-tag"><span class="glyphicon glyphicon-time"></span>
				9.00 น. - ปิด 21.00 น.</span><br>
			<br>

		</div>

		<div class="w3-display-bottomright w3-container">
			<span class><div class="w3-right-align">
					<p>
						<strong>49/104 หมู่ 4 หมู่บ้านฉัตรหลวง13<br>ถ.ปทุมธานี-สามโคก
					</p>
					</strong></span>
		</div>
	</div>
	<br>
	<br>
	</div>

	</div>

	<%-- modal login --%>
	<style>
.nav-tabs {
	margin-bottom: 15px;
}

.sign-with {
	margin-top: 25px;
	padding: 20px;
}

div#OR {
	height: 30px;
	width: 30px;
	border: 1px solid #C2C2C2;
	border-radius: 50%;
	font-weight: bold;
	line-height: 28px;
	text-align: center;
	font-size: 12px;
	float: right;
	position: absolute;
	right: -16px;
	top: 40%;
	z-index: 1;
	background: #DFDFDF;
}
</style>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">×</button>

					<h4 align="center" class="modal-title" id="myModalLabel">เข้าสู่ระบบ
						/สมัครสมาชิก</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8"
							style="border-right: 1px dotted #C2C2C2; padding-right: 30px;">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs">
								<li class="active"><a href="#Login" data-toggle="tab">เข้าสู่ระบบ</a></li>
								<li><a href="#Registration" data-toggle="tab">สมัครสมาชิก</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane active" id="Login">
									<html:form action="/customer" styleId="loginForm"
										styleClass="form-horizontal" method="post">
										<html:hidden property="id" />
										<html:hidden property="mode" />
										<div class="form-group">
											<label for="userName" class="col-sm-3 control-label">
												ชื่อผู้ใช้งาน</label>
											<div class="col-sm-9">
												<html:text property="userLogin"
													styleClass="form-control" styleId="userLogin" />
											</div>
										</div>
										<div class="form-group">
											<label for="password"
												class="col-sm-3 control-label"> รหัสผ่าน</label>
											<div class="col-sm-9">
												<html:text property="pwdLogin"
													styleClass="form-control" styleId="pwdLogin" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-2"></div>
											<div class="col-sm-10">
												<button type="submit" class="btn btn-success btn-sm"
													onclick="doLogin()">ตกลง</button>
												&nbsp;&nbsp;
												<storng>or</storng>
												&nbsp;&nbsp; <a href="javascript:;">ลืมรหัสผ่าน?</a>
											</div>
										</div>
										</form>
								</div>
								</html:form>

								<%-- REGISTORATION --%>
								<div class="tab-pane" id="Registration">
									<form role="form" class="form-horizontal">
										<html:form action="/customer" styleId="registerForm"
											styleClass="form-horizontal" method="post">
											<html:hidden property="mode" />
											<html:hidden property="id" />
											<div class="form-group">
												<label for="name" class="col-sm-3 control-label">
													ชื่อ-นามสกุล :</label>
												<div class="col-sm-9">
													<div class="row">
														<div class="col-md-3">
															<html:select styleClass="form-control" property="prefix"
																styleId="prefix">
																<html:option value="1">นาย</html:option>
																<html:option value="2">นางสาว</html:option>
																<html:option value="3">นาง</html:option>
															</html:select>
														</div>
														<div class="col-md-9">
															<html:text property="name" styleId="name"
																styleClass="form-control"></html:text>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="email" class="col-sm-3 control-label">
													อีเมล์ : </label>
												<%--  <div class="col-sm-3">
										<html:text property="email" styleId="email" styleClass="form-control"></html:text>
										</div> --%>
												<div class="col-sm-9">
													<html:text property="email" styleClass="form-control"
														styleId="email" />
												</div>
											</div>
											<div class="form-group">
												<label for="mobile" class="col-sm-3 control-label">
													เบอร์โทรศัพท์ : </label>
												<div class="col-sm-9">
													<html:text property="mobile" styleClass="form-control"
														styleId="mobile" />
												</div>
											</div>
											<div class="form-group">
												<label for="address" class="col-sm-3 control-label">
													ที่อยู่ :</label>
												<div class="col-sm-9">
													<html:text property="addr" styleClass="form-control"
														styleId="addr" />
												</div>
											</div>
											<div class="form-group">
												<label for="mobile" class="col-sm-3 control-label">
													ชื่อผู้ใช้งาน : </label>
												<div class="col-sm-9">
													<html:text property="username" styleClass="form-control"
														styleId="username" />
												</div>
											</div>
											<div class="form-group">
												<label for="password" class="col-sm-3 control-label">
													รหัสผ่าน :</label>
												<div class="col-sm-9">
													<html:text property="password" styleClass="form-control"
														styleId="password" />
												</div>
											</div>
											<div class="row">
												<div class="col-sm-2"></div>
												<div class="col-sm-10">
													<button type="button" class="btn btn-success btn-sm"
														id="btnSave" onclick="doSave()">บันทึก</button>
													<button type="button" class="btn btn-danger btn-sm"
														id="btnCancle" data-dismiss="modal">ยกเลิก</button>
												</div>
											</div>
									</form>
									</html:form>
								</div>
								<div id="OR" class="hidden-xs">OR</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row text-center sign-with">
								<div class="col-md-12">
									<h3>Sign in with</h3>
								</div>
								<div class="col-md-12">
									<div class="btn-group btn-group-justified">
										<a href="#" class="btn btn-primary">Facebook</a> <a href="#"
											class="btn btn-danger"> Google</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>


<script>
	// Get the modal
	var ddd = document.getElementById('myModal');

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == ddd) {
			ddd.style.display = "none";
		}
	}
</script>

</html>

