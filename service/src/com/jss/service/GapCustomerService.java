package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface GapCustomerService extends CoreService<GapCustomer, Serializable> {
	
	public List<GapCustomer> getCust(String id) throws DataAccessException;
	public List<GapCustomer> getLogin(String un , String pwd) throws DataAccessException;	
	
}
