package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.UserGroup;

public interface CarDao extends CoreDao<Car, Serializable> {

	public List<Car> findCarByCust(String custId) throws DataAccessException;
}
