package com.jss.dao.impl;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.ProductDao;
import com.jss.entity.Product;
import com.jss.entity.ProductType;

public class ProductDaoImpl extends CoreDaoImpl<Product, Serializable> implements ProductDao {

	public ProductDaoImpl(Class<Product> entityClass) {
		super(entityClass);
		
	}

	@Override
	public List<Product> findProductList(String productTypeId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Product.class);
		criteria.createAlias("productType","productType");
		criteria.addOrder(Order.asc("productType.id"));	
		criteria.addOrder(Order.asc("size"));
		
		if(productTypeId != null && ! productTypeId.equals("")){
			criteria.add(Restrictions.eq("productType.id", Integer.parseInt(productTypeId)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<Product> findProductListPrice(String price) throws DataAccessException {
		String sql = " SELECT product_code,";
		sql += " (SELECT product_weight FROM ma_product p1 WHERE p1.product_length = 10 AND p1.product_code = p2.product_code";
		sql += " GROUP BY p1.product_code) AS 10M,";
		sql += " (SELECT product_weight FROM ma_product p1 WHERE p1.product_length = 12 AND p1.product_code = p2.product_code";
		sql += " GROUP BY p1.product_code) AS 12M,";
		sql += " product_diff,";
		sql += " producttype_id";
		sql += " FROM ma_product p2";
		sql += " GROUP BY p2.product_code";
		sql += " ORDER BY p2.producttype_id,p2.product_size";
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Product> results = new ArrayList<Product>();
		if (objects != null && objects.size() > 0) {
			Double prices = Double.valueOf(price);
			for (Object[] obj : objects) {
				Product entity = new Product();

				Double m10 = null;
				Double m12 = null;
				Double priceOver = prices+Double.valueOf(String.valueOf(obj[3]));
				Double priceLess = priceOver+0.2;
				Double priceOverVat = (((priceOver*7)/100)+priceOver);
				Double priceLessVat = (((priceLess*7)/100)+priceLess);
				
				if(obj[2] != null && !obj[2].equals("")){
					m10 = Double.valueOf(String.valueOf(obj[1]));		
				}
				if(obj[3] != null && !obj[3].equals("")){
					m12 = Double.valueOf(String.valueOf(obj[2]));
				}
				
				entity.setCode(String.valueOf(obj[0]));

				entity.setProductType(new ProductType(Integer.parseInt(String.valueOf(obj[4]))));

				results.add(entity);
			}
		}
	
	return results;
	}

	@Override
	public List<Product> findProductByCode(String code)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Product.class);
		
		if(code != null && ! code.equals("")){
			criteria.add(Restrictions.eq("code", code));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}


	@Override
	public List<Product> findProductStockList(String productId,String productTypeId, String productCode,String productModel) throws DataAccessException {
		
			String sql = " SELECT ma_product.product_id,product_code,product_size,product_length,product_model,product_weight,";
			sql += " SUM(stock.stock_line) AS line, ";
			sql += " SUM(stock.stock_unit) AS unit, ";
			sql += " SUM(unit_line) AS unitLine,";
			sql += " SUM(unit_weight) AS unitWeight";
			sql += " FROM ma_product";			
			sql += " LEFT JOIN stock ON stock.product_id = ma_product.product_id ";
			sql += " LEFT JOIN ma_producttype ON ma_producttype.producttype_id = ma_product.producttype_id ";
			sql += " LEFT JOIN transportproduct ON transportproduct.price_id = ma_product.product_id";
			if(productId != null && !productId.equals("")){
				sql += " WHERE ma_product.product_id ="+productId;
			}else{
				sql += " WHERE ma_product.product_id <> 0 ";
			}
			
			if (productTypeId != null && !productTypeId.equals("")) {
				sql += " AND ma_product.producttype_id =  " + productTypeId;
			}
			if (productCode != null && !productCode.equals("")) {
				sql += " AND ma_product.product_code LIKE '%"+productCode+"%' ";
			}
			if (productModel != null && !productModel.equals("")) {
				sql += " AND ma_product.product_model = '" + productModel+"'";
			}
			
			sql += " GROUP BY ";
			sql += " ma_product.product_id ";
			sql += " ORDER BY ";
			sql += " ma_product.producttype_id, ";
			sql += " ma_product.product_size ";
		
			List<Object[]> objects = getSession().createSQLQuery(sql).list();
	
			List<Product> results = new ArrayList<Product>();
			if (objects != null && objects.size() > 0) {
				
				for (Object[] obj : objects) {
					Product entity = new Product();
					Integer line = 0;
					Double unit = 0.0;
					Integer unitLine = 0;
					Double unitWeight = 0.0;
					Integer totalLine = 0;
					Double totalUnit = 0.0;
					//System.out.println(String.valueOf(obj[2]));
					
					if(obj[6] != null && !obj[6].equals("")){
						line = Integer.parseInt(String.valueOf(obj[6]));		
					}
					if(obj[7] != null && !obj[7].equals("")){
						unit = Double.parseDouble(String.valueOf(obj[7]));
					}
					
					if(obj[8] != null && !obj[8].equals("")){
						unitLine = Integer.parseInt(String.valueOf(obj[8]));		
					}
					if(obj[9] != null && !obj[9].equals("")){
						unitWeight = Double.parseDouble(String.valueOf(obj[9]));
					}
					
					totalLine = line-unitLine;
					totalUnit = unit-unitWeight;
					
					entity.setId(Integer.parseInt(String.valueOf(obj[0])));
					entity.setCode(String.valueOf(obj[1]));
				
					
					results.add(entity);
				}
			}
		
		return results;
	}
	

}