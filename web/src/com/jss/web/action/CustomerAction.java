package com.jss.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;































import com.jss.entity.Constant;
import com.jss.entity.Customer;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapItem;
import com.jss.entity.GapOrder;
import com.jss.entity.GapOrderItem;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.OrderIncome;
import com.jss.entity.Product;
import com.jss.entity.Province;
import com.jss.entity.User;
import com.jss.entity.UserGroup;
import com.jss.service.GapCustomerService;
import com.jss.service.GapItemService;
import com.jss.service.GapOrderItemService;
import com.jss.service.GapOrderService;
import com.jss.service.OrderIncomeService;
import com.jss.service.OrderMenuService;


public class CustomerAction extends CoreAction {
	private OrderMenuService orderMenuService;
	private GapItemService gapItemService;
	private GapOrderItemService gapOrderItemService;
	private GapOrderService gapOrderService;
	private GapCustomerService gapCustomerService;




	public ActionForward customer(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU01");
	}
	
	public ActionForward cu012(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU012");
	}
	

	public ActionForward allMenu(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<OrderMenu> result = orderMenuService.getAllItems();
			
			/*dynaForm.set("resultList", result);*/
			request.setAttribute("resultList",result);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("CU03");
	}

	
	
	public ActionForward chooseMenu(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapItem> result = gapItemService.getAllItems();
			
			
		/*	dynaForm.set("resultList", result);*/
			request.setAttribute("resultList",result);
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU04");
	}
	
	public ActionForward menu(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			@SuppressWarnings("unused")
			DynaActionForm dynaForm = (DynaActionForm) form;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU05");
	}
	
	public ActionForward registerSave(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapCustomer entity = new GapCustomer();
		
			entity.setPrefix(dynaForm.getString("prefix")); //1นาย    2นางสาว   3นาง
			entity.setName(dynaForm.getString("name"));
			entity.setEmail(dynaForm.getString("email"));
			entity.setMobile(dynaForm.getString("mobile"));
			entity.setAddr(dynaForm.getString("addr"));
			entity.setUsername(dynaForm.getString("username"));
			entity.setPassword(dynaForm.getString("password"));
			gapCustomerService.saveItem(entity);
		
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("CU01");
	}
	
	public ActionForward customerPay(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("CU06");
	}
	
	public ActionForward customerLogin(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {	
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapCustomer> check = gapCustomerService.getLogin(dynaForm.getString("userLogin"), dynaForm.getString("pwdLogin"));
			if (check != null && check.size() > 0) {
				GapCustomer gapCustomer = check.iterator().next();
				setObjectSession(request, SESSION_USER, gapCustomer);
				
			
			
				
			} 
			else {
				return mapping.findForward("CU01");
				/*return mappingForward(mapping, request, "mode", "customer", "login.htm", "loginForm", "&er=90");*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			return mapping.findForward("CU01");
			/*return mappingForward(mapping, request, "mode", "customer", "login.htm", "loginForm", "&er=91");*/
		}

		return mappingForward(mapping, request, "mode", "chooseMenu", "customer.htm", "cusForm", "&er=91");
	}
	

	public ActionForward orderList(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapOrderItem> result = gapOrderItemService.getAllItems();
			request.setAttribute("resultList",result);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU07");
	}
	
	public ActionForward addToCart(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapOrderItem entity = new GapOrderItem();
			
			entity.setNoodleType(Integer.parseInt(dynaForm.getString("noodle")));
			entity.setVet(Integer.parseInt(dynaForm.getString("vet")));
			entity.setExtra(Integer.parseInt(dynaForm.getString("extra")));
			entity.setQty(Integer.parseInt(dynaForm.getString("qty")));
			entity.setGapItem(gapItemService.getItem(Integer.parseInt(dynaForm.getString("gapItemId"))));
		

			gapOrderItemService.saveItem(entity);
				
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return chooseMenu(mapping, form, request, response);
	}
	
	public ActionForward cart(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapOrder> result = gapOrderService.getAllItems();
			request.setAttribute("resultList",result);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU05");
	}
	
	public ActionForward typeDetail(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			
			String id = request.getParameter("id");
			List<GapItem> resultList = gapItemService.getItem(id);
			
			setJSONResult(request, setJsonView(resultList, null));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	
	public ActionForward typeDetailDrink(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			
			String id = request.getParameter("id");
			List<GapItem> resultList = gapItemService.getItem(id);
			
			setJSONResult(request, setJsonView(resultList, null));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	
	public ActionForward changeStatus(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapOrderItem entity = gapOrderItemService.getItem(Integer.parseInt(dynaForm.getString("id")));
			
			entity.setStatus(Integer.parseInt(dynaForm.getString("status")));
			gapOrderItemService.updateItem(entity);
			}
		 catch (Exception e) {
			e.printStackTrace();
		}

		return orderList(mapping, form, request, response);
	}
	
	public ActionForward changeStatusToGreen(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapOrderItem entity = new GapOrderItem();
			entity.setStatus(1);
			gapOrderItemService.updateItem(entity);
			}
		
		 catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU07");
	}
	
	
	public ActionForward about(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("CU08");
	}
	
	


	
	


	public OrderMenuService getOrderMenuService() {
		return orderMenuService;
	}


	public void setOrderMenuService(OrderMenuService orderMenuService) {
		this.orderMenuService = orderMenuService;
	}


	public GapItemService getGapItemService() {
		return gapItemService;
	}


	public void setGapItemService(GapItemService gapItemService) {
		this.gapItemService = gapItemService;
	}


	public GapOrderItemService getGapOrderItemService() {
		return gapOrderItemService;
	}


	public void setGapOrderItemService(GapOrderItemService gapOrderItemService) {
		this.gapOrderItemService = gapOrderItemService;
	}


	public GapOrderService getGapOrderService() {
		return gapOrderService;
	}


	public void setGapOrderService(GapOrderService gapOrderService) {
		this.gapOrderService = gapOrderService;
	}


	public GapCustomerService getGapCustomerService() {
		return gapCustomerService;
	}


	public void setGapCustomerService(GapCustomerService gapCustomerService) {
		this.gapCustomerService = gapCustomerService;
	}
	
	
	
	
	
	
	

	


}
