package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.District;

public interface DistrictService extends CoreService<District, Serializable> {
	
	public List<District> getDistrictbyProv(String provId) throws DataAccessException;
}
