<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
	$('#orderForm').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	menuCode: {
                validators: {
                    notEmpty: {
                    	message: '��س��к�: ��������'
                    }
                }
            },
            menuName: {
                validators: {
                    notEmpty: {
                    	message: '��س��к�: ��������'
                    }
                }
            },
            menuLink: {
                validators: {
                    notEmpty: {
                    	message: '��س��к�: �ԧ������'
                    }
                }
            },
                    }
           
    }).on('success.form.fv', function(e) {
        // Prevent submit form
        e.preventDefault();
        
        $("button[name='btnSave']").attr('disabled', true);
        
        document.orderForm.mode.value = 'menuSave';
		document.orderForm.submit();
    });
});
</script>

<script language="javascript" type="text/javascript">

function doEdit(id) {
  	document.orderForm.mode.value = 'menuEdit';
	document.orderForm.id.value = id;
    document.orderForm.submit();    
}

function doSearch(id) {
  	document.orderForm.mode.value = 'orderSearchNow';
	document.orderForm.id.value = id;
    document.orderForm.submit();    
}

function doDelete(id) {
	bootbox.dialog({
		size: "small",
		message: "�س��ͧ���ź��������¡�ù�����������?",
		title: "<span class='fred'>�׹�ѹ���ź������</span>",
		buttons: {
			ok: {
				label: "��ŧ",
		      	className: "btn-danger",
		      	callback: function() {
		      		document.orderForm.mode.value = 'menuDel';
		      		document.orderForm.id.value = id;
		      		document.orderForm.submit();
				}
			},
		    calcel: {
		      	label: "¡��ԡ",
		      	className: "btn-default",
		      	callback: function() {}
		    }
		}
	});
}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�ʴ���¡������� <span class="navigator">[US05]</span></h3>
				<span class="navigator" >�Է�������ҹ > �ʴ���¡�������</span><br><br>
			</div>       
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/order" styleId="orderForm" styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"> ����ʴ���¡�������:</label>
						<div class="col-sm-3">
							<html:select property="timeFood" styleClass="form-control">
							<html:option value="0">������</html:option>
							<html:option value="1">��������</html:option>
							<html:option value="2">�ѧ�������</html:option>
							</html:select>
						</div>
					</div>				
					<div class="form-group">
						<label class="col-sm-2 control-label"> ������§:</label>
						<div class="col-sm-3">
							<html:select property="nameFood" styleClass="form-control">
							<html:option value="1">���§�����觡�͹</html:option>
							<html:option value="2">���§��������쪹Դ���ǡѹ</html:option>
							
							</html:select>
						</div>
					</div>	
					
					
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-3">
							<button type="button" class="btn btn-success" 
							onclick="doSearch()">����</button>
							<button type="button" class="btn btn-danger" 
							onclick="location.href='order.htm?mode=orderNow'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
			</div>       
		</div>
		
			<c:if test="${fn:length(resultList) >= 0}">
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="50">�ӴѺ</th>
						<th width="400">��������</th>
						<th width="100">�����Ţ���</th>
						<th width="100">�ӹǹ</th>
						<th width="80" ></th>
				
					</tr>
				</thead>
				<tbody>
				 <c:forEach items="${resultList}" var="item" varStatus="row">
					<tr >
					    <td>${item.nameFood}</td>
					    <td>${item.name}</td>
					      <td>${item.link}</td>
					      <td>${item.icon}</td>
					  	<td align="center"> 				  		
							<a class="btn btn-info btn-xs" href="#" onclick="doEdit('${item.id}')"><i class="fa fa-pencil btn-info"></i></a>
							<a class="btn btn-danger btn-xs" href="#" onclick="doDelete('${item.id}')"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>					
				</c:forEach>
				</tbody>
			</table>
			</c:if>
	</div>    
</body>
</html>