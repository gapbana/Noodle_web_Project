<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(function(){
	$('#chkSearchAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkSearch').each(function(){ $('.chkSearch').prop('checked', true); });
		} else {
			$('.chkSearch').each(function(){ $('.chkSearch').prop('checked', false); });
		}
	});
	
	$('#chkAddAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkAdd').each(function() { $('.chkAdd').prop('checked', true); });
		} else {
			$('.chkAdd').each(function() { $('.chkAdd').prop('checked', false); });
		}
	});
	
	$('#chkEditAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkEdit').each(function() { $('.chkEdit').prop('checked', true); });
		} else {
			$('.chkEdit').each(function() { $('.chkEdit').prop('checked', false); });
		}
	});
	
	$('#chkDelAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkDelete').each(function() { $('.chkDelete').prop('checked', true); });
		} else {
			$('.chkDelete').each(function() { $('.chkDelete').prop('checked', false); });
		}
	});
	
	$('#chkPrintAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkPrint').each(function() { $('.chkPrint').prop('checked', true); });
		} else {
			$('.chkPrint').each(function() { $('.chkPrint').prop('checked', false); });
		}
	});
	
	$('#chkApproveAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkApprove').each(function() { $('.chkApprove').prop('checked', true); });
		} else {
			$('.chkApprove').each(function() { $('.chkApprove').prop('checked', false); });
		}
	});
	
	$('#chkDetailAll').click(function(){
		if ($(this).is(':checked')) {
			$('.chkDetail').each(function() { $('.chkDetail').prop('checked', true); });
		} else {
			$('.chkDetail').each(function() { $('.chkDetail').prop('checked', false); });
		}
	});
});

function doSavePermit() {
	var cSearch = '';
	$('.chkSearch').each(function() {
	    if($(this).is(':checked')) {
	    	cSearch += $(this).val() + ',';
	    }
	});
	
	var cAdd = '';
	$('.chkAdd').each(function() {
	    if($(this).is(':checked')) {
	    	cAdd += $(this).val() + ',';
	    }
	});
	
	var cEdit = '';
	$('.chkEdit').each(function() {
	    if($(this).is(':checked')) {
	    	cEdit += $(this).val() + ',';
	    }
	});
	
	var cDelete = '';
	$('.chkDelete').each(function() {
	    if($(this).is(':checked')) {
	    	cDelete += $(this).val() + ',';
	    }
	});
	
	var cPrint = '';
	$('.chkPrint').each(function() {
	    if($(this).is(':checked')) {
	    	cPrint += $(this).val() + ',';
	    }
	});
	
	var cAppv = '';
	$('.chkApprove').each(function() {
	    if($(this).is(':checked')) {
	    	cAppv += $(this).val() + ',';
	    }
	});
	
	var cDetail = '';
	$('.chkDetail').each(function() {
	    if($(this).is(':checked')) {
	    	cDetail += $(this).val() + ',';
	    }
	});
	
	document.userForm.mode.value = 'permitSave';
	document.userForm.hidSearch.value = cSearch.length > 0 ? cSearch.substring(0, cSearch.length-1) : '';
	document.userForm.hidAdd.value = cAdd.length > 0 ? cAdd.substring(0, cAdd.length-1) : '';
	document.userForm.hidEdit.value = cEdit.length > 0 ? cEdit.substring(0, cEdit.length-1) : '';
	document.userForm.hidDelete.value = cDelete.length > 0 ? cDelete.substring(0, cDelete.length-1) : '';
	document.userForm.hidPrint.value = cPrint.length > 0 ? cPrint.substring(0, cPrint.length-1) : '';
	document.userForm.hidApprove.value = cAppv.length > 0 ? cAppv.substring(0, cAppv.length-1) : '';
	document.userForm.submit();
}

function doBack() {
	location.href='user.htm?mode=userGroup';
	
}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�Է�������ҹ�к�  ����������ҹ��������к� <span class="navigator">[US02]</span> </h3>
				
			</div>       
		</div>
		<div class="row">
			<div class="col-lg-6">
				<span class="navigator" >�Է�������ҹ > �����š���������ҹ�к� > �Է�������ҹ�к�</span>				
			</div>
	     	<div class="col-lg-6 text-right">     	
	     		<button type="button" class="btn btn-primary btn-sm" onclick="location.href='user.htm?mode=userGroup'">��͹��Ѻ</button>
	        </div>   
		</div>
		<br>
		<div class="row">
			<html:form action="/user" styleId="userForm" styleClass="form-horizontal" method="post">
				<html:hidden property="mode"/>
				<html:hidden property="id" styleId="id"/>
				<html:hidden property="hidSearch" styleId="hidSearch"/>
				<html:hidden property="hidAdd" styleId="hidAdd"/>
				<html:hidden property="hidEdit" styleId="hidEdit"/>
				<html:hidden property="hidDelete" styleId="hidDelete"/>
				<html:hidden property="hidPrint" styleId="hidPrint"/>
				<html:hidden property="hidApprove" styleId="hidApprove"/>
		      	<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
					<thead>
						<tr class="cyan lighten-4">
							<th width="40%">���ء����ҹ</th>
							<th width="10%"><div align="center">����&nbsp;<input type="checkbox" id="chkSearchAll"/></div></th>
							<th width="10%"><div align="center">�ѹ�֡&nbsp;<input type="checkbox" id="chkAddAll"/></div></th>
							<th width="10%"><div align="center">���&nbsp;<input type="checkbox" id="chkEditAll"/></div></th>
							<th width="10%"><div align="center">ź&nbsp;<input type="checkbox" id="chkDelAll"/></div></th>
							<th width="10%"><div align="center">�����&nbsp;<input type="checkbox" id="chkPrintAll"/></div></th>
							<th width="10%"><div align="center">͹��ѵ�&nbsp;<input type="checkbox" id="chkApproveAll"/></div></th>
						</tr>
					</thead>
					<tbody>						
						<c:forEach items="${menuList}" var="item" varStatus="row">
							<c:if test="${item.type eq '1'}">
								<c:if test="${item.link ne '#'}">
									<tr>
										<td align="left"><b>${item.name}</b></td>
										<td align="center"><html:multibox property="checkSearch" value="${item.id}" styleClass="chkSearch"></html:multibox></td>
										<td align="center"><html:multibox property="checkAdd" value="${item.id}" styleClass="chkAdd"></html:multibox></td>
										<td align="center"><html:multibox property="checkEdit" value="${item.id}" styleClass="chkEdit"></html:multibox></td>
										<td align="center"><html:multibox property="checkDelete" value="${item.id}" styleClass="chkDelete"></html:multibox></td>
										<td align="center"><html:multibox property="checkPrint" value="${item.id}" styleClass="chkPrint"></html:multibox></td>
										<td align="center"><html:multibox property="checkApprove" value="${item.id}" styleClass="chkApprove"></html:multibox></td>										
									</tr>
								</c:if>
								<c:if test="${item.link eq '#'}">
									<tr>
										<td align="left"><b> ${item.name}</b></td>
										<td align="center"><html:multibox property="checkSearch" value="${item.id}" styleClass="chkSearch"></html:multibox></td>
										<td align="center"></td>
										<td align="center"></td>
										<td align="center"></td>
										<td align="center"></td>
										<td align="center"></td>
									</tr>
								</c:if>
							</c:if>
							<c:if test="${item.type ne '1'}">
								<tr>
									<td align="left">&nbsp;&nbsp;&nbsp; - ${item.name}</td>
									<td align="center"><html:multibox property="checkSearch" value="${item.id}" styleClass="chkSearch"></html:multibox></td>
									<td align="center"><html:multibox property="checkAdd" value="${item.id}" styleClass="chkAdd"></html:multibox></td>
									<td align="center"><html:multibox property="checkEdit" value="${item.id}" styleClass="chkEdit"></html:multibox></td>
									<td align="center"><html:multibox property="checkDelete" value="${item.id}" styleClass="chkDelete"></html:multibox></td>
									<td align="center"><html:multibox property="checkPrint" value="${item.id}" styleClass="chkPrint"></html:multibox></td>
									<td align="center"><html:multibox property="checkApprove" value="${item.id}" styleClass="chkApprove"></html:multibox></td>									
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
				</table>
			
		</div>
		<div class="row">
			<div class="form-group">
				<div class="col-sm-3 col-sm-offset-2">	
					<button type="button" class="btn btn-success" onclick="doSavePermit()">�ѹ�֡</button>           
		       		<button type="button" class="btn btn-danger" onclick="doBack()">¡��ԡ</button> 
		      	</div>
			</div>
		</div>
		</html:form>
		<br><br>
	</div>
</body>
</html>