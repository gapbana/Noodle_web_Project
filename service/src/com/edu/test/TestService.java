package com.edu.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.edu.util.DateUtil;
import com.edu.util.LogHelper;
import com.jss.entity.Constant;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapItem;
import com.jss.entity.GapOrder;
import com.jss.entity.GapOrderItem;
import com.jss.entity.Menu;
import com.jss.entity.OptionDTO;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.Running;
import com.jss.entity.OrdersProduct;
import com.jss.entity.Price;
import com.jss.entity.PriceHistory;
import com.jss.entity.Product;
import com.jss.entity.Stock;
import com.jss.entity.Transport;
import com.jss.entity.TransportProduct;
import com.jss.service.CarService;
import com.jss.service.ConstantService;
import com.jss.service.CustomerService;
import com.jss.service.DistrictService;
import com.jss.service.GapCustomerService;
import com.jss.service.GapItemService;
import com.jss.service.GapOrderItemService;
import com.jss.service.GapOrderService;
import com.jss.service.MenuService;
import com.jss.service.OrderMenuService;
import com.jss.service.RunningService;
import com.jss.service.OrdersProductService;
import com.jss.service.OrdersService;
import com.jss.service.PermissionService;
import com.jss.service.PriceHistoryService;
import com.jss.service.PriceService;
import com.jss.service.ProductService;
import com.jss.service.ProductTypeService;
import com.jss.service.ProvinceService;
import com.jss.service.StockService;
import com.jss.service.SubDistrictService;
import com.jss.service.TransportProductService;
import com.jss.service.TransportService;
import com.jss.service.UserGroupService;
import com.jss.service.UserService;

import junit.framework.TestCase;



public class TestService extends TestCase {
	
	  private UserService userService = null;
	  private UserGroupService userGroupService = null;
	  private MenuService menuService = null;
	  private PermissionService permissionService = null;
	  private DistrictService districtService = null;
	  private CustomerService customerService = null;
	  private SubDistrictService subDistrictService = null;
	  private ProductService productService = null;
	  private ProductTypeService productTypeService = null; 
	  private ProvinceService provinceService = null;
	  private CarService carService = null;
	  private StockService stockService = null;
	  private PriceService priceService = null;
	  private PriceHistoryService priceHistoryService = null;
	  private ConstantService constantService = null;
	  private OrdersService ordersService = null;
	  private OrdersProductService ordersProductService = null;
	  private RunningService runningService = null;
	  private TransportService transportService = null;
	  private TransportProductService transportProductService = null;
	  private OrderMenuService orderMenuService = null;
	  private GapItemService gapItemService = null;
	  private GapOrderItemService gapOrderItemService = null;
	  private GapCustomerService gapCustomerService = null;
	  private GapOrderService gapOrderService = null;
	  
	  
        private ApplicationContext ctx = null;
        SessionFactory sessionFactory = null;



        public TestService(String name) {
                super(name);
                String[] paths = { "/com/edu/conf/application-context.xml" };
                ctx = new ClassPathXmlApplicationContext(paths);
        }

        protected void setUp() throws Exception {
                super.setUp();
                sessionFactory = (SessionFactory) ctx.getBean("sessionFactory");
                Session s = sessionFactory.openSession();
                TransactionSynchronizationManager.bindResource(sessionFactory,new SessionHolder(s));
                userService = (UserService) ctx.getBean("userService");
                userGroupService = (UserGroupService) ctx.getBean("userGroupService");
                menuService = (MenuService) ctx.getBean("menuService");
                permissionService = (PermissionService) ctx.getBean("permissionService");
                districtService = (DistrictService) ctx.getBean("districtService");
                customerService = (CustomerService) ctx.getBean("customerService");
                subDistrictService = (SubDistrictService) ctx.getBean("subDistrictService");
                productService = (ProductService) ctx.getBean("productService");
                productTypeService = (ProductTypeService) ctx.getBean("productTypeService");
                provinceService = (ProvinceService) ctx.getBean("provinceService");
                carService = (CarService) ctx.getBean("carService");
                stockService = (StockService) ctx.getBean("stockService");
                priceService = (PriceService) ctx.getBean("priceService");
                priceHistoryService = (PriceHistoryService) ctx.getBean("priceHistoryService");
                constantService = (ConstantService) ctx.getBean("constantService");
                ordersService = (OrdersService) ctx.getBean("ordersService");
                ordersProductService = (OrdersProductService) ctx.getBean("ordersProductService");
                runningService = (RunningService) ctx.getBean("runningService");
                transportService = (TransportService) ctx.getBean("transportService");
                transportProductService = (TransportProductService) ctx.getBean("transportProductService");
                orderMenuService= (OrderMenuService) ctx.getBean("orderMenuService");
                gapItemService= (GapItemService) ctx.getBean("gapItemService");
                gapCustomerService= (GapCustomerService) ctx.getBean("gapCustomerService");
                gapOrderItemService= (GapOrderItemService) ctx.getBean("gapOrderItemService");
                gapOrderService= (GapOrderService) ctx.getBean("gapOrderService");
                
        }

        protected void tearDown() throws Exception {
                super.tearDown();
                SessionHolder holder = (SessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
                Session s = holder.getSession();
                s.flush();
                TransactionSynchronizationManager.unbindResource(sessionFactory);
                SessionFactoryUtils.closeSession(s);
                
                userService = null;
                userGroupService = null;
                menuService = null;
                permissionService = null;
                districtService = null;
                customerService = null;
                subDistrictService = null;
                productService = null;
                productTypeService = null;
                provinceService = null;
                carService = null;
                stockService = null;
                priceService = null;
                priceHistoryService= null;
                constantService = null;
                ordersService= null;
                ordersProductService=null;
                runningService=null;
                transportService=null;
                transportProductService=null;
                orderMenuService=null;
                gapItemService=null;
                gapOrderItemService=null;
                gapOrderService=null;
                gapCustomerService=null;
        }
        
        
        public void testAll() throws Exception {
            try{
        		
            	/*List<OrderMenu> result = orderMenuService.getAllItems();
            	System.out.println(result.size());*/
            	/*List<GapItem> result = gapItemService.getAllItems();
            	System.out.println(result.size());
            	 for(int i = 0; i < result.size(); i++) {
                     System.out.println(result.get(i).getType());
                 }*/
/*            	List<GapOrder> result = gapOrderService.getAllItems();
            	System.out.println(result.size());
            	for(int i = 0; i < result.size(); i++) {
                    System.out.println(result.get(i).getId());
            	}
            	*/
            	List<GapCustomer> result = gapCustomerService.getLogin("a", "a");
            	System.out.println(result.size());
            	/*List<GapOrderItem> result = gapOrderItemService.getAllItems();
            	System.out.println(result.size());
            	for(int i = 0; i < result.size(); i++) {
                    System.out.println(result.get(i).getStatus());
                  

            	}*/
            	
            }catch (Exception dae) {
               LogHelper.doError(dae.getMessage(), dae);
            }
        }
}

