<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>
<script defer
	src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<%@ include file="../inc_menu.jsp"%>

<script>
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

<script language="javascript" type="text/javascript">
/* 	function doAdd() {
		document.stockForm.mode.value = 'stockAdd';
		document.stockForm.id.value = id;
		document.stockForm.submit();
	} */

	function doSearch() {
		document.stockForm.mode.value = 'stockSearch';
		document.stockForm.submit();
	}

	function doRefresh() {
		location.href = 'stock.htm?mode=index';
	}

	function doEdit(id) {
		document.stockForm.mode.value = 'stockEdit';
		document.stockForm.id.value = id;
		document.stockForm.submit(); 
	}
	
	function doDelete(id) {
		document.stockForm.mode.value = 'stockDel';
		document.stockForm.id.value = id;
		document.stockForm.submit(); 
	}
	
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					��¡����������� <i class="fas fa-utensils"></i><span
						class="navigator"> [ST01]</span>
				</h3>
				<span class="navigator">���������</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/stock" styleId="stockForm"
					styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"> �����������:</label>
						<div class="col-sm-3">
							<html:select property="subType" styleId="subType"
								styleClass="form-control">
								<html:option value="0">������</html:option>
								<html:option value="1">���������</html:option>
								<html:option value="2">����ͧ����</html:option>
								<html:option value="3">����÷ҹ���</html:option>
							</html:select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">��������:</label>
						<div class="col-sm-3">
							<html:text property="name" styleId="name"
								styleClass="form-control"></html:text>
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<button type="button" class="btn btn-info" onclick="doSearch();">
								����</button>
							<button type="button" class="btn btn-warning"
								onclick="location.href='stock.htm?mode=stockAdd'">�������������</button>
							<button type="button" class="btn btn-grey lighten-3"
								onclick="doRefresh();">���������</button>
						</div>
					</div>
				</html:form>
			</div>
		</div>


		<table width="99%" cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered" id="result">
			<thead>
				<tr class="teal lighten-4">
					<th width="5%"><div align="center">�ӴѺ</div></th>
					<th width="35%">���������</th>
					<th width="15%"><div align="right">�Ҥ�</div></th>
					<th width="15%"><div align="right">�ٻ</div></th>
					<th width="15%"><div align="center">ʶҹ�</div></th>
					</th>
					<th width="15%"><div align="center">���</div></th>
					</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr>

						<td align="center">${row.index + 1 }</td>
						<td align="left">${item.type}</td>
						<td align="right">${item.price} �</td>
						<td align="right">${item.pic}</td>

						<td align="center"><c:if test="${item.sta eq '1'}">
								<p style="color: green;">�������˹���</p>
							</c:if> <c:if test="${item.sta eq '0'}">
								<p style="color: red;">�ͧ���</p>
							</c:if></td>

						<td align="center">
							<button class="btn btn-warning btn-sm" data-toggle="tooltip"
								title="���" data-placement="top" onclick="doEdit(${item.id})">
								<i class="fas fa-edit"></i>
							</button>
							<button class="btn btn-danger btn-sm" data-toggle="tooltip"
								title="ź" data-placement="top" onclick="doDelete(${item.id})">
								<i class="fas fa-trash-alt"></i>
							</button>
						</td>
					</tr>
				</c:forEach>

			</tbody>
		</table>

		<br> <br>
	</div>
</body>
</html>