<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
function doAdd(){
	document.transportForm.mode.value = 'transportAdd';
    document.transportForm.submit();    
}
function doSearch() {
  	document.transportForm.mode.value = 'transportSearch';
    document.transportForm.submit();    
}
function doEdit(id) {
	document.transportForm.mode.value = 'transportEdit';
	document.transportForm.id.value = id;
	document.transportForm.submit();
}
function doDetail(id) {
	document.transportForm.mode.value = 'transportDetail';
	document.transportForm.id.value = id;
	document.transportForm.submit();
}
function doDelete(id) {
	bootbox.dialog({
		size : "small",
		message : "�س��ͧ���ź��������¡�ù�����������?",
		title : "<span class='fred'>�׹�ѹ���ź������</span>",
		buttons : {
			ok : {
				label : "��ŧ",
				className : "btn-danger",
				callback : function() {
					
					document.transportForm.mode.value = 'transportDel';
					document.transportForm.id.value = id;
					document.transportForm.submit();
				}
			},
			calcel : {
				label : "¡��ԡ",
				className : "btn-default",
				callback : function() {
				}
			}
		}
	});
}
function doTransport(id) {
	bootbox.dialog({
		size : "small",
		message : "�س��ͧ���¡��ԡ��������¡�ù�����������?",
		title : "<span class='fred'>�׹�ѹ���ź������</span>",
		buttons : {
			ok : {
				label : "��ŧ",
				className : "btn-danger",
				callback : function() {
					document.transportForm.mode.value = 'transport';
					document.transportForm.id.value = id;
					document.transportForm.submit();
				}
			},
			calcel : {
				label : "¡��ԡ",
				className : "btn-default",
				callback : function() {
				}
			}
		}
	});
}
function doPrintPdf(id) {
	window.open("${pageContext.request.contextPath}/transport.htm?mode=printPricePdf&id="+id,"popwin","menubar=0,status=no");
}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">��¡�èѴ���Թ��� (Delivery) <span class="navigator">[TR01]</span></h3>
				<span class="navigator" >�Ѵ���Թ���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      		<html:form action="/transport" styleId="transportForm" styleClass="form-horizontal">
	      		<html:hidden property="mode"/>
	      		<html:hidden property="id"/>
				<div class="form-group">
					<label class="col-sm-2 control-label"> ������ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="sdate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
					<label class="col-sm-2 control-label"> �֧�ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="edate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
				</div>					
				<div class="form-group">
					<label class="col-sm-2 control-label"> �����١���:</label>
					<div class="col-sm-3">
						<html:text property="cusName" styleClass="form-control"></html:text>
					</div>
					<label class="col-sm-2 control-label"> ��ѡ�ҹ���:</label>
					<div class="col-sm-3">
					<html:select property="saleId" styleId="saleId" styleClass="form-control"  >
							<html:option value="">������</html:option>
							<html:optionsCollection property="comboSaleList" value="id" label="fullname"/>
						</html:select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">�Ţ������觫���:</label>
					<div class="col-sm-3">
						<html:text property="orderCode" styleClass="form-control"></html:text>
					</div>
					
					<label class="col-sm-2 control-label">ʶҹШѴ��:</label>
					<div class="col-sm-3">
					<html:select property="status" styleClass="form-control">
								<html:option value="">������</html:option>
								<html:option value="0">�ѧ�����Ѵ��</html:option>
								<html:option value="1">�Ѵ������</html:option>
								<html:option value="2">¡��ԡ��èѴ��</html:option>
							</html:select>
					</div>
					
				</div>				
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="button" class="btn btn-info"  onclick="doSearch();" >����</button>
						<button type="button" class="btn btn-default"  onclick="location.href='transport.htm?mode=index'" data-toggle="tooltip" data-placement="bottom" title="��������ҧ�����ŷ���͡">���������</button>
						<button type="button" class="btn btn-success"  onclick="doAdd();">�Դ���觫���</button>
					</div>
				</div>
	     	</div>
 		</div>     
 		<c:if test="${fn:length(resultList) > 0}">  
 		<div class="table-responsive">               
			<table width="100%" class="table table-striped table-bordered table-hover" id="dataTable" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="5%"><div align="center">�ӴѺ</div></th>
						<th width="10%"><div align="center">�ѹ���Ѵ��</div></th>
						<th width="8%"><div align="center">�Ţ������觫���</div></th>
						<th width="8%"><div align="center">�Ţ���㺨Ѵ��</div></th>
						<th width="20%">�١���</th>
						<th width="10%">��ѡ�ҹ���</th>
						<th width="8%"><div align="right">�ӹǹ (�ѹ)</div></th>
						<th width="12%"><div align="right">�ʹ�Թ (�ҷ)</div></th>
						<th width="5%"></th>	
						<th width="8%"></th>					
						<th width="8%"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr >
						<td align="center">${row.index + 1}</td>
						<td align="center">${item.dateShow }</td>
					    <td align="center">${item.orderCode }</td>
					    <td align="center">${item.code }</td>
					 	<td>${item.customerName }</td>
					 	<td>${item.saleName }</td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.transportUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.transportPrice}" /></td>
					 	<td align="center"> 
					 		<c:if test="${item.status == 1 }">	
								<span class="grey-text"><b>��������</b></span>
							</c:if>		
					 		<c:if test="${item.status == 2 }">	
								<button type="button" class="btn btn-success btn-xs" onclick="doTransport('${item.id}');">�Ѵ��</button>
							</c:if>	
							<c:if test="${item.status == 3 }">	
								<span class="green-text"><b>�Ѵ��</b></span>
							</c:if>							
					 	</td>
					 	<td align="center"> 				  		
							<button type="button"  class="btn btn-primary btn-xs"  onclick="doDetail('${item.id}');"><i class="fa fa-search"></i></button>
							<button type="button" class="btn btn-warning btn-xs" onclick="doPrintPdf(${item.id});"><i class="fa fa-print"></i></button>
						</td>
					  	<td align="center">
					  		<button type="button" class="btn btn-info btn-xs" onclick="doEdit('${item.id}')"><i class="fa fa-pencil"></i></button>
							<button type="button" class="btn btn-danger btn-xs" onclick="doDelete('${item.id}');"><i class="fa fa-trash-o"></i></button>
					  	</td>
					</tr>
					</c:forEach>							
				</tbody>
			</table>
			
			</div>
			</c:if>
			</html:form>
			<br><br>
	</div>    
</body>
</html>