<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">

function doSearch() {
	document.reportForm.mode.value = 'reportMonthlySalesSearch';
    document.reportForm.submit();    
}
function doExport() {
	document.reportForm.mode.value = 'exportExcelReport1';
    document.reportForm.submit();    
}


function doRefresh() {
	location.href='report.htm?mode=reportMonthlySales';
}

</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">��§ҹ�ʹ��»�Ш��ѹ  <span class="navigator">[RP03]</span></h3>
				<span class="navigator" >��§ҹ > ��§ҹ�ʹ��»�Ш��ѹ</span><br><br>
			</div>       
		</div>
		
 	
 		<div class="table-responsive">                     
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="5%"><div align="center">�ӴѺ</div></th>
						<th width="12%"><div align="center">�ѹ���</div></th>
						
						<th width="20%">�١���</th>
						<th width="10%">��ѡ�ҹ���</th>
						<th width="12%"><div align="right">�ʹ��� (�ѹ)</div></th>	
						<th width="12%"><div align="right">�ʹ��� (�ҷ)</div></th>			
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr >
						<td align="center">${row.index + 1}</td>
						<td align="center">${item.dateShow }</td>
						
					    <td>${item.custName }</td>
					 	<td>${item.saleName }</td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalPrice}" /></td>
					</tr>
					</c:forEach>	
				
					<tr>
						<td align="right" colspan="5"><label>���������</label></td>						
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalPrice}" /></td>
					</tr>	
				</tbody>
			</table>
		</div>

  
</body>
</html>