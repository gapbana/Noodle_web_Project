package com.jss.service.impl;

import java.io.Serializable;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.ProvinceDao;
import com.jss.entity.Province;
import com.jss.service.ProvinceService;

public class ProvinceServiceImpl extends CoreServiceImpl<Province, Serializable>implements ProvinceService {

	private ProvinceDao provinceDao;
	public ProvinceServiceImpl(ProvinceDao provinceDao) {
		super(provinceDao);
		this.provinceDao = provinceDao;
	}
	
}
