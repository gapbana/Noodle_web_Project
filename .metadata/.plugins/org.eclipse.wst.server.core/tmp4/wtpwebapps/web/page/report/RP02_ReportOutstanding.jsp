<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script type="text/javascript">
function doExport() {
	location.href='report.htm?mode=exportExcelReport2';   
}

</script>
</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">��§ҹ��ҧ���Թ��� <span class="navigator">[RP02]</span></h3>
				
			</div>       
		</div>    
			<div class="row">
			<div class="col-lg-6">
				<span class="navigator" >��§ҹ > ��§ҹ��ҧ���Թ���</span>		
			</div>
	     	<div class="col-lg-6 text-right">     	
	     		<button type="button" class="btn btn-primary btn-sm" onclick="doExport();">�������§ҹ</button>
	        </div>   
		</div>   
		<br>	
		<div class="row">
			<div class="col-lg-12">           
			<div class="table-responsive">                     
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="5%"><div align="center">�ӴѺ</div></th>
						<th width="10%"><div align="center">�ѹ���</div></th>
						<th width="10%"><div align="center">�Ţ������觫���</div></th>
						<th width="20%">�١���</th>
						<th width="15%">��ѡ�ҹ���</th>
						<th width="15%"><div align="right">�ӹǹ����Թ���(�ѹ)</div></th>		
						<th width="15%"><div align="right">�ӹǹ����Թ���(�ҷ)</div></th>	
						<th width="15%"><div align="right">�ӹǹ���Թ���(�ѹ)</div></th>		
						<th width="15%"><div align="right">�ӹǹ���Թ���(�ҷ)</div></th>
						<th width="15%"><div align="right">��ҧ�� (�ѹ)</div></th>					
						<th width="15%"><div align="right">��ҧ�� (�ҷ)</div></th>			
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr >
						<td align="center">${row.index + 1}</td>
						<td align="center">${item.dateShow }</td>
						<td align="center">${item.code }</td>
					   	<td>${item.custName }</td>
					 	<td>${item.saleName }</td>
					 	<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalUnit}" /></td>
					 	<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalPrice}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.tUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.tPrice}" /></td>
					 	<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalUnit - item.tUnit}" /></td>
					 	<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalPrice - item.tPrice}" /></td>
					</tr>
					</c:forEach>
					<tr >
						<td align="center" colspan="9"><label>���������</label></td>
					 	<td align="right" class="red-text"><label><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalUnit}" /></labe></td>
					 	<td align="right" class="red-text"><label><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalPrice}" /></labe></td>
					</tr>				
				</tbody>
			</table>
			</div>
			</div>
			</div>
	</div>    
</body>
</html>