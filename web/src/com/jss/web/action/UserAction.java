package com.jss.web.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.edu.util.LogHelper;
import com.jss.entity.Permission;
import com.jss.entity.ProductType;
import com.jss.entity.User;
import com.jss.entity.Menu;
import com.jss.entity.UserGroup;
import com.jss.service.MenuService;
import com.jss.service.PermissionService;
import com.jss.service.UserGroupService;
import com.jss.service.UserService;
import com.jss.web.common.OptionDTO;

public class UserAction extends CoreAction {
	public UserService userService;
	public UserGroupService userGroupService;
	public MenuService menuService;
	public PermissionService permissionService;

	public ActionForward userGroup(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<UserGroup> resultList = userGroupService.getGroupList(null);
			request.setAttribute("resultList", resultList);
			dynaForm.set("id", "");
			dynaForm.set("groupName", "");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("US01");
	}

	public ActionForward userGroupEdit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<UserGroup> resultList = userGroupService.getGroupList(null);
			request.setAttribute("resultList", resultList);
			UserGroup entity = userGroupService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("groupName", entity.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("US01");
	}

	public ActionForward userGroupSave(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			
			UserGroup entity;
			if (dynaForm.getString("id") != null
					&& !dynaForm.getString("id").equals("")) {
				entity = userGroupService.getItem(Integer.parseInt(dynaForm
						.getString("id")));
				entity.setName(dynaForm.getString("groupName"));
			/*	entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				userGroupService.updateItem(entity);
			} else {
				entity = new UserGroup();
				entity.setName(dynaForm.getString("groupName"));
		/*		entity.setCreateBy(getUserSession(request).getFullname());
				entity.setCreateDate(DateUtil.getCurrentDate());*/
				userGroupService.saveItem(entity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userGroup(mapping, dynaForm, request, response);
	}

	public ActionForward userGroupDel(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			userGroupService.removeItem(Integer.parseInt(dynaForm.getString("id")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userGroup(mapping, dynaForm, request, response);
	}

	public ActionForward permit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<Permission> checkList = permissionService.getPermissionByGroup(dynaForm.getString("id"));
			List<Menu> menuList = menuService.getAllMenu();
			String[] checkSearch = new String[menuList.size()];
			String[] checkAdd = new String[menuList.size()];
			String[] checkEdit = new String[menuList.size()];
			String[] checkDelete = new String[menuList.size()];
			String[] checkPrint = new String[menuList.size()];
			String[] checkApprove = new String[menuList.size()];

			if (checkList.size() > 0) {
				HashMap<String, Permission> map = new HashMap<String, Permission>();
				for (Permission permit : checkList) {
					String key = permit.getMenu().getId() + "";
					map.put(key, permit);
				}

				List<Menu> nMenuList = new ArrayList<Menu>();
				int i = 0;
				for (Menu menu : menuList) {
					String key = menu.getId() + "";

					if (map.containsKey(key)) {
						Permission group = map.get(key);
						menu.setPermission(group);

						checkSearch[i] = group.getSearch().equals("Y") ? menu.getId() + "" : "";
						checkAdd[i] = group.getAdd().equals("Y") ? menu.getId() + "" : "";
						checkEdit[i] = group.getEdit().equals("Y") ? menu.getId() + "" : "";
						checkDelete[i] = group.getDelete().equals("Y") ? menu.getId() + "" : "";
						checkPrint[i] = group.getPrint().equals("Y") ? menu.getId() + "" : "";
						checkApprove[i] = group.getApprove().equals("Y") ? menu.getId() + "" : "";
					} else {
						checkSearch[i] = "";
						checkAdd[i] = "";
						checkEdit[i] = "";
						checkDelete[i] = "";
						checkPrint[i] = "";
						checkApprove[i] = "";
					}

					nMenuList.add(menu);

					i++;
				}

				request.setAttribute("menuList", nMenuList);

			} else {

				int i = 0;
				for (Menu menu : menuList) {

					checkSearch[i] = "";
					checkAdd[i] = "";
					checkEdit[i] = "";
					checkDelete[i] = "";
					checkPrint[i] = "";
					checkApprove[i] = "";

					i++;
				}

				request.setAttribute("menuList", menuList);
			}

			dynaForm.set("checkSearch", checkSearch);
			dynaForm.set("checkAdd", checkAdd);
			dynaForm.set("checkEdit", checkEdit);
			dynaForm.set("checkDelete", checkDelete);
			dynaForm.set("checkPrint", checkPrint);
			dynaForm.set("checkApprove", checkApprove);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US02");
	}
	
	public ActionForward permitSave(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			DynaActionForm dynaForm = (DynaActionForm) form;

			String id = dynaForm.getString("id");

			String[] checkSearch = dynaForm.getString("hidSearch") != null && !dynaForm.getString("hidSearch").equals("") ? dynaForm.getString("hidSearch").split(",") : null;
			String[] checkAdd = dynaForm.getString("hidAdd") != null && !dynaForm.getString("hidAdd").equals("") ? dynaForm.getString("hidAdd").split(",") : null;
			String[] checkEdit = dynaForm.getString("hidEdit") != null && !dynaForm.getString("hidEdit").equals("") ? dynaForm.getString("hidEdit").split(",") : null;
			String[] checkDelete = dynaForm.getString("hidDelete") != null && !dynaForm.getString("hidDelete").equals("") ? dynaForm.getString("hidDelete").split(",") : null;
			String[] checkPrint = dynaForm.getString("hidPrint") != null && !dynaForm.getString("hidPrint").equals("") ? dynaForm.getString("hidPrint").split(",") : null;
			String[] checkApprove = dynaForm.getString("hidApprove") != null && !dynaForm.getString("hidApprove").equals("") ? dynaForm.getString("hidApprove").split(",") : null;

			List<Menu> menuList = menuService.getAllMenu();
			HashMap<String, Permission> map = new HashMap<String, Permission>();
			for (Menu menu : menuList) {
				Permission entity = new Permission();
				entity.setMenu(menu);
				entity.setUserGroup(userGroupService.getItem(Integer.parseInt(id)));
				entity.setSearch("N");
				entity.setAdd("N");
				entity.setEdit("N");
				entity.setDelete("N");
				entity.setPrint("N");
				entity.setApprove("N");
			/*	entity.setCreateBy(getUserSession(request).getFullname());
				entity.setCreateDate(DateUtil.getCurrentDate());
				entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());
*/
				entity.setCheck(false);

				map.put(menu.getId() + "", entity);
			}

			if (checkSearch != null && checkSearch.length > 0) {
				for (int i = 0; i < checkSearch.length; i++) {
					if (map.containsKey(checkSearch[i])) {
						Permission entity = map.get(checkSearch[i]);
						entity.setSearch("Y");
						entity.setCheck(true);
						map.put(checkSearch[i], entity);
					}
				}
			}

			if (checkAdd != null && checkAdd.length > 0) {
				for (int i = 0; i < checkAdd.length; i++) {
					if (map.containsKey(checkAdd[i])) {
						Permission entity = map.get(checkAdd[i]);
						entity.setAdd("Y");
						entity.setCheck(true);
						map.put(checkAdd[i], entity);
					}
				}
			}

			if (checkEdit != null && checkEdit.length > 0) {
				for (int i = 0; i < checkEdit.length; i++) {
					if (map.containsKey(checkEdit[i])) {
						Permission entity = map.get(checkEdit[i]);
						entity.setEdit("Y");
						entity.setCheck(true);
						map.put(checkEdit[i], entity);
					}
				}
			}

			if (checkDelete != null && checkDelete.length > 0) {
				for (int i = 0; i < checkDelete.length; i++) {
					if (map.containsKey(checkDelete[i])) {
						Permission entity = map.get(checkDelete[i]);
						entity.setDelete("Y");
						entity.setCheck(true);
						map.put(checkDelete[i], entity);
					}
				}
			}

			if (checkPrint != null && checkPrint.length > 0) {
				for (int i = 0; i < checkPrint.length; i++) {
					if (map.containsKey(checkPrint[i])) {
						Permission entity = map.get(checkPrint[i]);
						entity.setPrint("Y");
						entity.setCheck(true);
						map.put(checkPrint[i], entity);
					}
				}
			}

			if (checkApprove != null && checkApprove.length > 0) {
				for (int i = 0; i < checkApprove.length; i++) {
					if (map.containsKey(checkApprove[i])) {
						Permission entity = map.get(checkApprove[i]);
						entity.setApprove("Y");
						entity.setCheck(true);
						map.put(checkApprove[i], entity);
					}
				}
			}

			
			List<Permission> entityList = new ArrayList<Permission>(map.values());
			permissionService.savePermission(entityList, id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return userGroup(mapping, form, request, response);
	}
	
	public ActionForward user(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", null);
			dynaForm.set("groupName", "");
			dynaForm.set("fullname", "");
			dynaForm.set("status", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US03");
	}

	public ActionForward userSearch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<User> resultList = userService.getName(
					dynaForm.getString("fullname"),
					dynaForm.getString("userGroupName"),
					dynaForm.getString("status"));// ออกหน้าจอ ต้องปรับเสมอ
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US03");
	}

	public ActionForward userAdd(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			dynaForm.initialize(mapping);
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", null);
			dynaForm.set("groupName", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US04");
	}

	public ActionForward userEdit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			User entity = userService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("userGroup", String.valueOf(entity.getUserGroup().getId()));
			dynaForm.set("fname", entity.getFirstname());
			dynaForm.set("lname", entity.getLastname());
			dynaForm.set("nname", entity.getNickname());
			dynaForm.set("username", entity.getUsername());
			dynaForm.set("password", entity.getPassword());
			dynaForm.set("passwordConf", entity.getPassword());
			dynaForm.set("status", entity.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US04");
	}

	public ActionForward userDelete(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			userService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			List<User> resultList = userService.getName(
					dynaForm.getString("fullname"),
					dynaForm.getString("userGroupName"),
					dynaForm.getString("status"));// ออกหน้าจอ ต้องปรับเสมอ
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US03");
	}

	public ActionForward userSave(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			User entity;
			if (dynaForm.getString("id") != null
					&& !dynaForm.getString("id").equals("")) {
				entity = userService.getItem(Integer.parseInt(dynaForm
						.getString("id")));
				entity.setUserGroup(userGroupService.getItem(Integer
						.parseInt(dynaForm.getString("userGroup"))));
				entity.setStatus(dynaForm.getString("status"));
				entity.setFirstname(dynaForm.getString("fname"));
				entity.setLastname(dynaForm.getString("lname"));
				entity.setNickname(dynaForm.getString("nname"));
				entity.setUsername(dynaForm.getString("username"));
				entity.setPassword(dynaForm.getString("password"));
				entity.setPassword(dynaForm.getString("passwordConf"));
		/*		entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				userService.updateItem(entity);
			} else {
				entity = new User();
				entity.setUserGroup(userGroupService.getItem(Integer
						.parseInt(dynaForm.getString("userGroup"))));
				entity.setStatus(dynaForm.getString("status"));
				entity.setFirstname(dynaForm.getString("fname"));
				entity.setLastname(dynaForm.getString("lname"));
				entity.setNickname(dynaForm.getString("nname"));
				entity.setUsername(dynaForm.getString("username"));
				entity.setPassword(dynaForm.getString("password"));
				entity.setPassword(dynaForm.getString("passwordConf"));
			/*	entity.setCreateBy(getUserSession(request).getFullname());
				entity.setCreateDate(DateUtil.getCurrentDate());*/

				userService.saveItem(entity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return user(mapping, dynaForm, request, response);
	}

	public ActionForward menu(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			//List<ProductType> result = productTypeService.getAllProductType();
			dynaForm.set("comboMenuLevel", comboSeq());
			
			/*dynaForm.set("comboMenuLevel", comboSeq());*/
			List<Menu> resultList = menuService.getAllItems();
			request.setAttribute("resultList", resultList);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("US05");
	}

	public ActionForward menuEdit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<Menu> resultList = menuService.getAllItems();
			request.setAttribute("resultList", resultList);
			Menu entity = menuService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("menuCode", entity.getCode());
			dynaForm.set("menuName", entity.getName());
			dynaForm.set("menuLink", entity.getLink());
			dynaForm.set("menuIcon", entity.getIcon());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US05");
	}

	public ActionForward menuDel(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			menuService.removeItem(Integer.parseInt(dynaForm.getString("id")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return menu(mapping, dynaForm, request, response);
	}

	public ActionForward menuSave(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		Menu entity;
		try{
		if (dynaForm.getString("id") != null
				&& !dynaForm.getString("id").equals("")) {
			entity = menuService.getItem(Integer.parseInt(dynaForm
					.getString("id")));

			entity.setCode(dynaForm.getString("menuCode"));
			entity.setName(dynaForm.getString("menuName"));
			entity.setLink(dynaForm.getString("menuLink"));
			entity.setIcon(dynaForm.getString("menuIcon"));
	/*		entity.setUpdateBy("Admin");
			entity.setUpdateDate(DateUtil.getCurrentDate());*/
			menuService.updateItem(entity);

		} else {
			entity = new Menu();
			entity.setCode(dynaForm.getString("menuCode"));
			entity.setName(dynaForm.getString("menuName"));
			entity.setLink(dynaForm.getString("menuLink"));
			entity.setIcon(dynaForm.getString("menuIcon"));
		/*	entity.setCreateBy("Admin");
			entity.setCreateDate(DateUtil.getCurrentDate());*/
			menuService.saveItem(entity);

		}
		
			} catch (Exception e) {
				e.printStackTrace();
			}

		return menu(mapping, dynaForm, request, response);

	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UserGroupService getUserGroupService() {
		return userGroupService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

}
