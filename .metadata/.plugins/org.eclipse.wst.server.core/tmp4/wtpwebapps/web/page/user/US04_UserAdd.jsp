<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp"%>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#userForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},

			fields : {
				userGroupName: {
					validators : {
						notEmpty : {
							message : '��س����͡: ����������ҹ'
						}
					}
				},
				fname : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ͼ����ҹ�к�'
						}
					}
				},
				lname : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ʡ�ż����ҹ�к�'
						}
					}
				},
				nname : {
					validators : {
						notEmpty : {
							message : '��س��к�: ������蹼����ҹ�к�'
						}
					}
				},
				username : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ͼ����ҹ�к�'
						},
			            stringLength: {
			                min: 6,
			                max: 32,
			                message: '���ͼ����ҹ ��ͧ�����¡��� 6 ����ѡ�� �������Թ 32 ����ѡ��',							
			            },
			            regexp: {
	                        regexp: /^[a-zA-Z0-9]+$/,
	                        message: '���ͼ����ҹ ��ͧ�繵���ѡ�������ѧ�����е���Ţ ��ҹ��'
	                    }
					}
				},
				password : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ʼ�ҹ'
						},
		                stringLength: {
		                    min: 6,
		                    max: 20,
		                    message: '�к�: ���ʼ�ҹ ��ͧ�����¡��� 6 ����ѡ�� �������Թ 32 ����ѡ��'
		                }
					}
				},
				passwordConf : {
					validators : {
						notEmpty : {
							message : '��س��к�: �׹�ѹ���ʼ�ҹ'
						}, 
	                    identical: {
	                        field: 'password',
	                        message: '�׹�ѹ���ʼ�ҹ���١��ͧ'
	                    }
					}
				},
				status : {
					validators : {
						notEmpty : {
							message : '��س����͡: ʶҹ�'
						}
					}
				},
				

			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			$("button[name='btnSave']").attr('disabled', true);

			document.userForm.mode.value = 'userSave';
			document.userForm.submit();
		});
	});
</script>

<script language="javascript" type="text/javascript">
	function submitFormInit(mode) {
		document.nationForm.mode.value = mode;
		document.nationForm.edit.value = '';
		document.nationForm.submit();
	}

	function submitFormAdd(mode) {
		if ($("#accountForm").valid()) {
			document.nationForm.mode.value = mode;
			document.nationForm.submit();
		}
	}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�ѹ�֡/��䢢����ż����ҹ�к� <span class="navigator">[US04]</span></h3>
				<span class="navigator">�Է�������ҹ > �����ż����ҹ�к� > �ѹ�֡/��䢢����ż����ҹ�к�</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/user" styleId="userForm" styleClass="form-horizontal" method="post">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span> ����������ҹ:</label>
						<div class="col-sm-3">
							<html:select property="userGroup" styleClass="form-control">
								<html:option value="">������</html:option>
								<html:optionsCollection property="comboUserGroup" value="id" label="name" />
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>����:</label>
						<div class="col-sm-3">
							<html:text property="fname" styleClass="form-control" styleId="fname"></html:text>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>���ʡ��:</label>
						<div class="col-sm-3">
							<html:text property="lname" styleClass="form-control" styleId="lname"></html:text>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>�������:</label>
						<div class="col-sm-3">
							<html:text property="nname" styleClass="form-control" styleId="nname"></html:text>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>���ͼ����ҹ:</label>
						<div class="col-sm-3">
							<html:text property="username" styleClass="form-control" styleId="username"></html:text>
						</div>						
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>���ʼ�ҹ:</label>
						<div class="col-sm-3">
							<html:password property="password" styleClass="form-control" styleId="password"></html:password>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>�׹�ѹ���ʼ�ҹ:</label>
						<div class="col-sm-3">
							<html:password property="passwordConf" styleClass="form-control" styleId="passwordConf"></html:password>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span> ʶҹС����ҹ:</label>
						<div class="col-sm-2">
							<html:select property="status" styleClass="form-control">
								<html:option value="">������</html:option>
								<html:option value="1">��ҹ</html:option>
								<html:option value="0">�Դ�����ҹ</html:option>
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3 col-sm-offset-2">
							<button type="submit" class="btn btn-success" >�ѹ�֡</button>
							<button type="button" class="btn btn-danger" onclick="location.href='user.htm?mode=user'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
			</div>
		</div>
</body>