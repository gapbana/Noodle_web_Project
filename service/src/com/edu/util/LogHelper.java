package com.edu.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * <p>
 * Title: LogHelper class
 * </p>
 * 
 * <p>
 * Description: <br>
 * This is a Helper class , to help developer shorten their code of logging.
 * <br>
 * This class has 2 categories of log methods , first is for call from static
 * method that is cannot access the instance of thier class.<br>
 * The second is for call from any instance.<br>
 * Caller just call LogHelper any doLog method and send its instance (this) as a
 * source argument with another argument, such as message or throwable object.
 * <br>
 * Caller must check for isInfoEnabled ... before call log. Because in some case
 * it take time to prepare the message.
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 * 
 * 
 */
public class LogHelper {
	private static Log logger = LogFactory.getLog(LogHelper.class);

	private LogHelper() {
	}

	public static Log getLog(Object source) {
		return LogFactory.getLog(source.getClass());
	}

	/**
	 * static information method for comfortabiliy , in case of caller cannot
	 * access its instance<br>
	 * It is helpful if caller prepend its class name and method name with the
	 * message.
	 * 
	 * @param msg
	 *            String
	 */
	public static void doInfo(String msg) {
		// example block of code , do not delete these.
		// if (logger.isInfoEnabled()){ do not check because it not take time
		// here
		logger.info(msg);
		// }

	}

	/**
	 * static Information method that receive caller instance as source , and
	 * msg as string to log to appender.
	 * 
	 * @param source
	 *            Object
	 * @param msg
	 *            String
	 */
	public static void doInfo(Object source, String msg) {
		Log log = getLog(source);
		if (log.isInfoEnabled()) {
			log.info(msg);
		}
	}

	/**
	 * static debug method for comfortabiliy , in case of callerin case of
	 * caller cannot access its instance<br>
	 * 
	 * @param msg
	 *            String
	 */
	public static void doDebug(String msg) {
		logger.debug(msg);
	}

	/**
	 * 
	 * @param source
	 *            Object
	 * @param msg
	 *            String
	 */
	public static void doDebug(Object source, String msg) {
		Log log = getLog(source);

		log.debug(msg);
	}

	public static void doDebug(Object source, String msg, Throwable t) {
		Log log = getLog(source);
		log.debug(msg, t);
	}

	public static void doError(String msg, Throwable e) {
		logger.error(msg, e);
	}

	public static void doError(Object source, String msg, Throwable e) {
		Log log = getLog(source);
		log.error(msg, e);
	}

	public static void doWarn(String msg) {
		logger.warn(msg);
	}

	public static void doWarn(String msg, Throwable e) {
		logger.warn(msg, e);
	}

	public static void doWarn(Object source, String msg) {
		Log log = getLog(source);
		log.warn(msg);
	}

	public static void doWarn(Object source, String msg, Throwable e) {

		Log log = getLog(source);
		log.warn(msg, e);
	}

	public static void doTrace(String msg) {
		logger.trace(msg);
	}

	public static void doTrace(String msg, Throwable e) {
		logger.trace(msg, e);
	}

	public static void doTrace(Object source, String msg) {
		Log log = getLog(source);
		log.trace(msg);
	}

	public static void doTrace(Object source, String msg, Throwable e) {
		Log log = getLog(source);
		log.trace(msg, e);
	}

	public static void doFatal(String msg, Throwable e) {
		logger.trace(msg, e);
	}

	public static void doFatal(Object source, String msg) {
		Log log = getLog(source);
		log.trace(msg);
	}

	public static void doFatal(Object source, String msg, Throwable e) {
		Log log = getLog(source);
		log.trace(msg, e);
	}

}
