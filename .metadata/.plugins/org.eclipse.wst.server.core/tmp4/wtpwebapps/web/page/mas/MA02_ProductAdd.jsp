<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>

<%@ include file="../inc_menu.jsp"%>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#masterForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				productTypeId : {
					validators : {
						notEmpty : {
							message : '��س��к�: �������Թ���'
						}
					}
				},
				fname : {
					validators : {
						notEmpty : {
							message : '��س��к�: ����'
						}
					}
				},
				price : {
					validators : {
						notEmpty : {
							message : '��س��к�: �Ҥ�'
						}
					}
				},
				weight : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���˹ѡ ��./���'
						}
					}
				},

				diff : {
					validators : {
						notEmpty : {
							message : '��س��к�: �Ҥ���ǹ��ҧ'
						}
					}
				},
				model : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���'
						}
					}
				},
			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			$("button[name='btnSave']").attr('disabled', true);
			/*  alert(); */
			document.masterForm.mode.value = "productSave";
			document.masterForm.submit();
		});
	});
</script>

<script language="javascript" type="text/javascript">
	function submitFormInit(mode) {
		document.masterForm.mode.value = mode;
		document.masterForm.edit.value = '';
		document.masterForm.submit();
	}

	function submitFormAdd(mode) {
		if ($("#masterForm").valid()) {
			document.masterForm.mode.value = mode;
			document.masterForm.submit();
		}
	}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					�ѹ�֡/��䢢����������<span class="navigator">[MA02]</span>
				</h3>
				<span class="navigator">�����ž�鹰ҹ >
					�ѹ�֡/��䢢����������</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/master" styleId="masterForm"
					styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>�����������:</label>
						<div class="col-sm-3">
							<html:select property="productTypeId" styleClass="form-control">
								<html:option value="0">��س����͡�����������</html:option>
								<html:option value="1">���������</html:option>
								<html:option value="2">����÷ҹ���</html:option>
								<html:option value="3">����ͧ����</html:option>
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>����:</label>
						<div class="col-sm-3">
							<html:text property="fname" styleId="fname"
								styleClass="form-control"></html:text>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>�Ҥ�:</label>
						<div class="col-sm-3">
							<div class="input-group">
								<html:text property="price" styleId="price"
									styleClass="form-control"></html:text>
								<span class="input-group-addon">�ҷ</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>���͡�ٻ�����:</label>
						<div class="col-sm-3">
							<form name="frmUpload" method="post" action="upload.jsp"
								enctype="multipart/form-data">
								<input type="file" name="filUpload">
						</div>
					</div>
					</form>


					<%-- 
					
					///<form name="frmUpload" method="post" action="upload.jsp" enctype="multipart/form-data">
		<input type="file" name="filUpload">
		<input name="btnSubmit" type="submit" value="Submit">
	</form>/////
					
					
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>�������:</label>
						<div class="col-sm-3">						
							<div class="input-group">
								<html:text property="length" styleId="length" styleClass="form-control" ></html:text> 
	                     		<span class="input-group-addon">����</span>
	                   		</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>���˹ѡ ��./���:</label>
						<div class="col-sm-3">						
							<div class="input-group">
								<html:text property="weight" styleId="weight" styleClass="form-control" ></html:text> 
	                     		<span class="input-group-addon">���š���</span>
	                   		</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span>�Ҥ���ǹ��ҧ:</label>
						<div class="col-sm-3">						
							<div class="input-group">
								 <html:text property="diff" styleId="diff" styleClass="form-control" ></html:text> 
	                     		<span class="input-group-addon">�ҷ</span>
	                   		</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-6">
							 <label class="radio-inline">
							 	 <html:radio property="model" value="T"> �ç</html:radio>
						    </label>
						    <label class="radio-inline">
						    	 <html:radio property="model" value="F"> �Ѻ</html:radio>
						    </label>
						</div>
					</div>			
					<div class="form-group">
						<div class="col-sm-3 col-sm-offset-2">	
							<button type="submit" class="btn btn-success"> �ѹ�֡</button>           
		            		<button type="button" class="btn btn-danger" name="btnCancel" value="¡��ԡ" onclick="location.href='master.htm?mode=product'">¡��ԡ</button> 
		       			</div>
		       		</div>
		       		--%>
					<div class="form-group">
						<div class="col-sm-3 col-sm-offset-2">
							<button type="submit" class="btn btn-success">�ѹ�֡</button>
							<button type="button" class="btn btn-danger" name="btnCancel"
								value="¡��ԡ" onclick="location.href='master.htm?mode=product'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
</body>
</html>