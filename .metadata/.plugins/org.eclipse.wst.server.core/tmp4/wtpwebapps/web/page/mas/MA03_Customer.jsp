<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>

<%@ include file="../inc_menu.jsp" %>

<script language="javascript" type="text/javascript">
function doSearch() {
	document.masterForm.mode.value = 'customerSearch';
	document.masterForm.submit();
}
function doEdit(id) {
	document.masterForm.mode.value = 'customerEdit';
	document.masterForm.id.value = id;
	document.masterForm.submit();
}
function doAddCar(id) {
	document.masterForm.mode.value = 'car';
	document.masterForm.custId.value = id;
	document.masterForm.submit();
}
function doDelete(id) {
	bootbox.dialog({
		size : "small",
		message : "�س��ͧ���ź��������¡�ù�����������?",
		title : "<span class='fred'>�׹�ѹ���ź������</span>",
		buttons : {
			ok : {
				label : "��ŧ",
				className : "btn-danger",
				callback : function() {
					document.masterForm.mode.value = 'customerDel';
					document.masterForm.id.value = id;
					document.masterForm.submit();
				}
			},
			calcel : {
				label : "¡��ԡ",
				className : "btn-default",
				callback : function() {
				}
			}
		}
	});
}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�������١��� <span class="navigator">[MA03]</span></h3>
				<span class="navigator" >�����ž�鹰ҹ > �������١���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      		<html:form action="/master" styleId="masterForm" styleClass="form-horizontal">
				<html:hidden property="id"/>
				<html:hidden property="custId"/>
				<html:hidden property="mode"/>
				<div class="form-group">
					<label class="col-sm-2 control-label">�����١���:</label>
					<div class="col-sm-3">
						<html:text property="custNames" styleClass="form-control"></html:text>
					</div>
				</div>
							
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="button" class="btn btn-info" onclick="doSearch();">����</button>
						<button type="button" class="btn btn-default" name="btnRefresh" value="���������" onclick="location.href='master.htm?mode=customer'">���������</button>
						<button type="button" class="btn btn-success" name="btnAdd" value="����������" onclick="location.href='master.htm?mode=customerAdd1'">����������</button>
					</div>
				</div>
	         	</html:form>
	     	</div>
 		</div>      
 			 
			<table width=R"99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="5%"><div align="center">�ӴѺ</div></th>
						<th width="8%"><div align="center">�����١���</div></th>
						<th width="25%">�����١���</th>
						<th width="25%">�������</th>
						<th width="25%"><div align="center">�������Ѿ�� & �������</div></th>
						<th width="12%"> <div align="center">���</div> </th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr>
						<td align="center">${row.index + 1}</td>
						<td align="center">${item.id}</td>
					    <td>
					    <c:if test="${item.prefix eq '0'}">���</c:if> 
					    <c:if test="${item.prefix eq '1'}">�ҧ���</c:if> 
					    <c:if test="${item.prefix eq '3'}">�ҧ</c:if> 
								&nbsp;${item.name}</td>
					 	<td>${item.addr}</td>
					 	<td align="left">Tel. : ${item.mobile} <br>Email : ${item.email}</td>
					  	<td align="center"> 		  		
							<a class="btn btn-info btn-sm"  onclick="doEdit('${item.id}')"><i class="fas fa-pencil-alt"></i></a>
							<a class="btn btn-danger btn-sm" onclick="doDelete('${item.id}')"><i class="fas fa-trash-alt"></i></a>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			 
	</div>    
</body>
</html>