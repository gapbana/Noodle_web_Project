<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Inconsolata">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<%@ include file="../boostrap.jsp"%>
<%@ include file="../inc_menu2.jsp"%>
<%@ include file="../inc_header.jsp"%>


<script language="javascript" type="text/javascript">
function doAdd() {
	/* document.forms[1].mode.value = 'registerSave';
	document.forms[1].submit();  */
	alert("บันทึกเข้าสู่ระบบแล้ว");
}

function doDelete(id) {
	document.orderForm.mode.value = 'orderDel';
	document.orderForm.id.value = id;
	document.orderForm.submit(); 
	
}

function doPay() {
	document.orderForm.mode.value = 'payPage';
	document.orderForm.submit(); 
	
}

function doMenu() {
	document.orderForm.mode.value = 'menuPage';
	document.orderForm.submit(); 
	
}

</script>


<div class="col-lg-12">
	<h3 class="page-header" align="center">
		ตระกร้าสินค้า <i class="fa fa-shopping-basket "></i> <br>
		<br>
	</h3>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1">
			<html:form action="/order" styleId="orderForm"
				styleClass="form-horizontal">
				<html:hidden property="mode" />
				<html:hidden property="id" />
				<table class="table table-hover">
					<thead>
						<tr>
							<th>สินค้า</th>
							<th class="text-center">จำนวน</th>
							<th class="text-center">ราคา</th>
							<th class="text-center">รวม</th>
							<th class="text-center">#</th>
						</tr>

					</thead>
					<tbody>
						<c:forEach items="${resultList}" var="item" varStatus="row">
						<c:if test="${item.gapItem.subType eq '1'}">
							<tr>
								<td class="col-sm-8 col-md-6">
									<div class="media">
										<a class="thumbnail pull-left" href="#"> <img
											class="media-object" src="${item.gapItem.pic}"
											style="width: 120px;">
										</a>
										<div class="media-body">
											<h4 class="media-heading">
												&nbsp;&nbsp;${item.gapItem.type}</a>
											</h4>
											<%-- ชื่ออาหาร --%>
											<h5 class="media-heading">
												&nbsp; เส้น :
												<%-- เส้น --%>
												<c:if test="${item.noodleType eq '1'}">เส้นเล็ก</c:if>
												<c:if test="${item.noodleType eq '2'}">เส้นหมี่</c:if>
												<c:if test="${item.noodleType eq '3'}">เส้นใหญ่</c:if>
												<c:if test="${item.noodleType eq '4'}">เส้นมาม่า</c:if>
												<c:if test="${item.noodleType eq '5'}">วุ้นเส้น</c:if>
												<c:if test="${item.noodleType eq '6'}">เส้นบะหมี่</c:if>
											</h5>
											<span>&nbsp; <c:if test="${item.vet eq '1'}">ผัก : ใส่ผัก</c:if>
												<c:if test="${item.vet eq '0'}">ผัก : ไม่ใส่ผัก</c:if>
											</span> <br> <span>&nbsp; <c:if
													test="${item.extra eq '1'}">ขนาด : พิเศษ</c:if> <c:if
													test="${item.extra eq '0'}">ขนาด : ปกติ</c:if>
											</span>
										</div>
									</div>
								</td>
								<td class="col-sm-1 col-md-1" align="center">${item.qty}</td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.qty*item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1" align="center">
									<button type="button" class="btn btn-danger"
										onclick="doDelete(${item.id})">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</td>
							</tr>
							</c:if>
							
							<c:if test="${item.gapItem.subType eq '2'}">
							<tr>
								<td class="col-sm-8 col-md-6">
									<div class="media">
										<a class="thumbnail pull-left" href="#"> <img
											class="media-object" src="${item.gapItem.pic}"
											style="width: 120px;">
										</a>
										<div class="media-body">
											<h4 class="media-heading">
												&nbsp;&nbsp;${item.gapItem.type}</a>
											</h4>
											<%-- ชื่ออาหาร --%>
											
											<span>&nbsp;&nbsp; <c:if
													test="${item.extra eq '1'}">ใส่น้ำแข็ง</c:if> <c:if
													test="${item.extra eq '0'}">ไม่ใส่น้ำแข็ง</c:if>
											</span>
										</div>
									</div>
								</td>
								<td class="col-sm-1 col-md-1" align="center">${item.qty}</td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.qty*item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1" align="center">
									<button type="button" class="btn btn-danger"
										onclick="doDelete(${item.id})">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</td>
							</tr>
							</c:if>
							
							<c:if test="${item.gapItem.subType eq '3'}">
							<tr>
								<td class="col-sm-8 col-md-6">
									<div class="media">
										<a class="thumbnail pull-left" href="#"> <img
											class="media-object" src="${item.gapItem.pic}"
											style="width: 120px;">
										</a>
										<div class="media-body">
											<h4 class="media-heading">
												&nbsp;&nbsp;${item.gapItem.type}</a>
											</h4>
											<%-- ชื่ออาหาร --%>
											
										
										</div>
									</div>
								</td>
								<td class="col-sm-1 col-md-1" align="center">${item.qty}</td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1 text-center"><strong>${item.qty*item.gapItem.price}</strong></td>
								<td class="col-sm-1 col-md-1" align="center">
									<button type="button" class="btn btn-danger"
										onclick="doDelete(${item.id})">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</td>
							</tr>
							</c:if>
						</c:forEach>

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><h5>รวม</h5></td>
							<td class="text-right"><h5>
									<strong>฿ ${sumTotal}</strong>
								</h5></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><h5>ค่าจัดส่ง</h5></td>
							<td class="text-right"><h5>
									<strong>free</strong>
								</h5></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><h3>รวมทั้งหมด</h3></td>
							<td class="text-right"><h3>
									<strong>฿ ${sumTotal}</strong>
								</h3></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>
								<button type="button" class="btn btn-default" onclick="doMenu()">
									<span class="glyphicon glyphicon-shopping-cart"></span>
									เลือกอาหารต่อ
								</button>
							</td>
							<td>
								<button type="button" class="btn btn-success" onclick="doPay()">
									ชำระเงิน <span class="glyphicon glyphicon-play"></span>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
			</html:form>
		</div>
	</div>
</div>