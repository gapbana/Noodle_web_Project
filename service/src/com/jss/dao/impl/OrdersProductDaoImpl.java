package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.OrdersDao;
import com.jss.dao.OrdersProductDao;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;

public class OrdersProductDaoImpl extends CoreDaoImpl<OrdersProduct, Serializable> implements OrdersProductDao {

	public OrdersProductDaoImpl(Class<OrdersProduct> entityClass) {
		super(entityClass);
	}

	@Override
	public List<OrdersProduct> findProductbyOrders(String ordersId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(OrdersProduct.class);
		criteria.createAlias("orders", "orders");
		criteria.createAlias("product", "product");
		criteria.addOrder(Order.asc("product.code"));	
		criteria.addOrder(Order.asc("product.size"));
		
		if(ordersId != null && !ordersId.equals("")){
			criteria.add(Restrictions.eq("orders.id", Integer.parseInt(ordersId)));
		}

		return getHibernateTemplate().findByCriteria(criteria);
	}

}