package com.jss.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "ma_customer")
public class Customer extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "customer_id") 
	private int id;
	
	@Column(name = "customer_code")
	private String code;
	
	@Column(name = "customer_name")
	private String name;
	
	@Column(name = "customer_shortname")
	private String nameShort;

	@Column(name = "customer_rank")
	private String rank;
	
	@Column(name = "customer_address")
	private String address;
	
	
	@ManyToOne
	@JoinColumn(name = "province_id")
	private Province province;
	
	@ManyToOne
	@JoinColumn(name = "district_id")
	private District district;
	
	@ManyToOne
	@JoinColumn(name = "subdistrict_id")
	private SubDistrict subDistrict;
	

	@Column(name = "zipcode")
	private String zipcode;
	
	@Column(name = "contact_name")
	private String contactName;
	
	@Column(name = "contact_number")
	private String contactNumber;
	
	@Column(name = "office_number")
	private String officeNumber;

	@Column(name = "office_fax")
	private String officeFax;
	
	@Column(name = "office_email")
	private String officeEmail;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Transient
	private String fullAddress;
	
	
	public String getFullAddress() {
		String result = "";
		if(address != null){
			result += address;
		}
		if(subDistrict != null){
			if(province.getId() != 1){
				result += "ตำบล";
			}
			result += subDistrict.getName();
		}
		if (district != null) {
			if(province.getId() != 1){
				result += "อำเภอ";
			}
			result += district.getName();
		}
		if (province != null) {
			result += "จังหวัด"+ province.getName(); 
		}
		if (zipcode != null) {
			result += zipcode; 
		}
		return result;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	
	public Customer() {
	}

	public Customer(int id) {
		this.setId(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameShort() {
		return nameShort;
	}

	public void setNameShort(String nameShort) {
		this.nameShort = nameShort;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public SubDistrict getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(SubDistrict subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public String getOfficeEmail() {
		return officeEmail;
	}

	public void setOfficeEmail(String officeEmail) {
		this.officeEmail = officeEmail;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	
	
}
