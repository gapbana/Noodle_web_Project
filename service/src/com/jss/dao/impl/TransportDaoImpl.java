package com.jss.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.edu.util.DateUtil;
import com.jss.dao.CarDao;
import com.jss.dao.TransportDao;
import com.jss.entity.Car;
import com.jss.entity.Orders;
import com.jss.entity.Transport;

public class TransportDaoImpl extends CoreDaoImpl<Transport, Serializable> implements TransportDao {

	public TransportDaoImpl(Class<Transport> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Transport> findTransportList(String sdate, String edate,String custId, String saleId, String orderCode, String transportCode)throws DataAccessException {
		String sdates = DateUtil.date2StringEn(DateUtil.string2DateTh(sdate),"yyyy-MM-dd");
		String edates = DateUtil.date2StringEn(DateUtil.string2DateTh(edate),"yyyy-MM-dd");
		
		String sql = " SELECT";
		sql += " transport_id,order_date,order_code,transport_code,customer_name,CONCAT(firstname,' ',lastname) AS saleName,transport.status,";
		sql += " (SELECT SUM(unit_weight) FROM transportproduct WHERE transportproduct.transport_id = transport.transport_id) AS weight,";
		sql += " (SELECT SUM(price) FROM transportproduct WHERE transportproduct.transport_id = transport.transport_id) AS price,";
		sql += " (SELECT SUM(unit_weight) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalWeight,";
		sql += " (SELECT SUM(price) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalPrice";
		sql += " FROM transport";
		sql += " LEFT JOIN orders ON transport.orders_id = orders.order_id";
		sql += " LEFT JOIN ma_customer ON ma_customer.customer_id = transport.customer_id";
		sql += " LEFT JOIN ma_user ON ma_user.user_id = transport.sale_id";
		sql += " WHERE transport_id <> 0";
		if(!sdate.equals("") && !edate.equals("")){
			sql += " AND (transport_code BETWEEN '"+sdates+"' AND '"+edates+"')";
		}else if (!sdate.equals("") && edate.equals("")) {
			sql += " AND transport_code >= '"+sdates+"'";
		}else if(sdate.equals("") && !edate.equals("")){
 			sql += " AND transport_code <= '"+edates+"'";
		}
		if(custId != null && !custId.equals(""))
			sql += " AND transport.customer_id = "+Integer.parseInt(custId);
		if(saleId != null && !saleId.equals(""))
			sql += " AND transport.sale_id = "+saleId;
		if(orderCode != null && !orderCode.equals(""))
			sql += " AND order_code LIKE '%"+orderCode+"%'";
		if(transportCode != null && !transportCode.equals(""))
			sql += " AND transport_code ='%"+transportCode+"%'";
		
		sql += " ORDER BY transport_date,transport_code DESC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Transport> results = new ArrayList<Transport>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Transport entity = new Transport();
				Double ordersUnit = 0.0;
				Double orderPrice = 0.0;
				Double transportUnit = 0.0;
				Double transportPrice = 0.0;
				String code = "";
				
				if(obj[3] != null && !obj[3].equals(""))
					code = String.valueOf(obj[3]);
				
				if(obj[7] != null && !obj[7].equals(""))
					transportUnit = Double.parseDouble(String.valueOf(obj[7]));
				
				if(obj[8] != null && !obj[8].equals(""))
					transportPrice = Double.parseDouble(String.valueOf(obj[8]));
				
				if(obj[9] != null && !obj[9].equals(""))
					ordersUnit = Double.parseDouble(String.valueOf(obj[9]));		
				
				if(obj[10] != null && !obj[10].equals(""))
					orderPrice = Double.parseDouble(String.valueOf(obj[10]));
				
				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setDate(DateUtil.string2Date(String.valueOf(obj[1]),"yyyy-MM-dd"));
				entity.setOrderCode(String.valueOf(obj[2]));
				entity.setCode(code);
				entity.setCustomerName(String.valueOf(obj[4]));
				entity.setSaleName(String.valueOf(obj[5]));
				entity.setStatus(String.valueOf(obj[6]));
				entity.setOrdersUnit(ordersUnit);
				entity.setOrdersPrice(orderPrice);
				entity.setTransportUnit(transportUnit);
				entity.setTransportPrice(transportPrice);
				results.add(entity);
			}
		}
	
		return results;
	}

	@Override
	public List<Transport> findTransportCount(String year, String month) throws DataAccessException {
		Integer years = null;
		if(year != null && !year.equals(""))
			years = Integer.parseInt(year) - 543;
		String sql = " SELECT";
		sql += " transport_id,transport_date,transport_code,customer_name,CONCAT(firstname,' ',lastname) AS saleName ,transport.status,";
		sql += " (SELECT SUM(unit_weight) FROM transportproduct WHERE transportproduct.transport_id = transport.transport_id) AS totalWeight,";
		sql += " (SELECT SUM(price) FROM transportproduct WHERE transportproduct.transport_id = transport.transport_id) AS totalPrice";
		sql += " FROM transport";
		sql += " LEFT JOIN ma_customer ON ma_customer.customer_id = transport.customer_id";
		sql += " LEFT JOIN ma_user ON ma_user.user_id = transport.sale_id";
		sql += " WHERE  transport.status > 2";		
		if(!year.equals("") && !month.equals("")){			
			sql += " AND transport_date Like '"+years+"-"+month+"%' ";
		}else if (!year.equals("") && month.equals("")) {
			sql += " AND transport_date Like '"+years+"%'";
		}else if(year.equals("") && !month.equals("")){
 			sql += " AND transport_date Like '%-"+month+"-%'";
		}

		sql += " ORDER BY transport_date,transport_code DESC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Transport> results = new ArrayList<Transport>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Transport entity = new Transport();
				Double unit = 0.0;
				Double ptice = 0.0;
				String orderCode = "";
				
				if(obj[6] != null && !obj[6].equals(""))
					unit = Double.parseDouble(String.valueOf(obj[6]));		
				
				if(obj[7] != null && !obj[7].equals(""))
					ptice = Double.parseDouble(String.valueOf(obj[7]));
				
				if(obj[2] != null && !obj[2].equals(""))
					orderCode = String.valueOf(obj[2]);
				
				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setDate(DateUtil.string2Date(String.valueOf(obj[1]),"yyyy-MM-dd"));
				entity.setCode(orderCode);
				entity.setCustomerName(String.valueOf(obj[3])); 
				entity.setSaleName(String.valueOf(obj[4]));
				entity.setStatus(String.valueOf(obj[5]));
				entity.setTransportUnit(unit);
				entity.setTransportPrice(ptice);
				results.add(entity);
			}
		}
	
		return results;
	}

	

}