<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8"%>
<html>
<title>Home</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body,h1,h5 {font-family: "Raleway", sans-serif}
body, html {height: 100%}
.bgimg {
    background-image: url('./33.jpg');
    min-height: 100%;
    background-position: center;
    background-size: cover;
}
</style>
<body>

<%-- text center --%>
<div class="bgimg w3-display-container w3-text-white">
  <div class="w3-display-middle w3-jumbo" align="center">
    <p><strong>GTT Noodle</strong><br>ยินดีต้อนรับ</p>
  </div>
  
  <%-- menu and contact --%>
  <div class="w3-display-topleft w3-container w3-xlarge" align="left">
  <br>
    <p><button onclick="document.getElementById('menu').style.display='block'" class="w3-button w3-black">เมนู</button></p>
    <p><button onclick="document.getElementById('contact').style.display='block'" class="w3-button w3-black">ติดต่อ</button></p>
  </div>
  
  <%-- cart car --%>
  <div class="w3-display-topright w3-container w3-xlarge" align="right">
  <br>
    <p><button onclick="document.getElementById('menu').style.display='block'" class="w3-button w3-black">ตะกร้าสินค้า</button></p>
  </div>
  
  	<div class="w3-display-bottomleft w3-container">
    <span class="w3-tag">เปิดทุกวัน 9.00 น. - ปิด 21.00 น.</span><br><br>
  
 	 </div>    
  		
  	<div class="w3-display-bottomright w3-container">
 		<span class><div class="w3-right-align"><p><strong>49/104 หมู่ 4 หมู่บ้านฉัตรหลวง13<br>ถ.ปทุมธานี-สามโคก</p></strong></span></div>
  </div>
  <br>
  <br>
  	</div>
  
</div>

<!-- Menu Modal -->
<div id="menu" class="w3-modal">
  <div class="w3-modal-content w3-animate-zoom">
    <div class="w3-container w3-black w3-display-container">
      <span onclick="document.getElementById('menu').style.display='none'" class="w3-button w3-display-topright w3-large">x</span>
      <h1>อาหารทานเล่น</h1>
    </div>
    <div class="w3-container">
      <h4>เกี๊ยวทอด <b><span class="pull-right">฿ 10</span></b></h4>
      <h4>ลูกชิ้นปลานึ่ง <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>ลูกชิ้นทอด <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>แคปหมู <b><span class="pull-right">฿ 10</span></b></h4>
      <h4>ปอเปี๊ยะทอด <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>ขนมถ้วย<b><span class="pull-right">฿ 10</span></b></h4>
      <h4>ไข่ต้มยางมะตูม <b><span class="pull-right">฿ 10</span></b></h4>
      <h4>หมูสะเต๊ะ <b><span class="pull-right">฿ 10</span></b></h4>
     
    </div>
    <div class="w3-container w3-black">
      <h1>ก๋วยเตี๋ยว</h1>
    </div>
    <div class="w3-container">
      <h4>ก๋วยเตี๋ยวต้มยำ <b><span class="pull-right">฿ 70</span></b></h4>
      <h4>ก๋วยเตี๋ยวน้ำตก <b><span class="pull-right">฿ 50</span></b></h4>
      <h4>ก๋วยเตียวน้ำใส <b><span class="pull-right">฿ 50</span></b></h4>
      <h4>ก๋วยเตี๋ยวเย็นตาโฟ <b><span class="pull-right">฿ 60</span></b></h4>
      <h4>ก๋วยเตี๋ยวน้ำแดง <b><span class="pull-right">฿ 50</span></b></h4>
    </div>
    <div class="w3-container w3-black">
      <h1>เครื่องดื่ม</h1>
    </div>
    <div class="w3-container">
       <h4>น้ำเปล่า <b><span class="pull-right">฿ 10</span></b></h4>
      <h4>น้ำโค้ก <b><span class="pull-right">฿ 15</span></b></h4>
      <h4>น้ำสไปรท์ <b><span class="pull-right">฿ 15</span></b></h4>
      <h4>น้ำส้ม <b><span class="pull-right">฿ 15</span></b></h4>
      <h4>น้ำแดง<b><span class="pull-right">฿ 15</span></b></h4>
       <h4>น้ำเขียว <b><span class="pull-right">฿ 15</span></b></h4>
      <h4>น้ำมะพร้าว <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>น้ำส้มคั้น <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>น้ำกระเจี๊ยบ <b><span class="pull-right">฿ 20</span></b></h4>
      <h4>น้ำโอเลี้ยง<b><span class="pull-right">฿ 20</span></b></h4>
    </div>
  </div>
</div>

<!-- Contact Modal -->
<div id="contact" class="w3-modal">
  <div class="w3-modal-content w3-animate-zoom">
    <div class="w3-container w3-black">
      <span onclick="document.getElementById('contact').style.display='none'" class="w3-button w3-display-topright w3-large">x</span>
      <h1>ติดต่อเรา</h1>
    </div>
    <div class="w3-container">
      <p>จองโต๊ะ</p>
      <form action="/action_page.php" target="_blank">
        <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Name" required name="Name"></p>
        <p><input class="w3-input w3-padding-16 w3-border" type="number" placeholder="How many people" required name="People"></p>
        <p><input class="w3-input w3-padding-16 w3-border" type="datetime-local" placeholder="Date and time" required name="date" value="2017-11-16T20:00"></p>
        <p><input class="w3-input w3-padding-16 w3-border" type="text" placeholder="Message \ Special requirements" required name="Message"></p>
        <p><button class="w3-button" type="submit">SEND MESSAGE</button></p>
      </form>
    </div>
  </div>
</div>

</body>
</html>

