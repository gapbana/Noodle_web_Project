package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.SubDistrict;

public interface SubDistrictService extends CoreService<SubDistrict, Serializable> {
	
	public List<SubDistrict> getSubDistrictbyDistrict(String distId) throws DataAccessException;
}
