package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.Transport;
import com.jss.entity.User;

public interface TransportService extends CoreService<Transport, Serializable> {
	
	public Integer saveTransport(Transport entity ) throws DataAccessException;
	public List<Transport> getTransportList(String sdate,String edate,String custId,String saleId,String orderCode,String transportCode) throws DataAccessException;
	public List<Transport> getTransportCount(String year,String month) throws DataAccessException;
	
}
