package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrderMenu;
import com.jss.entity.OrdersProduct;


public interface OrderMenuDao extends CoreDao<OrderMenu, Serializable> {

	public List<OrderMenu> findOrder() throws DataAccessException;

	
}
