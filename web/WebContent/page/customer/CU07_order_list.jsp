<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<script defer
	src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<%@ include file="../inc_menu.jsp"%>


<script language="javascript" type="text/javascript">
	function doEdit(id) {
		$("#finish").hide();
		$("#no_finish").show();
	
	}
	
	function doUpdate(id,status) {
		document.cusForm.mode.value = 'changeStatus';
		document.cusForm.id.value = id;
		document.cusForm.status.value = status;
	    document.cusForm.submit(); 
		
	}
	

	
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					�ʴ���¡������÷���������� <span class="navigator">[CU07]</span>
				</h3>
				<span class="navigator"> �ʴ���¡�������</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/customer" styleId="userForm"
					styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<html:hidden property="status" />
					<div class="form-group">
						<label class="col-sm-2 control-label"> ����ʴ���¡�������:</label>
						<div class="col-sm-3">
							<html:select property="timeFood" styleClass="form-control">
								<html:option value="0">������</html:option>
								<html:option value="1">��������</html:option>
								<html:option value="2">�ѧ�������</html:option>
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"> ������§:</label>
						<div class="col-sm-3">
							<html:select property="sortFood" styleClass="form-control">
								<html:option value="1">���§�����觡�͹</html:option>
								<html:option value="2">���§��������쪹Դ���ǡѹ</html:option>
							</html:select>
						</div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-success">����</button>
							<button type="button" class="btn btn-danger"
								onclick="location.href='user.htm?mode=menu'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
			</div>
		</div>

		<c:if test="${fn:length(resultList) >= 0}">
			<table width="99%" cellpadding="0" cellspacing="0" border="0"
				class="table table-striped table-bordered" id="result">
				<thead>
					<tr class="cyan lighten-4">
						<th width="80"><div align="center">������</div></th>
						<th width="400">��������</th>
						<th width="100"><div align="center">�����Ţ���</div></th>
						<th width="80"><div align="center">ʶҹ�</div></th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
						<tr>
							<td align="center">${item.gapOrder.id}</td>
							<td>${item.gapItem.type} 
							<c:if test="${item.noodleType eq '1'}">- ������</c:if>
							<c:if test="${item.noodleType eq '2'}">- �������</c:if>
							<c:if test="${item.noodleType eq '3'}">- ����˭�</c:if>
							<c:if test="${item.noodleType eq '4'}">- ��������</c:if>
							<c:if test="${item.noodleType eq '5'}">- ���������</c:if>
							<c:if test="${item.noodleType eq '6'}">- ��鹺�����</c:if>
							
							<c:if test="${item.vet eq '0'}">- ������ѡ</c:if>
							<c:if test="${item.vet eq '1'}">- ���ѡ</c:if>
							
							<c:if test="${item.extra eq '0'}"></c:if>
							<c:if test="${item.extra eq '1'}">- <font color="red">�����</font></c:if>
							</td>
							<td align="center">${item.gapOrder.table}</td>
							

							<td align="center">
								<c:if test="${item.status eq '1'}">
									<button id="finish" class="btn btn-success btn-xs" onclick="doUpdate(${item.id},'0')"> ��������</button>
								</c:if> 
								<c:if test="${item.status eq '0'}">
									<button id="no_finish" class="btn btn-danger btn-xs" onclick="doUpdate(${item.id},'1')">�ѧ�������</button>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
</body>
</html>