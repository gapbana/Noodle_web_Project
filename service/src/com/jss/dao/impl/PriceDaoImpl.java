package com.jss.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.edu.util.DateUtil;
import com.jss.dao.PriceDao;
import com.jss.entity.Price;
import com.jss.entity.Product;
import com.jss.entity.ProductType;

public class PriceDaoImpl extends CoreDaoImpl<Price, Serializable> implements PriceDao {

	public PriceDaoImpl(Class<Price> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Price> findPriceList(String sdate, String edate)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Price.class);
		criteria.addOrder(Order.desc("createDate"));	
		
		if (sdate != null && !sdate.equals("")) {
			 Calendar cal = Calendar.getInstance();
			 cal.setTime(DateUtil.string2DateTh(sdate));
			 cal.set(Calendar.HOUR_OF_DAY, 0);
			 cal.set(Calendar.MINUTE, 0);
			 cal.set(Calendar.SECOND, 0);
			 cal.set(Calendar.MILLISECOND, 0);
			 criteria.add(Restrictions.ge("createDate", cal.getTime()));
		}
		if (edate != null && !edate.equals("")) {
			  Calendar cal = Calendar.getInstance();
			  cal.setTime(DateUtil.string2DateTh(edate));
			  cal.set(Calendar.HOUR_OF_DAY, 23);
			  cal.set(Calendar.MINUTE, 59);
			  cal.set(Calendar.SECOND, 59);
			  cal.set(Calendar.MILLISECOND, 0);
			  criteria.add(Restrictions.le("createDate", cal.getTime()));			
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<Product> findPriceDetail(String priceId)throws DataAccessException {
		String sql = " SELECT product_code,";
		sql += " (SELECT product_weight FROM ma_product p1 WHERE p1.product_length = 10 AND p1.product_code = p2.product_code";
		sql += " GROUP BY p1.product_code) AS 10M,";
		sql += " (SELECT product_weight FROM ma_product p1 WHERE p1.product_length = 12 AND p1.product_code = p2.product_code";
		sql += " GROUP BY p1.product_code) AS 12M,";
		sql += " product_diff,price_over,price_overvat,price_less,price_lessvat,producttype_id";
		sql += " FROM ma_product p2";
		sql += " LEFT JOIN pricehistory ph ON p2.product_id = ph.product_id";
		sql += " WHERE ph.price_id="+Integer.parseInt(priceId);
		sql += " GROUP BY p2.product_code";
		sql += " ORDER BY p2.producttype_id,p2.product_size";
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Product> results = new ArrayList<Product>();
		if (objects != null && objects.size() > 0) {
			for (Object[] obj : objects) {
				Product entity = new Product();

				entity.setCode(String.valueOf(obj[0]));
			
				entity.setProductType(new ProductType(Integer.parseInt(String.valueOf(obj[8]))));
				results.add(entity);
			}
		}
	
		return results;
	}

}