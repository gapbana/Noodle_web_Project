package com.jss.web.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapItem;
import com.jss.entity.Product;
import com.jss.entity.ProductType;
import com.jss.entity.Stock;
import com.jss.entity.User;
import com.jss.entity.UserGroup;
import com.jss.service.GapItemService;
import com.jss.service.ProductService;
import com.jss.service.ProductTypeService;
import com.jss.service.StockService;

public class StockAction extends CoreAction {

	private StockService stockService;
	private ProductService productService;
	private ProductTypeService productTypeService;
	private GapItemService gapItemService;
	private int status;
	private String subT;

	public ActionForward index(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			List<GapItem> result = gapItemService.getAllItems();
			dynaForm.set("resultList", result);
			/*
			 * request.setAttribute("resultList", null);
			 * dynaForm.set("productTypeId", ""); dynaForm.set("productCode",
			 * "");
			 */
			request.setAttribute("resultList", result);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("ST01");
	}

/*	public ActionForward stockSearch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<GapItem> resultList = gapItemService..get(
					dynaForm.getString("fullname"),
					dynaForm.getString("userGroupName"),
					dynaForm.getString("status"));// ออกหน้าจอ ต้องปรับเสมอ
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US03");
	}*/
	
	/*public ActionForward userSearch(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			List<User> resultList = userService.getName(
					dynaForm.getString("fullname"),
					dynaForm.getString("userGroupName"),
					dynaForm.getString("status"));// ออกหน้าจอ ต้องปรับเสมอ
			List<UserGroup> result = userGroupService.getAllItems();
			dynaForm.set("comboUserGroup", result);
			request.setAttribute("resultList", resultList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("US03");
	}
*/
	/*
	 * public ActionForward stockAdd(ActionMapping mapping, ActionForm form,
	 * HttpServletRequest request, HttpServletResponse response)throws Exception
	 * { try{ DynaActionForm dynaForm = (DynaActionForm) form; List<Stock>
	 * resultList = stockService.getStockByProduct(dynaForm.getString("id"));
	 * List<Product> result =
	 * productService.getProductStockList(dynaForm.getString
	 * ("id"),null,null,null); if (result != null && result.size() > 0) {
	 * Product entity = result.iterator().next();
	 * 
	 * dynaForm.set("id", String.valueOf(entity.getId()));
	 * 
	 * }
	 * 
	 * request.setAttribute("resultList", resultList); dynaForm.set("sid", "");
	 * dynaForm.set("time", ""); dynaForm.set("date", "" ); dynaForm.set("line",
	 * ""); dynaForm.set("unit", "");
	 * 
	 * }catch (Exception e) { e.printStackTrace(); } return
	 * mapping.findForward("ST02"); }
	 */

	public ActionForward stockAdd(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapItem entity = new GapItem();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("ST02");
	}
	
	
	public ActionForward ItemAdd(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapItem entity = new GapItem();

			entity.setType(dynaForm.getString("name"));
			entity.setPic("./"+dynaForm.getString("pic")+".jpg");
			entity.setPrice(Integer.valueOf(dynaForm.getString("price")));
			entity.setSubType(dynaForm.getString("subT"));  //1 noodle    2 drink      3 snack
			entity.setSta(dynaForm.getString("sta"));
			
			gapItemService.saveItem(entity);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return index(mapping, form, request, response);
	}

	/*
	 * public ActionForward stockEdit(ActionMapping mapping, ActionForm form,
	 * HttpServletRequest request, HttpServletResponse response)throws Exception
	 * { try{ DynaActionForm dynaForm = (DynaActionForm) form; List<Stock>
	 * resultList = stockService.getStockByProduct(dynaForm.getString("id"));
	 * List<Product> result =
	 * productService.getProductStockList(dynaForm.getString
	 * ("id"),null,null,null); if (result != null && result.size() > 0) {
	 * Product entity = result.iterator().next();
	 * request.setAttribute("productName", entity.getProductName());
	 * 
	 * dynaForm.set("id", String.valueOf(entity.getId()));
	 * 
	 * }
	 * 
	 * request.setAttribute("resultList", resultList); Stock stock =
	 * stockService.getItem(Integer.parseInt(dynaForm.getString("sid")));
	 * dynaForm.set("sid", String.valueOf(stock.getId())); dynaForm.set("time",
	 * stock.getTime()); dynaForm.set("date",
	 * DateUtil.date2StringTh(stock.getDate(), "dd/MM/yyyy"));
	 * dynaForm.set("line", String.valueOf(stock.getLine()));
	 * dynaForm.set("unit", String.valueOf(stock.getUnit()));
	 * 
	 * }catch (Exception e) { e.printStackTrace(); } return
	 * mapping.findForward("ST02"); }
	 */

	public ActionForward stockEdit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapItem> resultList = gapItemService.getItem(dynaForm.getString("id"));
			request.setAttribute("resultList", resultList);
			GapItem entity = gapItemService.getItem(Integer.parseInt(dynaForm.getString("id")));
			gapItemService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			dynaForm.set("resultList", resultList);;
			dynaForm.set("name", String.valueOf(entity.getType()));
			dynaForm.set("price", String.valueOf(entity.getPrice()));
			dynaForm.set("sta", Integer.valueOf(entity.getSta()));
			dynaForm.set("sub",String.valueOf(entity.getSubType()));
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("ST02");
	}
	
	public ActionForward stockDel(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try{
		
			gapItemService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
			/*return mapping.findForward("MA02");*/
		}catch (Exception e) { 
			e.printStackTrace();
		}
		return index(mapping, form, request, response);
	}
	

	public ActionForward stockSave(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			if (dynaForm.getString("sid") != null
					&& !dynaForm.getString("sid").equals("")) {
				Stock entity = stockService.getItem(Integer.parseInt(dynaForm
						.getString("sid")));
				entity.setDate(DateUtil.string2DateTh(dynaForm
						.getString("date")));
				entity.setLine(Integer.parseInt(dynaForm.getString("line")
						.trim()));
				entity.setUnit(Double.parseDouble(dynaForm.getString("unit")
						.trim()));
				entity.setTime(dynaForm.getString("time"));
				/*
				 * entity.setUpdateBy(getUserSession(request).getFullname());
				 * entity.setUpdateDate(DateUtil.getCurrentDate());
				 */
				stockService.updateItem(entity);
			} else {
				Stock entity = new Stock();
				if (dynaForm.getString("id") != null
						&& !dynaForm.getString("id").equals(""))
					entity.setProduct(productService.getItem(Integer
							.parseInt(dynaForm.getString("id"))));
				entity.setDate(DateUtil.string2DateTh(dynaForm
						.getString("date")));
				entity.setLine(Integer.parseInt(dynaForm.getString("line")
						.trim()));
				entity.setUnit(Double.parseDouble(dynaForm.getString("unit")
						.trim()));
				entity.setTime(dynaForm.getString("time"));
				/*
				 * entity.setCreateBy(getUserSession(request).getFullname());
				 * entity.setCreateDate(DateUtil.getCurrentDate());
				 */
				stockService.saveItem(entity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return stockAdd(mapping, form, request, response);
	}

	

	public ActionForward productStock(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			List<Product> result = new ArrayList<Product>();
			String id = request.getParameter("id");
			if (id != null && !id.equals("")) {
				result = productService.getProductStockList(id, null, null,
						null);
			}

			setJSONResult(request, setJsonView(result, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}

	public StockService getStockService() {
		return stockService;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}

	public GapItemService getGapItemService() {
		return gapItemService;
	}

	public void setGapItemService(GapItemService gapItemService) {
		this.gapItemService = gapItemService;
	}

}
