package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.User;
import com.jss.entity.UserGroup;

public interface UserGroupService extends CoreService<UserGroup, Serializable> {
	
	public List<UserGroup> getGroupList(String id) throws DataAccessException;
}
