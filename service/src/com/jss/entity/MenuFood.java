package com.jss.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "ma_product")
public class MenuFood extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "product_id") 
	private int id;
	

	@ManyToOne
	@JoinColumn(name = "producttype_id")
	private ProductType productType;
	

	@Column(name = "product_code")
	private String code;
	
	@Column(name = "product_name")
	private int name;
	
	@Column(name = "product_price")
	private int price;

	
	@Transient 
	private String productName;
	@Transient 
	private String productFullName;
	
	

	public MenuFood() {
	}

	public MenuFood(int id) {
		this.setId(id);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	
	public void setProductFullName(String productFullName) {
		this.productFullName = productFullName;
	}

	
	
	
}



	

