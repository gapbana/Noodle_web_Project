package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Customer;
import com.jss.entity.ProductType;

public interface ProductTypeService extends CoreService<ProductType, Serializable> {
	
	public List<ProductType> getAllProductType() throws DataAccessException;
}
