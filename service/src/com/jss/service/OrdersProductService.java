package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.User;

public interface OrdersProductService extends CoreService<OrdersProduct, Serializable> {
	
	public List<OrdersProduct> getProductbyOrders(String ordersId) throws DataAccessException;
}
