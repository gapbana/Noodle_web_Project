<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="inc_header.jsp"%>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#loginForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyp-hicon-refresh'
			},
			fields : {
				un : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ͼ����ҹ'
						}
					}
				},
				pwd : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���ʼ�ҹ'
						}
					}
				},
			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			//$("button[name='btnSave']").attr('disabled', true);

			document.loginForm.mode.value = 'login';
			document.loginForm.submit();
		});
	});
</script>
<style>
.lg {
	height: 35px;
	padding: 10px 16px;
	font-size: 16px;
	line-height: 1.3333333;
	border-radius: 3px;
}
</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="login-panel panel panel-default">
					<div class="panel-body">
						<html:form action="/login" method="post" styleId="loginForm"
							styleClass="form-horizontal">
							<html:hidden property="mode" />
							<fieldset>
								<div class="form-group-lg">
									<h3 align="center" class="page-header">�������к�  <i class="fa fa-key" aria-hidden="true"></i></h3>
								</div>
								<div class="form-group"></div>
								<div class="form-group">
									<label class="control-label col-sm-3" for="email">���ͼ����ҹ
										: </label>
									<div class="col-sm-8">
										<html:text property="un" styleId="un"
											styleClass="form-control lg"></html:text>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3" for="email">���ʼ�ҹ
										: </label>
									<div class="col-sm-8">
										<html:password property="pwd" styleId="pwd"
											styleClass="form-control lg"></html:password>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-9 col-md-offset-3">
										<span class="red-text">${error}</span>


									</div>
								</div>
								<div align="center">
									<input type="checkbox" name="mem" value="mem">
									�����������駶Ѵ�<BR>
									<BR>
								</div>
								
								

								<div align="center">
									<button type="submit" class="btn btn-info btn-lg">�������к�</button>
								</div>

							</fieldset>
						</html:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>