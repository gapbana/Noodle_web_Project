package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.ProductType;

public interface ProductTypeDao extends CoreDao<ProductType, Serializable> {

	public List<ProductType> findAllProductType() throws DataAccessException;
}
