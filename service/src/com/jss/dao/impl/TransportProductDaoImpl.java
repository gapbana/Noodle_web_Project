package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.TransportDao;
import com.jss.dao.TransportProductDao;
import com.jss.entity.Car;
import com.jss.entity.OrdersProduct;
import com.jss.entity.Transport;
import com.jss.entity.TransportProduct;

public class TransportProductDaoImpl extends CoreDaoImpl<TransportProduct, Serializable> implements TransportProductDao {

	public TransportProductDaoImpl(Class<TransportProduct> entityClass) {
		super(entityClass);
	}

	@Override
	public List<TransportProduct> findTransportProductByTransport(String transportId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(TransportProduct.class);
		criteria.createAlias("transport", "transport");
		criteria.createAlias("product", "product");
		criteria.addOrder(Order.asc("product.code"));	
		criteria.addOrder(Order.asc("product.size"));
		
		if(transportId != null && !transportId.equals("")){
			criteria.add(Restrictions.eq("transport.id", Integer.parseInt(transportId)));
		}

		return getHibernateTemplate().findByCriteria(criteria);
	}

	

}