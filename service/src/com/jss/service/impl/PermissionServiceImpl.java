package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.MenuDao;
import com.jss.dao.PermissionDao;
import com.jss.dao.UserDao;
import com.jss.entity.Menu;
import com.jss.entity.Permission;
import com.jss.entity.User;
import com.jss.service.MenuService;
import com.jss.service.PermissionService;
import com.jss.service.UserService;

public class PermissionServiceImpl extends CoreServiceImpl<Permission, Serializable>
		implements PermissionService {

	private PermissionDao permissionDao;

	public PermissionServiceImpl(PermissionDao permissionDao) {
		super(permissionDao);
		this.permissionDao = permissionDao;
	}

	@Override
	public List<Permission> getPermissionByGroup(String groupId)throws DataAccessException {
		return permissionDao.findPermissionByGroup(groupId);
	}

	@Override
	public boolean savePermission(List<Permission> entityList, String groupId)throws DataAccessException {
		boolean result = false;
		try {
			if (entityList != null && entityList.size() > 0) {
				permissionDao.delete(Integer.parseInt(groupId));

				for (Permission entity : entityList) {
					if (entity.isCheck()) {
						super.saveItem(entity);
					}
				}
			}

			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
