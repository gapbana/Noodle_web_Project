package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.UserGroup;

public interface OrdersDao extends CoreDao<Orders, Serializable> {

	public List<Orders> findOrdersList(String sdate,String edate,String custId,String saleId,String code,String status) throws DataAccessException;
	public List<Orders> findComboOrders(String custId) throws DataAccessException;
	public List<Orders> findOrdersReport1List(String year,String month) throws DataAccessException;
	public List<Orders> findMonthlyTransport() throws DataAccessException;
}
