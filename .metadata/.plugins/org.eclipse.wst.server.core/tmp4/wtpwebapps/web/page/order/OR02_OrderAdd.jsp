<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp"%>


<script src="lib/dist/card.js">
$('form').card({ container:
	'.card-wrapper', // *required* });
	
	</script>

<script language="javascript" type="text/javascript">


	$(document).ready(function() {
		$("#table").hide();
		$("#ctable").hide();
		$("#addr").hide();
		$("#addrform").hide();
		$("#credit").hide();
		$("#left_menu").hide();
			
	});
</script>

<script language="javascript" type="text/javascript">
	function doTable(id) {
		//to table
		if (id == "1") {   
			$("#table").show();
			$("#ctable").show();
			$("#addr").hide();
			$("#addrform").hide();
			$("#credit").hide();
		
		//take home
		} else if(id == "2"){
			$("#table").hide();
			$("#ctable").hide();
			$("#addr").show();
			$("#addrform").show();
			$("#credit").show();
			
		}else{
			$("#table").hide();
			$("#ctable").hide();
			$("#addr").hide();
			$("#addrform").hide();
			$("#credit").hide();
			
		}
	}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					�����Թ<span class="navigator">[OR02]</span>
				</h3>
				<span class="navigator">���͡�Թ��� > ��Ǩ�ͺ�Թ��� >
					�����Թ</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<ul class="nav nav-tabs">

					<li <c:if test="${tab eq '1' }">class="active"</c:if>><a
						data-toggle="tab" href="#menu1" id="m1">��������´��ê����Թ</a></li>
					<c:if test="${id ne null }">
						<li <c:if test="${tab eq '2' }">class="active"</c:if>><a
							data-toggle="tab" href="#menu2" id="m2">��������´</a></li>
						<li <c:if test="${tab eq '3' }">class="active"</c:if>><a
							data-toggle="tab" href="#menu3" id="m3">��¡���Թ���</a></li>
					</c:if>
				</ul>
				<html:form action="/order" styleId="orderForm"
					styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="tab" />
					<html:hidden property="id" styleId="id" />
					<html:hidden property="productId" styleId="pid" />
					<html:hidden property="priceId" styleId="prid" />




					<div class="container-fluid">
						<BR> <BR> <label class="col-sm-2 control-label">
							���͡�Ըա�ú�ԡ�� :</label>
						<div class="col-sm-3">
							<html:select property="service" styleId="service"
								styleClass="form-control" onchange="doTable(this.value);">
								<html:option value="0">���͡</html:option>
								<html:option value="1">���Կ������</html:option>
								<html:option value="2">�Ѵ�觷���ҹ(�դ�ҨѴ��)</html:option>
							</html:select>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label" id="ctable">���͡���:</label>
							<label class="col-sm-2 control-label" id="addr"> �������:</label>
							<div class="col-sm-4">
								<html:textarea property="addrform" styleId="addrform" rows="3"
									styleClass="form-control"></html:textarea>
								<html:select property="table" styleId="table"
									styleClass="form-control">
									<html:option value="0">���͡�����Ţ���</html:option>
									<html:option value="1">��������Ţ 1</html:option>
									<html:option value="2">��������Ţ 2</html:option>
									<html:option value="1">��������Ţ 3</html:option>
									<html:option value="2">��������Ţ 4</html:option>
									<html:option value="1">��������Ţ 5</html:option>
									<html:option value="2">��������Ţ 6</html:option>
								</html:select>
							</div>
						</div>



						<%-- credit card inform --%>
						<div id="credit" class="well well-lg" style="width: 800px">

							<form class="form-horizontal">
								<div class="form-group">
									
									<div class="col-sm-10">
										<h4 class="form-control-static">Add a payment method &ensp;<i class="fa fa-credit-card-alt"></i></h4>
									</div>
								</div>
							</form>
							
							<div class="form-group">
								<div class="col-sm-1"></div>
								<label class="col-sm-2 control-label" id="cnum"> Card
									number</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="cnum2"
										placeholder="XXXX-XXXX-XXXX-XXXX">
								</div>
							</div>


							<div class="form-inline">
								<div class="col-sm-1"></div>
								<label class="col-sm-2 control-label" id="edat">
									Expiration date</label>

								<div class="col-sm-3">
									<select class="form-control" id="emon">
										<option>01</option>
										<option>02</option>
										<option>03</option>
										<option>04</option>
										<option>05</option>
										<option>06</option>
										<option>07</option>
										<option>08</option>
										<option>09</option>
										<option>10</option>
										<option>11</option>
										<option>12</option>
									</select> / <select class="form-control" id="eyea">
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
										<option>2022</option>
										<option>2023</option>
										<option>2024</option>
										<option>2025</option>
										<option>2026</option>
										<option>2027</option>
										<option>2028</option>
										<option>2029</option>
									</select>
								</div>

								<label class="control-label" id="cvv"> CVV:</label> <input
									type="text" class="form-control" maxlength="3"
									style="width: 70px" id="cvv2" placeholder="XXX">
							</div>

							<BR>
							<%-- name on card --%>
							<div class="form-group">
								<div class="col-sm-1"></div>
								<label class="col-sm-2 control-label" id="cname"> Name
									on card</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="cname"
										placeholder="KRIDTANAN NOIBAHT">
								</div>
							</div>

						</div>

					</div>



					<%-- button section--%>
					<BR>
					<BR>
					<div class="form-group">

						<div class="col-sm-6 col-sm-offset-6">
							<button type="submit" class="btn btn-success">�Ѵ�</button>
							<button type="button" class="btn btn-default"
								onclick="doRefresh(1);">���������</button>
							<button type="button" class="btn btn-danger" onclick="doBack();">¡��ԡ</button>
						</div>
					</div>
			</div>
		</div>


	</div>
	</div>
	</html:form>
	</div>
	</div>
	</div>

</body>
</html>