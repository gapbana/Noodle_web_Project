package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapOrderItem;
import com.jss.entity.Menu;
import com.jss.entity.UserGroup;

public interface GapOrderItemDao extends CoreDao<GapOrderItem, Serializable> {

	public List<GapOrderItem> findOrderItems() throws DataAccessException;
}
