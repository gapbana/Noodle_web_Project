package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.ConstantDao;
import com.jss.entity.Car;
import com.jss.entity.Constant;

public class ConstantDaoImpl extends CoreDaoImpl<Constant, Serializable> implements ConstantDao {

	public ConstantDaoImpl(Class<Constant> entityClass) {
		super(entityClass);
	}

}