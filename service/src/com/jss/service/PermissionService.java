package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Menu;
import com.jss.entity.Permission;
import com.jss.entity.User;

public interface PermissionService extends CoreService<Permission, Serializable> {
	
	public List<Permission> getPermissionByGroup(String groupId) throws DataAccessException;
	public boolean savePermission(List<Permission> entityList, String groupId) throws DataAccessException;
}
