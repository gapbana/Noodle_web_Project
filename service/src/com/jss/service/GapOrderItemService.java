package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.GapOrderItem;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface GapOrderItemService extends CoreService<GapOrderItem, Serializable> {
	
	public List<GapOrderItem> getOrderItem() throws DataAccessException;
}
