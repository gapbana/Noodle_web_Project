package com.jss.web.action;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.edu.util.DateUtil;
import com.edu.util.LogHelper;
import com.jss.entity.Orders;
import com.jss.entity.Price;
import com.jss.entity.PriceHistory;
import com.jss.entity.Product;
import com.jss.entity.Stock;
import com.jss.service.OrdersService;
import com.jss.service.PriceHistoryService;
import com.jss.service.PriceService;
import com.jss.service.ProductService;

public class PriceAction extends CoreAction{	
	private PriceService priceService;
	private ProductService productService;
	private PriceHistoryService priceHistoryService;
	private OrdersService ordersService;
	
	public ActionForward index(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.set("id", "");
			dynaForm.set("sdate", "");
			dynaForm.set("edate", "");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("PR01");
	}
	public ActionForward priceSearch(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Price> result = priceService.getPriceList(dynaForm.getString("sdate"), dynaForm.getString("edate"));
			request.setAttribute("priceList", result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("PR01");
	}
	public ActionForward priceAdd(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Product> result = new ArrayList<>();
			dynaForm.set("price", "");
			request.setAttribute("productList", result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("PR02");
	}
	public ActionForward priceCal(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Product> result = new ArrayList<>();
			if(dynaForm.getString("price") != null && !dynaForm.getString("price").equals("")){
				result = productService.getProductListPrice(dynaForm.getString("price"));
				request.setAttribute("productList", result);
			}
			
			dynaForm.set("resultList", result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("PR02");
	}
	public ActionForward priceSave(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			
			if(dynaForm.getString("price") != null && !dynaForm.getString("price").equals("")){
				List<Product> ProductList = productService.getProductListPrice(dynaForm.getString("price"));
				Price price = new Price();
				price.setPrice(Double.valueOf(dynaForm.getString("price")));
		/*		price.setCreateBy(getUserSession(request).getFullname());
				price.setCreateDate(DateUtil.getCurrentDate());*/
				Integer priceId = priceService.savePrice(price);
				
				for (Product product : ProductList) {
					List<Product> productList = productService.getProductByCode(product.getCode());
					for (Product row : productList) {						
						PriceHistory entity = new PriceHistory();
						entity.setPrice(priceService.getItem(priceId));
						entity.setProduct(productService.getItem(row.getId()));
						
					/*	entity.setCreateBy(getUserSession(request).getFullname());
						entity.setCreateDate(DateUtil.getCurrentDate());*/
						priceHistoryService.saveItem(entity);						
						
						Product entiryP = productService.getItem(row.getId());
						
					/*	entiryP.setUpdateBy(getUserSession(request).getFullname());
						entiryP.setUpdateDate(DateUtil.getCurrentDate());*/
						productService.updateItem(entiryP);
					}						
				}
			}
			dynaForm.set("sdate", DateUtil.date2StringTh(DateUtil.getCurrentDate(), "dd/MM/yyyy"));
			dynaForm.set("edate", DateUtil.date2StringTh(DateUtil.getCurrentDate(), "dd/MM/yyyy"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return priceSearch(mapping, form, request, response);
	}
	
	public ActionForward priceDetail(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			Price entity = priceService.getItem(Integer.parseInt(dynaForm.getString("id")));
			List<Product> result = priceService.getPriceDetail(dynaForm.getString("id"));
			/*request.setAttribute("date", entity.getCreateDate());*/
			request.setAttribute("productList", result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("PR03");
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void printPricePdf(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
		
			List<Product> result = priceService.getPriceDetail(request.getParameter("id"));		 	
			Price entity = priceService.getItem(Integer.parseInt(request.getParameter("id")));
		 	HashMap<String,String> paramsMap = new HashMap<String,String>();	
			
			/*paramsMap.put("date", DateUtil.date2StringTh(entity.getCreateDate()) );*/
			
		 	
			String path =  "/pdf/price.jasper";
			JRDataSource dataSource = new JRBeanCollectionDataSource(result);
			executeReport(request, response, path, paramsMap, dataSource ,"/pdf/price.jasper", false);
		} catch (Exception e) {
			LogHelper.doError(e.getMessage(), e);
		}
	}
	
	public ActionForward productPrice(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			List<PriceHistory> result = new ArrayList<PriceHistory>();
			String id = request.getParameter("id");
			String oid = request.getParameter("oid");
			Orders entity = ordersService.getItem(Integer.parseInt(oid));
			if(entity != null){
				if (id != null && !id.equals("")) {
					result = priceHistoryService.getPriceHistoryByProduct(id,DateUtil.date2String(entity.getDate(), "yyyy-MM-dd") ,entity.getTime());	
				}
			}
			

			setJSONResult(request, setJsonView(result, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	
	public ActionForward productPriceByID(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			List<PriceHistory> result = new ArrayList<PriceHistory>();
			String id = request.getParameter("id");
			String pid = request.getParameter("pid");
			if (id != null && !id.equals("")) {
				result = priceHistoryService.getPriceHistoryByPriceID(id, pid);
			}
			

			setJSONResult(request, setJsonView(result, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	
	public PriceService getPriceService() {
		return priceService;
	}
	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}
	public ProductService getProductService() {
		return productService;
	}
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	public PriceHistoryService getPriceHistoryService() {
		return priceHistoryService;
	}
	public void setPriceHistoryService(PriceHistoryService priceHistoryService) {
		this.priceHistoryService = priceHistoryService;
	}
	public OrdersService getOrdersService() {
		return ordersService;
	}
	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}
	
	
	
	
	
}
