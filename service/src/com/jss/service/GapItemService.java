package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.GapItem;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface GapItemService extends CoreService<GapItem, Serializable> {
	
	public List<GapItem> getItem(String itemId) throws DataAccessException;
	public List<GapItem> getItemSearch(String subType,String type) throws DataAccessException;
	public List<GapItem> getBeverage(String type) throws DataAccessException;
}
