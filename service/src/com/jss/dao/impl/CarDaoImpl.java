package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.entity.Car;

public class CarDaoImpl extends CoreDaoImpl<Car, Serializable> implements CarDao {

	public CarDaoImpl(Class<Car> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Car> findCarByCust(String custId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Car.class);
		criteria.createAlias("customer", "customer");
		criteria.addOrder(Order.asc("licensePlate"));	
		
		if(custId != null && !custId.equals("")){
			criteria.add(Restrictions.eq("customer.id", Integer.parseInt(custId)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	

}