package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.OrderMenu;



public interface OrderMenuService extends CoreService<OrderMenu, Serializable> {
	

	public List<OrderMenu> getOrdersList() throws DataAccessException;

}
