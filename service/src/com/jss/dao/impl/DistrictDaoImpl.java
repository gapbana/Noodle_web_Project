package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.DistrictDao;
import com.jss.entity.District;

public class DistrictDaoImpl extends CoreDaoImpl<District, Serializable> implements DistrictDao {

	public DistrictDaoImpl(Class<District> entityClass) {
		super(entityClass);
		
	}

	@Override
	public List<District> findDistrictbyProv(String provId)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(District.class);
		criteria.createAlias("province", "province");
		criteria.addOrder(Order.asc("name"));	
		
		if(provId != null && !provId.equals("")){
			criteria.add(Restrictions.eq("province.id", Integer.parseInt(provId)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}


}