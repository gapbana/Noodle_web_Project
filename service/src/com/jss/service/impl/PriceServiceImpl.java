package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.PriceDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Price;
import com.jss.entity.Product;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.PriceService;
import com.jss.service.UserService;

public class PriceServiceImpl extends CoreServiceImpl<Price, Serializable> implements PriceService {

	private PriceDao priceDao;

	public PriceServiceImpl(PriceDao priceDao) {
		super(priceDao);
		this.priceDao = priceDao;
	}

	@Override
	public List<Price> getPriceList(String sdate, String edate) throws DataAccessException {
		return priceDao.findPriceList(sdate, edate);
	}

	@Override
	public List<Product> getPriceDetail(String priceId) throws DataAccessException {
		return priceDao.findPriceDetail(priceId);
	}

	@Override
	public Integer savePrice(Price price) throws DataAccessException {
		Integer id = priceDao.merge(price).getId();
//		priceDao.insertOrUpdate(price);
		return id;
	}


}
