package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.User;

public interface UserDao extends CoreDao<User, Serializable> {

	public List<User> findSale() throws DataAccessException;	
	public List<User> findName(String firstname) throws DataAccessException;
	
	public List<User> findLogin(String un , String pwd) throws DataAccessException;	
	public List<User> findName(String fullname, String userGroup, String status)throws DataAccessException;
	
	



	
	
}
