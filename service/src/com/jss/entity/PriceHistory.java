package com.jss.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "pricehistory")
public class PriceHistory extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "pricehistory_id") 
	private int id;

	@ManyToOne
	@JoinColumn(name = "price_id")
	private Price price;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@Column(name = "price_over")
	private Double priceOver;

	@Column(name = "price_overvat")
	private Double priceOverVat;

	@Column(name = "price_less")
	private Double priceLess;

	@Column(name = "price_lessvat")
	private Double priceLessVat;

	@Transient
	private boolean check;
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getPriceOver() {
		return priceOver;
	}

	public void setPriceOver(Double priceOver) {
		this.priceOver = priceOver;
	}

	public Double getPriceOverVat() {
		return priceOverVat;
	}

	public void setPriceOverVat(Double priceOverVat) {
		this.priceOverVat = priceOverVat;
	}

	public Double getPriceLess() {
		return priceLess;
	}

	public void setPriceLess(Double priceLess) {
		this.priceLess = priceLess;
	}

	public Double getPriceLessVat() {
		return priceLessVat;
	}

	public void setPriceLessVat(Double priceLessVat) {
		this.priceLessVat = priceLessVat;
	}
	
	@Transient
	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

}
