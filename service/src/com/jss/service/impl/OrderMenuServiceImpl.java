package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.OrderMenuDao;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrderMenu;
import com.jss.service.OrderMenuService;

public class OrderMenuServiceImpl extends CoreServiceImpl<OrderMenu, Serializable> implements OrderMenuService{

	private OrderMenuDao orderMenuDao;
	public OrderMenuServiceImpl(OrderMenuDao orderMenuDao) {
		super(orderMenuDao);
		this.orderMenuDao = orderMenuDao;
		
	}
	@Override
	public List<OrderMenu> getOrdersList() throws DataAccessException {
		
		return orderMenuDao.findOrder();
	}
	



	
}
