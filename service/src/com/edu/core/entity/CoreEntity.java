package com.edu.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;


@MappedSuperclass
public class CoreEntity implements Serializable {
	/**
     *
     */
	/*private static final long serialVersionUID = 1L;
	
	@Column(name = "createBy", insertable = true, updatable = false)
	private String createBy;
	
	@Column(name = "createDate", insertable = true, updatable = false)
	private Date createDate;
	
	@Column(name = "updateBy", insertable = true, updatable = true)
	private String updateBy;
	
	@Column(name = "updateDate", insertable = true, updatable = true)
	private Date updateDate;


	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	} 
	
	*/
	/*
	@Transient
	private String createDateShow;
	
	@Transient
	private String createDateShowSecond;

	public String getCreateDateShow() {
		return DateTimeUtil.parseOutputCalendarEn(getCreateDate());
	}

	public void setCreateDateShow(String createDateShow) {
		this.createDateShow = createDateShow;
	}
	
	public String getCreateDateShowSecond() {
		return DateTimeUtil.Date2String(getCreateDate());
	}

	public void setCreateDateShowSecond(String createDateShowSecond) {
		this.createDateShowSecond = createDateShowSecond;
	}*/
}
