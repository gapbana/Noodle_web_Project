package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.TransportDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.Transport;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.TransportService;
import com.jss.service.UserService;

public class TransportServiceImpl extends CoreServiceImpl<Transport, Serializable> implements TransportService {

	private TransportDao transportDao;

	public TransportServiceImpl(TransportDao transportDao) {
		super(transportDao);
		this.transportDao = transportDao;
	}

	@Override
	public Integer saveTransport(Transport entity) throws DataAccessException {
		Integer id = transportDao.merge(entity).getId();
		return id;
	}

	@Override
	public List<Transport> getTransportList(String sdate, String edate,String custId, String saleId, String orderCode, String transportCode)throws DataAccessException {
		return transportDao.findTransportList(sdate, edate, custId, saleId, orderCode, transportCode);
	}

	@Override
	public List<Transport> getTransportCount(String year, String month)throws DataAccessException {
		return transportDao.findTransportCount(year, month);
	}

}
