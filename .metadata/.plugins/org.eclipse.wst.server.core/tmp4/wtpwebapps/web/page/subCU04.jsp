<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html>

<script type="text/javascript">
$('.slide a').click(function () {
	  $('.slide.active').removeClass('active');
	  $(this).closest('.slide').addClass('active');
	  return false;
	});
</script>

<<style>
<style>
p {
    font-size: 35px;
}

body {
	margin: 0;
	font: 14px Helvetica, Arial, serif;
}

.title {
	text-align: center;
	color: #333;
	font-size: 2.0em;
}

.creds a {
	color: #000;
	text-decoration: none;
	border-bottom: 1px dotted #000;
}

.flip-menu {
	margin: 30px 0 0;
}

.flip-item-wrap {
	width: 25%;
	height: auto;
	float: left;
	position: relative;
	-webkit-perspective: 800px;
	-moz-perspective: 800px;
	-ms-perspective: 800px;
	-o-perspective: 800px;
	perspective: 800px;
}

@media screen and (min-width: 1280px) {
	.flip-item-wrap {
		width: 16.6%;
	}
}

@media screen and (max-width: 979px) {
	.flip-item-wrap {
		width: 33.3%;
	}
}

@media screen and (max-width: 639px) {
	.flip-item-wrap {
		width: 50%;
	}
}

@media screen and (max-width: 379px) {
	.flip-item-wrap {
		width: 100%;
	}
}

.flip-item-wrap img {
	width: 100%;
	height: auto;
	display: block;
	margin: 0;
}

.flip-item-wrap input {
	display: none;
}

.flip-item-wrap .fake-image {
	visibility: hidden;
}

.flip-item {
	display: block;
	width: 100%;
	height: 100%;
	float: left;
	position: absolute;
	top: 0;
	left: 0;
	cursor: pointer;
	color: #fff;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	-o-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transition: -webkit-transform 1s;
	-moz-transition: -moz-transform 1s;
	-o-transition: -o-transform 1s;
	transition: transform 1s;
}

.flip-item figure {
	display: block;
	position: absolute;
	width: 100%;
	height: 100%;
	margin: 0;
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	-ms-backface-visibility: hidden;
	-o-backface-visibility: hidden;
	backface-visibility: hidden;
}

.flip-item .back {
	width: 100%;
	display: block;
	margin: 0;
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flipper:checked+.flip-item {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flip-item-desc {
	background: rgba(0, 0, 0, 0.2);
	width:100%;
	height:100%; 
	padding: 5%;
	position: absolute;
	top: 0;
	left: 0;
	text-shadow: 1px 2px 1px rgba(0, 0, 0, 0.9);
	overflow: hidden;
}

.flip-item-title {
	font-size: 2.0em;
	margin: 0.6em 0 0.2em;
}
</style>


</style>
<style>
@import "bourbon";

$primary-color: hsl(350, 70, 60);

.slides {
  position: relative;
  @extend .cf;

  .slide {
    position: absolute;
    top: 0; // Have to declare this for transition: top to work
    width: calc(33% - 1em);
    max-height: 3.5em; // Kind of a magic number
    margin: 0.5em;
    padding: 1em;
    background: darken($primary-color, 20%);
    color: white;
    float: left;
    overflow: hidden;
    
    transition: max-height 0.25s       ease-in-out,
                width      0.25s 0.25s ease-in-out,
                left       0.25s 0.5s  ease-in-out,
                top        0.25s 0.75s ease-in-out;
    
    // This is probably the jankiest part
    @for $i from 1 through 3 {
      &:nth-child(#{$i}) {
        left: #{($i - 1) * 33.3333%};
      }
    }
 
    
    > a {
      display: block;
      padding-bottom: 1em;
      font-family: 'Oswald';
      text-transform: uppercase;
      text-decoration: none;
      color: lighten($primary-color, 15%);
      transition: color 2s;
    }
  }
  
  .slide.active {
    position: absolute;
    top: 4.5em;
    left: 0;
    width: 100%;
    max-height: 20em;
    float: none;
    
    transition: top        0.25s 1s    ease-in-out,
                left       0.25s 1.25s ease-in-out,
                width      0.25s 1.5s  ease-in-out,
                max-height 0.25s 1.75s ease-in-out;
    
    a { color: white; }
  }
}

/** PAGE STYLES **/
@import url(https://fonts.googleapis.com/css?family=Lato:300|Oswald);

*, *:before, *:after { @include box-sizing(border-box); }

html, body { width: 100%; height: 100%; }

html { font-size: 62.5%; }

body {
  background: $primary-color;
  font-family: 'Lato', sans-serif;
  font-size: 2em; 
  line-height: 1.5
}

.container {
  width: 90%;
  max-width: 1200px;
  margin: 0 auto;
}

h1 {
  margin: 0;
  padding: 1em;
  font-family: 'Oswald', sans-serif;
  font-size: 2em;
  text-transform: uppercase;
  text-align: center;
  color: lighten(hsl(120, 70, 70), 30%);
}

/**
 * For modern browsers
 * 1. The space content is one way to avoid an Opera bug when the
 *    contenteditable attribute is included anywhere else in the document.
 *    Otherwise it causes space to appear at the top and bottom of elements
 *    that are clearfixed.
 * 2. The use of `table` rather than `block` is only necessary if using
 *    `:before` to contain the top-margins of child elements.
 */
.cf:before,
.cf:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.cf:after {
    clear: both;
}

/**
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */
.cf {
    *zoom: 1;
}
</style>