package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.RunningDao;
import com.jss.entity.Car;
import com.jss.entity.Running;

public class RunningDaoImpl extends CoreDaoImpl<Running, Serializable> implements RunningDao {

	public RunningDaoImpl(Class<Running> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Running> findRunningCode(String type,String year, String month)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Running.class);
		
		criteria.add(Restrictions.eq("type", type));
		criteria.add(Restrictions.eq("year", year));
		criteria.add(Restrictions.eq("month", month));

		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	
	

}