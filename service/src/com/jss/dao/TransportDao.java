package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.Transport;
import com.jss.entity.UserGroup;

public interface TransportDao extends CoreDao<Transport, Serializable> {

	public List<Transport> findTransportList(String sdate,String edate,String custId,String saleId,String orderCode,String transportCode) throws DataAccessException;
	public List<Transport> findTransportCount(String year,String month) throws DataAccessException;
	
}
