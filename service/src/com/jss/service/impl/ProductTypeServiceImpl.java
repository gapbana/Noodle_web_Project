package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.ProductTypeDao;
import com.jss.entity.ProductType;
import com.jss.service.ProductTypeService;

public class ProductTypeServiceImpl extends CoreServiceImpl<ProductType, Serializable>implements ProductTypeService {

	private ProductTypeDao productTypeDao;
	public ProductTypeServiceImpl(ProductTypeDao productTypeDao) {
		super(productTypeDao);
		this.productTypeDao = productTypeDao;
	}
	@Override
	public List<ProductType> getAllProductType() throws DataAccessException {
		return productTypeDao.findAllProductType();
	}
	
}
