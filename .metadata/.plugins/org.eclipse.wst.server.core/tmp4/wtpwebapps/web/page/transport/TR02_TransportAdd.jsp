<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
	$('#transportForm').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields : {
        	date: {
	       		validators: {
	      			notEmpty: {
	             		message: '��س��к�: �ѹ���'
	     			}
	      		}
	 		},	
	 		orders: {
	       		validators: {
	      			notEmpty: {
	             		message: '��س����͡: ���觫���'
	     			}
	      		}
	 		},
	 		cust: {
	       		validators: {
	      			notEmpty: {
	             		message: '��س����͡: �١���'
	     			}
	      		}
	 		},
	 		sale: {
	       		validators: {
	      			notEmpty: {
	             		message: '��س����͡: ��ѡ�ҹ���'
	     			}
	      		}
	 		},
        }
        
    }).on('success.form.fv', function(e) {
        	// Prevent submit form
        e.preventDefault();
        
        $("button[name='btnSave']").attr('disabled', true);
       		/*  alert(); */
        document.transportForm.mode.value = "transportSave";
		document.transportForm.submit();  
    });
	
	$( "#m1" ).click(function() {
		document.transportForm.tab.value = 1;
	});
	$( "#m2" ).click(function() {
		document.transportForm.tab.value = 2;
	});
	$( "#m3" ).click(function() {
		document.transportForm.tab.value = 3;
	});
	
	$("#transports").hide();
	$("#car").hide();
	
	$( "#transport2" ).click(function() {
		if($("#transport2").is(":checked")){                
	    	$("#transports").show();
	    	$("#transportPrice").focus();
	    } else{
	    	$("#transportPrice").val(0);
	    	$("#transports").hide();
	    }
	}); 
	
	$( "#transport1" ).click(function() {
		if($("#transport1").is(":checked")){                
	    	$("#car").show();
	    } else{
	    	$("#car").hide();
	    }
	}); 
	
	$("#deposit").hide();
	$("#delivery").hide();
	$("#discountCash").hide();
	$("#discount300Ton").hide();
	$("#discount500Ton").hide();
	$("#discountS").hide();
	
		
	var totalAmount =  '${totalAmountTP}';
	var totalUnit = '${totalUnitTP}';
	var unit = '${unit}';
	
	if(unit >= 300 && unit < 500){
		$("#discount300Ton").show();
		var Ton300Total = (totalUnit*1000)*0.10;
		$("#Ton300Total").html("- "+numeral(Ton300Total).format('0,0.000'));
		totalAmount = totalAmount - Ton300Total;
		
	}else if (unit >= 500) {
		$("#discount500Ton").show();
		
		var Ton500Total = (totalUnit*1000)*0.20;
		$("#Ton500Total").html("- "+numeral(Ton500Total).format('0,0.000'));
		
		totalAmount = totalAmount - Ton500Total;

	}
	

	var transport =  $('input[name="transport"]:checked').val();
	if(transport =='N'){		
		$("#transports").show();
    	$("#transportPrice").focus();
    	
		$("#delivery").show();
		var delivery = $("#transportPrice").val();
		$("#deliveryM").html("+ "+numeral(delivery).format('0,0.000'));
	
		totalAmount =parseFloat(totalAmount) + parseFloat(delivery);
		
	}else if(transport =='Y'){
		$("#car").show();
	}
	
	var pay3 =  $('input[name="pay"]:checked').val();
	if(pay3 =='D'){		
		var payPercent = $("#payPercent").val();
		var money = (totalAmount*payPercent)/100;
		
		$("#deposit").show();
		
		$("#depositM").html(payPercent+"%");
		$("#depositT").html("- "+numeral(money).format('-0,0.000'));
		
		totalAmount = totalAmount - money;
	}else if(pay3 =='C'){

		$("#discountCash").show();		
		var cashTotal = (totalUnit*1000)*0.20;
		$("#cashTotal").html("- "+numeral(cashTotal).format('0,0.000'));
		
		totalAmount = totalAmount - cashTotal;
		
	}
	
	var discount = $("#discount").val();
	if(discount > 0 && discount <= 100){
		$("#discountS").show();
		$("#discountP").html(discount+"%");
		var totalDiscount = (totalAmount * discount)/100;
		$("#discountM").html("- "+numeral(totalDiscount).format('0,0.000'));
		
		totalAmount = totalAmount - totalDiscount;
		
	}
	
	$("#totalAmount").html(numeral(totalAmount).format('#,##0.000'));
	

});
</script>

<script language="javascript" type="text/javascript">
function doRefresh(tab) {
 	document.transportForm.mode.value = 'transportAdd';
	document.transportForm.tab.value = tab;
	document.transportForm.submit();    
}
function doBack() {
 	document.transportForm.mode.value = 'transportSearch';
 	document.transportForm.tab.value = '1';
	document.transportForm.submit();    
}
function doSave(tab) {
 	document.transportForm.mode.value = 'transportSave';
 	document.transportForm.tab.value = tab;
	document.transportForm.submit();    
}
function  doNext(tab) {
	document.transportForm.mode.value = 'orderAdd';
	document.transportForm.tab.value = tab;
	document.transportForm.submit();   
}
function  doAddProduct(tab) {
	if($('#product').val() != null){
		document.transportForm.mode.value = 'transportSave';
    	document.transportForm.tab.value = tab;
    	document.transportForm.submit(); 
	}     
 
}
function doComboOrders(custId) {
	$.post("${pageContext.request.contextPath}/order.htm?mode=comboOrders",
    {
		id: custId
    }, function(data) {       	
   		$('#orders').find('option').remove();
   		var options = '';
		if (data != null) {
			options = '<option value="">--���͡--</option>';
			for (var i = 0; i < data.length; i++) {
				options += '<option value="' + data[i].id + '">' + data[i].code+' ('+numeral(data[i].totalUnit).format('0,000.000')+' �ѹ) '+'</option>';
			}
			
		}else{
			options = '<option value="">--��������觫���--</option>';
		}
		$('#orders').html(options);
    },'json');
}
function doComboProduct(id) {
	$.post("${pageContext.request.contextPath}/master.htm?mode=comboProduct",
    {
		id: id
    }, function(data) {       	
   		$('#product').find('option').remove();
		var options = '<option value="">--���͡--</option>';
		if (data != null) {
			for (var i = 0; i < data.length; i++) {
				var model = '';
				if(data[i].model == 'T'){
					model = ' (�ç)';
				}else{
					model = ' (�Ѻ)';
				}
				options += '<option value="' + data[i].id + '">' + data[i].code+' '+data[i].size+' x '+data[i].length+' ��.'+model+'</option>';
			}
			
		}
		$('#product').html(options);
    },'json');
}
function doProductStock(id,mode) {
	$.post("${pageContext.request.contextPath}/stock.htm?mode=productStock",
    {
		id: id
    }, function(data) {   
    	$('#balancel').show();
    	$('#balanceu').show();
		$('#lineTotal').html(numeral(data[0].unitLine).format('0,0.000'));
		$('#unitTotal').html(numeral(data[0].unitWeight).format('0,0.000'));
		$('#weight').val(data[0].weight);
		doProductPrice(id,mode);
    },'json');
}
function doProductPrice(id,mode) {
	var vat = $('input[name="vat"]:checked').val();
	var pid = $('#prid').val();
	$.post("${pageContext.request.contextPath}/price.htm?mode=productPriceByID",
    {
		id: id,pid:pid
    }, function(data) {   
    	var price = 0;
		if(vat == 'Y'){
			price = data[0].priceLessVat;
			$('#txtPrice').html('��ӡ��� 15 �ѹ �Ҥ� = '+data[0].priceLessVat+' �ҷ , �ҡ���� 15 �ѹ = '+data[0].priceOverVat+' �ҷ');
		}else{
			price = data[0].priceLess;		
			$('#txtPrice').html('��ӡ��� 15 �ѹ �Ҥ� = '+data[0].priceLess+' �ҷ , �ҡ���� 15 �ѹ = '+data[0].priceOver+' �ҷ');
		}   
		if(mode == 1){
			$('#unit').val(0);
	    	$('#line').val(0);
	    	$('#amount').val(0);
	    	$('#price').val(price);
		}    	
		

    },'json');
}
function  doPrice(mode) {
	var pid = $('#prid').val();
	var vat = $('input[name="vat"]:checked').val();
	var line = $('#line').val();
	var weight = $('#weight').val();
	var unit = $('#unit').val();
	var id = $('#product').val();
	var totalWeight = (line*weight)/1000;
	var totalLine = (unit/weight)*1000;
	$.post("${pageContext.request.contextPath}/price.htm?mode=productPriceByID",
		{
			id: id,pid:pid
		}, function(data) { 
			var price = 0;
			if(totalWeight > 15){
				if(vat == 'Y'){
					price = data[0].priceOverVat;
				}else{
					price = data[0].priceOver;
				}
			}else{
				if(vat == 'Y'){
					price = data[0].priceLessVat;
				}else{
					price = data[0].priceLess;
				}
			}
			
			if(mode == '1' ){				
				$('#unit').val(totalWeight.toFixed(3));
				$('#price').val(price.toFixed(3));
				$('#amount').val(((line*weight)*price).toFixed(3));
			}else if (mode == '2') {
				$('#line').val(totalLine.toFixed(0));
				$('#price').val(price.toFixed(3));
				$('#amount').val(((unit*1000)*price).toFixed(3));
			}
		   
		},'json');

}
function doEdit(id) {
  	document.transportForm.mode.value = 'productEdit';
	document.transportForm.pid.value = id;
    document.transportForm.submit();    
}
function doDeleteProduct(id) {
	 bootbox.dialog({
		 	size : "small",
			message: "�س��ͧ���ź��������¡�ù�����������?",
			title: "<span class='fred'>�׹�ѹ���ź������</span>",
			buttons: {
				ok: {
					label: "��ŧ",
			      	className: "btn-danger",
			      	callback: function() {
			      		document.transportForm.mode.value = 'productDelete';
						document.transportForm.pid.value = id;
						document.transportForm.submit();
					}
				},
			    calcel: {
			      	label: "¡��ԡ",
			      	className: "btn-default",
			      	callback: function() {
			      	}
			    }
			}
	});
}

</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�Դ����Թ��� <span class="navigator">[TR02]</span></h3>
				<span class="navigator" >����Թ��� > �Դ����Թ���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      	  <ul class="nav nav-tabs">			   
			    <li <c:if test="${tab eq '1' }">class="active"</c:if>><a data-toggle="tab" href="#menu1" id="m1">����Թ���</a></li>
			    <c:if test="${id ne null }">
			   	 	<li <c:if test="${tab eq '2' }">class="active"</c:if>><a data-toggle="tab" href="#menu2" id="m2">��������´����Թ���</a></li>
			    	<li <c:if test="${tab eq '3' }">class="active"</c:if>><a data-toggle="tab" href="#menu3" id="m3">��¡���Թ���</a></li>
			    </c:if>
			  </ul>
				<html:form action="/transport" styleId="transportForm" styleClass="form-horizontal">
				<html:hidden property="mode" />
				<html:hidden property="tab" />
				<html:hidden property="id" styleId="id"/>
				<html:hidden property="productId" styleId="pid"/>
				<html:hidden property="priceId" styleId="prid"/>
			  <div class="tab-content">	
			  
			  <!-- ����Թ��� -->		   
			    <div id="menu1" class="tab-pane fade <c:if test="${tab eq 1 }">in active</c:if>">
			      		<p></p>
			 			<div class="form-group">
							<label class="col-sm-2 control-label"> �ѹ���:</label>
							<div class="col-sm-3">
								<div class="input-group">
		                     		<html:text property="date" styleId="date" styleClass="form-control datepicker"></html:text>
		                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
		                   		</div>						
							</div>
							<label class="col-sm-2 control-label"> ��ѡ�ҹ���:</label>
							<div class="col-sm-3">
								<html:select property="sale" styleId="sale" styleClass="form-control"  >
									<html:option value="">���͡</html:option>
									<html:optionsCollection property="comboSaleList" value="id" label="fullname"/>
								</html:select>
							</div>
						</div>	
			        	<div class="form-group">
							<label class="col-sm-2 control-label"> �١���:</label>
							<div class="col-sm-3">
								<html:select property="cust" styleId="cust" styleClass="form-control"  onchange="doComboOrders(this.value);">
									<html:option value="">���͡</html:option>
									<html:optionsCollection property="comboCustomerList" value="id" label="name"/>
								</html:select>
							</div>
							<label class="col-sm-2 control-label"> ���觫���:</label>
							<div class="col-sm-3">
								<html:select property="orders" styleId="orders" styleClass="form-control"  >
									<html:option value="">���͡</html:option>
									<html:optionsCollection property="comboOrderList" value="id" label="codeShow"/>
								</html:select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3 col-sm-offset-2">
								<button type="submit" class="btn btn-primary">�Ѵ�</button>
								<button type="button" class="btn btn-default" onclick="doRefresh(1);">���������</button>
								<button type="button" class="btn btn-danger" onclick="doBack();">¡��ԡ</button>
							</div>							
						</div>
			    </div>
			    
			    <!-- ��������´ -->
			    <div id="menu2" class="tab-pane fade <c:if test="${tab eq 2 }">in active</c:if>">
			     	<p></p>	     					     	
						<div class="form-group">							
							<label class="col-sm-2 control-label">��â���:</label>
							<div class="col-sm-5">
							 	<label class="radio-inline">
						      		<html:radio property="transport" value="Y" styleId="transport1"  ></html:radio>�Ѻ�ͧ &nbsp;
						    	</label>
						   		<label class="radio-inline">
						     		<html:radio property="transport" value="N" styleId="transport2"  ></html:radio>�����&nbsp;	     
						   	 	</label>						   	 	
						    </div>			    
						</div>
						<div class="form-group" id="transports">							
							<label class="col-sm-2 control-label">��Ң���:</label>
							<div class="col-sm-3">
								<html:text property="transportPrice" styleId="transportPrice" styleClass="form-control" size="5" maxlength="7" ></html:text>	     
							</div>
						</div>
						<div class="form-group" id="car">							
							<label class="col-sm-2 control-label">ö����:</label>
							<div class="col-sm-3">
								<html:select property="car" styleClass="form-control">
									<html:option value="">���͡</html:option>
									<html:optionsCollection property="comboCarList" value="id" label="licensePlate"/>
								</html:select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">�������:</label>
						    <div class="col-sm-5">			 
						    	<label class="checkbox-inline">
                                	<html:checkbox property="sample" value="Y" ></html:checkbox>������  
                                	<html:hidden property="sample" value=""/>
                                </label>						    	
							    <label class="checkbox-inline">
							      	<html:checkbox property="brochure" value="Y" ></html:checkbox>�ê����   
							      	<html:hidden property="brochure" value=""/>
							    </label>
							    <label class="checkbox-inline">
							      	<html:checkbox property="card" value="Y"></html:checkbox>����� 
							      	<html:hidden property="card" value=""/>
							    </label>
						    </div>													
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"> �����˵�:</label>
							<div class="col-sm-3">
		                     	<html:textarea property="remark" styleClass="form-control"  rows="3" ></html:textarea>
							</div>							
						</div>	
						<div class="form-group">
							<div class="col-sm-3 col-sm-offset-2">
								<button type="submit" class="btn btn-primary">�Ѵ�</button>
								<button type="button" class="btn btn-default" onclick="doRefresh(2);">���������</button>
								<button type="button" class="btn btn-danger" onclick="doBack();">¡��ԡ</button>
							</div>							
						</div>			
						<div class="table-responsive">  
						 <c:if test="${fn:length(orderspList) > 0}">  
						 <h3>��¡����觫����Թ���</h3>               
					      <table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="result" >
							<thead>						
								<tr class="grey lighten-4">
									<th width="5%"><div align="center">�ӴѺ</div></th>
									<th width="15%">�Թ���</th>
									<th width="14%"><div align="right">�Ҥҵ�Ҵ/��. (�ҷ)</div></th>	
									<th width="12%"><div align="right">�ҤҢ��/��. (�ҷ)</div></th>								
									<th width="10%"><div align="center">�ӹǹ  (���)</div></th>	
									<th width="10%"><div align="center">�ӹǹ  (�ѹ)</div></th>									
									<th width="10%"><div align="right">�Ҥ���� (�ҷ)</div></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${orderspList}" var="item" varStatus="row">
								<tr>
									<td align="center">${row.index + 1}</td>
							   		<td>${item.product.productFullName }</td>		
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceId.price}" /></td>	
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceUnit}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0" value = "${item.line}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0.000" value = "${item.weight}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${item.price}" /></td>				 		
								</tr>
								</c:forEach>
								<tr>
									<td align="right" colspan="4">���</td>
									<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0" value = "${line}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${unit}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${amount}" /></label></td>						 		
								</tr>												
							</tbody>
						  </table>
						  </c:if>
				      	</div>											
			    </div>
			    
			    <!-- ��¡���Թ��� -->
			    <div id="menu3" class="tab-pane fade <c:if test="${tab eq 3 }">in active</c:if>">
			    	<p></p>
			    	
			      		<div class="form-group">
							<label class="col-sm-2 control-label"> �������Թ���:</label>
							<div class="col-sm-4">
								<html:select property="productType" styleId="productType" styleClass="form-control" onchange="doComboProduct(this.value);">
									<html:option value="">���͡</html:option>
									<html:optionsCollection property="comboProductTypeList" value="id" label="name"/>
								</html:select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"> �Թ���:</label>
							<div class="col-sm-4">
								<html:select property="product" styleId="product" styleClass="form-control" onchange="doProductStock(this.value,1);">
									<html:option value="">���͡</html:option>	
									<html:optionsCollection property="comboProductList" value="id" label="productFullName"/>								
								</html:select>
							</div>
						</div>	
						<div class="form-group" id="balancel">
							<label class="col-sm-2 control-label">�ӹǹ�������:</label>
							<div class="col-sm-2 control-text"><span id="lineTotal"></span> ���</div>
						</div>
						<div class="form-group" id="balanceu">
							<label class="col-sm-2 control-label">�ӹǹ�������:</label>
							<div class="col-sm-2 control-text"><span id="unitTotal"></span> �ѹ</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">�Ҥ�/��.:</label>
							<div class="col-sm-2">						
								<div class="input-group">
		                     		<html:text property="price" styleId="price" styleClass="form-control" ></html:text>
		                     		<span class="input-group-addon">�ҷ</span>
		                   		</div>
		                   		<html:hidden property="weight" styleId="weight"/>
							</div>
							<div class="col-sm-5">						
								<span id="txtPrice" class="red-text"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">�ӹǹ��觫���:</label>
							<div class="col-sm-2">						
								<div class="input-group">
		                     		<html:text property="line" styleId="line" styleClass="form-control" onkeyup="doPrice(1);"></html:text>
		                     		<span class="input-group-addon">���</span>
		                   		</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">�ӹǹ��觫���:</label>
							<div class="col-sm-2">						
								<div class="input-group">
		                     		<html:text property="unit" styleId="unit" styleClass="form-control" onkeyup="doPrice(2);"></html:text>
		                     		<span class="input-group-addon">�ѹ</span>
		                   		</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">�Ҥ����:</label>
							<div class="col-sm-2">						
								<div class="input-group">
		                     		<html:text property="amount" styleId="amount" styleClass="form-control" ></html:text>
		                     		<span class="input-group-addon">�ҷ</span>
		                   		</div>
							</div>
						</div>				
						<div class="form-group">
							<div class="col-sm-3 col-sm-offset-2">	
								<button type="button" class="btn btn-primary" onclick="doAddProduct(3);">������¡���Թ���</button>    								
			       			</div>
			       		</div>
			       		<div class="table-responsive">  
						 <c:if test="${fn:length(tProductList) > 0}">  
						 <h3>��¡����觫����Թ���</h3>               
					      <table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="result" >
							<thead>						
								<tr class="grey lighten-4">
									<th width="5%"><div align="center">�ӴѺ</div></th>
									<th width="15%">�Թ���</th>
									<th width="14%"><div align="right">�Ҥҵ�Ҵ/��. (�ҷ)</div></th>	
									<th width="12%"><div align="right">�ҤҢ��/��. (�ҷ)</div></th>								
									<th width="10%"><div align="center">�ӹǹ  (���)</div></th>	
									<th width="10%"><div align="center">�ӹǹ  (�ѹ)</div></th>									
									<th width="10%"><div align="right">�Ҥ���� (�ҷ)</div></th>
									<th width="8%"></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${tProductList}" var="item" varStatus="row">
								<tr>
									<td align="center">${row.index + 1}</td>
							   		<td>${item.product.productFullName }</td>		
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceId.price}" /></td>	
							   		<td align="right"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceUnit}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0" value = "${item.line}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,##0.000" value = "${item.weight}" /></td>
							 		<td align="right"><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${item.price}" /></td>				 		
									<td align="center">
							 			<a class="btn btn-info btn-xs" href="#" onclick="doEdit('${item.id}');"><i class="fa fa-pencil btn-info"></i></a>
										<a class="btn btn-danger btn-xs" href="#" onclick="doDeleteProduct('${item.id}');"><i class="fa fa-trash-o"></i></a>
							 		</td>
								</tr>
								</c:forEach>
								<tr>
									<td align="right" colspan="4">���</td>
									<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0" value = "${totalLineTP}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${totalUnitTP}" /></label> </td>
							 		<td align="right"><label><fmt:formatNumber type ="number" pattern = "#,###,###,##0.000" value = "${totalAmountTP}" /></label></td>						 		
									<td></td>	
								</tr>								
								<tr id="discountCash">
									<td align="right" colspan="5">��ǹŴ�Թʴ </td>
									<td align="right"><label >20 ʵ.</label> </td>
							 		<td align="right"><label id="cashTotal"></label></td>	
							 		<td></td>			 		
								</tr>	
								<tr id="discount300Ton">
									<td align="right" colspan="5">��ǹŴ 300 �ѹ</td>
									<td align="right"><label>10 ʵ.</label> </td>
							 		<td align="right"><label id="Ton300Total"></label></td>	
							 		<td></td>						 		
								</tr>	
								<tr id="discount500Ton">
									<td align="right" colspan="5">��ǹŴ 500 �ѹ </td>
									<td align="right"><label>20 ʵ.</label> </td>
							 		<td align="right"><label id="Ton500Total"></label></td>		
							 		<td></td>					 		
								</tr>	
								<tr id="discountS">
									<td align="right" colspan="5">��ǹŴ</td>
							 		<td align="right"><label id="discountP"></label> </td>
							 		<td align="right"><label id="discountM"></label></td>	
							 		<td></td>						 		
								</tr>
								<tr id="deposit">
									<td align="right" colspan="5">�Ѵ��</td>
							 		<td align="right"><label id="depositM"></label> </td>
							 		<td align="right"><label id="depositT"></label></td>	
							 		<td></td>						 		
								</tr>	
								<tr id="delivery">
									<td align="right" colspan="6">��Ң��� </td>
							 		<td align="right"><label id="deliveryM"></label></td>	
							 		<td></td>						 		
								</tr>
								<tr>
									<td align="right" colspan="6">�Ҥ����������</td>
							 		<td align="right"><label id="totalAmount"></label></td>		
							 		<td></td>					 		
								</tr>							
							</tbody>
						  </table>
						  </c:if>
				      	</div>	
						 	
				      	<div class="form-group center">
							<div class="col-sm-3 col-sm-offset-2">
								<c:if test="${fn:length(tProductList) > 0}">    
								   	<c:if test="${isCode}">
										<button type="button" class="btn btn-success"  onclick="doSave('4');">�ѹ�֡</button>  										     
									</c:if> 
									<c:if test="${!isCode}">
										<button type="button" class="btn btn-success"  onclick="doBack();">�ѹ�֡</button>  
									</c:if>  
								</c:if> 
								<button type="button" class="btn btn-info"  onclick="doBack();">��͹��Ѻ</button> 
					         	
					         	
					     	</div>
						</div> 
			    </div>
			  </div>

				
	       	</html:form>
	    </div>
	</div>
</div>
</div>
</body>
</html>