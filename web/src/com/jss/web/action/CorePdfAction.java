package com.jss.web.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.jss.web.common.ThaiJRPdfExporter;

public abstract class CorePdfAction extends DispatchActionSupport {

	protected String all = "\u0E17\u0E31\u0E49\u0E07\u0E2B\u0E21\u0E14";

	protected void executeReport(HttpServletRequest request, HttpServletResponse response, String reportURIPath, Map paramsMap, JRDataSource datasource, String fileName, boolean saveFile) throws IOException, ServletException, Exception {
		ServletContext context = request.getSession().getServletContext();
		File reportFile = new File(context.getRealPath(reportURIPath));
		if (!reportFile.exists()) {
			throw new JRRuntimeException("Report file '" + reportURIPath + "' not found.");
		}

		JasperPrint jasperPrint = null;

		try {
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramsMap, datasource);

		} catch (JRException e) {
			e.printStackTrace();
		}

		try {
			response.setContentType("application/pdf");
			if(saveFile){
				String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + strFileName + ".pdf");
				response.setCharacterEncoding("UTF-8");
			}
			ThaiJRPdfExporter pdfExporter = new ThaiJRPdfExporter();
			//JRPdfExporter exporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

			OutputStream ouputStream = response.getOutputStream();
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);

			try {
				pdfExporter.exportReport();
			} catch (JRException e) {
				throw new ServletException(e);
			} finally {
				if (ouputStream != null) {
					try {
						ouputStream.close();
					} catch (IOException ex) {
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	protected void executeReportList(HttpServletResponse response, List<JasperPrint> jasperPrint,String fileName, boolean saveFile) throws IOException, ServletException, Exception {
		try {
			response.setContentType("application/pdf");
			if(saveFile){
				String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + strFileName + ".pdf");
				response.setCharacterEncoding("UTF-8");
			}
			
			//ByteArrayOutputStream baos = new ByteArrayOutputStream();
			//JRPdfExporter exporter = new JRPdfExporter();
			
			ThaiJRPdfExporter pdfExporter = new ThaiJRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrint);
//			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
//			exporter.exportReport();

			OutputStream ouputStream = response.getOutputStream();
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
//			exporter.exportReport();
//			ouputStream.close();

			try {
				pdfExporter.exportReport();
			} catch (JRException e) {
				throw new ServletException(e);
			} finally {
				if (ouputStream != null) {
					try {
						ouputStream.close();
					} catch (IOException ex) {
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected JasperPrint getJasperPrint(HttpServletRequest request, String reportURIPath, Map paramsMap, JRDataSource datasource) throws IOException, ServletException, Exception {
		ServletContext context = request.getSession().getServletContext();

		File reportFile = new File(context.getRealPath(reportURIPath));
		if (!reportFile.exists()) {
			throw new JRRuntimeException("Report file '" + reportURIPath + "' not found.");
		}

		JasperPrint jasperPrint = null;

		try {
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramsMap, datasource);

		} catch (JRException e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}

	protected JasperPrint getJasperPrintSub(HttpServletRequest request, String reportURIPath, String[] subReportURIPath, Map paramsMap, JRDataSource datasource) throws IOException, ServletException, Exception {
		ServletContext context = request.getSession().getServletContext();

		File reportFile = new File(context.getRealPath(reportURIPath));
		if (!reportFile.exists()) {
			throw new JRRuntimeException("Report file '" + reportURIPath + "' not found.");
		}
		if (subReportURIPath != null && subReportURIPath.length > 0) {
			for (int i = 0; i < subReportURIPath.length; i++) {
				paramsMap.put("subReport_" + (i + 1), context.getRealPath(subReportURIPath[i]));
			}
		}
		
		JasperPrint jasperPrint = null;

		try {
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramsMap, datasource);

		} catch (JRException e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}


	protected void executeReport(HttpServletRequest request, HttpServletResponse response, String reportURIPath, String[] subReportURIPath, Map paramsMap, JRDataSource datasource) throws IOException, ServletException, Exception {
		ServletContext context = request.getSession().getServletContext();

		File reportFile = new File(context.getRealPath(reportURIPath));
		if (!reportFile.exists()) {
			throw new JRRuntimeException("Report file '" + reportURIPath + "' not found.");
		}

		if (subReportURIPath != null && subReportURIPath.length > 0) {
			for (int i = 0; i < subReportURIPath.length; i++) {
				paramsMap.put("subReport_" + (i + 1), context.getRealPath(subReportURIPath[i]));
			}
		}

		JasperPrint jasperPrint = null;

		try {
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramsMap, datasource);

		} catch (JRException e) {
			e.printStackTrace();
		}

		try {
			response.setContentType("application/pdf");
			
			ThaiJRPdfExporter pdfExporter = new ThaiJRPdfExporter();
			//JRPdfExporter exporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

			OutputStream ouputStream = response.getOutputStream();
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
			
			try {
				pdfExporter.exportReport();
			} catch (JRException e) {
				throw new ServletException(e);
			} finally {
				if (ouputStream != null) {
					try {
						ouputStream.close();
					} catch (IOException ex) {
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void executeReport(HttpServletRequest request, HttpServletResponse response, String reportURIPath, String[] subReportURIPath, Map paramsMap, JRDataSource datasource, boolean saveFile, String fileName) throws IOException, ServletException, Exception {
		ServletContext context = request.getSession().getServletContext();

		File reportFile = new File(context.getRealPath(reportURIPath));
		if (!reportFile.exists()) {
			throw new JRRuntimeException("Report file '" + reportURIPath + "' not found.");
		}

		if (subReportURIPath != null && subReportURIPath.length > 0) {
			for (int i = 0; i < subReportURIPath.length; i++) {
				paramsMap.put("subReport_" + (i + 1), context.getRealPath(subReportURIPath[i]));
			}
		}

		JasperPrint jasperPrint = null;

		try {
			JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			jasperPrint = JasperFillManager.fillReport(jasperReport, paramsMap, datasource);

		} catch (JRException e) {
			e.printStackTrace();
		}

		try {
			response.setContentType("application/pdf");
			
			if(saveFile){
				String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
				response.setHeader("Content-Disposition", "attachment; filename=" + fileName + strFileName + ".pdf");
				response.setCharacterEncoding("UTF-8");
			}
			
			ThaiJRPdfExporter pdfExporter = new ThaiJRPdfExporter();
			//JRPdfExporter exporter = new JRPdfExporter();
			pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

			OutputStream ouputStream = response.getOutputStream();
			pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
			
			try {
				pdfExporter.exportReport();
			} catch (JRException e) {
				throw new ServletException(e);
			} finally {
				if (ouputStream != null) {
					try {
						ouputStream.close();
					} catch (IOException ex) {
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void nodataReport(HttpServletResponse response) throws DataAccessException, Exception {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		out.println("<html>");
		out.println("<head>");
		out.println("<script language='javascript' type='text/javascript'>");
		out.println("function chkgoto(){");
		out.println("alert('Data not found.')");
		out.println("window.close();';");
		out.println("}</script>");
		out.println("</head>");
		out.println("<body onLoad='chkgoto();'>");
		out.println("</body>");
		out.println("</html>");
	}
}
