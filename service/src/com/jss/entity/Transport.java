package com.jss.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;
import com.edu.util.DateUtil;

@Entity
@Table(name = "transport")
public class Transport extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "transport_id") 
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "orders_id")
	private Orders orders;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@ManyToOne
	@JoinColumn(name = "sale_id")
	private User sale;
	
	@Column(name = "credit")
	private String credit;
	
	@Column(name = "bill")
	private String  bill;
	
	@Column(name = "discount")
	private String  disCount;
		
	@Column(name = "transport_date")
	private Date date;
	
	@Column(name = "transport_code")
	private String code;
	
	@ManyToOne
	@JoinColumn(name = "car_id")
	private Car car;
	
	@Column(name = "sample")
	private String sample;
	
	@Column(name = "brochure")
	private String brochure;
	
	@Column(name = "card")
	private String card;

	@Column(name = "status")
	private String status;
	
	@Column(name = "transport")
	private String transport;
	
	@Column(name = "transport_price")
	private Double transportPrice;
	
	@Column(name = "remark")
	private String remark;
	
	@Transient
	private String dateShow;
	@Transient
	private String orderCode;
	@Transient
	private String customerName;
	@Transient
	private String saleName;
	@Transient
	private Double ordersUnit;
	@Transient
	private Double ordersPrice;
	@Transient
	private Double transportUnit;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public User getSale() {
		return sale;
	}

	public void setSale(User sale) {
		this.sale = sale;
	}

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public String getBrochure() {
		return brochure;
	}

	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public Double getTransportPrice() {
		return transportPrice;
	}

	public void setTransportPrice(Double transportPrice) {
		this.transportPrice = transportPrice;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public Double getOrdersUnit() {
		return ordersUnit;
	}

	public void setOrdersUnit(Double ordersUnit) {
		this.ordersUnit = ordersUnit;
	}

	public Double getOrdersPrice() {
		return ordersPrice;
	}

	public void setOrdersPrice(Double ordersPrice) {
		this.ordersPrice = ordersPrice;
	}

	public Double getTransportUnit() {
		return transportUnit;
	}

	public void setTransportUnit(Double transportUnit) {
		this.transportUnit = transportUnit;
	}

	public String getDateShow() {
		String date = DateUtil.date2StringTh(getDate(), "dd/MM/yyyy");
		return date;
	}

	public void setDateShow(String dateShow) {
		this.dateShow = dateShow;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getBill() {
		return bill;
	}

	public void setBill(String bill) {
		this.bill = bill;
	}

	public String getDisCount() {
		return disCount;
	}

	public void setDisCount(String disCount) {
		this.disCount = disCount;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	
	
	
	
	
	
}
