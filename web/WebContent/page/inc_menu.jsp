<%@page contentType="text/html; charset=windows-874"%>
<%@page import="org.apache.struts.action.DynaActionForm"%>
<%@page import="com.jss.entity.User"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@ include file="inc_header.jsp"%>
<script defer
	src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<% 
	DynaActionForm ssLoginForm = (DynaActionForm) request.getSession().getAttribute("loginForm");
	User ssLoginUser = ssLoginForm != null ? (User) ssLoginForm.get("ssLoginUser") : null;
%>

<div id="wrapper">
	<nav class="navbar navbar-default cyan navbar-static-top"
		role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

		</div>
		<!-- /.navbar-header -->


		<%-- username top right --%>
		<ul class="nav navbar-top-links navbar-right ">
			<!-- /.dropdown -->
			<li><a class="white-text"><%= ssLoginUser.getFullname() %></a></li>
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> <i
					class="fa fa-user fa-fw white-text"></i> <i
					class="fa fa-caret-down white-text"></i>
			</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#"
						onclick="location.href='login.htm?mode=logout'"><i
							class="fa fa-user-circle-o"></i> ��������ǹ���</a></li>
					<li><a href="#"
						onclick="location.href='login.htm?mode=logout'"><i
							class="fa fa-sign-out fa-fw"></i> �͡�ҡ�к�</a></li>
				</ul> <!-- /.dropdown-user --></li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation" id="left_menu">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
					<li><a href="#" onclick="location.href='login.htm?mode=home'"><i
							class="fa fa-home fa-fw"></i>&nbsp; ˹���á</a></li>

					<li><a href="#" onclick="location.href='stock.htm?mode=index'"><i
							class="fas fa-utensils"></i> &nbsp; ��¡�����������</a></li>
					<li><a href="#" onclick="location.href='order.htm?mode=index'"><i
							class="fa fa-shopping-basket "></i>&nbsp; �С����Թ���</a></li>
					<li><a href="#"
						onclick="location.href='transport.htm?mode=index'"><i
							class="fa fa-motorcycle"></i>&nbsp; ��¡�èѴ�������</a></li>
					<li><a href="#"><i class="fas fa-file-alt"></i> ��§ҹ<span
							class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#"
								onclick="location.href='report.htm?mode=reportDaySales'">��§ҹ�ʹ��»�Ш��ѹ</a></li>
							<li><a href="#"
								onclick="location.href='report.htm?mode=reportWeekSales'">��§ҹ�ʹ���Ẻ���͡�ѹ</a></li>
							<li><a href="#"
								onclick="location.href='report.htm?mode=reportMonthlySales'">��§ҹ�ʹ��»�Ш���͹</a></li>
							<li><a href="#"
								onclick="location.href='report.htm?mode=reportMonthlyTransport'">��§ҹ��ҧ���Թ���</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#"><i class="fa fa-cog fa-fw"></i>
							�����ž�鹰ҹ<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#"
								onclick="location.href='stock.htm?mode=index'">�����������</a></li>
							<li><a href="#"
								onclick="location.href='master.htm?mode=customerAll'">�������١���</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#"><i class="fa fa-th"></i> �Է�������ҹ<span
							class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#"
								onclick="location.href='user.htm?mode=userGroup'">�����š���������ҹ�к�</a></li>
							<li><a href="#" onclick="location.href='user.htm?mode=user'">�����ż����ҹ�к�</a></li>
							<li><a href="#" onclick="location.href='user.htm?mode=menu'">����������</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#"
						onclick="location.href='customer.htm?mode=orderList'"><i
							class="fas fa-clone"></i> �ʴ���¡������÷����������</a></li>
					<li><a href="#"><i class="fa fa-th"></i> ˹����� GTT
							Noodle<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="#"
								onclick="location.href='customer.htm?mode=customer'">˹����ѡ</a></li>
							<li><a href="#"
								onclick="location.href='customer.htm?mode=allMenu'">cart</a></li>
							<%--    <li><a href="#" onclick="location.href='user.htm?mode=user'" >�����ż����ҹ�к�</a></li>
                                <li><a href="#" onclick="location.href='user.htm?mode=menu'" >����������</a></li>
                           --%>

						</ul> <!-- /.nav-second-level --></li>

				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side -->
	</nav>