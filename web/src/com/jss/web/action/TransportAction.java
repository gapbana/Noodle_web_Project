package com.jss.web.action;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.edu.util.LogHelper;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.Product;
import com.jss.entity.ProductType;
import com.jss.entity.Running;
import com.jss.entity.Transport;
import com.jss.entity.TransportProduct;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.CustomerService;
import com.jss.service.OrdersProductService;
import com.jss.service.OrdersService;
import com.jss.service.PriceService;
import com.jss.service.ProductService;
import com.jss.service.ProductTypeService;
import com.jss.service.RunningService;
import com.jss.service.TransportProductService;
import com.jss.service.TransportService;
import com.jss.service.UserService;

public class TransportAction extends CoreAction{	
	private TransportService transportService;
	private TransportProductService transportProductService;
	private CustomerService customerService;
	private UserService userService;
	private ProductTypeService productTypeService;
	private ProductService productService;
	private OrdersService ordersService;
	private OrdersProductService ordersProductService;
	private PriceService priceService;
	private RunningService runningService;
	private CarService carService;
	
	public ActionForward index(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		List<Customer> custList = customerService.getAllCust();
		List<User> saleList = userService.getSale();
		List<Orders> resultList = new ArrayList<>();
		dynaForm.set("comboCustomerList", custList);
		dynaForm.set("comboSaleList", saleList);		
		request.setAttribute("resultList", resultList);
		
		return mapping.findForward("TR01");
	}
	
	public ActionForward transportSearch (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		List<Transport> resultList = transportService.getTransportList(dynaForm.getString("sdate"), dynaForm.getString("edate"), dynaForm.getString("custId"), dynaForm.getString("saleId"), dynaForm.getString("orderCode"), dynaForm.getString("transportCode"));
		request.setAttribute("resultList", resultList);
		dynaForm.set("tab", "");
		
		return mapping.findForward("TR01");
	}
	
	public ActionForward transportAdd (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		String tab = "1";
		if(dynaForm.getString("tab") != null && !dynaForm.getString("tab").equals("")){
			tab = dynaForm.getString("tab");
		}
		List<Customer> custList = customerService.getAllCust();
		List<User> saleList = userService.getSale();
		List<Orders> ordersList = new ArrayList<>();
		List<ProductType> productTypeList = productTypeService.getAllProductType();
		List<Car> comboCarList = new ArrayList<>();
		List<Product> productList = new ArrayList<>();
		if(dynaForm.getString("cust") != null && !dynaForm.getString("cust").equals("")){
			ordersList = ordersService.getComboOrders(dynaForm.getString("cust"));
		}
		
		dynaForm.set("comboProductTypeList", productTypeList);
		dynaForm.set("comboProductList", productList);
		dynaForm.set("comboCustomerList", custList);
		dynaForm.set("comboSaleList", saleList);
		dynaForm.set("comboOrderList", ordersList);		
		dynaForm.set("comboCarList", comboCarList);		
		request.setAttribute("tab", tab);
		request.setAttribute("id", null);
		dynaForm.set("id", null);
		dynaForm.set("tab", tab);
		dynaForm.set("date", null);
		dynaForm.set("sale", null);
		dynaForm.set("cust", null);
		dynaForm.set("orders", null);
		
		return mapping.findForward("TR02");
	}
	public ActionForward transportEdit (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		String tab = "1";
		if(dynaForm.getString("tab") != null && !dynaForm.getString("tab").equals("")){
			tab = dynaForm.getString("tab");
		}
		Transport entity = transportService.getItem(Integer.parseInt(dynaForm.getString("id")));
		Orders order = ordersService.getItem(entity.getOrders().getId());
		List<ProductType> productTypeList = productTypeService.getAllProductType();
		List<Product> productList = new ArrayList<>();
		List<Customer> custList = customerService.getAllCust();
		List<Car> comboCarList = carService.getCarByCust(String.valueOf(entity.getCustomer().getId()));
		List<User> saleList = userService.getSale();
		List<Orders> comboOrdersList = ordersService.getComboOrders(String.valueOf(entity.getCustomer().getId()));
		
		List<OrdersProduct> orderspList = ordersProductService.getProductbyOrders(String.valueOf(order.getId())); 
		Integer line = 0;
		Double unit = 0.0;
		Double amount = 0.0;
		String priceId = "0";
		Double cashTotal = 0.0;
		Double Ton300Total = 0.0;
		Double Ton500Total = 0.0;
		Double totalAmount = 0.0;
		Integer discountP = 0;
		Double discountM = 0.0;
		Integer depositP = 0;
		Double depositM = 0.0;
		if(orderspList.size() > 0){
			for (OrdersProduct row : orderspList) {
				line += row.getLine();
				unit += row.getWeight();
				amount += row.getPrice();
				
				priceId = String.valueOf(row.getPriceId().getId());		
			}
		}
		
		totalAmount = amount;
			
		if(unit >= 300 && unit <500){
			Ton300Total = (unit*1000)*0.1;
			totalAmount = totalAmount-Ton300Total;
		}else if (unit >= 500) {
			Ton500Total = (unit*1000)*0.2;
			totalAmount = totalAmount-Ton500Total;
		}
		
		if(Integer.parseInt(order.getDiscount()) > 0 && Integer.parseInt(order.getDiscount()) <= 100){
			discountP = Integer.parseInt(order.getDiscount());
			discountM = (totalAmount*discountP)/100;
			totalAmount = totalAmount - discountM;
		}
		
		if(order.getTransport().equals("N")){
			totalAmount = totalAmount + order.getTransportPrice();
		}
		
		if(order.getPay().equals("C")){
			cashTotal = (unit*1000)*0.2;
			totalAmount = totalAmount-cashTotal;
		}else if (order.getPay().equals("D")) {
			depositP = Integer.parseInt(order.getPayPercent());
			depositM = (totalAmount*depositP)/100;
			totalAmount = totalAmount-depositM;
		}
	
		List<TransportProduct> tProductList = new ArrayList<>();
		if(dynaForm.getString("id") != null && !dynaForm.getString("id").equals("")){
			tProductList = transportProductService.getTransportProductByTransport(dynaForm.getString("id"));
		}
		
		Integer totalLineTP = 0;
		Double totalUnitTP = 0.0;
		Double totalAmountTP = 0.0;
		
		if(tProductList.size() > 0){
			for (TransportProduct row : tProductList) {
				totalLineTP += row.getLine();
				totalUnitTP += row.getWeight();
				totalAmountTP += row.getPrice();
								
			}
		}
		
		request.setAttribute("line", line);
		request.setAttribute("unit", unit);
		request.setAttribute("amount", amount);
		request.setAttribute("totalAmount", totalAmount);
		request.setAttribute("orderspList", orderspList);
		request.setAttribute("cashTotal", cashTotal);
		request.setAttribute("Ton300Total", Ton300Total);
		request.setAttribute("Ton500Total", Ton500Total);
		request.setAttribute("discountP", discountP);
		request.setAttribute("discountM", discountM);
		request.setAttribute("depositP", depositP);
		request.setAttribute("depositM", depositM);
		request.setAttribute("transport", order.getTransport());
		request.setAttribute("transportM", order.getTransportPrice());
		
		request.setAttribute("totalLineTP", totalLineTP);
		request.setAttribute("totalUnitTP", totalUnitTP);
		request.setAttribute("totalAmountTP", totalAmountTP);
		request.setAttribute("tProductList", tProductList);
		
		request.setAttribute("credit", order.getCredit());
		request.setAttribute("discount", order.getDiscount());
		
		dynaForm.set("tab", tab);
		dynaForm.set("id", String.valueOf(entity.getId()));
		dynaForm.set("priceId",priceId);
		dynaForm.set("comboProductTypeList", productTypeList);
		dynaForm.set("comboProductList", productList);
		dynaForm.set("comboCustomerList", custList);
		dynaForm.set("comboSaleList", saleList);
		dynaForm.set("comboOrderList", comboOrdersList);
		dynaForm.set("comboCarList", comboCarList);	
		
		dynaForm.set("date", DateUtil.date2StringTh(entity.getDate(), "dd/MM/yyyy"));
		dynaForm.set("sale", String.valueOf(entity.getSale().getId()));
		dynaForm.set("cust", String.valueOf(entity.getCustomer().getId()));
		dynaForm.set("orders", String.valueOf(entity.getOrders().getId()));		
		dynaForm.set("book", order.getBook());
		dynaForm.set("bookPrice", String.valueOf(order.getBookPrice()));
		dynaForm.set("bill", order.getBill());
		dynaForm.set("pay", order.getPay());
		dynaForm.set("payPercent", order.getPayPercent());
		dynaForm.set("transport", entity.getTransport());
		String transportPrice = "0";
		if(entity.getTransportPrice() != null && !entity.getTransportPrice().equals("")){
			transportPrice = String.valueOf(entity.getTransportPrice());
		}
		dynaForm.set("transportPrice", transportPrice);
		dynaForm.set("sample",entity.getSample());
		dynaForm.set("brochure",entity.getBrochure());
		dynaForm.set("card",entity.getCard());
		dynaForm.set("vat",order.getVat());
		dynaForm.set("remark",entity.getRemark());

		request.setAttribute("tab", tab);
		request.setAttribute("id", entity.getId());
		request.setAttribute("book", order.getBook());
		request.setAttribute("pay", order.getPay());
		
		
		return mapping.findForward("TR02");
	}
	
	public ActionForward transportSave (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Integer transportId = null;
		String tab = "1";
		if(dynaForm.getString("tab") != null && !dynaForm.getString("tab").equals("")){
			tab = dynaForm.getString("tab");
		}
		
		if(dynaForm.getString("id") != null && !dynaForm.getString("id").equals("")){
			transportId = Integer.valueOf(dynaForm.getString("id"));
			Transport entity = transportService.getItem(Integer.parseInt(dynaForm.getString("id")));
			if(tab.equals("1")){
				
				entity.setDate(DateUtil.string2DateTh(dynaForm.getString("date")));
				entity.setSale(userService.getItem(Integer.parseInt(dynaForm.getString("sale"))));
				entity.setCustomer(customerService.getItem(Integer.parseInt(dynaForm.getString("cust"))));
				entity.setOrders(ordersService.getItem(Integer.parseInt(dynaForm.getString("orders"))));
				entity.setStatus("1");
				/*entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				transportService.updateItem(entity);
			}else if(tab.equals("2")){
				if(dynaForm.getString("car") != null && !dynaForm.getString("car").equals(""))
					entity.setCar(carService.getItem(Integer.parseInt(dynaForm.getString("car"))));		
				if(dynaForm.getString("transport") != null && !dynaForm.getString("transport").equals(""))
					entity.setTransport(dynaForm.getString("transport"));
				if(dynaForm.getString("transportPrice") != null && !dynaForm.getString("transportPrice").equals(""))
					entity.setTransportPrice(Double.valueOf(dynaForm.getString("transportPrice")));
				if(dynaForm.getString("remark") != null && !dynaForm.getString("remark").equals(""))
					entity.setRemark(dynaForm.getString("remark"));
				if(dynaForm.getString("sample") != null && !dynaForm.getString("sample").equals(""))
					entity.setSample(dynaForm.getString("sample"));
				if(dynaForm.getString("brochure") != null && !dynaForm.getString("brochure").equals(""))
					entity.setBrochure(dynaForm.getString("brochure"));
				if(dynaForm.getString("card") != null && !dynaForm.getString("card").equals(""))
					entity.setCard(dynaForm.getString("card"));	
				
				/*entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				
				transportService.updateItem(entity);
			
			}else if(tab.equals("3")){
				tab = "2";
				if(dynaForm.getString("product") != null && !dynaForm.getString("product").equals("")){
					if(dynaForm.getString("productId") != null && !dynaForm.getString("productId").equals("")){
						TransportProduct product = transportProductService.getItem(Integer.parseInt(dynaForm.getString("productId")));
						product.setTransport(transportService.getItem(Integer.parseInt(dynaForm.getString("id"))));
						product.setProduct(productService.getItem(Integer.parseInt(dynaForm.getString("product"))));
						product.setPriceUnit(Double.valueOf(dynaForm.getString("price")));
						product.setLine(Integer.parseInt(dynaForm.getString("line")));
						product.setWeight(Double.valueOf((dynaForm.getString("unit"))));
						product.setPrice(Double.valueOf(dynaForm.getString("amount")));
				/*		product.setUpdateBy(getUserSession(request).getFullname());
						product.setUpdateDate(DateUtil.getCurrentDate());*/
						transportProductService.updateItem(product);
					}else {
						TransportProduct product = new TransportProduct();
						product.setTransport(transportService.getItem(Integer.parseInt(dynaForm.getString("id"))));
						product.setProduct(productService.getItem(Integer.parseInt(dynaForm.getString("product"))));
						product.setPriceUnit(Double.valueOf(dynaForm.getString("price")));
						product.setPriceId(priceService.getItem(Integer.parseInt(dynaForm.getString("priceId"))));
						product.setLine(Integer.parseInt(dynaForm.getString("line")));
						product.setWeight(Double.valueOf((dynaForm.getString("unit"))));
						product.setPrice(Double.valueOf(dynaForm.getString("amount")));
						/*product.setCreateBy(getUserSession(request).getFullname());
						product.setCreateDate(DateUtil.getCurrentDate());*/
						transportProductService.saveItem(product);
					}
					
					dynaForm.set("productId", "");
					dynaForm.set("productType", "");
					dynaForm.set("product", "");
					dynaForm.set("price", "");
					dynaForm.set("priceId", "");
					dynaForm.set("line", "");
					dynaForm.set("unit", "");
					dynaForm.set("amount", "");
					
				}			
			}else if(tab.equals("4")){
				tab = "1";
				Date date = new Date();
				SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
				SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
				int year = Integer.parseInt(sdfYear.format(date)) + 43;
				String month = sdfMonth.format(date);
				List<Running> check = runningService.getRunningCode("TR",String.valueOf(year),  month);
				
				String code = "";
				if(check.size() == 0){
					Running running = new Running();
					running.setType("TR");
					running.setYear(String.valueOf(year));
					running.setMonth(month);
					running.setRunning("001");
				/*	running.setCreateBy(getUserSession(request).getFullname());
					running.setCreateDate(DateUtil.getCurrentDate());*/
					runningService.saveItem(running);
					code = "TR"+String.valueOf(year)+month+"001";
				}else {
					Running running = check.iterator().next(); 
					int numbers = Integer.parseInt(running.getRunning())+1;
					String number = String.format("%03d", numbers);
					
					running.setRunning(number);
			/*		running.setUpdateBy(getUserSession(request).getFullname());
					running.setUpdateDate(DateUtil.getCurrentDate());*/
					runningService.updateItem(running);
					
					code = running.getType()+running.getYear()+running.getMonth()+number;
				}
								
				entity.setStatus("2");
				entity.setCode(code);
			/*	entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				transportService.updateItem(entity);
	
				return transportSearch(mapping, dynaForm, request, response);
			}
			
			boolean isCode = true;
			if(entity.getCode() != null && !entity.getCode().equals("")){
				isCode = false;
			}
			request.setAttribute("isCode", isCode);
			
			
		}else{
			Transport entity = new Transport();
			entity.setDate(DateUtil.string2DateTh(dynaForm.getString("date")));
			entity.setSale(userService.getItem(Integer.parseInt(dynaForm.getString("sale"))));
			entity.setCustomer(customerService.getItem(Integer.parseInt(dynaForm.getString("cust"))));
			entity.setOrders(ordersService.getItem(Integer.parseInt(dynaForm.getString("orders"))));
			entity.setStatus("1");
			/*entity.setCreateBy(getUserSession(request).getFullname());
			entity.setCreateDate(DateUtil.getCurrentDate());*/
			transportId =  transportService.saveTransport(entity);
		}

		dynaForm.set("id", String.valueOf(transportId));
		dynaForm.set("tab", String.valueOf(Integer.valueOf(tab)+1));
		request.setAttribute("id", transportId);
		request.setAttribute("tab", Integer.valueOf(tab)+1);
		
		
		return transportEdit(mapping, dynaForm, request, response);
	}
	
	public ActionForward transportDel (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<TransportProduct> entityList = transportProductService.getTransportProductByTransport(dynaForm.getString("id"));
			transportProductService.removeItems(entityList);
			transportService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
			dynaForm.set("id", null);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return transportSearch(mapping, form, request, response);
	}
	
	public ActionForward productEdit (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;	
			String tab = "1";
			if(dynaForm.getString("tab") != null && !dynaForm.getString("tab").equals("")){
				tab = dynaForm.getString("tab");
			}
			if(dynaForm.getString("productId") != null && !dynaForm.getString("productId").equals("")){
				TransportProduct entity = transportProductService.getItem(Integer.parseInt(dynaForm.getString("productId")));
				List<Product> productList = productService.getProductList(String.valueOf(entity.getProduct().getProductType().getId()));
				dynaForm.set("comboProductList", productList);
				
				dynaForm.set("productType", String.valueOf(entity.getProduct().getProductType().getId()));
				dynaForm.set("product", String.valueOf(entity.getProduct().getId()));
				dynaForm.set("line", String.valueOf(entity.getLine()));
				dynaForm.set("unit", String.valueOf(entity.getWeight()));
				dynaForm.set("price", String.valueOf(entity.getPriceUnit()));
				dynaForm.set("amount", String.valueOf(entity.getPrice()));
				
				request.setAttribute("product", entity.getProduct().getId());
	
			}
			Transport transport = transportService.getItem(Integer.parseInt(dynaForm.getString("id")));
			Orders order = ordersService.getItem(transport.getOrders().getId());
			
			List<ProductType> productTypeList = productTypeService.getAllProductType();			
			List<Customer> custList = customerService.getAllCust();
			List<User> saleList = userService.getSale();
			List<TransportProduct> tProductList = transportProductService.getTransportProductByTransport(dynaForm.getString("id"));
			List<Orders> comboOrdersList = ordersService.getComboOrders(String.valueOf(transport.getCustomer().getId()));
			
			dynaForm.set("comboProductTypeList", productTypeList);
			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			request.setAttribute("id", transport.getId());
			request.setAttribute("tab", "3");
			Integer line = 0;
			Double unit = 0.0;
			Double amount = 0.0;
			Double totalAmount = 0.0;
			Double cashTotal = 0.0;
			Double Ton300Total = 0.0;
			Double Ton500Total = 0.0;
			Integer discountP = 0;
			Double discountM = 0.0;
			Integer depositP = 0;
			Double depositM = 0.0;
			String priceId = "";
			if(tProductList.size() > 0){
				for (TransportProduct row : tProductList) {
					line += row.getLine();
					unit += row.getWeight();
					amount += row.getPrice();
					
					priceId = String.valueOf(row.getPriceId().getId());		
				}
			}
			
			totalAmount = amount;
				
			if(unit >= 300 && unit <500){
				Ton300Total = (unit*1000)*0.1;
				totalAmount = totalAmount-Ton300Total;
			}else if (unit >= 500) {
				Ton500Total = (unit*1000)*0.2;
				totalAmount = totalAmount-Ton500Total;
			}
			
			if(Integer.parseInt(order.getDiscount()) > 0 && Integer.parseInt(order.getDiscount()) <= 100){
				discountP = Integer.parseInt(order.getDiscount());
				discountM = (totalAmount*discountP)/100;
				totalAmount = totalAmount - discountM;
			}
			
			if(order.getTransport().equals("N")){
				totalAmount = totalAmount + order.getTransportPrice();
			}
			
			if(order.getPay().equals("C")){
				cashTotal = (unit*1000)*0.2;
				totalAmount = totalAmount-cashTotal;
			}else if (order.getPay().equals("D")) {
				depositP = Integer.parseInt(order.getPayPercent());
				depositM = (totalAmount*depositP)/100;
				totalAmount = totalAmount-depositM;
			}
			
			
			request.setAttribute("totalLineTP", line);
			request.setAttribute("totalUnitTP", unit);
			request.setAttribute("totalAmountTP", totalAmount);
			request.setAttribute("tProductList", tProductList);
			
			request.setAttribute("cashTotal", cashTotal);
			request.setAttribute("Ton300Total", Ton300Total);
			request.setAttribute("Ton500Total", Ton500Total);
			request.setAttribute("discountP", discountP);
			request.setAttribute("discountM", discountM);
			request.setAttribute("depositP", depositP);
			request.setAttribute("depositM", depositM);
			request.setAttribute("transport", order.getTransport());
			request.setAttribute("transportM", order.getTransportPrice());
				
			
			dynaForm.set("tab", tab);
			dynaForm.set("id", String.valueOf(transport.getId()));
			dynaForm.set("priceId",priceId);
			dynaForm.set("comboProductTypeList", productTypeList);			
			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			dynaForm.set("comboOrderList", comboOrdersList);		
			dynaForm.set("date", DateUtil.date2StringTh(transport.getDate(), "dd/MM/yyyy"));
			dynaForm.set("sale", String.valueOf(transport.getSale().getId()));
			dynaForm.set("cust", String.valueOf(transport.getCustomer().getId()));
			dynaForm.set("orders", String.valueOf(transport.getOrders().getId()));
			dynaForm.set("credit", order.getCredit());
			dynaForm.set("book", order.getBook());
			dynaForm.set("bookPrice", String.valueOf(order.getBookPrice()));
			dynaForm.set("bill", order.getBill());
			dynaForm.set("pay", order.getPay());
			dynaForm.set("payPercent", order.getPayPercent());
			dynaForm.set("discount", order.getDiscount());
			dynaForm.set("transport", order.getTransport());
			dynaForm.set("transportPrice", String.valueOf(order.getTransportPrice()));
			dynaForm.set("sample",order.getSample());
			dynaForm.set("brochure",order.getBrochure());
			dynaForm.set("card",order.getCard());
			dynaForm.set("vat",order.getVat());
			dynaForm.set("remark",order.getRemark());

			request.setAttribute("tab", tab);
			request.setAttribute("book", order.getBook());
			request.setAttribute("pay", order.getPay());
			

		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("TR02");
	}
	public ActionForward productDelete (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;	
			
			if(dynaForm.getString("productId") != null && !dynaForm.getString("productId").equals("")){
				transportProductService.removeItem(Integer.parseInt(dynaForm.getString("productId")));
			}
			
			dynaForm.set("tab", "3");
			
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return transportEdit(mapping, form, request, response);
	}
	
	public ActionForward transport (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;	
			
			if(dynaForm.getString("id") != null && !dynaForm.getString("id").equals("")){
				Transport entity = transportService.getItem(Integer.parseInt(dynaForm.getString("id")));
				entity.setStatus("3");
		/*		entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				transportService.updateItem(entity);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return transportSearch(mapping, form, request, response);
	}
	
public ActionForward transportDetail (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		Transport entity = transportService.getItem(Integer.parseInt(dynaForm.getString("id")));
		Orders order = ordersService.getItem(entity.getOrders().getId());
		List<TransportProduct> productList = transportProductService.getTransportProductByTransport(dynaForm.getString("id"));
		Integer totalLine = 0;
		Double totalUnit = 0.0;
		Double totalAmount = 0.0;
		if(productList.size() > 0){
			for (TransportProduct row : productList) {
				totalLine += row.getLine();
				totalUnit += row.getWeight();
				totalAmount += row.getPrice();
			}
		}
		request.setAttribute("totalLine", totalLine);
		request.setAttribute("totalUnit", totalUnit);
		request.setAttribute("totalAmount", totalAmount);
		request.setAttribute("productList", productList);
		
		dynaForm.set("book", order.getBook());
		dynaForm.set("bookPrice", String.valueOf(order.getBookPrice()));
		dynaForm.set("bill", order.getBill());
		dynaForm.set("pay", order.getPay());
		dynaForm.set("payPercent", order.getPayPercent());
		dynaForm.set("transport", order.getTransport());
		dynaForm.set("transportPrice", String.valueOf(order.getTransportPrice()));
		dynaForm.set("sample", order.getSample());
		dynaForm.set("brochure", order.getBrochure());
		dynaForm.set("card", order.getCard());
		dynaForm.set("vat", order.getVat());
		
		request.setAttribute("transportCode", entity.getCode());
		request.setAttribute("orderCode", order.getCode());
		request.setAttribute("date", entity.getDateShow());
		request.setAttribute("custName", entity.getCustomer().getName());
		request.setAttribute("saleName", entity.getSale().getFullname());
		request.setAttribute("credit", order.getCredit());
		request.setAttribute("book", order.getBook());
		request.setAttribute("pay", order.getPay());
		request.setAttribute("discount", order.getDiscount()+"%");
		request.setAttribute("transport", order.getTransport());
		request.setAttribute("remark", order.getRemark());
		request.setAttribute("discount", order.getDiscount());
		return mapping.findForward("TR03");
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void printPricePdf(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			Transport entity = transportService.getItem(Integer.parseInt(request.getParameter("id")));
			List<TransportProduct> resultList = new ArrayList<TransportProduct>();
			List<TransportProduct> tProductList = transportProductService.getTransportProductByTransport(request.getParameter("id"));
			Orders order = ordersService.getItem(entity.getOrders().getId());
			
			DecimalFormat df = new DecimalFormat();
			df.applyPattern("#,##0.000");
			DecimalFormat df2 = new DecimalFormat();
			df2.applyPattern("#,##0");
			
			Integer line = 0;
			Double unit = 0.0;
			Double amount = 0.0;
			Double totalAmount = 0.0;
			
			int i= 1;
			if(tProductList.size() > 0){
				for (TransportProduct row : tProductList) {
					TransportProduct po = new TransportProduct();
					po.setColSeq(String.valueOf(i));

					po.setColPrice(df.format(row.getPriceId().getPrice()));
					po.setColPriceUnit(df.format(row.getPriceUnit()));
					po.setColLine(df2.format(row.getLine()));
					po.setColUnit(df.format(row.getWeight()));
					po.setColTotalPrice(df.format(row.getPrice()));
					resultList.add(po);
					
					line += row.getLine();
					unit += row.getWeight();
					amount += row.getPrice();
					
					i++;
				}
				if(i < 11){
					for (int j = 0; j < 8; j++) {	
						TransportProduct po = new TransportProduct();
						po.setColSeq("");
						po.setColName("");
						po.setColPrice("");
						po.setColPriceUnit("");
						po.setColLine("");
						po.setColUnit("");
						po.setColTotalPrice("");
						resultList.add(po);
					}
				}
					
			}else {
				if(i < 11){
					for (int j = 0; j < 8; j++) {
						TransportProduct po = new TransportProduct();
						po.setColSeq("");
						po.setColName("");
						po.setColPrice("");
						po.setColPriceUnit("");
						po.setColLine("");
						po.setColUnit("");
						po.setColTotalPrice("");
						resultList.add(po);
					}
				}
			}
			
			totalAmount = amount;
			
			Double Ton300Total = 0.0;
			Double Ton500Total = 0.0;
			if(unit >= 300 && unit < 500){
				Ton300Total = (unit*1000)*0.1;
				totalAmount = totalAmount-Ton300Total;
			}else if (unit >= 500) {
				Ton500Total = (unit*1000)*0.2;
				totalAmount = totalAmount-Ton500Total;
			}
			
			Double totalDiscount = 0.0;
			Integer discount = Integer.parseInt(order.getDiscount());
			if(discount > 0 && discount <= 100){
				totalDiscount = (totalAmount*discount)/100;
				totalAmount = totalAmount-totalDiscount;	
			}
			
			Double cashTotal = 0.0;
			Double deposit = 0.0;
			if(order.getPay().equals("C")){
				cashTotal = (unit*1000)*0.2;
				totalAmount = totalAmount-cashTotal;				
			}else if (order.getPay().equals("D")) {
				deposit = (totalAmount*Integer.parseInt(order.getPayPercent()))/100;
				totalAmount = totalAmount-deposit;	
			}
			
			
			if(order.getTransport().equals("N")){
				totalAmount = totalAmount + order.getTransportPrice();
			}
			
			HashMap<String,String> paramsMap = new HashMap<String,String>();	
			
			paramsMap.put("transportCode", entity.getCode());
			paramsMap.put("orderCode", order.getCode());
			paramsMap.put("transportDate", entity.getDateShow());
		/*	paramsMap.put("date", DateUtil.date2StringTh(entity.getCreateDate()));*/
			paramsMap.put("customerName", entity.getCustomer().getName());
			paramsMap.put("saleName", entity.getSale().getFullname());
			paramsMap.put("book", order.getBook());
			paramsMap.put("bookPrice", df.format(order.getBookPrice()));
			paramsMap.put("bill", order.getBill());
			paramsMap.put("credit", order.getCredit());
			paramsMap.put("pay", order.getPay());
			paramsMap.put("transport", entity.getTransport());
			paramsMap.put("transportPrice", df.format(entity.getTransportPrice()));
			paramsMap.put("car", entity.getCar().getLicensePlate());
			paramsMap.put("vat", order.getVat());
			paramsMap.put("sample", entity.getSample());
			paramsMap.put("brochure", entity.getBrochure());
			paramsMap.put("card", entity.getCard());		
			paramsMap.put("line", df2.format(line));
			paramsMap.put("unit", df.format(unit));
			paramsMap.put("amount", df.format(amount));
			paramsMap.put("payPercent", order.getPayPercent());
			paramsMap.put("cashTotal",df.format(cashTotal));
			paramsMap.put("deposit", df.format(deposit));
			paramsMap.put("Ton300Total", df.format(Ton300Total));
			paramsMap.put("Ton500Total", df.format(Ton500Total));
			paramsMap.put("discount", order.getDiscount());
			paramsMap.put("totalTransport", df.format(order.getTransportPrice()));
			paramsMap.put("totalDiscount", df.format(totalDiscount));
			paramsMap.put("totalAmount", df.format(totalAmount));
			paramsMap.put("remark", entity.getRemark());
			paramsMap.put("img", "/checked.png");
			
			String path =  "/pdf/transport.jasper";
			JRDataSource dataSource = new JRBeanCollectionDataSource(resultList);
			executeReport(request, response, path, paramsMap, dataSource ,"/pdf/transport.jasper", false);
			
		} catch (Exception e) {
			LogHelper.doError(e.getMessage(), e);
		}
	}

	public TransportService getTransportService() {
		return transportService;
	}

	public void setTransportService(TransportService transportService) {
		this.transportService = transportService;
	}

	public TransportProductService getTransportProductService() {
		return transportProductService;
	}

	public void setTransportProductService(
			TransportProductService transportProductService) {
		this.transportProductService = transportProductService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public OrdersProductService getOrdersProductService() {
		return ordersProductService;
	}

	public void setOrdersProductService(OrdersProductService ordersProductService) {
		this.ordersProductService = ordersProductService;
	}

	public PriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}

	public RunningService getRunningService() {
		return runningService;
	}

	public void setRunningService(RunningService runningService) {
		this.runningService = runningService;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}
	

	
}
