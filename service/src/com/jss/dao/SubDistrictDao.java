package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.SubDistrict;

public interface SubDistrictDao extends CoreDao<SubDistrict, Serializable> {

	public List<SubDistrict> findSubDistrictbyDistrict(String distId) throws DataAccessException;
}
