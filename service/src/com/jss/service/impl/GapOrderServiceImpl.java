package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.GapOrderDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapOrder;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.GapCustomerService;
import com.jss.service.GapOrderService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class GapOrderServiceImpl extends CoreServiceImpl<GapOrder, Serializable> implements GapOrderService {

	private GapOrderDao gapOrderDao;

	public GapOrderServiceImpl(GapOrderDao gapOrderDao) {
		super(gapOrderDao);
		this.gapOrderDao = gapOrderDao;
	}


	@Override
	public List<GapOrder> getOrder() throws DataAccessException {
		return gapOrderDao.findOrders();
	}


	@Override
	public List<GapOrder> getOrderToday() throws DataAccessException {
		return gapOrderDao.findOrderToday();
	}


	@Override
	public List<GapOrder> getOrderSelect(String startDay, String endDay)
			throws DataAccessException {
		return gapOrderDao.findOrderSelect(startDay, endDay);
	}


	@Override
	public List<GapOrder> getOrderMonth(String year, String month)
			throws DataAccessException {
		return gapOrderDao.findOrderMonth(year, month);
	}



	
}
