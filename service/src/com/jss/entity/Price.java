package com.jss.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;
import com.edu.util.DateUtil;

@Entity
@Table(name = "price")
public class Price extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "price_id") 
	private int id;
	
	@Column(name = "price")
	private Double price;

	public Price() {
	}

	public Price(int id) {
		this.setId(id);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Transient
	private String createDateShow;
	
	@Transient
	private int createDateShowYear;
	
	@Transient
	private String createDateShowMonth;

/*	public String getCreateDateShow() {
		return DateUtil.date2StringTh(getCreateDate());
	}
*/
	public void setCreateDateShow(String createDateShow) {
		this.createDateShow = createDateShow;
	}
	
/*	public int getCreateDateShowYear() {
		return DateUtil.yearOfDate(getCreateDate());
	}
*/
	public void setCreateDateShowYear(int createDateShowYear) {
		this.createDateShowYear = createDateShowYear;
	}

/*	public String getCreateDateShowMonth() {
		return DateUtil.monthOfDate(getCreateDate());
	}*/

	public void setCreateDateShowMonth(String createDateShowMonth) {
		this.createDateShowMonth = createDateShowMonth;
	}
	
	
	
	
}
