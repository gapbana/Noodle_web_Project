<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp"%>




<script language="javascript" type="text/javascript">

	function doSearch() {
		document.userForm.mode.value = 'userSearch';
		document.userForm.submit();
	}
	
	function doEdit(id) {
		document.userForm.mode.value = 'userEdit';
		document.userForm.id.value = id;   //��ͧ��ID
		document.userForm.submit();
	}
	
	
	
	



</script>
</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�����ż����ҹ�к� <span class="navigator">[US03]</span></h3>
				<span class="navigator">�Է�������ҹ > �����ż����ҹ�к�</span><br>
				<br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/user" styleId="userForm" styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"> ����������ҹ:</label>
						<div class="col-sm-3">
							<html:select property="userGroupName" styleClass="form-control">
								<html:option value="">������</html:option>
								<html:optionsCollection property="comboUserGroup" value="id"
									label="name" />
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">����:</label>
						<div class="col-sm-3">
							<html:text property="fullname" styleId="fullname"
								styleClass="form-control"></html:text>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"> ʶҹС����ҹ:</label>
						<div class="col-sm-2">
							<html:select property="status" styleClass="form-control">
								<html:option value="">������</html:option>
								<html:option value="1">��ҹ</html:option>
								<html:option value="0">�Դ�����ҹ</html:option>
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">

							<button type="button" class="btn btn-info" onclick="doSearch();">����</button>
							<button type="button" class="btn btn-default"
								onclick="location.href='user.htm?mode=user'">���������</button>
							<button type="button" class="btn btn-success"
								onclick="location.href='user.htm?mode=userAdd'">����������</button>


						</div>
					</div>
				</html:form>
			</div>
		</div>

		<c:if test="${fn:length(resultList)> 0 }">
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result">

				<thead>
					<tr class="cyan lighten-4">
						<th width="40">�ӴѺ</th>
						<th width="200">���͡���������ҹ�к�</th>
						<th>���� - ���ʡ�� (�������)</th>
						<th>���ͼ����ҹ�к�</th>
						<th><div align="center">ʶҹ�</div></th>
						<th width="100"></th>
					</tr>
				</thead>
				<tbody>					
					<c:forEach items="${resultList}" var="item" varStatus="row">
						<tr>
							<td align="center">${row.index + 1 }</td>
							<td align="left">${item.userGroup.name}</td>
							<td align="left">${item.firstname} ${item.lastname} <c:if test="${item.nickname ne null }">(${item.nickname})</c:if>
							</td>
							<td align="left">${item.username}</td>
							<td class="text-center"><c:if test="${item.status eq '1'}">��ҹ </c:if>
								<c:if test="${item.status eq '0'}">�״�����ҹ </c:if>
							<td class="text-center">
								<a href="#!" class="blue-text text-darken-2" onclick="doEdit('${item.id}')">
									<i class="mdi-editor-mode-edit"></i>
									<i class="fa fa-pencil-square-o fa-2x"></i>
								</a> 
								<a href="#!" class="materialize-red-text text-darken-3" onclick="doDelete('${item.id}')">
									<i class="mdi-action-delete"><i class="fa fa-trash fa-2x"></i>
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
</body>
</html>