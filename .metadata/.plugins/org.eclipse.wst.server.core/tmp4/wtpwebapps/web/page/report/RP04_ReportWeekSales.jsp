<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">

function doSearch() {
	document.reportForm.mode.value = 'reportMonthlySalesSearch';
    document.reportForm.submit();    
}
function doExport() {
	document.reportForm.mode.value = 'exportExcelReport1';
    document.reportForm.submit();    
}


function doRefresh() {
	location.href='report.htm?mode=reportMonthlySales';
}

</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">��§ҹ�ʹ���Ẻ���͡�ѹ  <span class="navigator">[RP01]</span></h3>
				<span class="navigator" >��§ҹ > ��§ҹ�ʹ���Ẻ���͡�ѹ</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
				<html:form action="/report" styleId="reportForm" styleClass="form-horizontal">
				<html:hidden property="mode"/>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"> ��͹:</label>
					<div class="col-sm-3">
						<html:select property="month"  styleClass="form-control">
							<html:option value="">������</html:option>
							<html:optionsCollection property="comboMonth" value="value" label="label"/>
						</html:select>	
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label"> ������ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="sdate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
					<label class="col-sm-2 control-label"> �֧�ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="edate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
				</div>	
									
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="button" class="btn btn-info" onclick="doSearch();">����</button>
						<button type="button" class="btn btn-default" onclick="doRefresh();">���������</button>
						<c:if test="${resultSize > 0}">  
							<button type="button" class="btn btn-primary" onclick="doExport();">�������§ҹ</button>
						</c:if>
					</div>
				</div>
	         	</html:form>
	     	</div>
 		</div>  
 		<c:if test="${resultSize > 0}">  
 		<div class="table-responsive">                     
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="5%"><div align="center">�ӴѺ</div></th>
						<th width="12%"><div align="center">�ѹ���</div></th>
						<th width="10%"><div align="center">�Ţ������觫���</div></th>
						<th width="20%">�١���</th>
						<th width="10%">��ѡ�ҹ���</th>
						<th width="12%"><div align="right">�ʹ��� (�ѹ)</div></th>	
						<th width="12%"><div align="right">�ʹ��� (�ҷ)</div></th>			
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
					<tr >
						<td align="center">${row.index + 1}</td>
						<td align="center">${item.dateShow }</td>
						<td align="center">${item.code }</td>
					    <td>${item.custName }</td>
					 	<td>${item.saleName }</td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${item.totalPrice}" /></td>
					</tr>
					</c:forEach>	
					<tr >
						<td align="right" colspan="5"><label>���������</label></td>						
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalUnit}" /></td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${totalPrice}" /></td>
					</tr>	
				</tbody>
			</table>
		</div>
		</c:if>
  
</body>
</html>