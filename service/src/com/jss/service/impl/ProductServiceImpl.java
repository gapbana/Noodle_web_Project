package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.ProductDao;
import com.jss.entity.Product;
import com.jss.service.ProductService;

public class ProductServiceImpl extends CoreServiceImpl<Product, Serializable>implements ProductService {

	private ProductDao productDao;
	public ProductServiceImpl(ProductDao productDao) {
		super(productDao);
		this.productDao = productDao;
	}
	@Override
	public List<Product> getProductList(String productTypeId) throws DataAccessException {
		
		return productDao.findProductList(productTypeId);
	}
	@Override
	public List<Product> getProductListPrice(String price) throws DataAccessException {
		return productDao.findProductListPrice(price);
	}
	@Override
	public List<Product> getProductByCode(String code)throws DataAccessException {
		return productDao.findProductByCode(code);
	}
	@Override
	public List<Product> getProductStockList(String productId,String productTypeId, String productCode, String productModel)throws DataAccessException {
		return productDao.findProductStockList(productId, productTypeId, productCode, productModel);
	}
	
}
