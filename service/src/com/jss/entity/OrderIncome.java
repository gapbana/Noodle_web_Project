package com.jss.entity;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "OrderIncome")

public class OrderIncome extends CoreEntity {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	
	@Column(name = "order_id") 
	private int id;
	
	@Column(name = "order_date")
	private Date date;
	
	@Column(name = "order_time")
	private String time;
	
	@Column(name = "customer_id")
	private int cusId;
	
	@Column(name = "sale_id")
	private int saleId;
	
	@Column(name = "order_code")
	private String code;
	
	@Column(name = "table")
	private String table;

	@Column(name = "status")
	private String status;
	
	
	@Column(name = "discount")
	private double discount;
	
	@Column(name = "pay")
	private int pay;
	
	@Column(name = "pay_type")
	private int pay_type;

	//@JoinColumn sale id
	//@JoinColumn cust id
	

	
}
