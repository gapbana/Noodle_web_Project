package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.PermissionDao;
import com.jss.entity.Permission;

public class PermissionDaoImpl extends CoreDaoImpl<Permission, Serializable> implements PermissionDao {

	public PermissionDaoImpl(Class<Permission> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Permission> findPermissionByGroup(String groupId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Permission.class);
		criteria.createAlias("userGroup", "userGroup");
		criteria.createAlias("menu", "menu");

		criteria.addOrder(Order.asc("menu.code"));

		if (groupId != null && !groupId.equals("")) {
			criteria.add(Restrictions.eq("userGroup.id", Integer.parseInt(groupId)));
		}

		return getHibernateTemplate().findByCriteria(criteria);
	}

	

}