package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CustomerDao;
import com.jss.entity.Customer;
import com.jss.service.CustomerService;

public class CustomerServiceImpl extends CoreServiceImpl<Customer, Serializable>implements CustomerService {

	private CustomerDao customerDao;
	public CustomerServiceImpl(CustomerDao customerDao) {
		super(customerDao);
		this.customerDao = customerDao;
	}
	@Override
	public List<Customer> getCustomersSearch(String name, String rank)throws DataAccessException {
		return customerDao.findCustomersSearch(name, rank);
	}
	@Override
	public List<Customer> getAllCust() throws DataAccessException {
		return customerDao.findAllCust();
	}
	
}
