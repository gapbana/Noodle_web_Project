package com.jss.service;

import java.io.Serializable;

import com.edu.core.service.CoreService;
import com.jss.entity.Province;

public interface ProvinceService extends CoreService<Province, Serializable> {
	
	//public List getEmployeePrefixNum() throws DataAccessException;
}
