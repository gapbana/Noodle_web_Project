<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<%@ include file="../boostrap.jsp"%>
<%@ include file="../inc_menu2.jsp" %>
<%@ include file="../inc_header.jsp" %>


<script language="javascript" type="text/javascript">
function doAdd() {
	/* document.forms[1].mode.value = 'registerSave';
	document.forms[1].submit();  */
	alert("บันทึกเข้าสู่ระบบแล้ว");
}
</script>


<div class="col-lg-12">
				<h3 class="page-header" align="center">
					ตระกร้าสินค้า <i class="fa fa-shopping-basket "></i>
					<br><br>
					</h3>
					</div>
					
<div class="container" >
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>สินค้า</th>
                        <th>จำนวน</th>
                        <th class="text-center">ราคา</th>
                        <th class="text-center">รวม</th>
                        <th class="text-center">#</th>
                    </tr>
                    
                </thead>
                <tbody>
              <c:forEach items="${resultList}" var="item" varStatus="row">
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src="https://food.mthai.com/app/uploads/2016/10/Tomyum.jpg" style="width: 120px;"> </a>
                            <div class="media-body">
                                <h4 class="media-heading">&nbsp;${item.soup}</a></h4>
                                <h5 class="media-heading">&nbsp; เส้น : <a href="#">${item.noodle}</a></h5>
                                <span>&nbsp;
                                <c:if test="${item.vet eq '1'}">ผัก : ใส่ผัก</c:if>
                                <c:if test="${item.vet eq '0'}">ผัก : ไม่ใส่ผัก</c:if>
                                </span>                             
                        </div>
                        </div></td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                      
                        <input type="email" class="form-control" id="count" value="${item.qty}">
                        
                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>${item.price}</strong></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>${item.price*item.qty}</strong></td>
                        <td class="col-sm-1 col-md-1" align="center">
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button></td>
                    </tr>
        </c:forEach>
                    
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h5>รวม</h5></td>
                        <td class="text-right"><h5><strong>$24.59</strong></h5></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h5>ค่าจัดส่ง</h5></td>
                        <td class="text-right"><h5><strong>free</strong></h5></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><h3>รวมทั้งหมด</h3></td>
                        <td class="text-right"><h3><strong>$31.53</strong></h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span> เลือกอาหารต่อ
                        </button></td>
                        <td>
                        <button type="button" class="btn btn-success">
                         	   ชำระเงิน <span class="glyphicon glyphicon-play"></span>
                        </button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
