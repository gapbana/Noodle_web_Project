package com.jss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "product_food")
public class ProductFood  extends CoreEntity{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	
	
	@Column(name = "name") 
	private String name;
	
	@Column(name = "price") 
	private int price;
	
	@Column(name = "product_id") 
	private int pro_id;
	
	@Column(name = "product_type") 
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getId() {
		return pro_id;
	}

	public void setId(int id) {
		this.pro_id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
