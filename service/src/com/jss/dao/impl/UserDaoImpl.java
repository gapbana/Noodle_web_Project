package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.UserDao;
import com.jss.entity.User;

public class UserDaoImpl extends CoreDaoImpl<User, Serializable> implements
		UserDao {

	public UserDaoImpl(Class<User> entityClass) {
		super(entityClass);
	}

	@Override
	public List<User> findName(String fullname , String userGroup , String status ) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		criteria.createAlias("userGroup", "userGroup");  //join table
	
		if (fullname != null && !fullname.equals("")) {
			criteria.add(Restrictions.or(Restrictions.like("firstname", "%" + fullname + "%"), 
					Restrictions.or(Restrictions.like("lastname", "%" + fullname + "%"), 
					Restrictions.like("nickname", "%" + fullname + "%"))));
		}
		
		if (userGroup != null && ! userGroup.equals("")) {
			criteria.add(Restrictions.eq("userGroup.id", Integer.parseInt(userGroup)));
		}
		
		if (status != null && !status.equals("")) {
			criteria.add(Restrictions.like("status", status));

		}
		return getHibernateTemplate().findByCriteria(criteria);

	}

	@Override
	public List<User> findLogin(String un, String pwd) throws DataAccessException {
			DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
	

			criteria.add(Restrictions.eq("username", un));			
			criteria.add(Restrictions.eq("password", pwd));
			criteria.add(Restrictions.eq("status", "1"));

		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<User> findSale() throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		criteria.createAlias("userGroup", "userGroup"); 
		criteria.addOrder(Order.asc("firstname"));	

		criteria.add(Restrictions.eq("userGroup.id", 3));

	return getHibernateTemplate().findByCriteria(criteria);
	}



}