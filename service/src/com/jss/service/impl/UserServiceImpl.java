package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.UserDao;
import com.jss.entity.User;
import com.jss.service.UserService;

public class UserServiceImpl extends CoreServiceImpl<User, Serializable>
		implements UserService {

	private UserDao userDao;

	public UserServiceImpl(UserDao userDao) {
		super(userDao);
		this.userDao = userDao;
	}


	@Override
	public List<User> getName(String fullname,String userGroup, String status) throws DataAccessException {
		// TODO Auto-generated method stub
		return userDao.findName(fullname,userGroup,status);
	}


	@Override
	public List<User> getLogin(String un, String pwd) throws DataAccessException {
		return userDao.findLogin(un, pwd);
	}


	@Override
	public List<User> getSale() throws DataAccessException {
		return userDao.findSale();
	}
}
