package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Stock;

public interface StockService extends CoreService<Stock, Serializable> {
	
	public List<Stock> getStockByProduct(String productId) throws DataAccessException;
	
}
