package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Product;
import com.jss.entity.Stock;


public interface ProductDao extends CoreDao<Product, Serializable> {
	

	public List<Product> findProductList(String productTypeId) throws DataAccessException;
	public List<Product> findProductListPrice(String price) throws DataAccessException;
	public List<Product> findProductByCode(String code) throws DataAccessException;
	public List<Product> findProductStockList(String productId,String productTypeId, String productCode,String productModel) throws DataAccessException;
	
}
