package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.RunningDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Running;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.RunningService;
import com.jss.service.UserService;

public class RunningServiceImpl extends CoreServiceImpl<Running, Serializable> implements RunningService {

	private RunningDao runningDao;

	public RunningServiceImpl(RunningDao runningDao) {
		super(runningDao);
		this.runningDao = runningDao;
	}

	@Override
	public List<Running> getRunningCode(String type,String year, String month)throws DataAccessException {
		return runningDao.findRunningCode(type,year, month);
	}

}
