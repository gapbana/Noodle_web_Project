package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Menu;
import com.jss.entity.Permission;
import com.jss.entity.UserGroup;

public interface MenuDao extends CoreDao<Menu, Serializable> {

	public List<Menu> findAllMenu() throws DataAccessException;
}
