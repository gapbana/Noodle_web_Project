package com.jss.web.action;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.dao.DataAccessException;

import com.edu.util.DateUtil;;

public class CoreExcelAction extends CoreAction {

	public static final int TOTAL_COLUMN_COORDINATE_POSITIONS = 1023; // MB
	public static final int TOTAL_ROW_COORDINATE_POSITIONS = 255; // MB
	public static final int PIXELS_PER_INCH = 96; // MB
	public static final double PIXELS_PER_MILLIMETRES = 3.78; 
	public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
	public static final int UNIT_OFFSET_LENGTH = 7;
	public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
	public static String fontName = "TH SarabunPSK";
	

	public static char setChar(int column) {
		return (char) (65 + column);
	}

//	public static void executeCsv(HttpServletResponse response, String txt) throws Exception {
//
//		if (txt != null) {
//			response.setContentType("application/text");
//			String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
//			response.setHeader("Content-Disposition", "attachment; filename=" + strFileName + ".csv");
//			response.setCharacterEncoding("UTF-8");
//
//			OutputStream out = response.getOutputStream();
//			out.write(txt.getBytes());
//			out.close();
//		} else {
//			excelNotFound(response);
//		}
//	}

	public static void executeExcel(HttpServletResponse response, XSSFWorkbook wb) throws Exception {

		if (wb != null) {
			response.setContentType("application/ms-excel");
			String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
			response.setHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
			response.setCharacterEncoding("UTF-8");

			OutputStream out = response.getOutputStream();
			wb.write(out);
			out.close();
		} else {
			excelNotFound(response);
		}
	}

//	public static void executeExcel(HttpServletResponse response, HSSFWorkbook wb) throws Exception {
//
//		if (wb != null) {
//			response.setContentType("application/ms-excel");
//			String strFileName = DateUtil.getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash();
//			response.setHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xls");
//			response.setCharacterEncoding("UTF-8");
//
//			OutputStream out = response.getOutputStream();
//			wb.write(out);
//			out.close();
//		} else {
//			excelNotFound(response);
//		}
//	}

	public static void excelNotFound(HttpServletResponse response) throws DataAccessException, Exception {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		out.println("<html>");
		out.println("<head>");
		out.println("<script language='javascript' type='text/javascript'>");
		out.println("function chkgoto(){");
		out.println("alert('Excel : File Not Found.');");
		out.println("window.close();");
		out.println("}</script>");
		out.println("</head>");
		out.println("<body onLoad='chkgoto();'>");
		out.println("</body>");
		out.println("</html>");
	}

	public static XSSFSheet getSheet(XSSFWorkbook wb, String sheetName, Boolean foot) {
		XSSFSheet sheet = wb.createSheet(sheetName);
//		sheet.getPrintSetup().setFitWidth((short) 1);
		sheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);

		sheet.setMargin(XSSFSheet.TopMargin, .40);
		sheet.setMargin(XSSFSheet.BottomMargin, .40);
		sheet.setMargin(XSSFSheet.LeftMargin, .40);
		sheet.setMargin(XSSFSheet.RightMargin, .40);
		sheet.getPrintSetup().setHeaderMargin(.00);
		sheet.getPrintSetup().setFooterMargin(.00);
		
		// fitWidth Height auto
		sheet.setFitToPage(true);
		sheet.getPrintSetup().setFitWidth((short) 1);
		sheet.getPrintSetup().setFitHeight((short) 0);

		if (foot) {
			Footer footer = sheet.getFooter();
			footer.setLeft(HSSFFooter.font(fontName, "Normal")
					+ HSSFFooter.fontSize((short) 10)
					+ "\u0E23\u0E32\u0E22\u0E07\u0E32\u0E19\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25 \u0E13 \u0E27\u0E31\u0E19\u0E17\u0E35\u0E48 "
					+ DateUtil.date2StringTh(DateUtil.getCurrentDate(), "d MMMM yyyy")
					+ "  \u0E40\u0E27\u0E25\u0E32 "
					+ DateUtil.date2StringTh(DateUtil.getCurrentDate(), "hh:mm"));
			footer.setRight(HSSFFooter.font(fontName, "Normal") + HSSFFooter.fontSize((short) 10)
					+ "\u0E2B\u0E19\u0E49\u0E32\u0E17\u0E35\u0E48 " + HSSFFooter.page()
					+ " \u0E08\u0E32\u0E01 " + HSSFFooter.numPages());
		}
		return sheet;
	}
	
	public static void getFooterTable(XSSFWorkbook wb, XSSFSheet sheet,int row, int column) {
		XSSFCellStyle style = wb.createCellStyle();
		style.setBorderTop(XSSFCellStyle.NO_FILL);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		
		XSSFRow rowi = sheet.createRow(row);
		rowi.setHeight((short) 50);
		for(int f = 0; f <= column; f++){			
			rowi.createCell(f).setCellStyle(style);
		}
	}
	
	public static XSSFCellStyle getStyle(XSSFWorkbook wb, int fontSize, boolean bold, String type, String align, String vertical, String bgColor, boolean wrapText, String border) {
		XSSFFont f = wb.createFont();
		if (bold) f.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);		
		f.setFontHeightInPoints((short) fontSize);
		f.setFontName(fontName);


		XSSFCellStyle style = wb.createCellStyle();
		style.setFont(f);
		
		if (align != null && align.equals("L")) { // left
			style.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		} else if (align != null && align.equals("R")) { // right
			style.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		} else if (align != null && align.equals("C")) { // center
			style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		} else if (align != null && align.equalsIgnoreCase("J")) { // JUSTIFY
			style.setAlignment(XSSFCellStyle.ALIGN_JUSTIFY);
		}

		if (vertical != null && vertical.equalsIgnoreCase("C")) {
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		} else if (vertical != null && vertical.equalsIgnoreCase("B")) {
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		} else if (vertical != null && vertical.equalsIgnoreCase("T")) {
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
		} else if (vertical != null && vertical.equalsIgnoreCase("J")) {
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_JUSTIFY);
		} else {
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		}

		style.setWrapText(wrapText);
		
		if(bgColor != null){
			if (bgColor.equals("Y")) { // yellow
				style.setFillForegroundColor(new XSSFColor(Color.decode("#fffeaa")));
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("G")) { // green
				style.setFillForegroundColor(new XSSFColor(Color.decode("#aaffc0")));
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("T")) { // turquoise
				style.setFillForegroundColor(new XSSFColor(Color.decode("#86d3ff")));
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("B")) { // turquoise
				style.setFillForegroundColor(new XSSFColor(Color.decode("#c4ebff")));
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			}
		}

		if (border != null) {
			if(border.equals("L")){
				style.setBorderTop(XSSFCellStyle.BORDER_THIN);
				style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
				style.setBorderRight(XSSFCellStyle.BORDER_THIN);
			}if(border.equals("D")){
				style.setBorderTop(XSSFCellStyle.BORDER_DOTTED);
				style.setBorderBottom(XSSFCellStyle.NO_FILL);
				style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
				style.setBorderRight(XSSFCellStyle.BORDER_THIN);
			}
			if(border.equals("De")){
				style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
				style.setBorderRight(XSSFCellStyle.BORDER_THIN);
			}
			if(border.equals("B")){
				style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			}if(border.equals("U")){
				style.setBorderTop(XSSFCellStyle.NO_FILL);
				style.setBorderBottom(XSSFCellStyle.NO_FILL);
				style.setBorderLeft(XSSFCellStyle.NO_FILL);
				style.setBorderRight(XSSFCellStyle.NO_FILL);
			}

		}

		XSSFDataFormat format = wb.createDataFormat();
		if (type != null && type.equals("I")) { // integer
			style.setDataFormat(format.getFormat("_(#,###,##0_);"));
		} else if (type != null && type.equals("D")) { // double
			style.setDataFormat(format.getFormat("_(#,###,##0.00_);"));
		} else if (type != null && type.equals("Y")) { // date
			style.setDataFormat(format.getFormat("[$-1070000]dd/mm/yyyy;@"));
		}
		return style;
	}

	public static XSSFFont getXSSFFont(XSSFWorkbook wb, int fontHeightInPoints, String bold) throws Exception {
		try {
			XSSFFont font = wb.createFont();
			font.setFontHeightInPoints((short) fontHeightInPoints);
			font.setFontName(fontName);
			if (bold != null && !bold.equals("") && bold.equals("B")) {
				font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			}
			return font;
		} catch (Exception e) {
			return null;
		}
	}

	public static XSSFCellStyle getFrame(XSSFCellStyle style, String frame) {
		if (frame != null && frame.indexOf("T") >= 0) {
			style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		}
		if (frame != null && frame.indexOf("B") >= 0) {
			style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		}
		if (frame != null && frame.indexOf("L") >= 0) {
			style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		}
		if (frame != null && frame.indexOf("R") >= 0) {
			style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		}
		return style;
	}

	public static HSSFCellStyle getFrame(HSSFCellStyle style, String top, String bottom, String left, String right) {
		if (top != null && top.equals("Y")) {
			style.setBorderTop(XSSFCellStyle.BORDER_THIN);

		} else {
			style.setBorderTop(XSSFCellStyle.NO_FILL);
		}
		if (bottom != null && bottom.equals("Y")) {
			style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		} else {
			style.setBorderBottom(XSSFCellStyle.NO_FILL);
		}
		if (left != null && left.equals("Y")) {
			style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		} else {
			style.setBorderLeft(XSSFCellStyle.NO_FILL);
		}
		if (right != null && right.equals("Y")) {
			style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		} else {
			style.setBorderRight(XSSFCellStyle.NO_FILL);
		}
		return style;
	}
	
	public static HSSFCellStyle getStyleHSSF(HSSFWorkbook wb, int fontSize, boolean bold, String type, String align, String vertical, String bgColor, boolean wrapText, String border) {
		HSSFFont f = wb.createFont();
		if (bold) f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);		
		f.setFontHeightInPoints((short) fontSize);
		f.setFontName(fontName);


		HSSFCellStyle style = wb.createCellStyle();
		style.setFont(f);
		
		if (align != null && align.equals("L")) { // left
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		} else if (align != null && align.equals("R")) { // right
			style.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		} else if (align != null && align.equals("C")) { // center
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		} else if (align != null && align.equalsIgnoreCase("J")) { // JUSTIFY
			style.setAlignment(HSSFCellStyle.ALIGN_JUSTIFY);
		}

		if (vertical != null && vertical.equalsIgnoreCase("C")) {
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		} else if (vertical != null && vertical.equalsIgnoreCase("B")) {
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_BOTTOM);
		} else if (vertical != null && vertical.equalsIgnoreCase("T")) {
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		} else if (vertical != null && vertical.equalsIgnoreCase("J")) {
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_JUSTIFY);
		} else {
			style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		}

		style.setWrapText(wrapText);
		
		if(bgColor != null){
			if (bgColor.equals("Y")) { // yellow
				style.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("G")) { // green
				style.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("T")) { // turquoise
				style.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			} else if (bgColor.equals("B")) { // turquoise
				style.setFillForegroundColor(HSSFColor.LAVENDER.index);
				style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			}
		}

		if (border != null) {
			if(border.equals("L")){
				style.setBorderTop(XSSFCellStyle.BORDER_THIN);
				style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
				style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
				style.setBorderRight(XSSFCellStyle.BORDER_THIN);
			}if(border.equals("D")){
				style.setBorderTop(XSSFCellStyle.BORDER_DOTTED);
				style.setBorderBottom(XSSFCellStyle.NO_FILL);
				style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
				style.setBorderRight(XSSFCellStyle.BORDER_THIN);
			}

		}

		HSSFDataFormat format = wb.createDataFormat();
		if (type != null && type.equals("I")) { // integer
			style.setDataFormat(format.getFormat("_(#,###,##0_);"));
		} else if (type != null && type.equals("D")) { // double
			style.setDataFormat(format.getFormat("_(#,###,##0.00_);"));
		} else if (type != null && type.equals("Y")) { // date
			style.setDataFormat(format.getFormat("[$-1070000]dd/mm/yyyy;@"));
		}
		return style;
	}

	public static int loadImg(String path, XSSFWorkbook wb, String imgType) throws IOException {
		int pictureIndex;
		int pictureType = 0;
		FileInputStream fis = null;
		ByteArrayOutputStream bos = null;
		try {
			fis = new FileInputStream(path);
			bos = new ByteArrayOutputStream();
			int c;
			while ((c = fis.read()) != -1)
				bos.write(c);
			if (imgType.equalsIgnoreCase("JPEG") || imgType.equalsIgnoreCase("JPG"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_JPEG;
			else if (imgType.equalsIgnoreCase("PNG"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_PNG;
			else if (imgType.equalsIgnoreCase("DIB"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_DIB;
			else if (imgType.equalsIgnoreCase("EMF"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_EMF;
			else if (imgType.equalsIgnoreCase("PICT"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_PICT;
			else if (imgType.equalsIgnoreCase("WMF"))
				pictureType = XSSFWorkbook.PICTURE_TYPE_WMF;

			pictureIndex = wb.addPicture(bos.toByteArray(), pictureType);

		} finally {
			if (fis != null)
				fis.close();
			if (bos != null)
				bos.close();
		}
		return pictureIndex;
	}

	/**
	 * pixel units to excel width units(units of 1/256th of a character width)
	 * 
	 * @param pxs
	 * @return
	 */

	public static short pixel2WidthUnits(int pxs) {
		short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH));
		widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];
		return widthUnits;
	}

	public static int pixel2WidthExcel(int pxs) {
		int widthUnits = pxs * 32;
		return widthUnits;
	}

	/**
	 * excel width units(units of 1/256th of a character width) to pixel units
	 * 
	 * @param widthUnits
	 * @return
	 */
	public static int widthUnits2Pixel(short widthUnits) {
		int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH;
		int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR;
		pixels += Math.round((float) offsetWidthUnits
				/ ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));
		return pixels;

	}

	public static void createRowForMerged(XSSFSheet sheet, int beginRow, int endRow, int beginCell,
			int endCell, XSSFCellStyle cellStyle) {

		for (int i = beginRow; i <= endRow; i++) {
			XSSFRow rH = sheet.createRow(i);
			createCellForMerged(rH, beginCell, endCell, cellStyle);
		}

	}

	public static void createCellForMerged(XSSFRow rH, int beginCell, int endCell, XSSFCellStyle cellStyle) {
		for (int i = beginCell; i <= endCell; i++) {
			XSSFCell cH = rH.createCell(i);
			if (cellStyle != null) {
				cH.setCellStyle(cellStyle);
			}
		}
	}

	public static void createCellForMerged(HSSFRow rH, int beginCell, int endCell, HSSFCellStyle cellStyle) {
		for (int i = beginCell; i <= endCell; i++) {
			HSSFCell cH = rH.createCell(i);
			if (cellStyle != null) {
				cH.setCellStyle(cellStyle);
			}
		}
	}

	public static void sheetAutoSize(XSSFSheet sheet, int start, int stop) {
		for (int i = start; i <= stop; i++) {
			sheet.autoSizeColumn(i);
		}
	}
}
