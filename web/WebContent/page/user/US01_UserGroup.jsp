<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp"%>
<script language="javascript" type="text/javascript">
	$(document).ready(funuserGroupction() {
		$('#userForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				groupName : {
					validators : {
						notEmpty : {
							message : '��س��к�: ���͡���������ҹ�к�'
						}
					}
				},
			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			$("button[name='btnSave']").attr('disabled', true);

			document.userForm.mode.value = 'userGroupSave';
			document.userForm.submit();
		});
	});
</script>

<script language="javascript" type="text/javascript">
	function doEdit(id) {
		document.userForm.mode.value = 'userGroupEdit';
		document.userForm.id.value = id;
		document.userForm.submit();
	}
	function doPermission(id) {
		document.userForm.mode.value = 'permit';
		document.userForm.id.value = id;
		document.userForm.submit();
	}
	function doDelete(id) {
		bootbox.dialog({
			size : "small",
			message : "�س��ͧ���ź��������¡�ù�����������?",
			title : "<span class='fred'>�׹�ѹ���ź������</span>",
			buttons : {
				ok : {
					label : "��ŧ",
					className : "btn-danger",
					callback : function() {
						document.userForm.mode.value = 'userGroupDel';
						document.userForm.id.value = id;
						document.userForm.submit();
					}
				},
				calcel : {
					label : "¡��ԡ",
					className : "btn-default",
					callback : function() {
					}
				}
			}
		});
	}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�����š���������ҹ�к� <span class="navigator">[US01]</span></h3>
				<span class="navigator">�Է�������ҹ >
					�����š���������ҹ�к�</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/user" styleId="userForm" styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"><span class="red-text">* </span> ���͡���������ҹ:</label>
						<div class="col-sm-3">
							<html:text property="groupName" styleClass="form-control"></html:text>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-info">�ѹ�֡</button>
							<button type="button" class="btn btn-danger"
								onclick="location.href='user.htm?mode=userGroup'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
			</div>
		</div>

		<c:if test="${fn:length(resultList) > 0}">
			<table width="99%" cellpadding="0" cellspacing="0" border="0"
				class="table table-striped table-bordered" id="result">
				<thead>
					<tr class="cyan lighten-4">
						<th width="40">�ӴѺ</th>
						<th>���͡���������ҹ�к�</th>
						<th width="100"></th>
						<th width="100"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${resultList}" var="item" varStatus="row">
						<tr>
							<td align="center">${row.index + 1}</td>
							<td>${item.name}</td>
							<td align="center">
								<button type="button" class="btn btn-success btn-xs" onclick="doPermission('${item.id}')">��˹��Է���</button></td>
							<td align="center">
								<a class="btn btn-info btn-xs" href="#" onclick="doEdit('${item.id}')">
									<i class="fa fa-pencil btn-info"></i>
								</a> 
								<a class="btn btn-danger btn-xs" href="#" onclick="doDelete('${item.id}')">
									<i class="fa fa-trash-o"></i>
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
</body>
</html>