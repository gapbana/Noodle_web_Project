package com.jss.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.edu.util.DateUtil;
import com.jss.dao.PriceDao;
import com.jss.dao.PriceHistoryDao;
import com.jss.entity.Price;
import com.jss.entity.PriceHistory;
import com.jss.entity.Product;
import com.jss.entity.ProductType;

public class PriceHistoryDaoImpl extends CoreDaoImpl<PriceHistory, Serializable> implements PriceHistoryDao {

	public PriceHistoryDaoImpl(Class<PriceHistory> entityClass) {
		super(entityClass);
	}

	@Override
	public List<PriceHistory> findPriceHistoryList(String priceId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(PriceHistory.class);
		criteria.createAlias("price","price");
		
		if(priceId != null && ! priceId.equals("")){
			criteria.add(Restrictions.eq("price.id", Integer.parseInt(priceId)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<PriceHistory> findPriceHistoryByProduct(String productId,String date, String time) throws DataAccessException {
		String dates = date+" "+time+":00";
		String sql = " SELECT * FROM pricehistory WHERE product_id = "+Integer.parseInt(productId);
		sql += " AND createDate <= '"+dates+"'";
		sql += " ORDER BY pricehistory_id DESC LIMIT 1";
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<PriceHistory> results = new ArrayList<PriceHistory>();
		if (objects != null && objects.size() > 0) {
			for (Object[] obj : objects) {
				PriceHistory entity = new PriceHistory();

				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setPrice(new Price(Integer.parseInt(String.valueOf(obj[1]))));
				entity.setProduct(new Product(Integer.parseInt(String.valueOf(obj[2]))));
				entity.setPriceOver(Double.valueOf(String.valueOf(obj[3])));
				entity.setPriceOverVat(Double.valueOf(String.valueOf(obj[4])));
				entity.setPriceLess(Double.valueOf(String.valueOf(obj[5])));
				entity.setPriceLessVat(Double.valueOf(String.valueOf(obj[6])));
				results.add(entity);
			}
		}
	
		return results;
	}

	@Override
	public List<PriceHistory> findPriceHistoryByPriceID(String productId,String priceId) throws DataAccessException {
		String sql = " SELECT * FROM pricehistory WHERE product_id = "+Integer.parseInt(productId);
		sql += " AND price_id = "+Integer.parseInt(priceId);
		sql += " ORDER BY pricehistory_id DESC LIMIT 1";
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<PriceHistory> results = new ArrayList<PriceHistory>();
		if (objects != null && objects.size() > 0) {
			for (Object[] obj : objects) {
				PriceHistory entity = new PriceHistory();

				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setPrice(new Price(Integer.parseInt(String.valueOf(obj[1]))));
				entity.setProduct(new Product(Integer.parseInt(String.valueOf(obj[2]))));
				entity.setPriceOver(Double.valueOf(String.valueOf(obj[3])));
				entity.setPriceOverVat(Double.valueOf(String.valueOf(obj[4])));
				entity.setPriceLess(Double.valueOf(String.valueOf(obj[5])));
				entity.setPriceLessVat(Double.valueOf(String.valueOf(obj[6])));
				results.add(entity);
			}
		}
	
		return results;
	}

}