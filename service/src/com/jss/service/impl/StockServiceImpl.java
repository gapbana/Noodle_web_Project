package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.StockDao;
import com.jss.entity.Stock;
import com.jss.service.StockService;

public class StockServiceImpl extends CoreServiceImpl<Stock, Serializable> implements StockService {

	private StockDao stockDao;
	public StockServiceImpl(StockDao stockDao) {
		super(stockDao);
		this.stockDao = stockDao;
	}
	
	@Override
	public List<Stock> getStockByProduct(String productId) throws DataAccessException {
		return stockDao.findStockByProduct(productId);
	}
	
	

	
}
