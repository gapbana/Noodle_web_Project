<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
 	 $('#result').DataTable();
 	
});
</script>

<script language="javascript" type="text/javascript">
function doSearch() {
	document.priceForm.mode.value = 'priceSearch';
	document.priceForm.submit();
}
function doPriceAdd() {
	document.priceForm.mode.value = 'priceAdd';
	document.priceForm.submit();
}
function doHistory(id) {
	document.priceForm.mode.value = 'priceDetail';
	document.priceForm.id.value = id;
	document.priceForm.submit();
}
function doRefresh() {
	location.href='price.htm?mode=index';
}
function doPrintPdf(id) {
	window.open("${pageContext.request.contextPath}/price.htm?mode=printPricePdf&id="+id,"popwin","menubar=0,status=no");
}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�Ҥ��Թ��� <span class="navigator">[PR01]</span></h3>
				<span class="navigator" >�Ҥ��Թ���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      		<html:form action="/price" styleId="priceForm" styleClass="form-horizontal">
	      		<html:hidden property="id"></html:hidden>
	      		<html:hidden property="mode"></html:hidden>
				<div class="form-group">
					<label class="col-sm-2 control-label"> ������ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="sdate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label"> �֧�ѹ���:</label>
					<div class="col-sm-3">
						<div class="input-group">
                     		<html:text property="edate" styleClass="form-control datepicker"></html:text>
                     		<span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                   		</div>						
					</div>
				</div>					
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button type="button" class="btn btn-info" onclick="doSearch();">����</button>
						<button type="button" class="btn btn-default"  onclick="doRefresh();">���������</button>
						<button type="button" class="btn btn-success" onclick="doPriceAdd();">����������</button>
					</div>
				</div>
	         	</html:form>
	     	</div>
 		</div>     
 		<c:if test="${fn:length(priceList)> 0 }">                 
			<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
				<thead>
					<tr class="cyan lighten-4">
						<th width="10%"><div align="center">�ӴѺ</div></th>
						<th width="10%"><div align="center">��</div></th>
						<th width="10%"><div align="center">��͹</div></th>
						<th width="20%"><div align="center">�ѹ����˹��Ҥ�</div></th>
						<th width="20%"><div align="right">�Ҥ����硵�Ҵ (�ҷ/��.)</div></th>
						<th width="10%"></th> 
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${priceList}" var="item" varStatus="row">
					<tr >
						<td align="center">${row.index + 1 }</td>
						<td align="center">${item.createDateShowYear }</td>
						<td align="center">${item.createDateShowMonth }</td>
					    <td align="center">${item.createDateShow }</td>
					 	<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.price}" /></td>
					 	<td align="center"> 				  		
							<a class="btn btn-warning btn-xs" href="#" onclick="doPrintPdf(${item.id});"><i class="fa fa-print"></i></a>
						</td>
					  	<td align="center"> 				  		
							<button class="btn btn-primary btn-xs" onclick="doHistory(${item.id});">�ٻ���ѵ�</button>
						</td>
					</tr>
					</c:forEach>						
				</tbody>
			</table>
			</c:if>
	</div>    
</body>
</html>