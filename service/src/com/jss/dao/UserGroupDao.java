package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.UserGroup;

public interface UserGroupDao extends CoreDao<UserGroup, Serializable> {

	public List<UserGroup> findGroupList(String id) throws DataAccessException;
}
