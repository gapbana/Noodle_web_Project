package com.jss.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.edu.util.DateUtil;
import com.jss.dao.CarDao;
import com.jss.dao.CustomerDao;
import com.jss.dao.OrdersDao;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.Orders;
import com.jss.entity.Stock;
import com.jss.entity.User;
import com.jss.service.CustomerService;

public class OrdersDaoImpl extends CoreDaoImpl<Orders, Serializable> implements OrdersDao {

	public OrdersDaoImpl(Class<Orders> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Orders> findOrdersList(String sdate, String edate,String custId, String saleId, String code, String status)throws DataAccessException {

		String sdates = DateUtil.date2StringEn(DateUtil.string2DateTh(sdate),"yyyy-MM-dd");
		String edates = DateUtil.date2StringEn(DateUtil.string2DateTh(edate),"yyyy-MM-dd");
		
		String sql = " SELECT";
		sql += " order_id,order_date,order_time,order_code,customer_name,CONCAT(firstname,' ',lastname) AS saleName ,orders.status,";
		sql += " (SELECT SUM(unit_weight) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalWeight,";
		sql += " (SELECT SUM(price) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalPrice";
		sql += " FROM orders";
		sql += " LEFT JOIN ma_customer ON ma_customer.customer_id = orders.customer_id";
		sql += " LEFT JOIN ma_user ON ma_user.user_id = orders.sale_id";
		sql += " WHERE order_id <> 0";
		if(!sdate.equals("") && !edate.equals("")){
			sql += " AND (order_date BETWEEN '"+sdates+"' AND '"+edates+"')";
		}else if (!sdate.equals("") && edate.equals("")) {
			sql += " AND order_date >= '"+sdates+"'";
		}else if(sdate.equals("") && !edate.equals("")){
 			sql += " AND order_date <= '"+edates+"'";
		}
		if(custId != null && !custId.equals(""))
		sql += " AND customer_id = "+Integer.parseInt(custId);
		if(saleId != null && !saleId.equals(""))
		sql += " AND sale_id = "+saleId;
		if(code != null && !code.equals(""))
		sql += " AND order_code LIKE '"+code+"'";
		if(status != null && !status.equals(""))
		sql += " AND orders.status ='"+status+"'";
		sql += " ORDER BY order_date,order_time DESC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Orders> results = new ArrayList<Orders>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Orders entity = new Orders();
				Double unit = 0.0;
				Double ptice = 0.0;
				String orderCode = "";
				
				if(obj[7] != null && !obj[7].equals(""))
					unit = Double.parseDouble(String.valueOf(obj[7]));		
				
				if(obj[8] != null && !obj[8].equals(""))
					ptice = Double.parseDouble(String.valueOf(obj[8]));
				
				if(obj[3] != null && !obj[3].equals(""))
					orderCode = String.valueOf(obj[3]);
				
				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setDate(DateUtil.string2Date(String.valueOf(obj[1]),"yyyy-MM-dd"));
				entity.setTime(String.valueOf(obj[2]));
				entity.setCode(orderCode);
				entity.setCustName(String.valueOf(obj[4])); 
				entity.setSaleName(String.valueOf(obj[5]));
				entity.setStatus(String.valueOf(obj[6]));
				entity.setTotalUnit(unit);
				entity.setTotalPrice(ptice);
				results.add(entity);
			}
		}
	
		return results;
	}

	@Override
	public List<Orders> findComboOrders(String custId) throws DataAccessException {
		String sql = " SELECT";
		sql += " order_id,order_code,";
		sql += " (SELECT SUM(unit_weight) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalWeight,";
		sql += " (SELECT SUM(unit_weight) FROM transportproduct LEFT JOIN transport ON  transportproduct.transport_id = transport.transport_id";
		sql += " WHERE transport.orders_id = orders.order_id AND transport.status = '3') AS totalUnit";
		sql += " FROM orders";
		sql += " WHERE orders.status = '3'";
		if(custId != null && !custId.equals(""))
		sql += " AND customer_id="+Integer.parseInt(custId);	
		sql += " ORDER BY order_date,order_time DESC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Orders> results = new ArrayList<Orders>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Orders entity = new Orders();
				Double unit = 0.0;
				String orderCode = "";
				Double totalUnit = 0.0;
				Double units = 0.0;
				if(obj[2] != null && !obj[2].equals(""))
					unit = Double.parseDouble(String.valueOf(obj[2]));		
				if(obj[3] != null && !obj[3].equals(""))
					units = Double.parseDouble(String.valueOf(obj[3]));
				
				totalUnit = unit-units;
				
				if(obj[1] != null && !obj[1].equals(""))
					orderCode = String.valueOf(obj[1]);
				
				if(totalUnit > 0){
					entity.setId(Integer.parseInt(String.valueOf(obj[0])));
					entity.setCode(orderCode);
					entity.setTotalUnit(totalUnit);
					results.add(entity);
				}
				
			}
		}
	
		return results;
	}

	@Override
	public List<Orders> findOrdersReport1List(String year, String month)throws DataAccessException {
		Integer years = null;
		if(year != null && !year.equals(""))
			years = Integer.parseInt(year) - 543;
		String sql = " SELECT";
		sql += " order_id,order_date,order_time,order_code,customer_name,CONCAT(firstname,' ',lastname) AS saleName ,orders.status,";
		sql += " (SELECT SUM(unit_weight) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalWeight,";
		sql += " (SELECT SUM(price) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS totalPrice";
		sql += " FROM orders";
		sql += " LEFT JOIN ma_customer ON ma_customer.customer_id = orders.customer_id";
		sql += " LEFT JOIN ma_user ON ma_user.user_id = orders.sale_id";
		sql += " WHERE order_id <> 0 AND orders.status = 3";
		if(!year.equals("") && !month.equals("")){			
			sql += " AND order_date Like '"+years+"-"+month+"%' ";
		}else if (!year.equals("") && month.equals("")) {
			sql += " AND order_date Like '"+years+"%'";
		}else if(year.equals("") && !month.equals("")){
 			sql += " AND order_date Like '%-"+month+"-%'";
		}

		sql += " ORDER BY order_date,order_time DESC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Orders> results = new ArrayList<Orders>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Orders entity = new Orders();
				Double unit = 0.0;
				Double ptice = 0.0;
				String orderCode = "";
				
				if(obj[7] != null && !obj[7].equals(""))
					unit = Double.parseDouble(String.valueOf(obj[7]));		
				
				if(obj[8] != null && !obj[8].equals(""))
					ptice = Double.parseDouble(String.valueOf(obj[8]));
				
				if(obj[3] != null && !obj[3].equals(""))
					orderCode = String.valueOf(obj[3]);
				
				entity.setId(Integer.parseInt(String.valueOf(obj[0])));
				entity.setDate(DateUtil.string2Date(String.valueOf(obj[1]),"yyyy-MM-dd"));
				entity.setTime(String.valueOf(obj[2]));
				entity.setCode(orderCode);
				entity.setCustName(String.valueOf(obj[4])); 
				entity.setSaleName(String.valueOf(obj[5]));
				entity.setStatus(String.valueOf(obj[6]));
				entity.setTotalUnit(unit);
				entity.setTotalPrice(ptice);
				results.add(entity);
			}
		}
	
		return results;
	}

	@Override
	public List<Orders> findMonthlyTransport() throws DataAccessException {
		String sql = " SELECT DISTINCT  orders.order_id,order_date,order_time,order_code,customer_name,CONCAT(firstname,' ',lastname) AS saleName,";
		sql += " (SELECT SUM(unit_weight) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS ordersUnit,";
		sql += " (SELECT SUM(price) FROM ordersproduct WHERE ordersproduct.order_id = orders.order_id) AS ordersPrice,";
		sql += " (SELECT SUM(unit_weight) FROM transportproduct LEFT JOIN transport ON transport.transport_id =  transportproduct.transport_id";
		sql += " WHERE transport.orders_id = orders.order_id AND transport.status = '3') AS transportUnit,";
		sql += " (SELECT SUM(price) FROM transportproduct LEFT JOIN transport ON transport.transport_id =  transportproduct.transport_id";
		sql += " WHERE transport.orders_id = orders.order_id AND transport.status = '3') AS transportPrice";
		sql += " FROM orders";
		sql += " LEFT JOIN transport ON transport.orders_id = orders.order_id";
		sql += " LEFT JOIN ma_customer ON orders.customer_id = ma_customer.customer_id";
		sql += " LEFT JOIN ma_user ON orders.sale_id = ma_user.user_id";
		sql += " WHERE orders.status = '3'";		
		sql += " ORDER BY order_date,order_time ASC";		
	
		List<Object[]> objects = getSession().createSQLQuery(sql).list();

		List<Orders> results = new ArrayList<Orders>();
		if (objects != null && objects.size() > 0) {
			
			for (Object[] obj : objects) {
				Orders entity = new Orders();
				Double unit = 0.0;
				Double ptice = 0.0;
				Double tunit = 0.0;
				Double tptice = 0.0;
				String orderCode = "";
				
				if(obj[6] != null && !obj[6].equals(""))
					unit = Double.parseDouble(String.valueOf(obj[6]));		
				
				if(obj[7] != null && !obj[7].equals(""))
					ptice = Double.parseDouble(String.valueOf(obj[7]));
				
				if(obj[8] != null && !obj[8].equals(""))
					tunit = Double.parseDouble(String.valueOf(obj[8]));		
				
				if(obj[9] != null && !obj[9].equals(""))
					tptice = Double.parseDouble(String.valueOf(obj[9]));
				
				if(obj[3] != null && !obj[3].equals(""))
					orderCode = String.valueOf(obj[3]);
				
				Double totalUnit = unit-tunit;
				Double totalPrice = ptice-tptice;
				if(totalUnit > 0 && totalPrice > 0){
					entity.setId(Integer.parseInt(String.valueOf(obj[0])));
					entity.setDate(DateUtil.string2Date(String.valueOf(obj[1]),"yyyy-MM-dd"));
					entity.setTime(String.valueOf(obj[2]));
					entity.setCode(orderCode);
					entity.setCustName(String.valueOf(obj[4])); 
					entity.setSaleName(String.valueOf(obj[5]));
					entity.setTotalUnit(unit);
					entity.setTotalPrice(ptice);
					entity.settUnit(tunit);
					entity.settPrice(tptice);
					results.add(entity);
				}
					
			}
		}
	
		return results;
	}

} 