package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Customer;

public interface CustomerDao extends CoreDao<Customer, Serializable> {

	public List<Customer> findAllCust() throws DataAccessException;
	public List<Customer> findCustomersSearch(String name,String rank) throws DataAccessException;
}
