package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.GapOrderItemDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapOrderItem;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.GapCustomerService;
import com.jss.service.GapOrderItemService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class GapOrderItemServiceImpl extends CoreServiceImpl<GapOrderItem, Serializable> implements GapOrderItemService {

	private GapOrderItemDao gapOrderItemDao;

	public GapOrderItemServiceImpl(GapOrderItemDao gapOrderItemDao) {
		super(gapOrderItemDao);
		this.gapOrderItemDao = gapOrderItemDao;
	}

	@Override
	public List<GapOrderItem> getOrderItem() throws DataAccessException {
		// TODO Auto-generated method stub
		return gapOrderItemDao.findOrderItems();
	}



	
}
