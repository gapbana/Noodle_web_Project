package com.jss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "ordersproduct")
public class OrdersProduct extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "ordersproduct_id") 
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "order_id")
	private Orders orders;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "price_id")
	private Price priceId;
	
	@Column(name = "unit_line")
	private Integer line;
	
	@Column(name = "unit_weight")
	private Double weight;
	
	@Column(name = "price_unit")
	private Double priceUnit;
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "amount")
	private Double amount;
	
	@Transient 
	private String colSeq;
	@Transient 
	private String colName;
	@Transient 
	private String colPrice;
	@Transient 
	private String colPriceUnit;
	@Transient 
	private String colLine;
	@Transient 
	private String colUnit;
	@Transient 
	private String colTotalPrice;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Price getPriceId() {
		return priceId;
	}

	public void setPriceId(Price priceId) {
		this.priceId = priceId;
	}

	public Double getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(Double priceUnit) {
		this.priceUnit = priceUnit;
	}

	public String getColSeq() {
		return colSeq;
	}

	public void setColSeq(String colSeq) {
		this.colSeq = colSeq;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public String getColPrice() {
		return colPrice;
	}

	public void setColPrice(String colPrice) {
		this.colPrice = colPrice;
	}

	public String getColPriceUnit() {
		return colPriceUnit;
	}

	public void setColPriceUnit(String colPriceUnit) {
		this.colPriceUnit = colPriceUnit;
	}

	public String getColLine() {
		return colLine;
	}

	public void setColLine(String colLine) {
		this.colLine = colLine;
	}

	public String getColUnit() {
		return colUnit;
	}

	public void setColUnit(String colUnit) {
		this.colUnit = colUnit;
	}

	public String getColTotalPrice() {
		return colTotalPrice;
	}

	public void setColTotalPrice(String colTotalPrice) {
		this.colTotalPrice = colTotalPrice;
	}
	
	

	
	
}
