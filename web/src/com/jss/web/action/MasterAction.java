 package com.jss.web.action;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.upload.FormFile;

import com.edu.util.DateUtil;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.District;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapItem;
import com.jss.entity.OrderMenu;
import com.jss.entity.Product;
import com.jss.entity.ProductType;
import com.jss.entity.Province;
import com.jss.entity.SubDistrict;
import com.jss.entity.User;
import com.jss.entity.UserGroup;
import com.jss.service.CarService;
import com.jss.service.CustomerService;
import com.jss.service.DistrictService;
import com.jss.service.GapCustomerService;
import com.jss.service.ProductService;
import com.jss.service.ProductTypeService;
import com.jss.service.ProvinceService;
import com.jss.service.SubDistrictService;

public class MasterAction extends CoreAction{	
	
	private ProductService productService;
	private ProductTypeService productTypeService;
	private ProvinceService provinceService;
	private DistrictService districtService;
	private SubDistrictService subDistrictService;
	private CustomerService customerService;
	private CarService carService;
	private GapCustomerService gapCustomerService;
	
	public ActionForward product(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);  //F5  //หน้าแอดแบบเซตให้ว่างใว้
			List<ProductType> result = productTypeService.getAllProductType();
			dynaForm.set("comboProductTypeList", result);
			request.setAttribute("resultList", null);
			dynaForm.set("productTypeId", "");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA01");
	}
	
	public ActionForward productSearch(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			List<Product> resultList = productService.getProductList(dynaForm.getString("productTypeId"));
			
			List<ProductType> result = productTypeService.getAllProductType();
			dynaForm.set("comboProductTypeList", result);
			request.setAttribute("resultList", resultList);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA01");
	}
	
	public ActionForward productAdd (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			List<ProductType> result = productTypeService.getAllProductType();
			dynaForm.set("comboProductTypeList", result);
			request.setAttribute("resultList", null);
			dynaForm.set("id", "");
			dynaForm.set("productCode", "");
			dynaForm.set("productTypeId", "");
			dynaForm.set("size", "");
			dynaForm.set("length", "");
			dynaForm.set("weight", "");
			dynaForm.set("hard", "");
			dynaForm.set("diff", "");
			dynaForm.set("model", "");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA02");
	}
	
	public ActionForward productSave (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		DynaActionForm dynaForm = (DynaActionForm) form;
		dynaForm.initialize(mapping);
		try{
		Product entity = new Product();
		
			String productCode = null; 
				if(dynaForm.getString("productTypeId").equals("1")){
					productCode = "RB" + dynaForm.getString("size");
					
			}else if (dynaForm.getString("productTypeId").equals("2")){
					productCode = "DB" + dynaForm.getString("size");
			}
		if (dynaForm.getString("id") != null&& !dynaForm.getString("id").equals("")) {
			entity = productService.getItem(Integer.parseInt(dynaForm.getString("id")));
			entity.setProductType(productTypeService.getItem(Integer.parseInt(dynaForm.getString("productTypeId")))); 
			entity.setCode(productCode);
/*			entity.setUpdateDate(DateUtil.getCurrentDate());
			entity.setUpdateBy(getUserSession(request).getFullname());*/
			productService.updateItem(entity);
			
		return productSearch(mapping, dynaForm, request, response);
			
		} else {
			entity = new Product();
			entity.setProductType(productTypeService.getItem(Integer.parseInt(dynaForm.getString("productTypeId"))));
			entity.setCode(productCode);
	/*		entity.setCreateDate(DateUtil.getCurrentDate());
			entity.setCreateBy(getUserSession(request).getFullname());*/
			productService.saveItem(entity);
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return product(mapping, dynaForm, request, response);
	}
	
	public ActionForward productEdit (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			Product entity = productService.getItem(Integer.parseInt(dynaForm.getString("id")));
			List<ProductType> result = productTypeService.getAllProductType();
			dynaForm.set("comboProductTypeList", result);
			request.setAttribute("resultList", null);
			dynaForm.set("productTypeId", String.valueOf(entity.getProductType().getId()));
	
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA02");
	}
	
	public ActionForward productDelete (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			productService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
			/*return mapping.findForward("MA02");*/
		}catch (Exception e) {
			e.printStackTrace();
		}
		return productSearch(mapping, form, request, response);
	}
	
	
	public ActionForward stock (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			
			dynaForm.initialize(mapping);
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA05");
	}
	
	public ActionForward customer (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			dynaForm.set("custNames", null);
			dynaForm.set("ranks", null);
			dynaForm.set("resultList", null);
			request.setAttribute("resultList",  null);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA03");
	}
	public ActionForward customerSearch (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			List<Customer> result = customerService.getCustomersSearch(dynaForm.getString("custNames"), dynaForm.getString("ranks"));
			dynaForm.set("resultList", result);
			request.setAttribute("resultList",result);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA03");
	}
	
	public ActionForward customerAll(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);
			List<GapCustomer> result = gapCustomerService.getAllItems();
			dynaForm.set("resultList", result);
			request.setAttribute("resultList",result);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA03");
	}
	
	public ActionForward customerAdd (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapCustomer entity = new GapCustomer();
			entity.setUsername(dynaForm.getString("usr"));
			entity.setPassword(dynaForm.getString("pwd"));
			entity.setAddr(dynaForm.getString("addr"));
			entity.setEmail(dynaForm.getString("email"));
			entity.setMobile(dynaForm.getString("contactTel"));
			entity.setPrefix(dynaForm.getString("prefix"));
			entity.setName(dynaForm.getString("custName"));
			
			gapCustomerService.saveItem(entity);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA04");
	}
	
	public ActionForward customerSave(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			GapCustomer entity = new GapCustomer();
			entity.setUsername(dynaForm.getString("usr"));
			entity.setPassword(dynaForm.getString("pwd"));
			entity.setAddr(dynaForm.getString("addr"));
			entity.setEmail(dynaForm.getString("email"));
			entity.setMobile(dynaForm.getString("contactTel"));
			entity.setPrefix(dynaForm.getString("prefix"));
			entity.setName(dynaForm.getString("custName"));
			
			gapCustomerService.saveItem(entity);

		}catch (Exception e) {
			e.printStackTrace();
		}
		return customerAll(mapping, form, request, response);
	}
	
	
	public ActionForward customerAdd1(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			

		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA04");
	}
	
	
	public ActionForward customerEdit (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapCustomer> resultList = gapCustomerService.getCust(dynaForm.getString("id"));
			request.setAttribute("resultList", resultList);
			GapCustomer entity = gapCustomerService.getItem(Integer.parseInt(dynaForm.getString("id")));
			gapCustomerService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			dynaForm.set("prefix", String.valueOf(entity.getPrefix()));
			dynaForm.set("custName", String.valueOf(entity.getName()));
			dynaForm.set("addr", String.valueOf(entity.getAddr()));
			dynaForm.set("contactTel", String.valueOf(entity.getMobile()));
			dynaForm.set("email", String.valueOf(entity.getEmail()));
			dynaForm.set("usr", String.valueOf(entity.getUsername()));
			dynaForm.set("pwd", String.valueOf(entity.getPassword()));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("MA04");
	}

	
	public ActionForward customerDel(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			gapCustomerService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return customerAll(mapping, dynaForm, request, response);
	}
	
	public ActionForward car (ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Car> carList = carService.getCarByCust(dynaForm.getString("custId"));		
			request.setAttribute("carList", carList);
			
			dynaForm.set("id", "");		
			dynaForm.set("licensePlate", "");
			dynaForm.set("brand", "");
			dynaForm.set("models", "");
			dynaForm.set("driverName", "");
		}
		 catch (Exception e) {
				e.printStackTrace();
			}
		return mapping.findForward("MA05");
	}
	
	public ActionForward carSave(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			FileOutputStream fos = null;
			BufferedOutputStream bos = null;
			String exts = null;
			FormFile img = (FormFile) dynaForm.get("uploadFile");
			if (img.getFileName() != null && img.getFileName().lastIndexOf(".") != -1) {
//				img = (FormFile) dynaForm.get("uploadFile");
				exts = img.getFileName().substring(img.getFileName().lastIndexOf("."));
				exts = exts.replace(".", "");
			}
			String filePath = (String) getObjectSession(request, PATH_UPLOAD);			
			String imgName = DateUtil.date2String(new Date(), "yyyyMMddHHmmssSSS") + "." + exts;
			
			boolean isImg = false;
			File logImg = new File(filePath + "image/car/");
			if (!logImg.exists()) {
				isImg = logImg.mkdirs(); // logFile.createNewFile();
			} else {
				isImg = true;
			}
			
			
			if (img.getFileName() != null && img.getFileName().lastIndexOf(".") != -1) {
				fos = new FileOutputStream(filePath + "image/car/" + imgName);
				bos = new BufferedOutputStream(fos);
				bos.write(img.getFileData());
			}

			
			if (dynaForm.getString("id") != null && !dynaForm.getString("id").equals("")) {
				Car entity = carService.getItem(Integer.parseInt(dynaForm.getString("id")));
				entity.setCustomer(customerService.getItem(Integer.parseInt(dynaForm.getString("custId"))));
				entity.setLicensePlate(dynaForm.getString("licensePlate"));
				entity.setBrand(dynaForm.getString("brand"));
				entity.setModel(dynaForm.getString("models"));
				entity.setDriverName(dynaForm.getString("driverName"));
				if (img.getFileName() != null && img.getFileName().lastIndexOf(".") != -1)
					entity.setDriverImg(imgName);
	/*			entity.setUpdateBy(getUserSession(request).getFullname());
				entity.setUpdateDate(DateUtil.getCurrentDate());*/
				carService.updateItem(entity);
			} else {
				Car entity = new Car();
				entity.setCustomer(customerService.getItem(Integer.parseInt(dynaForm.getString("custId"))));
				entity.setLicensePlate(dynaForm.getString("licensePlate"));
				entity.setBrand(dynaForm.getString("brand"));
				entity.setModel(dynaForm.getString("models"));
				entity.setDriverName(dynaForm.getString("driverName"));		
				if (img.getFileName() != null && img.getFileName().lastIndexOf(".") != -1)
					entity.setDriverImg(imgName);
		/*		entity.setCreateBy(getUserSession(request).getFullname());
				entity.setCreateDate(DateUtil.getCurrentDate());*/
				carService.saveItem(entity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return car(mapping, dynaForm, request, response);
	}
	
	public ActionForward carEdit(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Car> carList = carService.getCarByCust(dynaForm.getString("custId"));	
			Car entity = carService.getItem(Integer.parseInt(dynaForm.getString("id")));
			request.setAttribute("carList", carList);
			dynaForm.set("licensePlate", entity.getLicensePlate());
			dynaForm.set("brand", entity.getBrand());
			dynaForm.set("models", entity.getModel());
			dynaForm.set("driverName", entity.getDriverName());
			dynaForm.set("custId", String.valueOf(entity.getCustomer().getId()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("MA05");
	}
	
	
	public ActionForward carDel(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try {
			
			dynaForm.initialize(mapping);
			carService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return car(mapping, dynaForm, request, response);
	}
	
	public ActionForward comboDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			List<District> results = new ArrayList<District>();
			String id = request.getParameter("id");
			if (id != null && !id.equals("")) {
				results = districtService.getDistrictbyProv(id);
			}

			setJSONResult(request, setJsonView(results, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	public ActionForward comboSubDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			List<SubDistrict> results = new ArrayList<SubDistrict>();
			String id = request.getParameter("id");
			if (id != null && !id.equals("")) {
				results = subDistrictService.getSubDistrictbyDistrict(id);
			}

			setJSONResult(request, setJsonView(results, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	
	public ActionForward comboProduct(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			List<Product> results = new ArrayList<Product>();
			String id = request.getParameter("id");
			if (id != null && !id.equals("")) {
				results = productService.getProductList(id);
			}

			setJSONResult(request, setJsonView(results, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}
	

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}

	public ProvinceService getProvinceService() {
		return provinceService;
	}

	public void setProvinceService(ProvinceService provinceService) {
		this.provinceService = provinceService;
	}

	public DistrictService getDistrictService() {
		return districtService;
	}

	public void setDistrictService(DistrictService districtService) {
		this.districtService = districtService;
	}

	public SubDistrictService getSubDistrictService() {
		return subDistrictService;
	}

	public void setSubDistrictService(SubDistrictService subDistrictService) {
		this.subDistrictService = subDistrictService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public GapCustomerService getGapCustomerService() {
		return gapCustomerService;
	}

	public void setGapCustomerService(GapCustomerService gapCustomerService) {
		this.gapCustomerService = gapCustomerService;
	}
	
	

}
