package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Stock;

public interface StockDao extends CoreDao<Stock, Serializable> {

	
	public List<Stock> findStockByProduct(String productId) throws DataAccessException;

}
