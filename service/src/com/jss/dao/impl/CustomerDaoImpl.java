package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CustomerDao;
import com.jss.entity.Customer;

public class CustomerDaoImpl extends CoreDaoImpl<Customer, Serializable> implements CustomerDao {

	public CustomerDaoImpl(Class<Customer> entityClass) {
		super(entityClass);
		
	}

	@Override
	public List<Customer> findCustomersSearch(String name, String rank)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class);
		criteria.addOrder(Order.asc("name"));	
		
		if(name != null && !name.equals("")){
			criteria.add(Restrictions.eq("name", name));
		}
		if(rank != null && !rank.equals("")){
			criteria.add(Restrictions.eq("rank", rank));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<Customer> findAllCust() throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class);
		criteria.addOrder(Order.asc("name"));	
	
		return getHibernateTemplate().findByCriteria(criteria);
	}

	

}