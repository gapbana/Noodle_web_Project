package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Transport;
import com.jss.entity.TransportProduct;
import com.jss.entity.User;

public interface TransportProductService extends CoreService<TransportProduct, Serializable> {
	
	public List<TransportProduct> getTransportProductByTransport(String transportId) throws DataAccessException;
}
