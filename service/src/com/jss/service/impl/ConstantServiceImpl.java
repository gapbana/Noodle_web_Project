package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.ConstantDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Constant;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.ConstantService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class ConstantServiceImpl extends CoreServiceImpl<Constant, Serializable> implements ConstantService {

	private ConstantDao constantDao;

	public ConstantServiceImpl(ConstantDao constantDao) {
		super(constantDao);
		this.constantDao = constantDao;
	}

}
