package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.GapCustomerService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class GapCustomerServiceImpl extends CoreServiceImpl<GapCustomer, Serializable> implements GapCustomerService {

	private GapCustomerDao gapCustomerDao;
	public GapCustomerServiceImpl(GapCustomerDao gapCustomerDao) {
		super(gapCustomerDao);
		this.gapCustomerDao = gapCustomerDao;
	}

	@Override
	public List<GapCustomer> getLogin(String un, String pwd) throws DataAccessException {
		return gapCustomerDao.findLogin(un, pwd);
	}

	@Override
	public List<GapCustomer> getCust(String id) throws DataAccessException {
		return gapCustomerDao.findCust(id);
	}

	



	
}
