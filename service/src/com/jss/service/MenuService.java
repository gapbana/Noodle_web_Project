package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface MenuService extends CoreService<Menu, Serializable> {
	
	public List<Menu> getAllMenu() throws DataAccessException;
}
