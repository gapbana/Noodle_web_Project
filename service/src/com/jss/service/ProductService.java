package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Product;



public interface ProductService extends CoreService<Product, Serializable> {
	
	public List<Product> getProductList(String productTypeId) throws DataAccessException;
	public List<Product> getProductListPrice(String price) throws DataAccessException;
	public List<Product> getProductByCode(String code) throws DataAccessException;
	public List<Product> getProductStockList(String productId,String productTypeId, String productCode,String productModel) throws DataAccessException;
	
}
