<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Inconsolata">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<script defer
	src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"
	integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ"
	crossorigin="anonymous"></script>


<%@ include file="../boostrap.jsp"%>
<%@ include file="../inc_menu2.jsp"%>
<%@ include file="../inc_header.jsp"%>
</html>
<style>
.login-dialog .modal-dialog {
	width: 300px;
}
</style>


<script language="javascript" type="text/javascript">
/* $(document).ready(function(){
	  $("#vet1").attr("checked","checked");
	}), */
	
function doOutStock(){
	alert("สินค้าหมด กรุณาเลือกสินค้าอื่น");
}

function doAdd(id) {

	$("#moreInfo").modal()
	
	 $.post("${pageContext.request.contextPath}/customer.htm?mode=typeDetail",
			    {
			  id: id
			    }, function(data) {
			    	$("#noodleBody").hide();
			    	$("#drinkBody").hide();
			    	$("#snackBody").hide();
			    	$("#prices").html(data[0].price);
			    	$("#picc").html(data[0].pic);
			    	$("#typee").html(data[0].type);
			    	$("#staa").html(data[0].sta);
			    	$("#gapItemId").val(data[0].id);
			    	$("#subType").val(data[0].subType);
			    	
			    	 if (data[0].subType =='1'){
			    		$("#noodleBody").show();
			    	}
			    	else if (data[0].subType == '2') {
			    		$("#drinkBody").show();
					}
			    	else{
			    		$("#snackBody").show();
			    	} 
			    },'json');  
}

function doCart() {
	
	document.cusForm.mode.value = 'addToCart';
	document.cusForm.submit();
	
	
}

</script>





<style>
.nav-pills>li.active>a,.nav-pills>li.active>a:focus {
	color: black;
	background-color: #A9A9A9;
}

.nav-pills>li.active>a:hover {
	background-color: #efcb00;
	color: black;
}
</style>

<div align="center">
	<%-- MENU ICON --%>
	<br> <img src="./menu-icon.png" alt="" width="420">
</div>
<br>
<br>
<ul class="nav nav-pills nav-justified">
	<li class="active"><a data-toggle="tab" href="#home">ทั้งหมด</a></li>
	<li><a data-toggle="tab" href="#noodleTab">ก๋วยเตี๋ยว</a></li>
	<li><a data-toggle="tab" href="#snackTab">ขนม</a></li>
	<li><a data-toggle="tab" href="#drinkTab">เครื่องดื่ม</a></li>
</ul>


<div class="tab-content">
	<div id="home" class="tab-pane fade in active">
		<div class="flip-menu">
			<c:forEach items="${resultList}" var="item" varStatus="row">
				<section class="flip-item-wrap">
					<img class="fake-image" src="${item.pic}" alt=""> <input
						type="checkbox" class="flipper" id="${row.index + 1 }"> <label
						for="${row.index + 1 }" class="flip-item">
						<figure class="front">
							<img src="${item.pic}" alt=""></img>
						</figure>
						<figure class="back">
							<img src="${item.pic}" alt=""></img>
							<div class="flip-item-desc">
								<h4 class="flip-item-title" align="center">${item.type}</h4>
								<br>
								<p align="center">฿ ${item.price}</p>

								<br>


								<div align="center">

									<!-- ถ้าเป็น ก๋วยเตี๊ยวเข้า if นี้ทำให้ปุ่ม add ขึ้น modal ก๋วยเตี๋ยว -->
									<c:if test="${item.sta eq '0' }">
										<button class="btn btn-danger" onclick="doOutStock()">
											สินค้าหมด &nbsp;<i class="fas fa-exclamation-triangle"></i>
										</button>
									</c:if>
									<c:if test="${item.sta eq '1' }">
										<button class="btn btn-primary" onclick="doAdd(${item.id})">
											+ เพิ่มลงตะกร้า <i class="fas fa-cart-plus"></i>
										</button>
									</c:if>
								</div>
							</div>

						</figure>
					</label>
				</section>
			</c:forEach>
		</div>
	</div>

	<div id="noodleTab" class="tab-pane fade">
		<div class="flip-menu">
			<c:forEach items="${resultList}" var="item" varStatus="row">
				<c:if test="${item.subType eq '1'}">
					<section class="flip-item-wrap">
						<img class="fake-image" src="${item.pic}" alt=""> <input
							type="checkbox" class="flipper" id="${row.index + 1 }"> <label
							for="${row.index + 1 }" class="flip-item">
							<figure class="front">
								<img src="${item.pic}" alt=""></img>
							</figure>
							<figure class="back">
								<img src="${item.pic}" alt=""></img>
								<div class="flip-item-desc">
									<h4 class="flip-item-title" align="center">${item.type}</h4>
									<br>
									<p align="center">฿ ${item.price}</p>

									<br>


									<div align="center">

										<!-- ถ้าเป็น ก๋วยเตี๊ยวเข้า if นี้ทำให้ปุ่ม add ขึ้น modal ก๋วยเตี๋ยว -->
										<c:if test="${item.sta eq '0' }">
											<button class="btn btn-danger" onclick="doOutStock()">
												สินค้าหมด &nbsp;<i class="fas fa-exclamation-triangle"></i>
											</button>
										</c:if>
										<c:if test="${item.sta eq '1' }">
											<button class="btn btn-primary" onclick="doAdd(${item.id})">
												+ เพิ่มลงตะกร้า <i class="fas fa-cart-plus"></i>
											</button>
										</c:if>
									</div>
								</div>

							</figure>
						</label>
					</section>
				</c:if>
			</c:forEach>
		</div>
	</div>
	
	<div id="snackTab" class="tab-pane fade">
		<div class="flip-menu">
			<c:forEach items="${resultList}" var="item" varStatus="row">
				<c:if test="${item.subType eq '3'}">
					<section class="flip-item-wrap">
						<img class="fake-image" src="${item.pic}" alt=""> <input
							type="checkbox" class="flipper" id="${row.index + 1 }"> <label
							for="${row.index + 1 }" class="flip-item">
							<figure class="front">
								<img src="${item.pic}" alt=""></img>
							</figure>
							<figure class="back">
								<img src="${item.pic}" alt=""></img>
								<div class="flip-item-desc">
									<h4 class="flip-item-title" align="center">${item.type}</h4>
									<br>
									<p align="center">฿ ${item.price}</p>

									<br>


									<div align="center">

										<!-- ถ้าเป็น ก๋วยเตี๊ยวเข้า if นี้ทำให้ปุ่ม add ขึ้น modal ก๋วยเตี๋ยว -->
										<c:if test="${item.sta eq '0' }">
											<button class="btn btn-danger" onclick="doOutStock()">
												สินค้าหมด &nbsp;<i class="fas fa-exclamation-triangle"></i>
											</button>
										</c:if>
										<c:if test="${item.sta eq '1' }">
											<button class="btn btn-primary" onclick="doAdd(${item.id})">
												+ เพิ่มลงตะกร้า <i class="fas fa-cart-plus"></i>
											</button>
										</c:if>
									</div>
								</div>

							</figure>
						</label>
					</section>
				</c:if>
			</c:forEach>
		</div>
	</div>
	
	<div id="drinkTab" class="tab-pane fade">
		<div class="flip-menu">
			<c:forEach items="${resultList}" var="item" varStatus="row">
				<c:if test="${item.subType eq '2'}">
					<section class="flip-item-wrap">
						<img class="fake-image" src="${item.pic}" alt=""> <input
							type="checkbox" class="flipper" id="${row.index + 1 }"> <label
							for="${row.index + 1 }" class="flip-item">
							<figure class="front">
								<img src="${item.pic}" alt=""></img>
							</figure>
							<figure class="back">
								<img src="${item.pic}" alt=""></img>
								<div class="flip-item-desc">
									<h4 class="flip-item-title" align="center">${item.type}</h4>
									<br>
									<p align="center">฿ ${item.price}</p>

									<br>


									<div align="center">

										<!-- ถ้าเป็น ก๋วยเตี๊ยวเข้า if นี้ทำให้ปุ่ม add ขึ้น modal ก๋วยเตี๋ยว -->
										<c:if test="${item.sta eq '0' }">
											<button class="btn btn-danger" onclick="doOutStock()">
												สินค้าหมด &nbsp;<i class="fas fa-exclamation-triangle"></i>
											</button>
										</c:if>
										<c:if test="${item.sta eq '1' }">
											<button class="btn btn-primary" onclick="doAdd(${item.id})">
												+ เพิ่มลงตะกร้า <i class="fas fa-cart-plus"></i>
											</button>
										</c:if>
									</div>
								</div>

							</figure>
						</label>
					</section>
				</c:if>
			</c:forEach>
		</div>
	</div>

	<!-- Modal -->

	<div class="modal fade" id="moreInfo" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<html:form action="/customer" styleId="moreInfo"
					styleClass="form-horizontal" method="post">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<html:hidden property="gapItemId" styleId="gapItemId" />
					<%-- <html:hidden property="subType" styleId="subType" />  --%>

					<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" align="center">กรุณาเลือกรายละเอียด</h4>
						<img id="picc" src="pic2" alt="">
						<div>
							<div class="col-md-9">
								<h4>
									<font color="#696969"><b><span id="typee"></span></b></font>
								</h4>
							</div>

							<h4>
								<font color="#696969"><b>ราคา :</b>&nbsp;<span
									id="prices"></span> บาท</font>
							</h4>
						</div>
					</div>

					<div class="modal-body" id="noodleBody">


						<span id="pricess"></span>
						<h5>
							<b> กรุณาเลือกเส้น </b> <font color="#808080">(เลือก 1
								ประเภท)</font>
						</h5>

						<div class="col-sm-5">
							<html:select styleClass="form-control" styleId="noodle"
								property="noodle">
								<html:option value="0">เลือกเส้น</html:option>
								<html:option value="1">เส้นเล็ก</html:option>
								<html:option value="2">เส้นหมี่</html:option>
								<html:option value="3">เส้นใหญ่</html:option>
								<html:option value="4">มาม่า</html:option>
								<html:option value="5">วุ้นเส้น</html:option>
								<html:option value="6">บะหมี่</html:option>
							</html:select>

						</div>


						<br> <br>

						<hr></hr>
						<div class="clearboth"></div>

						<h5>
							<b> ใส่ผักหรือไม่ </b> <font color="#808080"></font>
						</h5>

						<div>
							<fieldset id="Vet">
								<div>
									<div class="col-sm-5">
										<html:radio property="vet" value="1">&nbsp;ใส่ผัก</html:radio>
									</div>

									<div class="col-sm-5">
										<html:radio property="vet" value="0">&nbsp;ไม่ใส่ผัก</html:radio>
									</div>
								</div>
							</fieldset>
						</div>


						<hr></hr>
						<div class="clearboth"></div>

						<h5>
							<b> เลือกขนาด </b> <font color="#808080">(จำเป็นต้องเลือก
								1 ประเภท)</font>
						</h5>

						<div>
							<fieldset id="Extra">
								<div>
									<div class="col-sm-5">
										<html:radio property="extra" value="0">&nbsp;ขนาดปกติ (+0 บาท)</html:radio>
									</div>

									<div class="col-sm-5">
										<html:radio property="extra" value="1">&nbsp;ขนาดพิเศษ (+10 บาท)</html:radio>
									</div>
								</div>
							</fieldset>
						</div>


						

					</div>

					<div class="modal-body" id="drinkBody">

						<span id="pricess"></span>
						<h5>
							<b> เลือกประเภท</b> <font color="#808080">(จำเป็นต้องเลือก
								1 ประเภท)</font>
						</h5>

						<div>
							<fieldset id="Extra">
								<div>
									<div class="col-sm-5">
										<html:radio property="extra" value="0">&nbsp;ไม่ใส่น้ำแข็ง(+0 บาท)</html:radio>
									</div>

									<div class="col-sm-5">
										<html:radio property="extra" value="1">&nbsp;ใส่น้ำแข็ง (+0 บาท)</html:radio>
									</div>
								</div>
							</fieldset>
						</div>
					</div>

					<div class="modal-body" id="snackBody"></div>



					<div class="modal-footer">
						<div align="left" class="col-sm-6">
							<div class="col-sm-10" style="display: inline">

								<html:select styleClass="form-control" styleId="qty"
									property="qty" style="inline">
									<html:option value="1">1 หน่วย</html:option>
									<html:option value="2">2 หน่วย</html:option>
									<html:option value="3">3 หน่วย</html:option>
									<html:option value="4">4 หน่วย</html:option>
									<html:option value="5">5 หน่วย</html:option>
								</html:select>

							</div>
						</div>
						<div align="right" class="col-sm-6">
							<button type="button" class="btn btn-primary" onclick="doCart()">
								เพิ่มลงในตะกร้า <i class="fas fa-cart-plus"></i>
							</button>
						</div>
					</div>
			</div>

			</html:form>
		</div>
	</div>



</div>
</div>
</div>



<div id="snack" class="tab-pane fade">
	<div class="flip-menu2">
		<c:forEach items="${resultList}" var="item" varStatus="row">
			<c:if test="${item.subType eq 'snack'}">
				<section class="flip-item-wrap2">
					<img class="fake-image2" src="${item.pic}" alt=""> <input
						type="checkbox" class="flipper2" id="${item.id}"> <label
						for="${item.id}" class="flip-item2">
						<figure class="front">
							<img src="${item.pic}" alt=""></img>
						</figure>
						<figure class="back">
							<img src="${item.pic}" alt=""></img>
							<div class="flip-item-desc2">
								<h4 class="flip-item-title2" align="center">${item.type}</h4>
								<p align="center">฿ ${item.price}</p>

								<br> <br>
								<div align="center">

									<button class="btn btn-danger">
										+ add <span class="glyphicon glyphicon-shopping-cart"></span>
									</button>
								</div>
							</div>

						</figure>
					</label>
				</section>
			</c:if>
		</c:forEach>
	</div>
</div>


<div id="drink" class="tab-pane fade">
	<div class="flip-menu">
		<c:forEach items="${resultList}" var="item" varStatus="row">
			<c:if test="${item.subType eq 'drink'}">
				<section class="flip-item-wrap">
					<img class="fake-image" src="${item.pic}" alt=""> <input
						type="checkbox" class="flipper" id="${item.id}"> <label
						for="${item.id}" class="flip-item">
						<figure class="front">
							<img src="${item.pic}" alt=""></img>
						</figure>
						<figure class="back">
							<img src="${item.pic}" alt=""></img>
							<div class="flip-item-desc">
								<h4 class="flip-item-title" align="center">${item.type}</h4>
								<p align="center">฿ ${item.price}</p>

								<br> <br>
								<div align="center">

									<button class="btn btn-danger">
										+ add <span class="glyphicon glyphicon-shopping-cart"></span>
									</button>
								</div>
							</div>

						</figure>
					</label>
				</section>
			</c:if>
		</c:forEach>
	</div>
</div>




<%-- Style --%>
<style>
p {
	font-size: 35px;
}

body {
	margin: 0;
	font: 14px Helvetica, Arial, serif;
}

.title {
	text-align: center;
	color: #333;
	font-size: 2.0em;
}

.creds a {
	color: #000;
	text-decoration: none;
	border-bottom: 1px dotted #000;
}

.flip-menu {
	margin: 30px 0 0;
}

.flip-item-wrap {
	width: 25%;
	height: auto;
	float: left;
	position: relative;
	-webkit-perspective: 800px;
	-moz-perspective: 800px;
	-ms-perspective: 800px;
	-o-perspective: 800px;
	perspective: 800px;
}

@media screen and (min-width: 1280px) {
	.flip-item-wrap {
		width: 16.6%;
	}
}

@media screen and (max-width: 979px) {
	.flip-item-wrap {
		width: 33.3%;
	}
}

@media screen and (max-width: 639px) {
	.flip-item-wrap {
		width: 50%;
	}
}

@media screen and (max-width: 379px) {
	.flip-item-wrap {
		width: 100%;
	}
}

.flip-item-wrap img {
	width: 100%;
	height: auto;
	display: block;
	margin: 0;
}

.flip-item-wrap input {
	display: none;
}

.flip-item-wrap .fake-image {
	visibility: hidden;
}

.flip-item {
	display: block;
	width: 100%;
	height: 100%;
	float: left;
	position: absolute;
	top: 0;
	left: 0;
	cursor: pointer;
	color: #fff;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	-o-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transition: -webkit-transform 1s;
	-moz-transition: -moz-transform 1s;
	-o-transition: -o-transform 1s;
	transition: transform 1s;
}

.flip-item figure {
	display: block;
	position: absolute;
	width: 100%;
	height: 100%;
	margin: 0;
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	-ms-backface-visibility: hidden;
	-o-backface-visibility: hidden;
	backface-visibility: hidden;
}

.flip-item .back {
	width: 100%;
	display: block;
	margin: 0;
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flipper:checked+.flip-item {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flip-item-desc {
	background: rgba(0, 0, 0, 0.2);
	width: 100%;
	height: 100%;
	padding: 5%;
	position: absolute;
	top: 0;
	left: 0;
	text-shadow: 1px 2px 1px rgba(0, 0, 0, 0.9);
	overflow: hidden;
}

.flip-item-title {
	font-size: 2.0em;
	margin: 0.6em 0 0.2em;
}
</style>

<style>
p {
	font-size: 35px;
}

body {
	margin: 0;
	font: 14px Helvetica, Arial, serif;
}

.title {
	text-align: center;
	color: #333;
	font-size: 2.0em;
}

.creds a {
	color: #000;
	text-decoration: none;
	border-bottom: 1px dotted #000;
}

.flip-menu2 {
	margin: 30px 0 0;
}

.flip-item-wrap2 {
	width: 25%;
	height: auto;
	float: left;
	position: relative;
	-webkit-perspective: 800px;
	-moz-perspective: 800px;
	-ms-perspective: 800px;
	-o-perspective: 800px;
	perspective: 800px;
}

@media screen and (min-width: 1280px) {
	.flip-item-wrap2 {
		width: 16.6%;
	}
}

@media screen and (max-width: 979px) {
	.flip-item-wrap2 {
		width: 33.3%;
	}
}

@media screen and (max-width: 639px) {
	.flip-item-wrap2 {
		width: 50%;
	}
}

@media screen and (max-width: 379px) {
	.flip-item-wrap2 {
		width: 100%;
	}
}

.flip-item-wrap2 img {
	width: 100%;
	height: auto;
	display: block;
	margin: 0;
}

.flip-item-wrap2 input {
	display: none;
}

.flip-item-wrap2 .fake-image {
	visibility: hidden;
}

.flip-item2 {
	display: block;
	width: 100%;
	height: 100%;
	float: left;
	position: absolute;
	top: 0;
	left: 0;
	cursor: pointer;
	color: #fff;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	-o-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transition: -webkit-transform 1s;
	-moz-transition: -moz-transform 1s;
	-o-transition: -o-transform 1s;
	transition: transform 1s;
}

.flip-item2 figure {
	display: block;
	position: absolute;
	width: 100%;
	height: 100%;
	margin: 0;
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	-ms-backface-visibility: hidden;
	-o-backface-visibility: hidden;
	backface-visibility: hidden;
}

.flip-item2 .back {
	width: 100%;
	display: block;
	margin: 0;
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flipper:checked+.flip-item {
	-webkit-transform: rotateY(180deg);
	-moz-transform: rotateY(180deg);
	-ms-transform: rotateY(180deg);
	-o-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.flip-item-desc {
	background: rgba(0, 0, 0, 0.2);
	width: 100%;
	height: 100%;
	padding: 5%;
	position: absolute;
	top: 0;
	left: 0;
	text-shadow: 1px 2px 1px rgba(0, 0, 0, 0.9);
	overflow: hidden;
}

.flip-item-title {
	font-size: 2.0em;
	margin: 0.6em 0 0.2em;
}

.center {
	width: 150px;
	margin: 40px auto;
}
</style>

