package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.OrdersDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.OrdersService;
import com.jss.service.UserService;

public class OrdersServiceImpl extends CoreServiceImpl<Orders, Serializable> implements OrdersService {

	private OrdersDao ordersDao;

	public OrdersServiceImpl(OrdersDao ordersDao) {
		super(ordersDao);
		this.ordersDao = ordersDao;
	}

	@Override
	public Integer saveOrders(Orders entity) throws DataAccessException {
		Integer id = ordersDao.merge(entity).getId();
		return id;
	}

	@Override
	public List<Orders> getOrdersList(String sdate, String edate,	String custId, String saleId, String code, String status)throws DataAccessException {
		return ordersDao.findOrdersList(sdate, edate, custId, saleId, code, status);
	}

	@Override
	public List<Orders> getComboOrders(String custId) throws DataAccessException {
		return ordersDao.findComboOrders(custId);
	}

	@Override
	public List<Orders> getOrdersReport1List(String year, String month) throws DataAccessException {
		return ordersDao.findOrdersReport1List(year, month);
	}

	@Override
	public List<Orders> getMonthlyTransport() throws DataAccessException {
		return ordersDao.findMonthlyTransport();
	}


}
