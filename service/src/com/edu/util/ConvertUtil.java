package com.edu.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ConvertUtil {

	public static String readableFileSize(long size) {
		if (size <= 0)
			return "0";
		final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " "
				+ units[digitGroups];
	}

	public static String convertMonthEn(String m) {
		String str = "";
		if (m != null && !m.equals("")) {
			if (m.equals("1") || m.equals("01")) {
				str = "January";
			} else if (m.equals("2") || m.equals("02")) {
				str = "February";
			} else if (m.equals("3") || m.equals("03")) {
				str = "March";
			} else if (m.equals("4") || m.equals("04")) {
				str = "April";
			} else if (m.equals("5") || m.equals("05")) {
				str = "May";
			} else if (m.equals("6") || m.equals("06")) {
				str = "June";
			} else if (m.equals("7") || m.equals("07")) {
				str = "July";
			} else if (m.equals("8") || m.equals("08")) {
				str = "August";
			} else if (m.equals("9") || m.equals("09")) {
				str = "September";
			} else if (m.equals("10")) {
				str = "October";
			} else if (m.equals("11")) {
				str = "November";
			} else if (m.equals("12")) {
				str = "December";
			}
		}
		return str;
	}

	public static String convertMonthTh(String m) {
		String str = "";
		if (m != null && !m.equals("")) {
			if (m.equals("1") || m.equals("01")) {
				str = "\u0E21\u0E01\u0E23\u0E32\u0E04\u0E21";
			} else if (m.equals("2") || m.equals("02")) {
				str = "\u0E01\u0E38\u0E21\u0E20\u0E32\u0E1E\u0E31\u0E19\u0E18\u0E4C";
			} else if (m.equals("3") || m.equals("03")) {
				str = "\u0E21\u0E35\u0E19\u0E32\u0E04\u0E21";
			} else if (m.equals("4") || m.equals("04")) {
				str = "\u0E40\u0E21\u0E29\u0E32\u0E22\u0E19";
			} else if (m.equals("5") || m.equals("05")) {
				str = "\u0E1E\u0E24\u0E29\u0E20\u0E32\u0E04\u0E21";
			} else if (m.equals("6") || m.equals("06")) {
				str = "\u0E21\u0E34\u0E16\u0E38\u0E19\u0E32\u0E22\u0E19";
			} else if (m.equals("7") || m.equals("07")) {
				str = "\u0E01\u0E23\u0E01\u0E0E\u0E32\u0E04\u0E21";
			} else if (m.equals("8") || m.equals("08")) {
				str = "\u0E2A\u0E34\u0E07\u0E2B\u0E32\u0E04\u0E21";
			} else if (m.equals("9") || m.equals("09")) {
				str = "\u0E01\u0E31\u0E19\u0E22\u0E32\u0E22\u0E19";
			} else if (m.equals("10")) {
				str = "\u0E15\u0E38\u0E25\u0E32\u0E04\u0E21";
			} else if (m.equals("11")) {
				str = "\u0E1E\u0E24\u0E28\u0E08\u0E34\u0E01\u0E32\u0E22\u0E19";
			} else if (m.equals("12")) {
				str = "\u0E18\u0E31\u0E19\u0E27\u0E32\u0E04\u0E21";
			}
		}
		return str;
	}

	public static String convertMonthThAbbr(String day) {
		String str = "";
		if (day != null && !day.equals("")) {
			if (day.equals("1") || day.equals("01")) {
				str = "\u0E21.\u0E04.";
			} else if (day.equals("2") || day.equals("02")) {
				str = "\u0E01.\u0E1E.";
			} else if (day.equals("3") || day.equals("03")) {
				str = "\u0E21\u0E35.\u0E04.";
			} else if (day.equals("4") || day.equals("04")) {
				str = "\u0E40\u0E21.\u0E22.";
			} else if (day.equals("5") || day.equals("05")) {
				str = "\u0E1E.\u0E04.";
			} else if (day.equals("6") || day.equals("06")) {
				str = "\u0E21\u0E34.\u0E22.";
			} else if (day.equals("7") || day.equals("07")) {
				str = "\u0E01.\u0E04.";
			} else if (day.equals("8") || day.equals("08")) {
				str = "\u0E2A.\u0E04.";
			} else if (day.equals("9") || day.equals("09")) {
				str = "\u0E1E.\u0E22.";
			} else if (day.equals("10")) {
				str = "\u0E15.\u0E04.";
			} else if (day.equals("11")) {
				str = "\u0E1E.\u0E22.";
			} else if (day.equals("12")) {
				str = "\u0E18.\u0E04.";
			}
		}
		return str;
	}

	public static String Unicode2ASCII(String unicode) {

		if (unicode == null)
			return "";

		StringBuffer ascii = new StringBuffer(unicode);
		int code;
		for (int i = 0; i < unicode.length(); i++) {
			code = (int) unicode.charAt(i);
			if ((0xE01 <= code) && (code <= 0xE5B)) {
				ascii.setCharAt(i, (char) (code - 0xD60));
			}
		}
		return ascii.toString();
	}

	public static String ASCII2Unicode(String ascii) {
		StringBuffer unicode = new StringBuffer(ascii);
		int code;
		for (int i = 0; i < ascii.length(); i++) {
			code = (int) ascii.charAt(i);
			if ((0xA1 <= code) && (code <= 0xFB)) {
				unicode.setCharAt(i, (char) (code + 0xD60));
			}
		}
		return unicode.toString();
	}

	public static String getZeroSpare(int maxlen, String data) {
		String spare = "";
		if (data.length() < maxlen) {
			for (int i = 0; i < maxlen - data.length(); i++) {
				spare = spare + "0";
			}
		}
		return spare + data;
	}
	
	public static String getZeroSpareEnd(int maxlen, String data) {
		String spare = "";
		if (data.length() < maxlen) {
			for (int i = 0; i < maxlen - data.length(); i++) {
				spare = spare + "0";
			}
		}
		return  data + spare;
	}

//	public static String getZeroSpareFront(int maxlen, String data) {
//		String spare = "";
//		if (data.length() < maxlen) {
//			for (int i = 0; i < maxlen - data.length(); i++) {
//				spare = "0" + spare;
//			}
//		}
//		return spare + data;
//	}

	public static int CounterPoint(String str) {
		int j = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.substring(i, i + 1).equals(".")) {
				j++;
			}
		}
		return j;
	}

	public static String CounterGroupCreditShow(int groupCredit, int groupCreditMax) {
		if (groupCredit < groupCreditMax) {
			return groupCredit + " - " + groupCreditMax;
		} else {
			return "" + groupCredit;
		}
	}
	
	public static String getSpace(int maxlen) {
		String space = "";
		for (int i = 0; i < maxlen; i++) {
			space += " ";
		}
		return space;
	}

	public static String stringOrder(String strOrder) {
		String a = "";
		if (strOrder.lastIndexOf(".") == -1)
			return strOrder.length() == 1 ? "0" + strOrder : strOrder;
		else {
			int position = 0;
			Boolean pointFirst = true;
			for (int i = 0; i < strOrder.length(); i++) {
				if (strOrder.substring(i, i + 1).equals(".")) {
					if (i == 1)
						a += "0"; // insert 0 in first

					if (pointFirst == true) { // check first
						a += strOrder.substring(0, i);
					} else {
						if ((i - position) == 2) { // if length back equal 1
							a += "0" + strOrder.substring(position + 1, i);
						} else {
							a += strOrder.substring(position + 1, i);
						}
					}
					pointFirst = false;
					position = i;
				} else { // back
					if (i == strOrder.length() - 1) {
						if ((strOrder.length() - position) == 2) { // if length
																	// back
																	// equal 1
							a += "0" + strOrder.substring(position + 1, strOrder.length());
						} else {
							a += strOrder.substring(position + 1, strOrder.length());
						}
					}
				}
			}
		}
		return a;
	}

	public static String cutString2Show(String strIn, int length) {
		String sub = "";
		if (strIn.length() > length) {
			sub = strIn.substring(0, length) + "...";
		} else {
			sub = strIn;
		}

		return sub;
	}

	public static String convertIdCardFormat(String idCard) {
		String str = idCard;
		if (idCard.length() == 13) {
			str = idCard.substring(0, 1) + "-" + idCard.substring(1, 5) + "-" + idCard.substring(5, 10) + "-"
					+ idCard.substring(10, 12) + "-" + idCard.substring(12, 13);
		}
		return str;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public static String getLengthLeft(int maxlen, String data) {
		String spare = "";
		if (data.length() < maxlen) {
			for (int i = 0; i < maxlen - data.length(); i++) {
				spare = spare + " ";
			}
		}
		return data + spare;
	}

	public static String getLengthRight(int maxlen, String data) {
		String spare = "";
		if (data.length() < maxlen) {
			for (int i = 0; i < maxlen - data.length(); i++) {
				spare = spare + " ";
			}
		}
		return spare + data;
	}
	
	public static String getLevelOrder(String groupLevel){
		
		String[] groupArray = groupLevel.replace("."," ").split(" ");
		String groupLevelOrder = "";
		if(groupArray.length>0){
			for (String string : groupArray) {
				if(string.length()<2){
					groupLevelOrder += "0"+string;
				}else{
					groupLevelOrder += string;
				}
			}
		}else{
			if(groupLevel.length()<2){
				groupLevelOrder = "0"+groupLevel;
			}else{
				groupLevelOrder = groupLevel;
			}
		}		
		return groupLevelOrder;
	}
}
