package com.jss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.criterion.Order;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "order_menu")

public class OrderMenu {
	
	//private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	
	@Column(name = "menu_id") 
	private int id;
	
	@Column(name = "order_id") 
	private int or_Id;
	
	@Column(name = "quantity") 
	private int qty;
	
	@Column(name = "product_id") 
	private int product;
	
	@Column(name = "noodle_type") 
	private String noodle;
	

	@Column(name = "vetgetable") 
	private String vet;
	
	@Column(name = "soup_type") 
	private String soup;
	
	@Column(name = "price") 
	private String price;
	
	@Column(name = "extra") 
	private String extra;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOr_Id() {
		return or_Id;
	}

	public void setOr_Id(int or_Id) {
		this.or_Id = or_Id;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public String getNoodle() {
		return noodle;
	}

	public void setNoodle(String noodle) {
		this.noodle = noodle;
	}

	public String getVet() {
		return vet;
	}

	public void setVet(String vet) {
		this.vet = vet;
	}

	public String getSoup() {
		return soup;
	}

	public void setSoup(String soup) {
		this.soup = soup;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	
	
	

}
