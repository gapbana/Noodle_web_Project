<%@page import="org.apache.struts.action.DynaActionForm"%>
<%@page import="com.jss.entity.GapCustomer"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@ include file="inc_header.jsp" %>

<% 
	DynaActionForm ssLoginForm = (DynaActionForm) request.getSession().getAttribute("cusForm");
 	GapCustomer ssLoginUser = ssLoginForm != null ? (GapCustomer) ssLoginForm.get("ssLoginUser") : null;
%>

<%-- อันนี้มีแต่ username  --%>

<div id="wrapper">
        <nav class="navbar navbar-default green navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
            </div>
            <!-- /.navbar-header -->


<%-- username top right --%>
            <ul class="nav navbar-top-links navbar-right ">
                <!-- /.dropdown -->
                <li><a class="white-text"><%= ssLoginUser.getUsername() %></a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw white-text"></i> <i class="fa fa-caret-down white-text"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">    
                         <li><a href="#"  onclick="location.href='login.htm?mode=logout'"><i class="fa fa-user-circle-o"></i>  ข้อมูลส่วนตัว</a></li>             
                        <li><a href="#"  onclick="location.href='login.htm?mode=logout'"><i class="fa fa-sign-out fa-fw"></i> ออกจากระบบ</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        
        <style>
.navbar {
background: #FF99CC;
border-color: #09F;
 
}    
</style>