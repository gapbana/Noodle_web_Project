package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapOrder;
import com.jss.entity.Menu;
import com.jss.entity.UserGroup;

public interface GapOrderDao extends CoreDao<GapOrder, Serializable> {

	public List<GapOrder> findOrders() throws DataAccessException;
	public List<GapOrder> findOrderToday() throws DataAccessException;
	public List<GapOrder> findOrderSelect(String startDay, String endDay) throws DataAccessException;
	public List<GapOrder> findOrderMonth(String mear, String month) throws DataAccessException;
}
