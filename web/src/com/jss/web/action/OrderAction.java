package com.jss.web.action;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.edu.util.DateUtil;
import com.edu.util.LogHelper;
import com.jss.entity.Customer;
import com.jss.entity.GapItem;
import com.jss.entity.GapOrder;
import com.jss.entity.GapOrderItem;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.Product;
import com.jss.entity.ProductType;
import com.jss.entity.Running;
import com.jss.entity.User;
import com.jss.service.CustomerService;
import com.jss.service.GapItemService;
import com.jss.service.GapOrderItemService;
import com.jss.service.GapOrderService;
import com.jss.service.OrdersProductService;
import com.jss.service.OrdersService;
import com.jss.service.PriceService;
import com.jss.service.ProductService;
import com.jss.service.ProductTypeService;
import com.jss.service.RunningService;
import com.jss.service.UserService;
import com.jss.web.common.SumReport;

public class OrderAction extends CoreAction {
	private CustomerService customerService;
	private ProductService productService;
	private ProductTypeService productTypeService;
	private UserService userService;
	private OrdersService ordersService;
	private OrdersProductService ordersProductService;
	private PriceService priceService;
	private RunningService runningService;
	private GapOrderService gapOrderService;
	private GapOrderItemService gapOrderItemService;
	private GapItemService gapItemService;

	public ActionForward index(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<GapOrderItem> result = gapOrderItemService.getAllItems();
			int total = 0;
			int sumTotal= 0;
			for (GapOrderItem row : result) {
				total = row.getQty()*row.getGapItem().getPrice();
				sumTotal += total;
			}
			request.setAttribute("sumTotal", sumTotal);
			request.setAttribute("resultList", result);
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR01");
	}
	

	public ActionForward orderDel(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try{
		
			gapOrderItemService.removeItem(Integer.parseInt(dynaForm.getString("id")));
			
			/*return mapping.findForward("MA02");*/
		}catch (Exception e) { 
			e.printStackTrace();
		}
		return index(mapping, form, request, response);
	}
	
	public ActionForward payPage(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try{
	
		}catch (Exception e) { 
			e.printStackTrace();
		}
		return mapping.findForward("OR05");
	}
	
	public ActionForward menuPage(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {
		DynaActionForm dynaForm = (DynaActionForm) form;
		try{
	
		}catch (Exception e) { 
			e.printStackTrace();
		}
		return mapping.findForward("CU04");
	}
	

	public ActionForward orderNow(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.initialize(mapping);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR04");
	}

	public ActionForward orderSearch(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<Orders> resultList = ordersService.getOrdersList(
					dynaForm.getString("sdate"), dynaForm.getString("edate"),
					dynaForm.getString("custId"), dynaForm.getString("saleId"),
					dynaForm.getString("orderCode"),
					dynaForm.getString("status"));

			for (Orders orders : resultList) {
				Double totalPrice = orders.getTotalPrice();
				if (orders.getTotalUnit() >= 300 && orders.getTotalUnit() < 500) {
					Double Ton300Total = (orders.getTotalUnit() * 1000) * 0.1;
					totalPrice = totalPrice - Ton300Total;

				} else if (orders.getTotalUnit() >= 500) {
					Double Ton500Total = (orders.getTotalUnit() * 1000) * 0.2;
					totalPrice = totalPrice - Ton500Total;
				}

				Orders entity = ordersService.getItem(orders.getId());
				String transport = entity.getTransport();
				if (transport.equals("N")) {
					totalPrice = totalPrice + orders.getTransportPrice();
				}

				if (Integer.parseInt(entity.getDiscount()) > 0
						&& Integer.parseInt(entity.getDiscount()) <= 100) {
					Double discount = (totalPrice * Integer.parseInt(entity
							.getDiscount())) / 100;
					totalPrice = totalPrice - discount;
				}

				String pay = entity.getPay();
				if (pay.equals("D")) {
					Double payPercent = (totalPrice * Integer.parseInt(entity
							.getPayPercent())) / 100;
					totalPrice = totalPrice - payPercent;
				} else if (pay.equals("C")) {
					Double discount = (orders.getTotalUnit() * 1000) * 0.2;
					totalPrice = totalPrice - discount;
				}

				orders.setTotalPrice(totalPrice);

			}

			request.setAttribute("resultList", resultList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR01");
	}

	public ActionForward orderAdd(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			String tab = "1";
			if (dynaForm.getString("tab") != null
					&& !dynaForm.getString("tab").equals("")) {
				tab = dynaForm.getString("tab");
			}
			List<ProductType> productTypeList = productTypeService
					.getAllProductType();
			List<Product> productList = new ArrayList<>();
			List<Customer> custList = customerService.getAllCust();
			List<User> saleList = userService.getSale();
			dynaForm.set("comboProductTypeList", productTypeList);
			dynaForm.set("comboProductList", productList);

			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			request.setAttribute("tab", tab);
			request.setAttribute("id", null);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR02");
	}

	public ActionForward orderEdit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			String tab = "1";
			if (dynaForm.getString("tab") != null
					&& !dynaForm.getString("tab").equals("")) {
				tab = dynaForm.getString("tab");
			}
			List<ProductType> productTypeList = productTypeService
					.getAllProductType();
			List<Product> productList = new ArrayList<>();
			List<Customer> custList = customerService.getAllCust();
			List<User> saleList = userService.getSale();
			List<OrdersProduct> orderspList = ordersProductService
					.getProductbyOrders(dynaForm.getString("id"));
			Orders entity = ordersService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("comboProductTypeList", productTypeList);
			dynaForm.set("comboProductList", productList);
			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			request.setAttribute("id", entity.getId());
			request.setAttribute("tab", tab);
			Integer totalLine = 0;
			Double totalUnit = 0.0;
			Double totalAmount = 0.0;
			if (orderspList.size() > 0) {
				for (OrdersProduct row : orderspList) {
					totalLine += row.getLine();
					totalUnit += row.getWeight();
					totalAmount += row.getPrice();
				}
			}

			request.setAttribute("totalLine", totalLine);
			request.setAttribute("totalUnit", totalUnit);
			request.setAttribute("totalAmount", totalAmount);
			request.setAttribute("orderspList", orderspList);
			dynaForm.set("tab", tab);
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("date",
					DateUtil.date2StringTh(entity.getDate(), "dd/MM/yyyy"));
			dynaForm.set("time", entity.getTime());
			dynaForm.set("cust", String.valueOf(entity.getCustomer().getId()));
			dynaForm.set("sale", String.valueOf(entity.getSale().getId()));
			dynaForm.set("credit", entity.getCredit());
			dynaForm.set("bill", entity.getBill());
			dynaForm.set("discount", entity.getDiscount());
			dynaForm.set("book", entity.getBook());
			dynaForm.set("bookPrice", String.valueOf(entity.getBookPrice()));
			dynaForm.set("pay", entity.getPay());
			dynaForm.set("payPercent", entity.getPayPercent());
			dynaForm.set("transport", entity.getTransport());
			dynaForm.set("transportPrice",
					String.valueOf(entity.getTransportPrice()));
			dynaForm.set("sample", entity.getSample());
			dynaForm.set("brochure", entity.getBrochure());
			dynaForm.set("card", entity.getCard());
			dynaForm.set("remark", entity.getRemark());
			dynaForm.set("vat", entity.getVat());

			boolean isCode = true;
			if (entity.getCode() != null && !entity.getCode().equals("")) {
				isCode = false;
			}
			request.setAttribute("isCode", isCode);
			request.setAttribute("transport", entity.getTransport());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR02");
	}

	public ActionForward orderSave(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			String tab = "1";
			if (dynaForm.getString("tab") != null
					&& !dynaForm.getString("tab").equals("")) {
				tab = dynaForm.getString("tab");
			}
			Integer ordersId;
			if (dynaForm.getString("id") != null
					&& !dynaForm.getString("id").equals("")) {
				ordersId = Integer.parseInt(dynaForm.getString("id"));
				Orders entity = ordersService.getItem(ordersId);
				if (tab.equals("1")) {
					entity.setDate(DateUtil.string2DateTh(dynaForm
							.getString("date")));
					entity.setTime(dynaForm.getString("time"));
					entity.setCustomer(customerService.getItem(Integer
							.parseInt(dynaForm.getString("cust"))));
					entity.setSale(userService.getItem(Integer
							.parseInt(dynaForm.getString("sale"))));

					ordersService.updateItem(entity);
				} else if (tab.equals("2")) {
					if (dynaForm.getString("credit") != null
							&& !dynaForm.getString("credit").equals(""))
						entity.setCredit(dynaForm.getString("credit"));
					if (dynaForm.getString("book") != null
							&& !dynaForm.getString("book").equals(""))
						entity.setBook(dynaForm.getString("book"));
					if (dynaForm.getString("bookPrice") != null
							&& !dynaForm.getString("bookPrice").equals(""))
						entity.setBookPrice(Double.valueOf(dynaForm
								.getString("bookPrice")));
					if (dynaForm.getString("bill") != null
							&& !dynaForm.getString("bill").equals(""))
						entity.setBill(dynaForm.getString("bill"));
					if (dynaForm.getString("pay") != null
							&& !dynaForm.getString("pay").equals(""))
						entity.setPay(dynaForm.getString("pay"));
					if (dynaForm.getString("payPercent") != null
							&& !dynaForm.getString("payPercent").equals(""))
						entity.setPayPercent(dynaForm.getString("payPercent"));
					if (dynaForm.getString("transport") != null
							&& !dynaForm.getString("transport").equals(""))
						entity.setTransport(dynaForm.getString("transport"));
					if (dynaForm.getString("transportPrice") != null
							&& !dynaForm.getString("transportPrice").equals(""))
						entity.setTransportPrice(Double.valueOf(dynaForm
								.getString("transportPrice")));
					if (dynaForm.getString("remark") != null
							&& !dynaForm.getString("remark").equals(""))
						entity.setRemark(dynaForm.getString("remark"));
					if (dynaForm.getString("sample") != null
							&& !dynaForm.getString("sample").equals(""))
						entity.setSample(dynaForm.getString("sample"));
					if (dynaForm.getString("brochure") != null
							&& !dynaForm.getString("brochure").equals(""))
						entity.setBrochure(dynaForm.getString("brochure"));
					if (dynaForm.getString("card") != null
							&& !dynaForm.getString("card").equals(""))
						entity.setCard(dynaForm.getString("card"));
					if (dynaForm.getString("discount") != null
							&& !dynaForm.getString("discount").equals(""))
						entity.setDiscount(dynaForm.getString("discount"));
					entity.setVat(dynaForm.getString("vat"));
					/*
					 * entity.setUpdateBy(getUserSession(request).getFullname());
					 * entity.setUpdateDate(DateUtil.getCurrentDate());
					 */

					ordersService.updateItem(entity);
				} else if (tab.equals("3")) {
					tab = "2";
					if (dynaForm.getString("product") != null
							&& !dynaForm.getString("product").equals("")) {
						if (dynaForm.getString("productId") != null
								&& !dynaForm.getString("productId").equals("")) {
							OrdersProduct orderP = ordersProductService
									.getItem(Integer.parseInt(dynaForm
											.getString("productId")));
							orderP.setOrders(ordersService.getItem(Integer
									.parseInt(dynaForm.getString("id"))));
							orderP.setProduct(productService.getItem(Integer
									.parseInt(dynaForm.getString("product"))));
							orderP.setPriceId(priceService.getItem(Integer
									.parseInt(dynaForm.getString("priceId"))));
							orderP.setPriceUnit(Double.valueOf(dynaForm
									.getString("price")));
							orderP.setLine(Integer.parseInt(dynaForm
									.getString("line")));
							orderP.setWeight(Double.valueOf((dynaForm
									.getString("unit"))));
							orderP.setPrice(Double.valueOf(dynaForm
									.getString("amount")));
							/*
							 * orderP.setUpdateBy(getUserSession(request).
							 * getFullname());
							 * orderP.setUpdateDate(DateUtil.getCurrentDate());
							 */
							ordersProductService.updateItem(orderP);
						} else {
							OrdersProduct orderP = new OrdersProduct();
							orderP.setOrders(ordersService.getItem(Integer
									.parseInt(dynaForm.getString("id"))));
							orderP.setProduct(productService.getItem(Integer
									.parseInt(dynaForm.getString("product"))));
							orderP.setPriceId(priceService.getItem(Integer
									.parseInt(dynaForm.getString("priceId"))));
							orderP.setPriceUnit(Double.valueOf(dynaForm
									.getString("price")));
							orderP.setLine(Integer.parseInt(dynaForm
									.getString("line")));
							orderP.setWeight(Double.valueOf((dynaForm
									.getString("unit"))));
							orderP.setPrice(Double.valueOf(dynaForm
									.getString("amount")));
							/*
							 * orderP.setCreateBy(getUserSession(request).
							 * getFullname());
							 * orderP.setCreateDate(DateUtil.getCurrentDate());
							 */
							ordersProductService.saveItem(orderP);
						}

					}
				} else if (tab.equals("4")) {
					tab = "1";
					Date date = new Date();
					SimpleDateFormat sdfYear = new SimpleDateFormat("yy");
					SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
					int year = Integer.parseInt(sdfYear.format(date)) + 43;
					String month = sdfMonth.format(date);
					List<Running> check = runningService.getRunningCode("PO",
							String.valueOf(year), month);

					String code = "";
					if (check.size() == 0) {
						Running running = new Running();
						running.setType("PO");
						running.setYear(String.valueOf(year));
						running.setMonth(month);
						running.setRunning("001");
						/*
						 * running.setCreateBy(getUserSession(request).getFullname
						 * ());
						 * running.setCreateDate(DateUtil.getCurrentDate());
						 */
						runningService.saveItem(running);
						code = "PO" + String.valueOf(year) + month + "001";
					} else {
						Running running = check.iterator().next();
						int numbers = Integer.parseInt(running.getRunning()) + 1;
						String number = String.format("%03d", numbers);

						running.setRunning(number);
						/*
						 * running.setUpdateBy(getUserSession(request).getFullname
						 * ());
						 * running.setUpdateDate(DateUtil.getCurrentDate());
						 */
						runningService.updateItem(running);

						code = running.getType() + running.getYear()
								+ running.getMonth() + number;
					}

					entity.setStatus("2");
					entity.setCode(code);
					/*
					 * entity.setUpdateBy(getUserSession(request).getFullname());
					 * entity.setUpdateDate(DateUtil.getCurrentDate());
					 */
					ordersService.updateItem(entity);

					return orderSearch(mapping, dynaForm, request, response);
				}

				boolean isCode = true;
				if (entity.getCode() != null && !entity.getCode().equals("")) {
					isCode = false;
				}
				request.setAttribute("isCode", isCode);

			} else {
				Orders entity = new Orders();
				entity.setDate(DateUtil.string2DateTh(dynaForm
						.getString("date")));
				entity.setTime(dynaForm.getString("time"));
				entity.setCustomer(customerService.getItem(Integer
						.parseInt(dynaForm.getString("cust"))));
				entity.setSale(userService.getItem(Integer.parseInt(dynaForm
						.getString("sale"))));
				entity.setStatus("1");
				/*
				 * entity.setCreateBy(getUserSession(request).getFullname());
				 * entity.setCreateDate(DateUtil.getCurrentDate());
				 */
				ordersId = ordersService.saveOrders(entity);
			}

			List<OrdersProduct> orderspList = new ArrayList<>();
			if (dynaForm.getString("id") != null
					&& !dynaForm.getString("id").equals("")) {
				orderspList = ordersProductService.getProductbyOrders(dynaForm
						.getString("id"));
			}

			request.setAttribute("orderspList", orderspList);
			Integer totalLine = 0;
			Double totalUnit = 0.0;
			Double totalAmount = 0.0;
			if (orderspList.size() > 0) {
				for (OrdersProduct row : orderspList) {
					totalLine += row.getLine();
					totalUnit += row.getWeight();
					totalAmount += row.getPrice();
				}
			}

			request.setAttribute("totalLine", totalLine);
			request.setAttribute("totalUnit", totalUnit);
			request.setAttribute("totalAmount", totalAmount);

			dynaForm.set("product", null);
			dynaForm.set("line", null);
			dynaForm.set("unit", null);
			dynaForm.set("amount", null);
			dynaForm.set("weight", null);
			dynaForm.set("price", null);
			dynaForm.set("productType", null);
			dynaForm.set("productId", null);
			dynaForm.set("id", String.valueOf(ordersId));
			dynaForm.set("tab", String.valueOf(Integer.valueOf(tab) + 1));
			request.setAttribute("tab", Integer.valueOf(tab) + 1);
			request.setAttribute("id", ordersId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward("OR02");
	}



	public ActionForward productDelete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			if (dynaForm.getString("productId") != null
					&& !dynaForm.getString("productId").equals("")) {
				ordersProductService.removeItem(Integer.parseInt(dynaForm
						.getString("productId")));
			}

			List<ProductType> productTypeList = productTypeService
					.getAllProductType();
			List<Product> productList = new ArrayList<>();
			List<Customer> custList = customerService.getAllCust();
			List<User> saleList = userService.getSale();
			List<OrdersProduct> orderspList = ordersProductService
					.getProductbyOrders(dynaForm.getString("id"));
			Orders entity = ordersService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("comboProductTypeList", productTypeList);
			dynaForm.set("comboProductList", productList);
			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			request.setAttribute("id", entity.getId());
			request.setAttribute("tab", "3");
			Integer totalLine = 0;
			Double totalUnit = 0.0;
			Double totalAmount = 0.0;
			if (orderspList.size() > 0) {
				for (OrdersProduct row : orderspList) {
					totalLine += row.getLine();
					totalUnit += row.getWeight();
					totalAmount += row.getPrice();
				}
			}

			request.setAttribute("totalLine", totalLine);
			request.setAttribute("totalUnit", totalUnit);
			request.setAttribute("totalAmount", totalAmount);
			request.setAttribute("orderspList", orderspList);
			dynaForm.set("tab", "3");
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("date",
					DateUtil.date2StringTh(entity.getDate(), "dd/MM/yyyy"));
			dynaForm.set("time", entity.getTime());
			dynaForm.set("cust", String.valueOf(entity.getCustomer().getId()));
			dynaForm.set("sale", String.valueOf(entity.getSale().getId()));
			dynaForm.set("credit", entity.getCredit());
			dynaForm.set("bill", entity.getBill());
			dynaForm.set("discount", entity.getDiscount());
			dynaForm.set("book", entity.getBook());
			dynaForm.set("bookPrice", String.valueOf(entity.getBookPrice()));
			dynaForm.set("pay", entity.getPay());
			dynaForm.set("payPercent", entity.getPayPercent());
			dynaForm.set("transport", entity.getTransport());
			dynaForm.set("transportPrice",
					String.valueOf(entity.getTransportPrice()));
			dynaForm.set("sample", entity.getSample());
			dynaForm.set("brochure", entity.getBrochure());
			dynaForm.set("card", entity.getCard());
			dynaForm.set("remark", entity.getRemark());
			dynaForm.set("vat", entity.getVat());

			request.setAttribute("transport", entity.getTransport());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("OR02");
	}

	public ActionForward productEdit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			DynaActionForm dynaForm = (DynaActionForm) form;
			if (dynaForm.getString("productId") != null
					&& !dynaForm.getString("productId").equals("")) {
				OrdersProduct entity = ordersProductService.getItem(Integer
						.parseInt(dynaForm.getString("productId")));
				List<Product> productList = productService
						.getProductList(String.valueOf(entity.getProduct()
								.getProductType().getId()));
				dynaForm.set("comboProductList", productList);

				dynaForm.set(
						"productType",
						String.valueOf(entity.getProduct().getProductType()
								.getId()));
				dynaForm.set("product",
						String.valueOf(entity.getProduct().getId()));
				dynaForm.set("line", String.valueOf(entity.getLine()));
				dynaForm.set("unit", String.valueOf(entity.getWeight()));
				dynaForm.set("price", String.valueOf(entity.getPriceUnit()));
				dynaForm.set("amount", String.valueOf(entity.getPrice()));

				request.setAttribute("product", entity.getProduct().getId());

			}

			List<ProductType> productTypeList = productTypeService
					.getAllProductType();
			List<Customer> custList = customerService.getAllCust();
			List<User> saleList = userService.getSale();
			List<OrdersProduct> orderspList = ordersProductService
					.getProductbyOrders(dynaForm.getString("id"));
			Orders entity = ordersService.getItem(Integer.parseInt(dynaForm
					.getString("id")));
			dynaForm.set("comboProductTypeList", productTypeList);
			dynaForm.set("comboCustomerList", custList);
			dynaForm.set("comboSaleList", saleList);
			request.setAttribute("id", entity.getId());
			request.setAttribute("tab", "3");
			Integer totalLine = 0;
			Double totalUnit = 0.0;
			Double totalAmount = 0.0;
			if (orderspList.size() > 0) {
				for (OrdersProduct row : orderspList) {
					totalLine += row.getLine();
					totalUnit += row.getWeight();
					totalAmount += row.getPrice();
				}
			}

			request.setAttribute("totalLine", totalLine);
			request.setAttribute("totalUnit", totalUnit);
			request.setAttribute("totalAmount", totalAmount);
			request.setAttribute("orderspList", orderspList);
			dynaForm.set("tab", "3");
			dynaForm.set("id", String.valueOf(entity.getId()));
			dynaForm.set("date",
					DateUtil.date2StringTh(entity.getDate(), "dd/MM/yyyy"));
			dynaForm.set("time", entity.getTime());
			dynaForm.set("cust", String.valueOf(entity.getCustomer().getId()));
			dynaForm.set("sale", String.valueOf(entity.getSale().getId()));
			dynaForm.set("credit", entity.getCredit());
			dynaForm.set("bill", entity.getBill());
			dynaForm.set("discount", entity.getDiscount());
			dynaForm.set("book", entity.getBook());
			dynaForm.set("bookPrice", String.valueOf(entity.getBookPrice()));
			dynaForm.set("pay", entity.getPay());
			dynaForm.set("payPercent", entity.getPayPercent());
			dynaForm.set("transport", entity.getTransport());
			dynaForm.set("transportPrice",
					String.valueOf(entity.getTransportPrice()));
			dynaForm.set("sample", entity.getSample());
			dynaForm.set("brochure", entity.getBrochure());
			dynaForm.set("card", entity.getCard());
			dynaForm.set("remark", entity.getRemark());
			dynaForm.set("vat", entity.getVat());

			request.setAttribute("transport", entity.getTransport());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("OR02");
	}

	public ActionForward orderAppove(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		Orders entity = ordersService.getItem(Integer.parseInt(dynaForm
				.getString("id")));
		entity.setStatus(dynaForm.getString("status"));
		/*
		 * entity.setUpdateBy(getUserSession(request).getFullname());
		 * entity.setUpdateDate(DateUtil.getCurrentDate());
		 */
		ordersService.updateItem(entity);

		return orderSearch(mapping, dynaForm, request, response);
	}

	public ActionForward orderDetail(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		DynaActionForm dynaForm = (DynaActionForm) form;
		Orders entity = ordersService.getItem(Integer.parseInt(dynaForm
				.getString("id")));
		List<OrdersProduct> orderspList = ordersProductService
				.getProductbyOrders(dynaForm.getString("id"));
		Integer totalLine = 0;
		Double totalUnit = 0.0;
		Double totalAmount = 0.0;
		if (orderspList.size() > 0) {
			for (OrdersProduct row : orderspList) {
				totalLine += row.getLine();
				totalUnit += row.getWeight();
				totalAmount += row.getPrice();
			}
		}
		request.setAttribute("totalLine", totalLine);
		request.setAttribute("totalUnit", totalUnit);
		request.setAttribute("totalAmount", totalAmount);
		request.setAttribute("orderspList", orderspList);

		dynaForm.set("book", entity.getBook());
		dynaForm.set("bookPrice", String.valueOf(entity.getBookPrice()));
		dynaForm.set("bill", entity.getBill());
		dynaForm.set("pay", entity.getPay());
		dynaForm.set("payPercent", entity.getPayPercent());
		dynaForm.set("transport", entity.getTransport());
		dynaForm.set("transportPrice",
				String.valueOf(entity.getTransportPrice()));
		dynaForm.set("sample", entity.getSample());
		dynaForm.set("brochure", entity.getBrochure());
		dynaForm.set("card", entity.getCard());
		dynaForm.set("vat", entity.getVat());

		request.setAttribute("code", entity.getCode());
		request.setAttribute("date", entity.getDateShow());
		request.setAttribute("custName", entity.getCustomer().getName());
		request.setAttribute("saleName", entity.getSale().getFullname());
		request.setAttribute("credit", entity.getCredit());
		request.setAttribute("book", entity.getBook());
		request.setAttribute("pay", entity.getPay());
		request.setAttribute("discount", entity.getDiscount() + "%");
		request.setAttribute("transport", entity.getTransport());
		request.setAttribute("remark", entity.getRemark());
		request.setAttribute("discount", entity.getDiscount());
		return mapping.findForward("OR03");
	}

	public ActionForward comboOrders(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			List<Orders> results = new ArrayList<Orders>();
			String id = request.getParameter("id");
			if (id != null && !id.equals("")) {
				results = ordersService.getComboOrders(id);
			}

			setJSONResult(request, setJsonView(results, null));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnToJson(mapping);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void printPricePdf(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			Orders entity = ordersService.getItem(Integer.parseInt(request
					.getParameter("id")));
			List<OrdersProduct> resultList = new ArrayList<>();
			List<OrdersProduct> orderspList = ordersProductService
					.getProductbyOrders(request.getParameter("id"));

			DecimalFormat df = new DecimalFormat();
			df.applyPattern("#,##0.000");
			DecimalFormat df2 = new DecimalFormat();
			df2.applyPattern("#,##0");

			Integer line = 0;
			Double unit = 0.0;
			Double amount = 0.0;
			Double totalAmount = 0.0;
			int i = 1;
			if (orderspList.size() > 0) {
				for (OrdersProduct row : orderspList) {
					OrdersProduct po = new OrdersProduct();
					po.setColSeq(String.valueOf(i));

					po.setColPrice(df.format(row.getPriceId().getPrice()));
					po.setColPriceUnit(df.format(row.getPriceUnit()));
					po.setColLine(df2.format(row.getLine()));
					po.setColUnit(df.format(row.getWeight()));
					po.setColTotalPrice(df.format(row.getPrice()));
					resultList.add(po);

					line += row.getLine();
					unit += row.getWeight();
					amount += row.getPrice();

					i++;
				}
				if (i < 11) {
					for (int j = 0; j < 8; j++) {
						OrdersProduct po = new OrdersProduct();
						po.setColSeq("");
						po.setColName("");
						po.setColPrice("");
						po.setColPriceUnit("");
						po.setColLine("");
						po.setColUnit("");
						po.setColTotalPrice("");
						resultList.add(po);
					}
				}

			} else {
				if (i < 11) {
					for (int j = 0; j < 8; j++) {
						OrdersProduct po = new OrdersProduct();
						po.setColSeq("");
						po.setColName("");
						po.setColPrice("");
						po.setColPriceUnit("");
						po.setColLine("");
						po.setColUnit("");
						po.setColTotalPrice("");
						resultList.add(po);
					}
				}
			}

			totalAmount = amount;

			Double Ton300Total = 0.0;
			Double Ton500Total = 0.0;
			if (unit >= 300 && unit < 500) {
				Ton300Total = (unit * 1000) * 0.1;
				totalAmount = totalAmount - Ton300Total;
			} else if (unit >= 500) {
				Ton500Total = (unit * 1000) * 0.2;
				totalAmount = totalAmount - Ton500Total;
			}

			Double totalDiscount = 0.0;
			Integer discount = Integer.parseInt(entity.getDiscount());
			if (discount > 0 && discount <= 100) {
				totalDiscount = (totalAmount * discount) / 100;
				totalAmount = totalAmount - totalDiscount;
			}

			Double cashTotal = 0.0;
			Double deposit = 0.0;
			if (entity.getPay().equals("C")) {
				cashTotal = (unit * 1000) * 0.2;
				totalAmount = totalAmount - cashTotal;
			} else if (entity.getPay().equals("D")) {
				deposit = (totalAmount * Integer.parseInt(entity
						.getPayPercent())) / 100;
				totalAmount = totalAmount - deposit;
			}

			if (entity.getTransport().equals("N")) {
				totalAmount = totalAmount + entity.getTransportPrice();
			}

			HashMap<String, String> paramsMap = new HashMap<String, String>();

			paramsMap.put("orderCode", entity.getCode());
			paramsMap.put("orderDate", entity.getDateShow());
			/*
			 * paramsMap.put("date",
			 * DateUtil.date2StringTh(entity.getCreateDate(),"dd/MM/yyyy"));
			 */
			paramsMap.put("customerName", entity.getCustomer().getName());
			paramsMap.put("saleName", entity.getSale().getFullname());
			paramsMap.put("book", entity.getBook());
			paramsMap.put("bookPrice", df.format(entity.getBookPrice()));
			paramsMap.put("bill", entity.getBill());
			paramsMap.put("credit", entity.getCredit());
			paramsMap.put("pay", entity.getPay());
			paramsMap.put("transport", entity.getTransport());
			paramsMap.put("transportPrice",
					df.format(entity.getTransportPrice()));
			paramsMap.put("vat", entity.getVat());
			paramsMap.put("sample", entity.getSample());
			paramsMap.put("brochure", entity.getBrochure());
			paramsMap.put("card", entity.getCard());
			paramsMap.put("line", df2.format(line));
			paramsMap.put("unit", df.format(unit));
			paramsMap.put("amount", df.format(amount));
			paramsMap.put("payPercent", entity.getPayPercent());
			paramsMap.put("cashTotal", df.format(cashTotal));
			paramsMap.put("deposit", df.format(deposit));
			paramsMap.put("Ton300Total", df.format(Ton300Total));
			paramsMap.put("Ton500Total", df.format(Ton500Total));
			paramsMap.put("discount", entity.getDiscount());
			paramsMap.put("totalTransport",
					df.format(entity.getTransportPrice()));
			paramsMap.put("totalDiscount", df.format(totalDiscount));
			paramsMap.put("totalAmount", df.format(totalAmount));
			paramsMap.put("remark", entity.getRemark());
			paramsMap.put("img", "/checked.png");

			String path = "/pdf/orders.jasper";
			JRDataSource dataSource = new JRBeanCollectionDataSource(resultList);
			executeReport(request, response, path, paramsMap, dataSource,
					"/pdf/orders.jasper", false);
		} catch (Exception e) {
			LogHelper.doError(e.getMessage(), e);
		}
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public OrdersProductService getOrdersProductService() {
		return ordersProductService;
	}

	public void setOrdersProductService(
			OrdersProductService ordersProductService) {
		this.ordersProductService = ordersProductService;
	}

	public PriceService getPriceService() {
		return priceService;
	}

	public void setPriceService(PriceService priceService) {
		this.priceService = priceService;
	}

	public RunningService getRunningService() {
		return runningService;
	}

	public void setRunningService(RunningService runningService) {
		this.runningService = runningService;
	}

	public GapOrderService getGapOrderService() {
		return gapOrderService;
	}

	public void setGapOrderService(GapOrderService gapOrderService) {
		this.gapOrderService = gapOrderService;
	}

	public GapOrderItemService getGapOrderItemService() {
		return gapOrderItemService;
	}

	public void setGapOrderItemService(GapOrderItemService gapOrderItemService) {
		this.gapOrderItemService = gapOrderItemService;
	}

	public GapItemService getGapItemService() {
		return gapItemService;
	}

	public void setGapItemService(GapItemService gapItemService) {
		this.gapItemService = gapItemService;
	}

}
