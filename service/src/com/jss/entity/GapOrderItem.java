package com.jss.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "gap_order_item")
public class GapOrderItem extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "order_item_id") 
	private int id;
	
	@Column(name = "quantity") 
	private int qty;
	
	@ManyToOne
	@JoinColumn(name = "order_id")
	private GapOrder gapOrder;
	
	@ManyToOne
	@JoinColumn(name = "item_id")
	private GapItem gapItem;
	
	@Column(name = "vetgetable") 
	private int vet;
	
	@Column(name = "noodle_type") 
	private int noodleType;
	
	@Column(name = "extra") 
	private int extra;
	
	@Column(name = "all_price") 
	private int allPrice;
	
	@Column(name = "status") 
	private int status;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getVet() {
		return vet;
	}

	public void setVet(int vet) {
		this.vet = vet;
	}

	public int getNoodleType() {
		return noodleType;
	}

	public void setNoodleType(int noodleType) {
		this.noodleType = noodleType;
	}

	public int getExtra() {
		return extra;
	}

	public void setExtra(int extra) {
		this.extra = extra;
	}

	public int getAllPrice() {
		return allPrice;
	}

	public void setAllPrice(int allPrice) {
		this.allPrice = allPrice;
	}

	public GapOrder getGapOrder() {
		return gapOrder;
	}

	public void setGapOrder(GapOrder gapOrder) {
		this.gapOrder = gapOrder;
	}

	public GapItem getGapItem() {
		return gapItem;
	}

	public void setGapItem(GapItem gapItem) {
		this.gapItem = gapItem;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	






	
	


	
	
	

	
}
