package com.edu.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;


public class DateUtil {

	//G	Era designator	Text	AD
	//y	Year	Year	1996; 96
	//M	Month in year	Month	July; Jul; 07
	//w	Week in year	Number	27
	//W	Week in month	Number	2
	//D	Day in year	Number	189
	//d	Day in month	Number	10
	//F	Day of week in month	Number	2
	//E	Day in week	Text	Tuesday; Tue
	//a	Am/pm marker	Text	PM
	//H	Hour in day (0-23)	Number	0
	//k	Hour in day (1-24)	Number	24
	//K	Hour in am/pm (0-11)	Number	0
	//h	Hour in am/pm (1-12)	Number	12
	//m	Minute in hour	Number	30
	//s	Second in minute	Number	55
	//S	Millisecond	Number	978
	//z	Time zone	General time zone	Pacific Standard Time; PST; GMT-08:00
	//Z	Time zone	RFC 822 time zone	-0800
	
	public static int monthBetween(String monthStart,String yearStart,String monthStop,String yearStop) {
		int m1 = Integer.parseInt(monthStart);
		int m2 = Integer.parseInt(monthStop);		
		int y1 = Integer.parseInt(yearStart);
		int y2 = Integer.parseInt(yearStop);
		int i = 0;	
		if(y2 > y1){
				return (m2 + ((y2 - y1) * 12)) - m1;
		}else if(y2 == y1){
			if(m2 > m1){
				return m2 - m1;
			}
		}
		return i;
	}
	
    public static int date2age(java.util.Date date) {
        int y = Integer.parseInt(date2String(date,"yyyy"));
        int m = Integer.parseInt(date2String(date,"M"));
        int d = Integer.parseInt(date2String(date,"d"));
        
        int cy = Integer.parseInt(date2String(getCurrentDate(),"yyyy"));
        int cm = Integer.parseInt(date2String(getCurrentDate(),"M"));
        int cd = Integer.parseInt(date2String(getCurrentDate(),"d"));
        
        int res = cy - y;
        if(res < 1){
        	return 0;
        }else{
        	if(m > cm){
        		res--;
        	}else if(m == cm && d > cd){
        		res--;
        	}
        }
      return res;
    }
    
    public static int date2age(java.util.Date date,java.util.Date curDate) {
        int y = Integer.parseInt(date2String(date,"yyyy"));
        int m = Integer.parseInt(date2String(date,"M"));
        int d = Integer.parseInt(date2String(date,"d"));
        
        int cy = Integer.parseInt(date2String(curDate,"yyyy"));
        int cm = Integer.parseInt(date2String(curDate,"M"));
        int cd = Integer.parseInt(date2String(curDate,"d"));
        
        int res = cy - y;
        if(res < 1){
        	return 0;
        }else{
        	if(m > cm){
        		res--;
        	}else if(m == cm && d > cd){
        		res--;
        	}
        }
      return res;
    }
    
    public static String date2ageFull(java.util.Date date) {
    	if(date != null){
        	return date2ageFull(date,getCurrentDate());
    	}else{
    		return "";
    	}
    }

    public static String date2ageFull(java.util.Date dateStart,java.util.Date dateStop) {
    	
    	GregorianCalendar cal1 = new GregorianCalendar();
        cal1.setTimeInMillis(dateStart.getTime());

        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.setTimeInMillis(dateStop.getTime());

        int years = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
        int months = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
        int days = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
        if (days < 0) {
            months--;
            days += cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
        }
        if (months < 0) {
            years--;
            months += 12;
        }
        
    	String dateStr=""; 
    	if(years > 0){   		
    		dateStr += years + " �� ";
    	}
	    
    	if(months != 0){
    		dateStr += months + " ��͹  ";
	    }
    	
    	if(days != 0){
    		dateStr += days + " �ѹ";
	    }
    	return dateStr;
    }
    
    public static int date2ageMonth(java.util.Date date) {
        int y = Integer.parseInt(date2String(date,"yyyy"));
        int m = Integer.parseInt(date2String(date,"M"));
        int d = Integer.parseInt(date2String(date,"d"));
        
        int cy = Integer.parseInt(date2String(getCurrentDate(),"yyyy"));
        int cm = Integer.parseInt(date2String(getCurrentDate(),"M"));
        int cd = Integer.parseInt(date2String(getCurrentDate(),"d"));
        
        int mon = 0;
        int res = cy - y;
        if(res < 1){
        	return 0;
        }else{
        	mon = cm-m;
        	if (mon < 1) {
        		return 0;
        	}
        }
      return mon;
    }

    public static List<Date> dayOfYear(String year,String day) {
    	Date dateStart = DateUtil.string2DateTh("01/10/" + String.valueOf(Integer.parseInt(year)-1));
    	Date dateStop = DateUtil.string2DateTh("30/09/" + year);  	
    	
    	List objList = new ArrayList();
    	for(long i=dateStart.getTime();i <= dateStop.getTime();){
    		Date date = new Date(i);
    		if(day != null &&  !day.equals("") && day.equals(DateUtil.date2String(date, "E")))
    		objList.add(date);	   		
    		i += (1000 * 60 * 60 * 24);
    	}
    	return objList;    	
    }
    
    public static String retireYear(java.util.Date birthday) {
        if (birthday == null || birthday.equals("")) {
            return "";
        }
        int y = Integer.parseInt(date2StringTh(birthday,"yyyy"));
        int m = Integer.parseInt(date2String(birthday,"MM"));
        int d = Integer.parseInt(date2String(birthday,"dd"));
        if(m >= 10){
            if(m == 10 && d == 1){
                return y + 60 + "";
            }else{
                return y + 61 + "";
            }
        }else{
            return y + 60 + "";
        }
    }

    public static String dateTh2dateEn(String date){
        if (date == null || date.equals("")) {
            return "";
        }
        return date2String(string2DateTh(date));
    }

    public static String date2Show(java.util.Date date){
            if (date == null || date.equals("")) {
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
            String strDate = sdf.format(date);
            return strDate;
    }

    public static String date2String(java.util.Date date){
            if (date == null || date.equals("")) {
                    return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = sdf.format(date);
            return strDate;
    }

    public static String date2String(java.util.Date date,String format){
            if (date == null || date.equals("")) {
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            String strDate = sdf.format(date);
            return strDate;
    }
    
    public static String date2StringEn(java.util.Date date,String format){
        if (date == null || date.equals("")) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, new Locale ("en", "US"));
        String strDate = sdf.format(date);
        return strDate;
    }

    public static String date2StringTh(java.util.Date date){
            if (date == null || date.equals("")) {
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy  HH:mm น.", new Locale("th","th"));
            String strDate = sdf.format(date);
            return strDate;
    }

    public static String date2StringTh(java.util.Date date,String format){
            if (date == null || date.equals("")) {
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat(format, new Locale("th","th"));
            String strDate = sdf.format(date);
            return strDate;
    }



    public static java.util.Date string2Date(String dateStr){
            if (dateStr == null) {
                return new java.util.Date();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                    java.util.Date date = sdf.parse(dateStr);
                    return date;
            } catch (Exception x) {
                    return null;
            }
    }

    public static java.util.Date string2DateTh(String dateStr,String format){
        if (dateStr == null) {
                return new java.util.Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, new Locale("th","th"));
        try {
                java.util.Date date = sdf.parse(dateStr);
                return date;
        } catch (Exception x) {
                return null;
        }
}
    
    public static java.util.Date string2DateEn(String dateStr,String format){
        if (dateStr == null) {
                return new java.util.Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, new Locale("en","en"));
        try {
                java.util.Date date = sdf.parse(dateStr);
                return date;
        } catch (Exception x) {
                return null;
        }
}
    
    public static java.util.Date string2Date(String dateStr,String format){
        if (dateStr == null) {
            return new java.util.Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
                java.util.Date date = sdf.parse(dateStr);
                return date;
        } catch (Exception x) {
                return null;
        }
}
    public static java.util.Date string2DateTh(String dateStr){
            if (dateStr == null) {
                    return new java.util.Date();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("th","th"));
            try {
                    java.util.Date date = sdf.parse(dateStr);
                    return date;
            } catch (Exception x) {
                    return null;
            }
    }


	public static Timestamp getCurrentTimestamp() throws Exception {
		return new Timestamp(new java.util.Date().getTime());
	}

	public static java.util.Date getCurrentDate() {
		return new java.util.Date();
	}

        public static String getBudgetYear() throws Exception {
            String returnYear = "";
            String currDate = date2StringTh(getCurrentDate());
            String month = currDate.substring(3, 5);
            String year = currDate.substring(6, 10);
            if(Integer.parseInt(month) >= 10){
                returnYear = String.valueOf(Integer.parseInt(year)+ 1);
            }else{
                returnYear = year;
            }
            return returnYear;
        }

        public static String getBudgetYear(Date date) throws Exception {
            String returnYear = "";
            String currDate = date2StringTh(date);
            String month = currDate.substring(3, 5);
            String year = currDate.substring(6, 10);
            if (Integer.parseInt(month) >= 10) {
                returnYear = String.valueOf(Integer.parseInt(year) + 1);
            } else {
                returnYear = year;
            }
            return returnYear;
        }

        public static String getCurrentYear() throws Exception {
            String currDate = date2StringTh(getCurrentDate());
            return currDate.substring(6, 10);
        }

        public static String getCurrentMonth() throws Exception {
            String currDate = date2String(getCurrentDate());
            return currDate.substring(3, 5);
        }

        public static String getCurrentDDMMYYYYhh24miss_CEExcludeColonSlash() throws
                Exception {
            String tsStr = getCurrentTimestamp().toString();
            tsStr = tsStr.substring(0, 4) + tsStr.substring(5, 7) +
                    tsStr.substring(8, 10) +
                    tsStr.substring(11, 19).replaceAll(":", "");
            return tsStr;
        }

        public static long getDayAmount(String finishDate, String startDate) {
        	Long durationMs = DateUtil.string2DateTh(finishDate).getTime() - DateUtil.string2DateTh(startDate).getTime();
    		long days = ((durationMs / (1000 * 60 * 60)) / 24) + 1;
            return days;
        }

        public static long getDayAmount(Date finishDate, Date startDate) {
        	Long durationMs = finishDate.getTime() - startDate.getTime();
    		long days = ((durationMs / (1000 * 60 * 60)) / 24) + 1;
            return days;
        }
        
        public static Date getPlusDay(Date startDate, long numberDay) {
    		long days = startDate.getTime() + ((numberDay) * (1000 * 60 * 60) * 24);
            return new Date(days);
        }
        
        public static Date getMinusDay(Date startDate, long numberDay) {
    		long days = startDate.getTime() - ((numberDay) * (1000 * 60 * 60) * 24);
            return new Date(days);
        }
        
        public static Boolean getBetween(Date startDate, Date stopDate) {
        	if(startDate != null && stopDate != null){
        		if(startDate.getTime() <= getCurrentDate().getTime() && getCurrentDate().getTime() <= getPlusDay(stopDate,1).getTime()){
        			return true;
        		}else{
        			return false;
        		}        		
        	}else{
        		return false;
        	}
            
        }
        
        public static Date getDateWithoutTime() throws ParseException {
    		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    		Date today = new Date();

    		Date todayWithZeroTime = formatter.parse(formatter.format(today));
    		return todayWithZeroTime;
    	}
        
        public static String date2ageYear(java.util.Date dateStart,java.util.Date dateStop) {
        	
        	GregorianCalendar cal1 = new GregorianCalendar();
            cal1.setTimeInMillis(dateStart.getTime());

            GregorianCalendar cal2 = new GregorianCalendar();
            cal2.setTimeInMillis(dateStop.getTime());

            int years = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int months = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int days = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
            if (days < 0) {
                months--;
                days += cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            if (months < 0) {
                years--;
                months += 12;
            }
            
        	return years+"";
        }
        
        public static String date2ageMonth(java.util.Date dateStart,java.util.Date dateStop) {
        	
        	GregorianCalendar cal1 = new GregorianCalendar();
            cal1.setTimeInMillis(dateStart.getTime());

            GregorianCalendar cal2 = new GregorianCalendar();
            cal2.setTimeInMillis(dateStop.getTime());

            int years = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int months = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int days = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
            if (days < 0) {
                months--;
                days += cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            if (months < 0) {
                years--;
                months += 12;
            }
            
        	return months+"";
        }
        
        public static String date2ageDay(java.util.Date dateStart,java.util.Date dateStop) {
        	
        	GregorianCalendar cal1 = new GregorianCalendar();
            cal1.setTimeInMillis(dateStart.getTime());

            GregorianCalendar cal2 = new GregorianCalendar();
            cal2.setTimeInMillis(dateStop.getTime());

            int years = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
            int months = cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
            int days = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
            if (days < 0) {
                months--;
                days += cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            if (months < 0) {
                years--;
                months += 12;
            }
            
        	return days+"";
        }
        
        public static long getTimeDuration(String time1 , String time2) throws ParseException {
        	SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        	Date date1 = format.parse(time1);
        	Date date2 = format.parse(time2);
        	long difference = date2.getTime() - date1.getTime(); 
            return difference;
        }
        
   
        public static String getPlusTime(String time, int plusMin) throws ParseException {
        	SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        	Date date1 = format.parse(time);
        	long timePlus = date1.getTime() + (plusMin * (1000*60));
        	
        	Date date2 = new Date(timePlus);
            return format.format(date2);
        }
        
        public static int getMinute(String time) throws ParseException {
        	if(time.length() == 5){
        	Integer hour = Integer.parseInt(time.substring(0, 2)) * 60;
        	Integer min = Integer.parseInt(time.substring(3, 5));
        	return hour + min;
        	}else{
        		return 0;
        	}
        }
        
        public static int getMinuteOT(String time) throws ParseException {
        	if(!time.equals("") && !time.equals("0")){        	
        	Integer hour = Integer.parseInt(time.replace(".5", "")) * 60;

        	if(time.contains(".5")){
        		return hour + 30;
        	}else{
        		return hour;
        	}
        	}else{
        		return 0;
        	}
        }
        public static int yearOfDate(Date date) {
    		if (date != null) {
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date);    			
    			return cal.get(Calendar.YEAR)+543;
    		} else {
    			return 0;
    		}
    	}
        public static String monthOfDate(Date date) {
    		if (date != null) {
    			Calendar cal = Calendar.getInstance();
    			cal.setTime(date);
    			return getMonthName(cal.get(Calendar.MONTH));
    		} else {
    			return null;
    		}
    	}
        
        public static String getMonthName(int month) {
    		String[] data = new String[] { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
    		return data[month];
    	}
}
