package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.SubDistrictDao;
import com.jss.entity.SubDistrict;
import com.jss.service.SubDistrictService;

public class SubDistrictServiceImpl extends CoreServiceImpl<SubDistrict, Serializable>implements SubDistrictService {

	private SubDistrictDao subDistrictDao;
	public SubDistrictServiceImpl(SubDistrictDao subDistrictDao) {
		super(subDistrictDao);
		this.subDistrictDao = subDistrictDao;
	}
	@Override
	public List<SubDistrict> getSubDistrictbyDistrict(String distId)throws DataAccessException {
		return subDistrictDao.findSubDistrictbyDistrict(distId);
	}
	
}
