package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.User;

public interface CarService extends CoreService<Car, Serializable> {
	
	public List<Car> getCarByCust(String custId) throws DataAccessException;
}
