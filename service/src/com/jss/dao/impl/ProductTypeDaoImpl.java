package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.ProductTypeDao;
import com.jss.entity.Customer;
import com.jss.entity.ProductType;

public class ProductTypeDaoImpl extends CoreDaoImpl<ProductType, Serializable> implements ProductTypeDao {

	public ProductTypeDaoImpl(Class<ProductType> entityClass) {super(entityClass);}

	@Override
	public List<ProductType> findAllProductType() throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(ProductType.class);
		criteria.addOrder(Order.asc("name"));	
	
		return getHibernateTemplate().findByCriteria(criteria);
	}

	
	

}