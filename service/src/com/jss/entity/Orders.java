package com.jss.entity;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;
import com.edu.util.DateUtil;

@Entity
@Table(name = "orders")
public class Orders extends CoreEntity{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name = "order_id") 
	private int id;
	
	@Column(name = "order_code")
	private String code;
	
	@Column(name = "order_date")
	private Date date;
	
	@Column(name = "vat")
	private String vat;
	
	@Column(name = "order_time")
	private String time;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@ManyToOne
	@JoinColumn(name = "sale_id")
	private User sale;
	
	@Column(name = "credit")
	private String credit;
	
	@Column(name = "bill")
	private String bill;
	
	@Column(name = "discount")
	private String discount;
	
	@Column(name = "book")
	private String book;
	
	@Column(name = "book_price")
	private Double bookPrice;

	@Column(name = "pay")
	private String pay;
	
	@Column(name = "pay_percent")
	private String payPercent;
	
	@Column(name = "transport")
	private String transport;
	
	@Column(name = "transport_price")
	private Double transportPrice;
	
	@Column(name = "sample")
	private String sample;
	
	@Column(name = "brochure")
	private String brochure;
	
	@Column(name = "card")
	private String card;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "remark")
	private String remark;
	
	
	@Transient
	private String dateShow;
	@Transient
	private Double totalUnit;
	@Transient
	private Double totalPrice;
	@Transient
	private Double tUnit;
	@Transient
	private Double tPrice;
	@Transient
	private String custName;
	@Transient
	private String saleName;
	@Transient
	private String codeShow;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public User getSale() {
		return sale;
	}

	public void setSale(User sale) {
		this.sale = sale;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getBill() {
		return bill;
	}

	public void setBill(String bill) {
		this.bill = bill;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}

	public String getPayPercent() {
		return payPercent;
	}

	public void setPayPercent(String payPercent) {
		this.payPercent = payPercent;
	}

	public String getTransport() {
		return transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public Double getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(Double bookPrice) {
		this.bookPrice = bookPrice;
	}

	public Double getTransportPrice() {
		return transportPrice;
	}

	public void setTransportPrice(Double transportPrice) {
		this.transportPrice = transportPrice;
	}

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public String getBrochure() {
		return brochure;
	}

	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}
	public String getDateShow() {
		String date = DateUtil.date2StringTh(getDate(), "dd/MM/yyyy")+" "+getTime()+" น.";
		return date;
	}

	public void setDateShow(String dateShow) {
		this.dateShow = dateShow;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public Double getTotalUnit() {
		return totalUnit;
	}

	public void setTotalUnit(Double totalUnit) {
		this.totalUnit = totalUnit;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	public String getCodeShow() {
		DecimalFormat df = new DecimalFormat( "#,##0.000" );
		return getCode()+ " ("+df.format(getTotalUnit())+") ";
	}

	public void setCodeShow(String codeShow) {
		this.codeShow = codeShow;
	}

	public Double gettUnit() {
		return tUnit;
	}

	public void settUnit(Double tUnit) {
		this.tUnit = tUnit;
	}

	public Double gettPrice() {
		return tPrice;
	}

	public void settPrice(Double tPrice) {
		this.tPrice = tPrice;
	}
	
	
	
	
}
