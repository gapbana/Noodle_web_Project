<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="inc_menu.jsp" %>
<script language="javascript" type="text/javascript">

function doOeders() {
	location.href='report.htm?mode=reportMonthlySalesHome';
}


function doStock() {
	location.href='stock.htm?mode=stockSearch';
}
function doTransport1() {
	location.href='transport.htm?mode=transportSearch';
}

function doTransport2() {
	location.href='report.htm?mode=reportMonthlyTransport';
}
  
</script>
</head>
<body>
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">˹����ѡ</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <html:form action="/login" styleId="loginForm">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${orderUnit}" /></div>
                                    <div>��§ҹ�ʹ���</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                               	<a href="#" onclick="doOeders();">
	                                <span class="pull-left">��������´</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                           	</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-truck fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${transportUnit}" /></div>
                                    <div>�ʹ�Ѵ���Թ���</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <a href="#" onclick="doTransport1();">
	                                <span class="pull-left">��������´</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                           	</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fas fa-bomb fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${total}"/></div>
                                    <div>�ʹ��ҧ���Թ���</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                            	<a href="#" onclick="doTransport2();">
	                                <span class="pull-left">��������´</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                           	</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-building fa-4x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><fmt:formatNumber type ="number" pattern = "#,##0.000" value = "${stockUnit}"/></div>
                                    <div>�ӹǹ�Թ��Ҥ������</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                              	<a href="#" onclick="doStock();">
	                                <span class="pull-left">��������´</span>
	                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                                <div class="clearfix"></div>
	                           	</a>
                            </div>
                        </a>
                    </div>
                </div>                
            </div>
       		</html:form>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div> 
</body>
</html>