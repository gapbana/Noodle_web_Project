package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.UserGroup;

public interface OrdersProductDao extends CoreDao<OrdersProduct, Serializable> {

	public List<OrdersProduct> findProductbyOrders(String ordersId) throws DataAccessException;
}
