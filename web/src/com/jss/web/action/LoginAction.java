package com.jss.web.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.springframework.dao.DataAccessException;
import org.springframework.web.struts.DispatchActionSupport;

import com.edu.util.DateUtil;
import com.jss.entity.Constant;
import com.jss.entity.Orders;
import com.jss.entity.Product;
import com.jss.entity.Stock;
import com.jss.entity.Transport;
import com.jss.entity.User;
import com.jss.service.ConstantService;
import com.jss.service.OrdersService;
import com.jss.service.ProductService;
import com.jss.service.StockService;
import com.jss.service.TransportService;
import com.jss.service.UserService;

public class LoginAction extends CoreAction{	
	private UserService userService;
	private ConstantService constantService;
	private OrdersService ordersService;
	private TransportService transportService;
	private StockService stockService;
	private ProductService productService;
	
	public ActionForward index(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {	
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			dynaForm.set("un", "g");
			dynaForm.set("pwd", "g");
			String txter = null;
			String er = request.getParameter("er");
			if(!er.equals("") && er != null){
				if(er.equals("91")){
					txter = "ไม่สามารถเข้าสู่ระบบได้ กรุณาเข้าสู่ระบบใหม่อีกครั้ง";
				}else if (er.equals("90")) {
					txter = "ชื่อผู้ใช้งาน หรือ รหัสผ่านผิด กรุณากรอกใหม่อีกครั้ง";
				}
			}
			
			request.setAttribute("error", txter);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("LOG");
	}
	public ActionForward home(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {	
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("HOME");
	}
	public ActionForward login(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {	
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			List<User> check = userService.getLogin(dynaForm.getString("un"), dynaForm.getString("pwd"));
			if (check != null && check.size() > 0) {
				User user = check.iterator().next();
				setObjectSession(request, SESSION_USER, user);
				dynaForm.set("ssLoginUser", user);
				Constant constant = constantService.getItem(1);
				setObjectSession(request, PATH_UPLOAD, constant.getPath());
			

			} else {
				return mappingForward(mapping, request, "mode", "index", "login.htm", "loginForm", "&er=90");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return mappingForward(mapping, request, "mode", "index", "login.htm", "loginForm", "&er=91");
		}

		return mappingForward(mapping, request, "mode", "home", "login.htm", "userForm", null);
	}
	public ActionForward logout(ActionMapping mapping, ActionForm form,  HttpServletRequest request, HttpServletResponse response)throws Exception {	
		try{
			DynaActionForm dynaForm = (DynaActionForm) form;
			resetForm(dynaForm, mapping, request);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return mappingForward(mapping, request, "mode", "index", "login.htm", "loginForm", "&er=");
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public ConstantService getConstantService() {
		return constantService;
	}
	public void setConstantService(ConstantService constantService) {
		this.constantService = constantService;
	}
	public OrdersService getOrdersService() {
		return ordersService;
	}
	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}
	public TransportService getTransportService() {
		return transportService;
	}
	public void setTransportService(TransportService transportService) {
		this.transportService = transportService;
	}
	public StockService getStockService() {
		return stockService;
	}
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}
	public ProductService getProductService() {
		return productService;
	}
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	
	
	
	
	
	
}
