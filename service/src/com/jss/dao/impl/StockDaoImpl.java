package com.jss.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.StockDao;
import com.jss.entity.Product;
import com.jss.entity.Stock;

public class StockDaoImpl extends CoreDaoImpl<Stock, Serializable> implements StockDao {

	public StockDaoImpl(Class<Stock> entityClass) {
		super(entityClass);
		}	

	@Override
	public List<Stock> findStockByProduct(String productId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Stock.class);
		criteria.createAlias("product","product");
		criteria.addOrder(Order.desc("date"));	
		
		if(productId != null && ! productId.equals("")){
			criteria.add(Restrictions.eq("product.id", Integer.parseInt(productId)));
		}
	
		return getHibernateTemplate().findByCriteria(criteria);
	}

}
	