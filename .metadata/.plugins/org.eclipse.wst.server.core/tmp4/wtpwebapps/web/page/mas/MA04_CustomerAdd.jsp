<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp" %>

<script language="javascript" type="text/javascript">
	function doSave() {
		document.masterForm.mode.value = "customerSave";
		document.masterForm.submit();
	}
  
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�ѹ�֡/��䢢������١��� <span class="navigator">[MA04]</span></h3>
				<span class="navigator" >�����ž�鹰ҹ > �ѹ�֡/��䢢������١���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
	      		<html:form action="/master" styleId="masterForm" styleClass="form-horizontal" method="post">
	      		<html:hidden property="id"/>
	      		<html:hidden property="mode"/>
				<div class="form-group">
					<label class="col-sm-2 control-label">�ӹ�˹��:</label>
					<div class="col-sm-2">
						<html:select styleClass="form-control" property="prefix" styleId="prefix">
								<html:option value="1">���</html:option>
								<html:option value="2">�ҧ���</html:option>
								<html:option value="3">�ҧ</html:option>
						</html:select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">�����١���:</label>
					<div class="col-sm-5">
						<html:text property="custName" styleId="custName" styleClass="form-control"></html:text>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">�������:</label>
					<div class="col-sm-5">
						<html:textarea property="addr" styleId="addr" rows="3" styleClass="form-control"></html:textarea>						
					</div>
				</div>	
				<%-- <div class="form-group">
					<label class="col-sm-2 control-label"> �ѧ��Ѵ:</label>
					<div class="col-sm-3">
						<html:select property="province" styleClass="form-control" onclick="doComboDistrict(this.value);">
							<html:option value="">���͡</html:option>
							<html:optionsCollection property="comboProvince" value="id" label="name"/>
						</html:select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">�����:</label>
					<div class="col-sm-3">
						<html:select property="district" styleId="district" styleClass="form-control" onclick="doComboSubDistrict(this.value);">
							<html:option value="">���͡</html:option>
							<html:optionsCollection property="comboDistrict" value="id" label="name"/>
						</html:select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">�Ӻ�:</label>
					<div class="col-sm-3">
						<html:select property="subDistrict" styleId="subDistrict" styleClass="form-control">
							<html:option value="">���͡</html:option>
							<html:optionsCollection property="comboSubDistrict" value="id" label="name"/>
						</html:select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">������ɳ���:</label>
					<div class="col-sm-3">
						<html:text property="postCode" styleId="postCode" styleClass="form-control"></html:text>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">���Դ���:</label>
					<div class="col-sm-3">
						<html:text property="contactName" styleId="contactName" styleClass="form-control"></html:text>
					</div>
				</div> --%>
				<div class="form-group">
					<label class="col-sm-2 control-label">����Դ���:</label>
					<div class="col-sm-3">
						<html:text property="contactTel" styleId="contactTel" styleClass="form-control"></html:text>
					</div>
				</div>
			
				<div class="form-group">
					<label class="col-sm-2 control-label">������:</label>
					<div class="col-sm-3">
						<html:text property="email" styleId="email" styleClass="form-control"></html:text>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">username:</label>
					<div class="col-sm-3">
						<html:text property="usr" styleId="usr" styleClass="form-control"></html:text>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-2 control-label">password:</label>
					<div class="col-sm-3">
						<html:text property="pwd" styleId="pwd" styleClass="form-control"></html:text>
					</div>
				</div>	
					
				<div class="form-group">
					<div class="col-sm-3 col-sm-offset-2">	
						<button type="submit" class="btn btn-success" onclick="doSave()">�ѹ�֡</button>           
	            		<button type="button" class="btn btn-danger"  onclick="location.href='master.htm?mode=customer'">¡��ԡ</button> 
	       			</div>
	       		</div>
	       </html:form>
	       	<br><br><br><br>
	       	
</body>
</html>