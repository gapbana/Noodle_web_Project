package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.User;

public interface UserService extends CoreService<User, Serializable> {
	
	public List<User> getSale() throws DataAccessException;	
	public List<User> getLogin(String un , String pwd) throws DataAccessException;	
	public List<User> getName(String fullname,String userGroup, String status) throws DataAccessException ;
}
