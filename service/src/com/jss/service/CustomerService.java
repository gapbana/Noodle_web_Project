package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Customer;

public interface CustomerService extends CoreService<Customer, Serializable> {
	
	public List<Customer> getAllCust() throws DataAccessException;
	public List<Customer> getCustomersSearch(String name,String rank) throws DataAccessException;
}
