package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Orders;
import com.jss.entity.User;

public interface OrdersService extends CoreService<Orders, Serializable> {
	
	public Integer saveOrders(Orders entity ) throws DataAccessException;
	public List<Orders> getOrdersList(String sdate,String edate,String custId,String saleId,String code,String status) throws DataAccessException;
	public List<Orders> getComboOrders(String custId) throws DataAccessException;
	public List<Orders> getOrdersReport1List(String year,String month) throws DataAccessException;
	public List<Orders> getMonthlyTransport() throws DataAccessException;
}
