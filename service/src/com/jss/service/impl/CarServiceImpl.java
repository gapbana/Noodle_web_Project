package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class CarServiceImpl extends CoreServiceImpl<Car, Serializable> implements CarService {

	private CarDao carDao;

	public CarServiceImpl(CarDao carDao) {
		super(carDao);
		this.carDao = carDao;
	}

	@Override
	public List<Car> getCarByCust(String custId) throws DataAccessException {
		return carDao.findCarByCust(custId);
	}
}
