package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.MenuDao;
import com.jss.entity.Menu;

public class MenuDaoImpl extends CoreDaoImpl<Menu, Serializable> implements MenuDao {

	public MenuDaoImpl(Class<Menu> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Menu> findAllMenu() throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Menu.class);
		criteria.addOrder(Order.asc("code"));

		return getHibernateTemplate().findByCriteria(criteria);
	}

	
}