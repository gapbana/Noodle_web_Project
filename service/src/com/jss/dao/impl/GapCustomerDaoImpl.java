package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.OrderMenuDao;
import com.jss.dao.OrdersDao;
import com.jss.dao.OrdersProductDao;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.GapCustomer;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.User;

public class GapCustomerDaoImpl extends CoreDaoImpl<GapCustomer, Serializable> implements GapCustomerDao {

	public GapCustomerDaoImpl(Class<GapCustomer> entityClass) {
		super(entityClass);
	}

	@Override
	public List<GapCustomer> findLogin(String un, String pwd) {
		DetachedCriteria criteria = DetachedCriteria.forClass(GapCustomer.class);
		criteria.add(Restrictions.eq("username", un));			
		criteria.add(Restrictions.eq("password", pwd));

	return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<GapCustomer> findCust(String id) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		return getHibernateTemplate().findByCriteria(criteria);
	}

	

	

}