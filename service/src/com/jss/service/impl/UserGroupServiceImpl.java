package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.UserDao;
import com.jss.dao.UserGroupDao;
import com.jss.entity.UserGroup;
import com.jss.service.UserGroupService;

public class UserGroupServiceImpl extends CoreServiceImpl<UserGroup, Serializable>
		implements UserGroupService {

	private UserGroupDao accountGroupDao;

	public UserGroupServiceImpl(UserGroupDao accountGroupDao) {
		super(accountGroupDao);
		this.accountGroupDao = accountGroupDao;
	}

	@Override
	public List<UserGroup> getGroupList(String id) throws DataAccessException {
		return accountGroupDao.findGroupList(id);
	}
}
