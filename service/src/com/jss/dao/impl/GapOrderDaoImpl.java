package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.GapOrderDao;
import com.jss.dao.OrderMenuDao;
import com.jss.dao.OrdersDao;
import com.jss.dao.OrdersProductDao;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapOrder;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;

public class GapOrderDaoImpl extends CoreDaoImpl<GapOrder, Serializable> implements GapOrderDao {

	public GapOrderDaoImpl(Class<GapOrder> entityClass) {
		super(entityClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<GapOrder> findOrders() throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GapOrder> findOrderToday() throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GapOrder> findOrderSelect(String startDay, String endDay)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GapOrder> findOrderMonth(String mear, String month)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}


	

}