package com.jss.web.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jss.entity.GapCustomer;
import com.jss.web.common.OptionDTO;

@SuppressWarnings("deprecation")
public abstract class CoreAction extends CorePdfAction {
	
	public static final String SESSION_USER = "SSUser";
	public static final String PATH_UPLOAD = "SSPathUpload";

	protected List<OptionDTO> comboYear() throws Exception {
		List<OptionDTO> results = new ArrayList<OptionDTO>();
		try {
			int yearStart = Calendar.getInstance().get(Calendar.YEAR) + 543;
			// List<OptionDTO> resultList = new ArrayList();
			for (int i = yearStart; i > (yearStart - 5); i--) {
				OptionDTO y = new OptionDTO();
				y.setValue(i + "");
				y.setLabel(i + "");
				results.add(y);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}
	
	protected List<OptionDTO> comboSeq() throws Exception {
		List<OptionDTO> results = new ArrayList<OptionDTO>();
		try {
			for (int i = 1; i < 16; i++) {
				OptionDTO y = new OptionDTO();
				y.setValue(i + "");
				y.setLabel(i + "");
				results.add(y);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}
	
	protected String setJsonView(List<?> objs, String dateFmt) {
		Gson gson = null;
		if (dateFmt != null && !dateFmt.equals("")) {
			gson = new GsonBuilder().setDateFormat(dateFmt).create();
		} else {
			gson = new Gson();
		}
		return gson.toJson(objs);
	}
	
	protected void setJSONResult(HttpServletRequest request, String value) throws Exception {
		request.setAttribute("jsonScript", value);
	}

	protected void setJSONReturn(HttpServletRequest request, boolean value) throws Exception {
		if (value)
			request.setAttribute("jsonScript", "[{\"action\": \"0\"}]");
		else
			request.setAttribute("jsonScript", "[{\"action\": \"1\"}]");
	}
	
	protected ActionForward returnToJson(ActionMapping mapping) {
		return mapping.findForward("json_tag");
	}

	protected ActionForward mappingForward(ActionMapping mapping, HttpServletRequest request, String useParam, String useMode, String actionPath, String formName, String queryStr) {
		request.setAttribute("useParam", useParam);
		request.setAttribute("useMode", useMode);
		request.setAttribute("actionPath", actionPath);
		request.setAttribute("formName", formName);
		request.setAttribute("queryStr", queryStr == null ? "" : queryStr);
		return mapping.findForward("redirect");
	}
	protected Object getObjectSession(HttpServletRequest request, String key) throws Exception {
		return request.getSession().getAttribute(key);
	}
	protected void setObjectSession(HttpServletRequest request, String key, Object value) throws Exception {
		request.getSession().setAttribute(key, value);
	}
	protected GapCustomer getUserSession(HttpServletRequest request) throws Exception {
		return (GapCustomer) getObjectSession(request, SESSION_USER);
	}
	
	
	protected void resetForm(DynaActionForm dynaForm, ActionMapping mapping, HttpServletRequest request) throws Exception {
		dynaForm.initialize(mapping);
		request.getSession().invalidate();
	}
	
	
	
}
