package com.jss.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.CoreService;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Price;
import com.jss.entity.Product;
import com.jss.entity.User;

public interface PriceService extends CoreService<Price, Serializable> {
	
	public List<Price> getPriceList(String sdate, String edate) throws DataAccessException;
	public List<Product> getPriceDetail(String priceId) throws DataAccessException;
	public Integer savePrice(Price price) throws DataAccessException;
}
