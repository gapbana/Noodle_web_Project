package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapItemDao;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.GapItem;
import com.jss.entity.GapItem;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.GapItemService;
import com.jss.service.GapItemService;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class GapItemServiceImpl extends CoreServiceImpl<GapItem, Serializable> implements GapItemService {

	private GapItemDao gapItemDao;

	public GapItemServiceImpl(GapItemDao gapItemDao) {
		super(gapItemDao);
		this.gapItemDao = gapItemDao;
	}

	@Override
	public List<GapItem> getBeverage(String type) throws DataAccessException {
		return gapItemDao.findBeverage(type);
	}

	@Override
	public List<GapItem> getItem(String itemId) throws DataAccessException {
		return gapItemDao.findItem(itemId);
	}

	@Override
	public List<GapItem> getItemSearch(String subType, String type)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return gapItemDao.findItem(subType, type);
	}



	
}
