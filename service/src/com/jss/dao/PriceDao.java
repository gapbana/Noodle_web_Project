package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Price;
import com.jss.entity.Product;
import com.jss.entity.UserGroup;

public interface PriceDao extends CoreDao<Price, Serializable> {

	public List<Price> findPriceList(String sdate, String edate) throws DataAccessException;
	public List<Product> findPriceDetail(String priceId) throws DataAccessException;
}
