package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.CarDao;
import com.jss.dao.MenuDao;
import com.jss.dao.TransportDao;
import com.jss.dao.TransportProductDao;
import com.jss.dao.UserDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Transport;
import com.jss.entity.TransportProduct;
import com.jss.entity.User;
import com.jss.service.CarService;
import com.jss.service.MenuService;
import com.jss.service.TransportProductService;
import com.jss.service.TransportService;
import com.jss.service.UserService;

public class TransportProductServiceImpl extends CoreServiceImpl<TransportProduct, Serializable> implements TransportProductService {

	private TransportProductDao transportProductDao;

	public TransportProductServiceImpl(TransportProductDao transportProductDao) {
		super(transportProductDao);
		this.transportProductDao = transportProductDao;
	}

	@Override
	public List<TransportProduct> getTransportProductByTransport(String transportId) throws DataAccessException {
		return transportProductDao.findTransportProductByTransport(transportId);
	}


}
