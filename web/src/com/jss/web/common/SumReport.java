package com.jss.web.common;

public class SumReport {

	private String sumName;
	private String sumDetail;
	private String sumValue;

	public SumReport() {
	}

	public SumReport(SumReport dto) {
		this.setSumName(dto.getSumName());
		this.setSumDetail(dto.getSumDetail());
		this.setSumValue(dto.getSumValue());
	}

	public SumReport(String name, String detail, String value) {
		this.setSumName(name);
		this.setSumDetail(detail);
		this.setSumValue(value);
	}

	public String getSumName() {
		return sumName;
	}

	public void setSumName(String sumName) {
		this.sumName = sumName;
	}

	public String getSumDetail() {
		return sumDetail;
	}

	public void setSumDetail(String sumDetail) {
		this.sumDetail = sumDetail;
	}

	public String getSumValue() {
		return sumValue;
	}

	public void setSumValue(String sumValue) {
		this.sumValue = sumValue;
	}

	
}
