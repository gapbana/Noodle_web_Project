<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
 	$("#accountForm").validate({
 		rules: {
 			id: "required",
 			nationNameTh: "required",
 			nationNameEn: {
 				required : true	
 			}
 		}
 	});
 	 $('#result').DataTable();
});
</script>

<script language="javascript" type="text/javascript">
  function submitFormAdd(mode) {
	  if ($("#accountForm").valid()) {
	  	document.masterForm.mode.value = mode;
	    document.masterForm.submit();
	  }
  }
  
  function doEdit(id) {
  	document.masterForm.mode.value = 'productEdit';
	document.masterForm.id.value = id;
    document.masterForm.submit();    
  }
  function doDelete(id) {
		 bootbox.dialog({
			 	size : "small",
				message: "�س��ͧ���ź��������¡�ù�����������?",
				title: "<span class='fred'>�׹�ѹ���ź������</span>",
				buttons: {
					ok: {
						label: "��ŧ",
				      	className: "btn-danger",
				      	callback: function() {
				      		document.masterForm.mode.value = 'productDelete';
							document.masterForm.id.value = id;
							document.masterForm.submit();
						}
					},
				    calcel: {
				      	label: "¡��ԡ",
				      	className: "btn-default",
				      	callback: function() {
				      	}
				    }
				}
		});
	}
  function doSearch() {
		document.masterForm.mode.value = 'productSearch';
		document.masterForm.submit();
	}
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�����������<span class="navigator">[MA01]</span></h3>
				<span class="navigator" >�����ž�鹰ҹ > �����������</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
				<html:form action="/master" styleId="masterForm" styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />	      	
						<div class="form-group">
							<label class="col-sm-2 control-label"> �������Թ���:</label>
							<div class="col-sm-3">
								<html:select property="productTypeId" styleId="productTypeId" styleClass="form-control"  >
									<html:option value="">������</html:option>
									<html:optionsCollection property="comboProductTypeList" value="id" label="name"/>
								</html:select>
							</div>
						</div>						
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2">
								<button type="button" class="btn btn-info" 	onclick="doSearch();"> ����</button>
								<button type="button" class="btn btn-grey lighten-3" onclick="location.href='master.htm?mode=product'">���������</button>
								<button type="button" class="btn btn-success" onclick="location.href='master.htm?mode=productAdd'">����������</button>
							</div>
						</div>
		         </html:form>
		     	</div>
	 		</div>  
	 		   
	 		
				<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="result" >
					<thead>
						<tr class="cyan lighten-4">
							<th width="5%" class="text-center"> �ӴѺ</th>
							<th width="8%" class="text-center"> �����Թ���</th>
							<th width="15%" class="text-center"> ������</th>
							<th width="30%" class="text-center"> ���������</th>
							<th width="8%" class="text-center"> �Ҥ�</th>
							
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${resultList}" var="item" varStatus="row">
							<tr >
								<td align="center">${row.index + 1 }</td>
								<td align="left">${item.code }</td>
							    <td align="left">${item.productType.name }</td>
							 	<td align="right">${item.name }</td>
							 	<td align="right">${item.price }</td>
							 
							 	
							</tr>
						</c:forEach>
					</tbody>
				</table>
		
		<br><br>
	</div>    
</body>
</html>