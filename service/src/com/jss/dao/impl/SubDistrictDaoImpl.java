package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.SubDistrictDao;
import com.jss.entity.SubDistrict;

public class SubDistrictDaoImpl extends CoreDaoImpl<SubDistrict, Serializable> implements SubDistrictDao {

	public SubDistrictDaoImpl(Class<SubDistrict> entityClass) {
		super(entityClass);
		
	}

	@Override
	public List<SubDistrict> findSubDistrictbyDistrict(String distId)throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(SubDistrict.class);
		criteria.createAlias("district", "district");
		criteria.addOrder(Order.asc("name"));	
		
		if(distId != null && !distId.equals("")){
			criteria.add(Restrictions.eq("district.id", Integer.parseInt(distId)));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}



}