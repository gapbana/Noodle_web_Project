package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.DistrictDao;
import com.jss.entity.District;
import com.jss.service.DistrictService;

public class DistrictServiceImpl extends CoreServiceImpl<District, Serializable>implements DistrictService {

	private DistrictDao districtDao;
	public DistrictServiceImpl(DistrictDao districtDao) {
		super(districtDao);
		this.districtDao = districtDao;
	}
	@Override
	public List<District> getDistrictbyProv(String provId) throws DataAccessException {
		return districtDao.findDistrictbyProv(provId);
	}
	
}
