<!DOCTYPE html>

<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Customer Relationship Management</title>

	<!-- CSS -->

    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/css/color.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
      <!-- jQuery -->
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/datatables/js/dataTables.bootstrap.js"></script>
     <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap/js/bootbox.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/js/numeral.min.js"></script>

	<!-- Validate -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/ui/sbadmin2/dist/css/formValidation.css"/>
	<script type="text/javascript" src="${pageContext.request.contextPath}/ui/sbadmin2/dist/js/formValidation.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/ui/sbadmin2/dist/js/framework/bootstrap.js"></script>
    
    <!-- Date Picker -->
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js" charset="UTF-8"></script>
    
    <!-- Date Picker -->
    <link href="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" /> 
    <script src="${pageContext.request.contextPath}/ui/sbadmin2/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    
    
    <link href="https://fonts.googleapis.com/css?family=Pridi:200,400" rel="stylesheet">
    
  	<script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true,				//Set เป็นปี พ.ศ.
                viewMode: 'years'
                
            });
          //  }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
           $('#timepicker').timepicker({
        	    showMeridian:false,
           });
      
        });
    </script>
  	
  
   </head>