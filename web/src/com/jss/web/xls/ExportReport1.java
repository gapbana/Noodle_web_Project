package com.jss.web.xls;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.jss.entity.Orders;
import com.jss.web.action.CoreExcelAction;


public class ExportReport1 extends CoreExcelAction{
	
	public static XSSFWorkbook exportExcel(HttpServletRequest request, List<Orders> resultList) throws Exception{
		
		XSSFWorkbook wb = new XSSFWorkbook();	
		DecimalFormat dd = new DecimalFormat("#,##0.000");
		
		XSSFSheet sheet = getSheet(wb, "ReportSale", true);
		//sheet.getPrintSetup().setLandscape(true);
		
		sheet.setColumnWidth(0, pixel2WidthExcel(40));
		sheet.setColumnWidth(1, pixel2WidthExcel(150));
		sheet.setColumnWidth(2, pixel2WidthExcel(100));
		sheet.setColumnWidth(3, pixel2WidthExcel(250));
		sheet.setColumnWidth(4, pixel2WidthExcel(170));
		sheet.setColumnWidth(5, pixel2WidthExcel(120));
		sheet.setColumnWidth(6, pixel2WidthExcel(150));
		
		/* Style Header */
		XSSFCellStyle header = getStyle(wb, 16, true, "S", "C", null, null, false, null);
		XSSFCellStyle headerTable = getStyle(wb, 14, true, "S", "C", "T", "B", false, "L");
		XSSFCellStyle detailCenter = getStyle(wb, 13, false, "S", "C", "T", null, false, "D");
		XSSFCellStyle detailLeft = getStyle(wb, 13, false, "S", "L", "T", null, true, "D");
		XSSFCellStyle detailRight = getStyle(wb, 13, false, "S", "R", "T", null, true, "D");
		XSSFCellStyle footRight = getStyle(wb, 13, true, "S", "R", "T", "B", true, "D");
		
		/* Header */
		XSSFRow row0 = sheet.createRow(0);
		XSSFCell c0row0 = row0.createCell(0);
		c0row0.setCellStyle(header);
		c0row0.setCellValue("\u0e23\u0e32\u0e22\u0e07\u0e32\u0e19\u0e22\u0e2d\u0e14\u0e02\u0e32\u0e22\u0e1b\u0e23\u0e30\u0e08\u0e33\u0e40\u0e14\u0e37\u0e2d\u0e19");
		//รายงานยอดขายประจำเดือน
		
		//merge
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
		//Repeat
		wb.setRepeatingRowsAndColumns(0, -1, -1, 0, 2);
		
		XSSFRow row1 = sheet.createRow(1);
		XSSFCell c0row1 = row1.createCell(0);
		c0row1.setCellStyle(headerTable);
		c0row1.setCellValue("\u0e25\u0e33\u0e14\u0e31\u0e1a"); //ลำดับ
		
		XSSFCell c1row1 = row1.createCell(1);
		c1row1.setCellStyle(headerTable);
		c1row1.setCellValue("\u0e27\u0e31\u0e19\u0e17\u0e35\u0e48"); //วันที่
		
		XSSFCell c2row1 = row1.createCell(2);
		c2row1.setCellStyle(headerTable);
		c2row1.setCellValue("\u0e40\u0e25\u0e02\u0e17\u0e35\u0e48\u0e43\u0e1a\u0e2a\u0e31\u0e48\u0e07\u0e0b\u0e37\u0e49\u0e2d");//เลขที่ใบสั่งซื้อ
		
		XSSFCell c3row1 = row1.createCell(3);
		c3row1.setCellStyle(headerTable);
		c3row1.setCellValue("\u0e25\u0e39\u0e01\u0e04\u0e49\u0e32");//	ลูกค้า
		
		XSSFCell c4row1 = row1.createCell(4);
		c4row1.setCellStyle(headerTable);
		c4row1.setCellValue("\u0e1e\u0e19\u0e31\u0e01\u0e07\u0e32\u0e19\u0e02\u0e32\u0e22");//พนักงานขาย
		
		XSSFCell c5row1 = row1.createCell(5);
		c5row1.setCellStyle(headerTable);
		c5row1.setCellValue("\u0e22\u0e2d\u0e14\u0e02\u0e32\u0e22 \u0028\u0e15\u0e31\u0e19\u0029");//ยอดขาย (ตัน)
		
		XSSFCell c6row1 = row1.createCell(6);
		c6row1.setCellStyle(headerTable);
		c6row1.setCellValue("\u0e22\u0e2d\u0e14\u0e02\u0e32\u0e22 \u0028\u0e1a\u0e32\u0e17\u0029");//ยอดขาย (บาท)
		
		/* Detail */
		int i = 2;
		int od = 1;
		Double totalUnit = 0.0;
		Double totalPrice = 0.0;
		for(Orders entity : resultList){
			
			totalUnit += entity.getTotalUnit();
			totalPrice += entity.getTotalPrice();
			
			XSSFRow rowi = sheet.createRow(i++);
			
			XSSFCell c0rowi = rowi.createCell(0);
			c0rowi.setCellStyle(detailCenter);
			c0rowi.setCellValue(od);
			
			XSSFCell c1rowi = rowi.createCell(1);
			c1rowi.setCellStyle(detailCenter);
			c1rowi.setCellValue(entity.getDateShow());

			
			XSSFCell c2rowi = rowi.createCell(2);
			c2rowi.setCellStyle(detailCenter);
			c2rowi.setCellValue(entity.getCode());

			
			XSSFCell c3rowi = rowi.createCell(3);
			c3rowi.setCellStyle(detailLeft);
			c3rowi.setCellValue(entity.getCustName());

			XSSFCell c4rowi = rowi.createCell(4);
			c4rowi.setCellStyle(detailLeft);
			c4rowi.setCellValue(entity.getSaleName());
			
			XSSFCell c5rowi = rowi.createCell(5);
			c5rowi.setCellStyle(detailRight);
			c5rowi.setCellValue(dd.format(entity.getTotalUnit()));
			
			XSSFCell c6rowi = rowi.createCell(6);
			c6rowi.setCellStyle(detailRight);
			c6rowi.setCellValue(dd.format(entity.getTotalPrice()));
			
			od++;
		}
		sheet.addMergedRegion(new CellRangeAddress(i, i, 0, 4));
		
		XSSFRow rowi = sheet.createRow(i++);
		
		XSSFCell c0rowi = rowi.createCell(0);
		c0rowi.setCellStyle(footRight);
		c0rowi.setCellValue("รวมทั้งหมด");
		
		XSSFCell c5rowi = rowi.createCell(5);
		c5rowi.setCellStyle(footRight);
		c5rowi.setCellValue(dd.format(totalUnit));
		
		XSSFCell c6rowi = rowi.createCell(6);
		c6rowi.setCellStyle(footRight);
		c6rowi.setCellValue(dd.format(totalPrice));
		

		getFooterTable(wb, sheet, i, 6);
		return wb;
	}
}
