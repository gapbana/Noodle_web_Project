package com.jss.service.impl;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.edu.util.DateUtil;
import com.jss.dao.PriceHistoryDao;
import com.jss.dao.ProductDao;
import com.jss.entity.PriceHistory;
import com.jss.entity.Product;
import com.jss.service.PriceHistoryService;

public class PriceHistoryServiceImpl extends CoreServiceImpl<PriceHistory, Serializable> implements PriceHistoryService {

	private PriceHistoryDao priceHistoryDao;
	private ProductDao productDao;

	public PriceHistoryServiceImpl(PriceHistoryDao priceHistoryDao,ProductDao productDao) {
		super(priceHistoryDao);
		this.priceHistoryDao = priceHistoryDao;
		this.productDao = productDao;
	}

	@Override
	public boolean savePriceHistory(List<PriceHistory> phList ) throws DataAccessException {
		boolean result = false;
		try {
			for (PriceHistory entity : phList) {
				super.saveItem(entity);
				/*if (entity.isCheck()) {
					super.saveItem(entity);
				}*/
			}
			
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public List<PriceHistory> getPriceHistoryList(String priceId)throws DataAccessException {
		return priceHistoryDao.findPriceHistoryList(priceId);
	}

	@Override
	public List<PriceHistory> getPriceHistoryByProduct(String productId,String date, String time) throws DataAccessException {
		return priceHistoryDao.findPriceHistoryByProduct(productId,date,time);
	}

	@Override
	public List<PriceHistory> getPriceHistoryByPriceID(String productId,String priceId) throws DataAccessException {
		return priceHistoryDao.findPriceHistoryByPriceID(productId, priceId);
	}



}
