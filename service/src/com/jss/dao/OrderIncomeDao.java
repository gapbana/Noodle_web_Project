package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrdersProduct;


public interface OrderIncomeDao extends CoreDao<OrdersProduct, Serializable> {

	public List< OrderIncome> findOrder() throws DataAccessException;
}
