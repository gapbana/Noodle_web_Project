package com.jss.dao;

import java.io.Serializable;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Province;

public interface ProvinceDao extends CoreDao<Province, Serializable> {

}
