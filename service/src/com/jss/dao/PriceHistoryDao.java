package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.Car;
import com.jss.entity.Menu;
import com.jss.entity.Price;
import com.jss.entity.PriceHistory;
import com.jss.entity.UserGroup;

public interface PriceHistoryDao extends CoreDao<PriceHistory, Serializable> {

	public List<PriceHistory> findPriceHistoryList(String priceId) throws DataAccessException;
	public List<PriceHistory> findPriceHistoryByProduct(String productId,String date, String time) throws DataAccessException;
	public List<PriceHistory> findPriceHistoryByPriceID(String productId,String priceId) throws DataAccessException;
}
