<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>

<%@ include file="../inc_menu.jsp"%>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#stockForm').formValidation({
			message : 'This value is not valid',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				/* sta : {
					validators : {
						notEmpty : {
							message : '��س��к�: �������Թ���'
						}
					}
				},  */
				name : {
					validators : {
						notEmpty : {
							message : '��س��к�: ����'
						}
					}
				},
				price : {
					validators : {
						notEmpty : {
							message : '��س��к�: �Ҥ�'
						}
					}
				},
				/* pic : {
					validators : {
						notEmpty : {
							message : '��س���� : �ٻ'
						}
					}
				},
				 */

				
			}

		}).on('success.form.fv', function(e) {
			// Prevent submit form
			e.preventDefault();

			$("button[name='btnSave']").attr('disabled', true);
			alert();
			document.stockForm.mode.value = "ItemAdd";
			document.stockForm.submit();
		});
	});
</script>

<script language="javascript" type="text/javascript">
	function submitFormInit(mode) {
		document.stockForm.mode.value = mode;
		document.stockForm.edit.value = '';
		document.stockForm.submit();
	}

	function submitFormAdd(mode) {
		if ($("#stockForm").valid()) {
			document.stockForm.mode.value = mode;
			document.stockForm.submit();
		}
	}
	function doSave() {
		document.stockForm.mode.value = "ItemAdd";
		document.stockForm.submit();
	}
	
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					�ѹ�֡/��䢢����������<span class="navigator">[ST02]</span>
				</h3>
				<span class="navigator">�����ž�鹰ҹ >
					�ѹ�֡/��䢢����������</span><br> <br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<html:form action="/stock" styleId="stockForm"
					styleClass="form-horizontal">
					<html:hidden property="mode" />
					<html:hidden property="id" />
					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>�����������:</label>
						<div class="col-sm-3">
							<html:select property="subT" styleClass="form-control">
								<html:option value="1">���������</html:option>
								<html:option value="2">����ͧ����</html:option>
								<html:option value="3">����÷ҹ���</html:option>
							</html:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>����:</label>
						<div class="col-sm-3">
							<html:text property="name" styleId="name"
								styleClass="form-control"></html:text>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>�Ҥ�:</label>
						<div class="col-sm-3">
							<div class="input-group">
								<html:text property="price" styleId="price"
									styleClass="form-control"></html:text>
								<span class="input-group-addon">�ҷ</span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>ʶҹ������:</label>
						<div class="col-sm-3">
							<html:select property="sta" styleClass="form-control">
								<html:option value="1">�������˹���</html:option>
								<html:option value="0">����˹���</html:option>
							</html:select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><span
							class="red-text">* </span>���͡�ٻ�����:</label>
						<div class="col-sm-3">
							<form name="frmUpload" method="post" action="upload.jsp"
								enctype="multipart/form-data">
								<input type="file" name="pic">
						</div>
					</div>
					</form>

					<div class="form-group">
						<div class="col-sm-3 col-sm-offset-2">
							<button type="button" class="btn btn-success" onclick="doSave()">�ѹ�֡</button>
							<button type="button" class="btn btn-danger" name="btnCancel"
								value="¡��ԡ" onclick="location.href='stock.htm?mode=product'">¡��ԡ</button>
						</div>
					</div>
				</html:form>
</body>
</html>