package com.jss.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.service.impl.CoreServiceImpl;
import com.jss.dao.MenuDao;
import com.jss.dao.UserDao;
import com.jss.entity.Menu;
import com.jss.entity.User;
import com.jss.service.MenuService;
import com.jss.service.UserService;

public class MenuServiceImpl extends CoreServiceImpl<Menu, Serializable>
		implements MenuService {

	private MenuDao menuDao;

	public MenuServiceImpl(MenuDao menuDao) {
		super(menuDao);
		this.menuDao = menuDao;
	}

	@Override
	public List<Menu> getAllMenu() throws DataAccessException {
		return menuDao.findAllMenu();
	}
}
