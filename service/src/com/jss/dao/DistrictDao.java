package com.jss.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import com.edu.core.dao.CoreDao;
import com.jss.entity.District;

public interface DistrictDao extends CoreDao<District, Serializable> {

	public List<District> findDistrictbyProv(String provId) throws DataAccessException;
}
