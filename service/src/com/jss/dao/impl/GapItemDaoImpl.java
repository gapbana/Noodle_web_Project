package com.jss.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

import com.edu.core.dao.impl.CoreDaoImpl;
import com.jss.dao.CarDao;
import com.jss.dao.GapCustomerDao;
import com.jss.dao.GapItemDao;
import com.jss.dao.OrderMenuDao;
import com.jss.dao.OrdersDao;
import com.jss.dao.OrdersProductDao;
import com.jss.entity.Car;
import com.jss.entity.Customer;
import com.jss.entity.GapCustomer;
import com.jss.entity.GapItem;
import com.jss.entity.OrderIncome;
import com.jss.entity.OrderMenu;
import com.jss.entity.Orders;
import com.jss.entity.OrdersProduct;
import com.jss.entity.User;

public class GapItemDaoImpl extends CoreDaoImpl<GapItem, Serializable> implements GapItemDao {

	public GapItemDaoImpl(Class<GapItem> entityClass) {
		super(entityClass);
	
	}

	@Override
	public List<GapItem> findItem(String itemId) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(GapItem.class);
		if (itemId != null && !itemId.equals("")) {
			criteria.add(Restrictions.eq("id", Integer.parseInt(itemId)));
		}
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<GapItem> findBeverage(String type) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(Customer.class);
		if (type.equals("drink")) {
			criteria.add(Restrictions.eq("type", type));
		}
		return getHibernateTemplate().findByCriteria(criteria);
	}

	@Override
	public List<GapItem> findItem(String subType, String type)
			throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(GapItem.class);
		/*criteria.createAlias("userGroup", "userGroup");  //join table*/	
		
		if (type != null && !type.equals("")) {
			criteria.add((Restrictions.like("name", "%" + type + "%")));
		}
		
		if (subType != null && !subType.equals("")) {
			criteria.add(Restrictions.like("status", subType));
		}
		
		return getHibernateTemplate().findByCriteria(criteria);
	}



	

	

}