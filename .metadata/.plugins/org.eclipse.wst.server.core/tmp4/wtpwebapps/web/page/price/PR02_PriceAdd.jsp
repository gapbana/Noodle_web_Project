<%@page contentType="text/html; charset=windows-874"%>
<%@taglib uri="/tld/struts-html.tld" prefix="html"%>
<%@taglib uri="/tld/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/tld/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/tld/edu-util.tld" prefix="edu-util"%>
<%@taglib uri="/tld/displaytag-12.tld" prefix="display"%>
<%@taglib uri="/tld/struts-bean-el.tld" prefix="bean-el"%>
<%@taglib uri="/tld/fn.tld" prefix="fn"%>
<%@taglib uri="/tld/c.tld" prefix="c"%>
<%@taglib uri="/tld/fmt.tld" prefix="fmt"%>

<%@ include file="../inc_menu.jsp" %>
<script language="javascript" type="text/javascript">
$(document).ready(function() {    		
	$('#priceForm').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields : {
        	price: {
	    		validators: {
	   				notEmpty: {
	           			message: '��س��к�: �Ҥ����硵�Ҵ'
	           		}
	     		}
	      	},
        }
        
    }).on('success.form.fv', function(e) {
        	// Prevent submit form
        e.preventDefault();
        
        $("button[name='btnSave']").attr('disabled', true);
       		/*  alert(); */
         document.priceForm.mode.value = "priceCal";
		 document.priceForm.submit(); 
    });
});
</script>

<script language="javascript" type="text/javascript">
  function doSave() {
  	document.priceForm.mode.value = 'priceSave';
    document.priceForm.submit();    
  }
  
  function submitFormAdd(mode) {
	  if ($("#accountForm").valid()) {
	  	document.nationForm.mode.value = mode;
	    document.nationForm.submit();
	  }
  }
</script>

</head>
<body>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">�ѹ�֡�Ҥ��Թ��� <span class="navigator">[PR02]</span></h3>
				<span class="navigator" >�Ҥ��Թ��� > �ѹ�֡�Ҥ��Թ���</span><br><br>
			</div>       
		</div>
		<div class="row">
	      	<div class="col-lg-12">
				<html:form action="/price" styleId="priceForm" styleClass="form-horizontal">
	      		<html:hidden property="id"></html:hidden>
	      		<html:hidden property="mode"></html:hidden>
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="red-text">* </span>�Ҥ����硵�Ҵ:</label>
					<div class="col-sm-2">
						<div class="input-group">
							<html:text property="price" styleClass="form-control text-right"></html:text>
                     		<span class="input-group-addon">�ҷ</span>
                   		</div>					
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3 col-sm-offset-2">	
					<c:if test="${fn:length(productList) eq 0 }"> 
						<button type="submit" class="btn btn-primary" >�ӹǳ</button>   
					</c:if>
					<c:if test="${fn:length(productList) >0}"> 
						<button type="button" class="btn btn-success" onclick="doSave();">�ѹ�֡</button>   
					</c:if>						        
	            		<button type="button" class="btn btn-default" onclick="location.href='price.htm?mode=priceAdd'">���������</button>
	            		<button type="button" class="btn btn-danger" onclick="location.href='price.htm?mode=index'">¡��ԡ</button> 
	       			</div>
	       		</div>
	       		</html:form>
	       		<c:if test="${fn:length(productList)> 0 }">    
	       		<div class="panel panel-info">
	      			<div class="panel-heading black-text" style="font-size:18px;"> ������鹡��</div>
	      		<div class="panel-body">
	       		<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="result" >
					<thead>						
						<tr class="grey lighten-4">
							<th width="10%" rowspan="2">����</th>
							<th colspan="2"><div align="center">��./���</div></th>
							<th width="16%" rowspan="2"><div align="right">�Ҥ���ǹ��ҧ (�ҷ/��.)</div></th>
							<th width="16%" colspan="2"><div align="center">>15 �ѹ</div></th>
							<th width="16%" colspan="2"><div align="center"><15 �ѹ</div></th>
						</tr>
						<tr class="grey lighten-4">
							
							<th width="10%"><div align="center">10 ����</div></th>
							<th width="10%"><div align="center">12 ����</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productList}" var="item" varStatus="row">
						<c:if test="${item.productType.id eq '1' }">   
						<tr>
							<td>${item.code }</td>
					   		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m10}" /></td>
					 		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m12}" /></td>
					 		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.diff}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceOver}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceLess}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>
					 						 		
						</tr>
						</c:if>
						</c:forEach>
					</tbody>
				</table>
					</div>
	    </div>
	    <div class="panel panel-info">
     		<div class="panel-heading black-text" style="font-size:18px;">���硢������</div>
     		<div class="panel-body">
				<table width="99%" cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" id="result" >
					<thead>
						<tr class="grey lighten-2">
							<tr class="grey lighten-4">
							<th width="10%" rowspan="2">����</th>
							<th colspan="2"><div align="center">��./���</div></th>
							<th width="16%" rowspan="2"><div align="right">�Ҥ���ǹ��ҧ (�ҷ/��.)</div></th>
							<th width="16%" colspan="2"><div align="center">>15 �ѹ</div></th>
							<th width="16%" colspan="2"><div align="center"><15 �ѹ</div></th>
						</tr>
						<tr class="grey lighten-4">
							
							<th width="10%"><div align="center">10 ����</div></th>
							<th width="10%"><div align="center">12 ����</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>
							<th width="10%"><div align="right">�Ҥ��ط��</div></th>
							<th width="10%"><div align="right">������� 7%</div></th>

						</tr>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productList}" var="item" varStatus="row">
						<c:if test="${item.productType.id eq '2' }">   
						<tr>
							<td>${item.code }</td>
					   		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m10}" /></td>
					 		<td align="center"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.m12}" /></td>
					 		<td align="right"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.diff}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceOver}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>
					 		<td align="right" class="green-text"><fmt:formatNumber type ="number" pattern = "0.00" value = "${item.priceLess}" /></td>
					 		<td align="right" class="red-text"><fmt:formatNumber type ="number" pattern = "0.000" value = "${item.priceOverVat}" /></td>			 		
						</tr>
						</c:if>
						</c:forEach>
					</tbody>
				</table>
			</div>
			</div>
			</c:if>
			</div>   
	      	<br>
	      	<br>
</body>
</html>