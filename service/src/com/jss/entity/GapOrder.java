package com.jss.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.edu.core.entity.CoreEntity;

@Entity
@Table(name = "gap_order")
public class GapOrder extends CoreEntity {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id")
	private int id;

	@Column(name = "create_date")
	private String date;

	@Column(name = "create_time")
	private String time;

	@Column(name = "tax")
	private int tax;

	@Column(name = "status")
	private String status;

	@Column(name = "sale_id")
	private int sale;

	@Column(name = "discount")
	private int discount;

	@Column(name = "customer_id")
	private int custId;
	
	@Column(name = "table")
	private int table;

	/*
	 * public GapCustomer getGapCustomer() { return gapCustomer; }
	 * 
	 * public void setGapCustomer(GapCustomer gapCustomer) { this.gapCustomer =
	 * gapCustomer; }
	 */

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

}
